# SRD-5.1-DE

![CCBY4](https://img.shields.io/badge/license-CC--BY--4.0-green)

Wizards of the Coast hat am 26.07.2023 das SRD 5.1 f�r Dungeons & Dragons in den Sprachen Deutsch, Franz�sisch, Italienisch und Spanisch als PDF-Dateien ver�ffentlicht ([DnDBeyond](https://www.dndbeyond.com/community-update)). Dieses Projekt �bersetzt die deutschsprachige PDF in maschinenlesbare Formate.

## Ziele

1. Grobe Konvertierung des PDF in HTML ![done](https://img.shields.io/badge/done-green)
1. Eine saubere, unstyled HTML-Fassung des PDF ![done](https://img.shields.io/badge/done-green)
1. CSS f�r die HTML-Fassung ![done](https://img.shields.io/badge/done-green)
1. Monster, Magische Gegenst�nde und Zauberspr�che in separaten Files ![done](https://img.shields.io/badge/done-green)
1. Auf Basis der HTML-Fassung weitere Formate erstellen (XML, MD, JSON, YAML, LaTeX) ![ip](https://img.shields.io/badge/in%20progress-yellow)
1. Transformation in gel�ufige XML-Formate (Fight Club 5e, Foundry) ![notstarted](https://img.shields.io/badge/not%20started-red)
1. Bereitstellung (der JSON-Fassung) per API �ber openrpg.de ![ip](https://img.shields.io/badge/in%20progress-yellow)

## Demo

Den aktuellen Stand der Konvertierung kannst du unter [openrpg.de/srd/5e/de](https://openrpg.de/srd/5e/de/) ansehen.

## Requirements
F�r den Build-Prozess werden die folgenden Werkzeuge ben�tigt:
* xmllint
* xsltproc

Einige optionale Schritte im Build-Prozess ben�tigen dar�ber hinaus die folgenden Werkzeuge:
* jsonlint-php (JSON Validierung)
* jsonschema (JSON Schema-Validierung) ![ip](https://img.shields.io/badge/in%20progress-yellow)
* jq (JSON pretty printing)

Ubuntu et al: `apt install libxml2-utils xsltproc jsonlint jq python3-jsonschema`

## Build

Die Bigfile-Version muss nicht generiert werden, sondern wird onthefly im Webserver erzeugt. Die davon abgeleiteten Konvertierungen (Singlefile, XML, JSON etc.) werden wie folgt erzeugt:

```
build/make-monsters.sh
build/make-magicitems.sh
build/make-spells.sh
build/make-compendium.sh
```

## Konvertierung

* make-monsters.sh: Shellskript erzeugt verschiedene Konvertierungen
* srd-51-de.dtd: Document Type Definition f�r alle XML
* monster-&ast;.html: HTML-Konvertierung einzelner Wertebl�cke. Enth�lt einfaches Styling (basic.css) ![done](https://img.shields.io/badge/done-green) 
* monster-&ast;.xml: XML-Konvertierung einzelner Wertebl�cke ![done](https://img.shields.io/badge/done-green)
* monster-&ast;.json: JSON-Konvertierung einzelner Wertebl�cke ![done](https://img.shields.io/badge/done-green)
* monster-&ast;.homebrewery.md: Markdown-Konvertierung einzelner Wertebl�cke ![done](https://img.shields.io/badge/done-green)
* monster-&ast;.rpgtex.tex: LaTeX-Konvertierung einzelner Wertebl�cke ![done](https://img.shields.io/badge/done-green)
* magicitem-&ast; und spell-&ast; analog zu monster-&ast;

## Mitarbeit

Mitarbeit ist willkommen! F�r kleinere Korrekturen und Erg�nzungen sind Pull-Requests und Issues der geringste Aufwand, daher bitte nach M�glichkeit diese benutzen. Wenn du dich im gr��eren Umfang einbringen m�chtest, dann setze dich [mit mir in Verbindung](https://www.dnddeutsch.de/kontakt).

## Lizenz

Dieses Code-Repository ist frei zu den Bedingungen der [CC-BY-4.0 (Creative Commons Namensnennung 4.0 International Public License)](https://openrpg.de/license/cc/by/4.0/de/) verf�gbar

![CC](https://openrpg.de/img/cc.png) ![BY](https://openrpg.de/img/by.png)