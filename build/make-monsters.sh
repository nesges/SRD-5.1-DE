#!/bin/bash

. `dirname $0`/env.sh

function lint {
    # tag xpath [optional]
    # uses global $HTML
    CONTENT=`$XMLLINT --xpath "$2" $HTML 2> /dev/null`
    if [[ $CONTENT ]] || [[ "$3" != "optional" ]]
    then
        echo "<$1>$CONTENT</$1>"
    fi
}

function split_trait {
    # tag xpath
    # uses global XML
    local ARRAY=$($XMLLINT --xpath "$2" $XML 2> /dev/null)
    if [[ $ARRAY ]]
    then
        echo "    <$1s>" >> "$WEB/monster/$i.xml"
        RAW=`lint raw "$2"`
        echo '        '"$RAW" >> "$WEB/monster/$i.xml"
        if [[ "$RAW" == *"; Hieb, Stich und Wucht durch nichtmagische Angriffe"* ]]
        then
            ARRAY=`echo "$ARRAY" | sed 's/; Hieb, Stich und Wucht durch nichtmagische Angriffe//'`
        fi
        IFS=,
        for A in $ARRAY
        do
            A=`echo $A | xargs`
            echo "        <$1>$A</$1>" >> "$WEB/monster/$i.xml"
        done

        if [[ "$RAW" == *"; Hieb, Stich und Wucht durch nichtmagische Angriffe"* ]]
        then
            echo "        <$1>Hieb, Stich und Wucht durch nichtmagische Angriffe</$1>" >> "$WEB/monster/$i.xml"
        fi
        
        IFS=$OIFS
        echo "    </$1s>" >> "$WEB/monster/$i.xml"
    fi
}

for F in "$WEB/monster/monster.html" "$WEB/monster/kreaturen.html" "$WEB/monster/nsc.html"
do 
    XML=$F

    if [[ ! -r $XML ]]
    then
        echo "can't read $XML"
        exit
    fi

    if [[ $1 ]]
    then
        MONSTERS=$1
    else
        MONSTERS=`grep 'class="monster"' $F | sed -e 's/\r/\n/' | sed -e 's/<.*id="\(.*\)">/\1/'`
    fi

    for i in $MONSTERS
    do 
        NAME=`$XMLLINT --xpath "//*[@id='$i']/h5/text()" $F 2> /dev/null`
        ERROR=0
        
        if [[ $NAME ]]
        then
            echo -n "$NAME "
            
            #######################################################################
            # HTML
            HTML="$WEB/monster/$i.html"
            
            echo '<!DOCTYPE html>' > "$HTML"
            echo '<html lang="de">' >> "$HTML"
            echo '    <head>' >> "$HTML"
            echo '        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />' >> "$HTML"
            echo '        <meta name="viewport" content="width=device-width, initial-scale=1" />' >> "$HTML"
            echo "        <title>$NAME :: SRD 5.1 de</title>" >> "$HTML"
            echo '        <link rel="stylesheet" href="'"$BASEURL"'/css/basic.css" />' >> "$HTML"
            echo '        <link rel="apple-touch-icon" sizes="180x180" href="'"$BASEURL"'/img/favicon/apple-touch-icon.png" />' >> "$HTML"
            echo '        <link rel="icon" type="image/png" sizes="32x32" href="'"$BASEURL"'/img/favicon/favicon-32x32.png" />' >> "$HTML"
            echo '        <link rel="icon" type="image/png" sizes="16x16" href="'"$BASEURL"'/img/favicon/favicon-16x16.png" />' >> "$HTML"
            echo '        <link rel="manifest" href="'"$BASEURL"'/img/favicon/site.webmanifest" />' >> "$HTML"
            echo '    </head>' >> "$HTML"
            echo '    <body>' >> "$HTML"
            echo '        <div id="copy" class="srd-51-de">' >> "$HTML"
            $XMLLINT --xpath "//*[@id='$i']" $F 2> /dev/null | sed 's/^/            /' >> "$HTML"
            echo '        </div>' >> "$HTML"
            echo '    </body>' >> "$HTML"
            echo '</html>' >> "$HTML"
            
            if [[ -s $HTML ]]
            then
                echo -n "."
            else
                echo -n "0"
                ERROR=1
            fi
        
            # pretty print HTML 
            # tidy messes up our markup too much, maybe reimplement this later
            # if [[ -x $TIDY ]]
            # then
            #     $TIDY -i --indent-spaces 4 -w 9001 --tidy-mark no -asxhtml $HTML 2>/dev/null > $HTML~
            #     mv "$HTML~" "$HTML"
            #     echo -n "."
            # else
            #     echo -n "x"
            # fi
        
            #######################################################################
            # XML
            # using xmllint to do single xpath queries is extremely inefficient
            # maybe take some time to refactor XML generation
            
            
            echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' > "$WEB/monster/$i.xml"
            echo "<!--<!DOCTYPE monster SYSTEM '$BASEURL/srd-51-de.dtd'>-->" >> "$WEB/monster/$i.xml"
            echo "<monster>" >> "$WEB/monster/$i.xml"
            echo '    <WORK-IN-PROGRESS-WARNING>THIS IS A DEVELOPMENT VERSION. USE AT OWN RISK</WORK-IN-PROGRESS-WARNING>' >> "$WEB/monster/$i.xml"
            echo "    <id>$i</id>" >> "$WEB/monster/$i.xml"
            echo "    <src>$XML</src>" >> "$WEB/monster/$i.xml"
            echo '    '`lint name "//*[@id='$i']/h5/text()"` >> "$WEB/monster/$i.xml"
            echo '    '`lint size-type-raw "//*[@id='$i']/div[@class='type']/text()"` >> "$WEB/monster/$i.xml"
            echo '    '`lint size "substring-before(//*[@id='$i']/div[@class='type']/text(),' ')"` | sed 's/er\?</</' >> "$WEB/monster/$i.xml"
            echo '    '`lint type "substring-after(//*[@id='$i']/div[@class='type']/text(),' ')"` >> "$WEB/monster/$i.xml"
            echo '    '`lint alignment "//*[@id='$i']/div[@class='alignment']/text()"` >> "$WEB/monster/$i.xml"
            echo '    <armor-class>' >> "$WEB/monster/$i.xml"
            echo '        '`lint raw "//*[@id='$i']/div[@class='stat_name' and text()='Rüstungsklasse'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo '        '`lint value "substring-before(concat(//*[@id='$i']/div[@class='stat_name' and text()='Rüstungsklasse'][1]/following::div[@class='stat_value'][1]/text(), ' '), ' ')"` >> "$WEB/monster/$i.xml"
            echo '        '`lint info "substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Rüstungsklasse'][1]/following::div[@class='stat_value'][1]/text(), ' ')" optional` >> "$WEB/monster/$i.xml"
            echo '    </armor-class>' >> "$WEB/monster/$i.xml"
            echo '    <hit-points>' >> "$WEB/monster/$i.xml"
            echo '        '`lint raw "//*[@id='$i']/div[@class='stat_name' and text()='Trefferpunkte'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo '        '`lint value "substring-before(concat(//*[@id='$i']/div[@class='stat_name' and text()='Trefferpunkte'][1]/following::div[@class='stat_value'][1]/text(), ' '), ' ')"` >> "$WEB/monster/$i.xml"
            echo '        '`lint formula "substring-before(substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Trefferpunkte'][1]/following::div[@class='stat_value'][1]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '    </hit-points>' >> "$WEB/monster/$i.xml"
            echo '    <speeds>' >> "$WEB/monster/$i.xml"
            echo '        '`lint raw "//*[@id='$i']/div[@class='stat_name' and text()='Bewegungsrate'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo '        '`lint walk "substring-before(concat(//*[@id='$i']/div[@class='stat_name' and text()='Bewegungsrate'][1]/following::div[@class='stat_value'][1]/text(), ','), ',')"` >> "$WEB/monster/$i.xml"
            echo '        '`lint burrow "substring-before(concat(substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Bewegungsrate'][1]/following::div[@class='stat_value'][1]/text(), 'Graben '), ','), ',')" optional` >> "$WEB/monster/$i.xml"
            echo '        '`lint climb "substring-before(concat(substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Bewegungsrate'][1]/following::div[@class='stat_value'][1]/text(), 'Klettern '), ','), ',')" optional` >> "$WEB/monster/$i.xml"
            echo '        '`lint fly "substring-before(concat(substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Bewegungsrate'][1]/following::div[@class='stat_value'][1]/text(), 'Fliegen '), ','), ',')" optional` >> "$WEB/monster/$i.xml"
            echo '        '`lint swim "substring-before(concat(substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Bewegungsrate'][1]/following::div[@class='stat_value'][1]/text(), 'Schwimmen '), ','), ',')" optional` >> "$WEB/monster/$i.xml"
            echo '    </speeds>' >> "$WEB/monster/$i.xml"
            echo '    <attributes>' >> "$WEB/monster/$i.xml"
            echo '        <attribute>' >> "$WEB/monster/$i.xml"
            echo '            <class>str</class>' >> "$WEB/monster/$i.xml"
            echo '            '`lint value "substring-before(//*[@id='$i']/div[@class='attributes']/div[@class='v'][1]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '            '`lint modifier "substring-before(substring-after(//*[@id='$i']/div[@class='attributes']/div[@class='v'][1]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '        </attribute>' >> "$WEB/monster/$i.xml"
            echo '        <attribute>' >> "$WEB/monster/$i.xml"
            echo '            <class>dex</class>' >> "$WEB/monster/$i.xml"
            echo '            '`lint value "substring-before(//*[@id='$i']/div[@class='attributes']/div[@class='v'][2]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '            '`lint modifier "substring-before(substring-after(//*[@id='$i']/div[@class='attributes']/div[@class='v'][2]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '        </attribute>' >> "$WEB/monster/$i.xml"
            echo '        <attribute>' >> "$WEB/monster/$i.xml"
            echo '            <class>con</class>' >> "$WEB/monster/$i.xml"
            echo '            '`lint value "substring-before(//*[@id='$i']/div[@class='attributes']/div[@class='v'][3]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '            '`lint modifier "substring-before(substring-after(//*[@id='$i']/div[@class='attributes']/div[@class='v'][3]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '        </attribute>' >> "$WEB/monster/$i.xml"
            echo '        <attribute>' >> "$WEB/monster/$i.xml"
            echo '            <class>int</class>' >> "$WEB/monster/$i.xml"
            echo '            '`lint value "substring-before(//*[@id='$i']/div[@class='attributes']/div[@class='v'][4]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '            '`lint modifier "substring-before(substring-after(//*[@id='$i']/div[@class='attributes']/div[@class='v'][4]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '        </attribute>' >> "$WEB/monster/$i.xml"
            echo '        <attribute>' >> "$WEB/monster/$i.xml"
            echo '            <class>wis</class>' >> "$WEB/monster/$i.xml"
            echo '            '`lint value "substring-before(//*[@id='$i']/div[@class='attributes']/div[@class='v'][5]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '            '`lint modifier "substring-before(substring-after(//*[@id='$i']/div[@class='attributes']/div[@class='v'][5]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '        </attribute>' >> "$WEB/monster/$i.xml"
            echo '        <attribute>' >> "$WEB/monster/$i.xml"
            echo '            <class>cha</class>' >> "$WEB/monster/$i.xml"
            echo '            '`lint value "substring-before(//*[@id='$i']/div[@class='attributes']/div[@class='v'][6]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '            '`lint modifier "substring-before(substring-after(//*[@id='$i']/div[@class='attributes']/div[@class='v'][6]/text(), '('), ')')"` >> "$WEB/monster/$i.xml"
            echo '        </attribute>' >> "$WEB/monster/$i.xml"
            echo '    </attributes>' >> "$WEB/monster/$i.xml"
            
            echo -n `split_trait saving-throw "//*[@id='$i']/div[@class='stat_name' and text()='Rettungswürfe'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait skill "//*[@id='$i']/div[@class='stat_name' and text()='Fertigkeiten'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait damage-resistance "//*[@id='$i']/div[@class='stat_name' and text()='Schadensresistenzen'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait damage-vulnerability "//*[@id='$i']/div[@class='stat_name' and text()='Schadensanfälligkeiten'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait damage-immunity "//*[@id='$i']/div[@class='stat_name' and text()='Schadensimmunitäten'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait condition-immunity "//*[@id='$i']/div[@class='stat_name' and text()='Zustandsimmunitäten'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait sense "//*[@id='$i']/div[@class='stat_name' and text()='Sinne'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            echo -n `split_trait language "//*[@id='$i']/div[@class='stat_name' and text()='Sprachen'][1]/following::div[@class='stat_value'][1]/text()"` >> "$WEB/monster/$i.xml"
            
            echo '    '`lint challenge "substring-before(//*[@id='$i']/div[@class='stat_name' and text()='Herausforderungsgrad'][1]/following::div[@class='stat_value'][1]/text(), ' ')"` >> "$WEB/monster/$i.xml"
            echo '    '`lint xp "substring-before(substring-after(//*[@id='$i']/div[@class='stat_name' and text()='Herausforderungsgrad'][1]/following::div[@class='stat_value'][1]/text(), '('), ' EP)')"` >> "$WEB/monster/$i.xml"
            
            IFS=$(echo -en "\n\b")
            
            SPELLCASTING_TRAIT=
            SPELLCASTING_INNATE_TRAIT=
            TRAITS=$($XMLLINT --xpath "//*[@id='$i']/div[@class='trait_name']/text()" $XML 2> /dev/null)
            if [[ $TRAITS ]]
            then
                # don't print <traits> if TRAITS has ONLY Zauberwirken|Angeborenes Zauberwirken
                # they go to <spellcast-trait*> and would leave <traits> empty
                if [[ $TRAITS != 'Zauberwirken' ]] && [[ $TRAITS != 'Angeborenes Zauberwirken' ]]
                then
                    echo '    <traits>' >> "$WEB/monster/$i.xml"
                fi
        
                for TRAIT in $TRAITS
                do
                    if [[ $TRAIT == "Zauberwirken" ]]
                    then
                        SPELLCASTING_TRAIT=`$XMLLINT --xpath "//*[@id='$i']/div[@class='trait_name' and text()='$TRAIT'][1]/following::div[@class='trait_value'][1]" $XML 2> /dev/null`
                    elif [[ $TRAIT == "Angeborenes Zauberwirken" ]]
                    then
                        SPELLCASTING_INNATE_TRAIT=`$XMLLINT --xpath "//*[@id='$i']/div[@class='trait_name' and text()='$TRAIT'][1]/following::div[@class='trait_value'][1]" $XML 2> /dev/null`
                    else
                        echo '        <trait>' >> "$WEB/monster/$i.xml"
                        echo "            <name>$TRAIT</name>" >> "$WEB/monster/$i.xml"
                        echo '            '`lint value "//*[@id='$i']/div[@class='trait_name' and text()='$TRAIT'][1]/following::div[@class='trait_value'][1]//text()"` >> "$WEB/monster/$i.xml"
                        echo '        </trait>' >> "$WEB/monster/$i.xml"
                    fi
                done
        
                if [[ $TRAITS != 'Zauberwirken' ]] && [[ $TRAITS != 'Angeborenes Zauberwirken' ]]
                then
                    echo '    </traits>' >> "$WEB/monster/$i.xml"
                fi
            fi
            
            if [[ $SPELLCASTING_TRAIT ]]
            then
                echo "$SPELLCASTING_TRAIT" | perl -ne 's#<div class="trait_value">(.*)(?:<br\s+/>)?#<spellcasting-trait>\n    <spellcasting-text>\1</spellcasting-text>\n    <spells>#;
                    s#<div class="prepared_spell_level">((Zaubertricks|\d+).*?\s\((beliebig oft|\d+).*?\))\s*</div>\s*#<spell-list>\n            <level>\2</level>\n            <slots>\3</slots>\n#;
                    s#<div class="prepared_spell_list">(.*?)</div>#        \1\n        </spell-list>#;
                    s#\s*<a.*?href="\#(.*?)">(.*?)</a>(?:,\s+)*#\n            <spell-reference>\n                <id>\1</id>\n                <name>\2</name>\n            </spell-reference>#g;
                    s#<div class="note">(.*?)</div>#        <spellcasting-note>\1</spellcasting-note>\n#;
                    s#</div>#    </spells>\n</spellcasting-trait>#;
                    s#<br\s*/>##;
                    print' >> "$WEB/monster/$i.xml"
            fi
            
            if [[ $SPELLCASTING_INNATE_TRAIT ]]
            then
                echo "$SPELLCASTING_INNATE_TRAIT" | perl -ne 's#<div class="trait_value">(.*)(?:<br\s+/>)?#<spellcasting-innate-trait>\n    <spellcasting-text>\1</spellcasting-text>\n    <spells>#;
                    s#<div class="innate_spell_frequency">(.*?)\s*</div>\s*#<spell-list>\n            <frequency>\1</frequency>\n#;
                    s#<div class="innate_spell_list">(.*?)</div>#        \1\n        </spell-list>#;
                    s#\s*<a.*?href="\#(.*?)">(.*?)</a>(?:,\s+)*#\n            <spell-reference>\n                <id>\1</id>\n                <name>\2</name>\n            </spell-reference>#g;
                    s#</div>#    </spells>\n</spellcasting-innate-trait>#;
                    s#<br\s*/>##;
                    print' >> "$WEB/monster/$i.xml"
            fi
            
            ACTIONS=$($XMLLINT --xpath "//*[@id='$i']/div[@class='action_name']/text()" $XML 2> /dev/null)
            if [[ $ACTIONS ]]
            then
                echo '    <actions>' >> "$WEB/monster/$i.xml"
                for ACTION in $ACTIONS
                do
                    echo '        <action>' >> "$WEB/monster/$i.xml"
                    echo "            <name>$ACTION</name>" >> "$WEB/monster/$i.xml"
                    echo '            '`lint value "//*[@id='$i']/div[@class='action_name' and text()='$ACTION'][1]/following::div[@class='action_value'][1]//text()"` >> "$WEB/monster/$i.xml"
                    echo '        </action>' >> "$WEB/monster/$i.xml"
                done
                echo '    </actions>' >> "$WEB/monster/$i.xml"
            fi
            
            LACTIONS=$($XMLLINT --xpath "//*[@id='$i']/div[@class='legendary_action_name']/text()" $XML 2> /dev/null)
            if [[ $LACTIONS ]]
            then
                echo '    <legendary-actions>' >> "$WEB/monster/$i.xml"
                echo '        '`lint legendary-text "//*[@id='$i']/div[@class='legendary-text']/text()"` >> "$WEB/monster/$i.xml"
                
                for LACTION in $LACTIONS
                do
                    echo '        <legendary-action>' >> "$WEB/monster/$i.xml"
                    echo "            <name>$LACTION</name>" >> "$WEB/monster/$i.xml"
                    echo '            '`lint value "//*[@id='$i']/div[@class='legendary_action_name' and text()='$LACTION'][1]/following::div[@class='legendary_action_value'][1]//text()"` >> "$WEB/monster/$i.xml"
                    echo '        </legendary-action>' >> "$WEB/monster/$i.xml"
                done
                echo '    </legendary-actions>' >> "$WEB/monster/$i.xml"
            fi
            
            REACTIONS=$($XMLLINT --xpath "//*[@id='$i']/div[@class='reaction_name']/text()" $XML 2> /dev/null)
            if [[ $REACTIONS ]]
            then
                echo '    <reactions>' >> "$WEB/monster/$i.xml"
                for REACTION in $REACTIONS
                do
                    echo '        <reaction>' >> "$WEB/monster/$i.xml"
                    echo "            <name>$REACTION</name>" >> "$WEB/monster/$i.xml"
                    echo '            '`lint value "//*[@id='$i']/div[@class='reaction_name' and text()='$REACTION'][1]/following::div[@class='reaction_value'][1]//text()"` >> "$WEB/monster/$i.xml"
                    echo '        </reaction>' >> "$WEB/monster/$i.xml"
                done
                echo '    </reactions>' >> "$WEB/monster/$i.xml"
            fi
            
            echo '</monster>' >> "$WEB/monster/$i.xml"
            echo -n "."
            
            # swap spellcasting-note line
            # in the current srd version this line actually exists in monster-erzmagier.xml only
            if [[ $i = "monster-erzmagier" ]]
            then
                perl -e 'open(F, "<", $ARGV[0]); @f=<F>; close(F); foreach(@f) { if(/spellcasting-note/) {$note=$_} else {print $_.$note; $note=""} }' "$WEB/monster/$i.xml" > "$WEB/monster/~$i.xml"
                mv "$WEB/monster/~$i.xml" "$WEB/monster/$i.xml"
            fi
            
            # check validity of XML
            $XMLLINT --dtdvalid "$DIR/dtd/srd-51-de.dtd" --noblanks --noout "$WEB/monster/$i.xml"
            echo -n "."
        
            # pretty print XML
            XMLLINT_INDENT="    " $XMLLINT --format "$WEB/monster/$i.xml" > "$WEB/monster/~$i.xml"
            mv "$WEB/monster/~$i.xml" "$WEB/monster/$i.xml"
            echo -n "."
                    
            #######################################################################
            # JSON
            # monster-to-json.xsl replicates the same structure as XML, but leaves raw-elements
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/monster-to-json.xsl" "$WEB/monster/$i.xml" > "$WEB/monster/$i.json"
                echo -n "."
            else
                echo -n "x"
            fi
            
            # check validity of JSON
            if [[ -x $JSONLINT ]]
            then
                $JSONLINT "$WEB/monster/$i.json" > /dev/null
                echo -n "."
            else
                echo -n "x"
            fi
        
            # pretty print JSON
            if [[ -x $JQ ]]
            then
                $JQ . --indent 4 "$WEB/monster/$i.json" > "$WEB/monster/~$i.json"
                mv "$WEB/monster/~$i.json" "$WEB/monster/$i.json"
                echo -n "."
            else
                echo -n "x"
            fi
            
            #######################################################################
            # MARKDOWN
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/monster-to-homebrewery-markdown.xsl" "$WEB/monster/$i.xml" > "$WEB/monster/$i.homebrewery.md"
                $XSLTPROC "$DIR/xsl/monster-to-gmbinder-markdown.xsl" "$WEB/monster/$i.xml" > "$WEB/monster/$i.gmbinder.md"
                echo -n "."
            else
                echo -n "x"
            fi
            
            #######################################################################
            # LATEX
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/monster-to-rpgtex-latex.xsl" "$WEB/monster/$i.xml" > "$WEB/monster/$i.rpgtex.tex"
                echo -n "."
            else
                echo -n "x"
            fi

            #######################################################################
            # YAML
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/monster-to-fantasystatblocks-yaml.xsl" "$WEB/monster/$i.xml" > "$WEB/monster/$i.fantasystatblocks.yaml"
                echo -n "."
            else
                echo -n "x"
            fi
            
            #######################################################################

            if [[ $ERROR > 0 ]]
            then
                echo -e ' \u203C''ERROR''\u203C'
            else
                echo -e ' \u2713'
            fi
        fi
    done
done