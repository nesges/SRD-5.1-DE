<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:variable name="spell-links">true</xsl:variable>
    <xsl:variable name="spell-links-baseurl">https://openrpg.de/srd/5e/de/#</xsl:variable>
    <xsl:variable name="spell-links-extension"></xsl:variable>

<xsl:template match="/magicitem">
    <xsl:apply-templates select="name" />
    <xsl:text>*</xsl:text><xsl:value-of select="type" /><xsl:text>, </xsl:text><xsl:value-of select="rarity" />
    <xsl:if test="attunement/text() != ''">
        <xsl:text> (</xsl:text><xsl:value-of select="attunement" /><xsl:text>)</xsl:text>    
    </xsl:if>
    <xsl:text>*&#xa;:&#xa;</xsl:text>
    <xsl:apply-templates select="description/html" />
    <xsl:text>&#xa;&#xa;</xsl:text>
</xsl:template>

<xsl:template match="name">
    <xsl:text>&#xa;#### </xsl:text><xsl:value-of select="." /><xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="html">
    <xsl:value-of select="." />
</xsl:template>

</xsl:stylesheet>
