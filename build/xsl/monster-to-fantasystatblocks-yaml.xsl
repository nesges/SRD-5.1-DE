<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>

    <xsl:template match="/monster">
        <xsl:text>```statblock&#xa;</xsl:text>
        <xsl:text>name: "</xsl:text><xsl:value-of select="name" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>size: "</xsl:text><xsl:value-of select="size" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>type: "</xsl:text><xsl:value-of select="type" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>alignment: "</xsl:text><xsl:value-of select="alignment" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>ac: "</xsl:text><xsl:value-of select="armor-class/raw" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>hp: "</xsl:text><xsl:value-of select="hit-points/raw" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>speed: "</xsl:text><xsl:value-of select="speeds/raw" /><xsl:text>"&#xa;</xsl:text>
        <xsl:text>stats: [</xsl:text><xsl:value-of select="attributes/attribute/class[text()='str']/following::value" /><xsl:text>, </xsl:text>
            <xsl:value-of select="attributes/attribute/class[text()='dex']/following::value" /><xsl:text>, </xsl:text>
            <xsl:value-of select="attributes/attribute/class[text()='con']/following::value" /><xsl:text>, </xsl:text>
            <xsl:value-of select="attributes/attribute/class[text()='int']/following::value" /><xsl:text>, </xsl:text>
            <xsl:value-of select="attributes/attribute/class[text()='wis']/following::value" /><xsl:text>, </xsl:text>
            <xsl:value-of select="attributes/attribute/class[text()='cha']/following::value" /><xsl:text>]&#xa;</xsl:text>
        <xsl:if test="saving-throws">
            <xsl:text>saves:&#xa;</xsl:text>
            <xsl:apply-templates select="/saving-throws/saving-throw" />
        </xsl:if>
        <xsl:if test="skills">
            <xsl:text>skillsaves:&#xa;</xsl:text>
            <xsl:apply-templates select="skills/skill" />
        </xsl:if>
        <xsl:if test="damage-vulnerabilitys">
            <xsl:text>damage_vulnerabilities: "</xsl:text><xsl:value-of select="damage-vulnerabilitys/raw" /><xsl:text>"&#xa;</xsl:text>
        </xsl:if>
        <xsl:if test="damage-resistances">
            <xsl:text>damage_resistances: "</xsl:text><xsl:value-of select="damage-resistances/raw" /><xsl:text>"&#xa;</xsl:text>
        </xsl:if>
        <xsl:if test="damage-immunitys">
            <xsl:text>damage_immunities: "</xsl:text><xsl:value-of select="damage-immunitys/raw" /><xsl:text>"&#xa;</xsl:text>
        </xsl:if>
        <xsl:if test="condition-immunitys">
            <xsl:text>condition_immunities: "</xsl:text><xsl:value-of select="condition-immunitys/raw" /><xsl:text>"&#xa;</xsl:text>
        </xsl:if>
        <xsl:if test="senses">
            <xsl:text>senses: "</xsl:text><xsl:value-of select="senses/raw" /><xsl:text>"&#xa;</xsl:text>
        </xsl:if>
        <xsl:if test="languages">
            <xsl:text>languages: "</xsl:text><xsl:value-of select="languages/raw" /><xsl:text>"&#xa;</xsl:text>
        </xsl:if>
        <xsl:text>cr: </xsl:text><xsl:value-of select="challenge" /><xsl:text>&#xa;</xsl:text>

        <xsl:if test="spellcasting-trait | spellcasting-innate-trait">
            <xsl:text>spells:&#xa;</xsl:text>
            <xsl:apply-templates select="spellcasting-trait | spellcasting-innate-trait" />
        </xsl:if>
        <xsl:if test="traits">
            <xsl:text>traits:&#xa;</xsl:text>
            <xsl:apply-templates select="traits/trait" />
        </xsl:if>
        <xsl:if test="actions">
            <xsl:text>actions:&#xa;</xsl:text>
            <xsl:apply-templates select="actions/action" />
        </xsl:if>
        <xsl:if test="legendary-actions">
            <xsl:text>legendary_actions:&#xa;</xsl:text>
            <xsl:apply-templates select="legendary-actions/legendary-action" />
        </xsl:if>
        <xsl:if test="bonus-actions">
            <xsl:text>bonus_actions:&#xa;</xsl:text>
            <xsl:apply-templates select="bonus-actions/bonus-action" />
        </xsl:if>
        <xsl:if test="reactions">
            <xsl:text>reactions:&#xa;</xsl:text>
            <xsl:apply-templates select="reactions/reaction" />
        </xsl:if>
        <xsl:text>```&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="saving-throw | skill">
        <xsl:text>  - "</xsl:text><xsl:value-of select="substring-before(., ' +')" />": <xsl:value-of select="substring-after(., '+')" /><xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template match="spellcasting-trait">
        <xsl:text>  - "</xsl:text><xsl:value-of select="./spellcasting-text" /><xsl:text>"&#xa;</xsl:text>
        <xsl:for-each select="spells/spell-list">
            <xsl:text>  - </xsl:text>
            <xsl:choose>
                <xsl:when test="level = 'Zaubertricks'"><xsl:value-of select="level" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="level" />. Grad</xsl:otherwise>
            </xsl:choose>
            <xsl:text> (</xsl:text>
            <xsl:value-of select="slots | frequency" />
            <xsl:choose>
                <xsl:when test="slots = 'beliebig oft'"></xsl:when>
                <xsl:when test="slots = '1'"><xsl:text> Platz</xsl:text></xsl:when>
                <xsl:otherwise><xsl:text> Plätze</xsl:text></xsl:otherwise>
            </xsl:choose>
            <xsl:text>): "</xsl:text>
            <xsl:apply-templates select="spell-reference" />
            <xsl:text>"&#xa;</xsl:text>
        </xsl:for-each>
        <xsl:if test="spells/spellcasting-note">
            <xsl:text>  - "</xsl:text>
            <xsl:value-of select="spells/spellcasting-note" />
            <xsl:text>"&#xa;</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="spellcasting-innate-trait">
        <xsl:text>  - "</xsl:text><xsl:value-of select="./spellcasting-text" /><xsl:text>"&#xa;</xsl:text>
        <xsl:for-each select="spells/spell-list">
            <xsl:text>  - </xsl:text>
            <xsl:value-of select="frequency" />
            <xsl:text>: "</xsl:text>
            <xsl:apply-templates select="spell-reference" />
            <xsl:text>"&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="spell-reference">
        <xsl:value-of select="name" /><xsl:if test="following-sibling::*">, </xsl:if>
    </xsl:template>
    
    <xsl:template match="trait | action | legendary-action | bonus-action | reaction">
      <xsl:text>  - name: "</xsl:text><xsl:value-of select="name" /><xsl:text>"&#xa;</xsl:text>
      <xsl:text>    desc: "</xsl:text><xsl:value-of select="value" /><xsl:text>"&#xa;</xsl:text>
    </xsl:template>

</xsl:stylesheet>