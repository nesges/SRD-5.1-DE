<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />

<xsl:template match="/magicitem">
    <xsl:text>\DndItemHeader{</xsl:text>
    <xsl:value-of select="name" />
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="type" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="rarity" />
    <xsl:text>}</xsl:text>
    <xsl:if test="attunement/text() != ''">
        <xsl:text> (</xsl:text><xsl:value-of select="attunement" /><xsl:text>)</xsl:text>    
    </xsl:if>
    <xsl:text>&#xa;</xsl:text>
    <xsl:value-of select="description/text" />
    <xsl:if test="description/text-full != description/text">
        <xsl:text>&#xa;% Gesamter Text: </xsl:text>
        <xsl:value-of select="description/text-full" />
        <xsl:text>&#xa;</xsl:text>
    </xsl:if>
    <xsl:text>&#xa;</xsl:text>
</xsl:template>

</xsl:stylesheet>
