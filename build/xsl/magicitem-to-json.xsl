<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>

    <xsl:template match="magicitem">{<xsl:apply-templates />}</xsl:template>

    <!-- ignore those elements -->
    <xsl:template match="raw" />
    
    <xsl:template match="text | text-full | html">
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:variable name="quot">"</xsl:variable>
        <xsl:variable name="rhquot">&#x201D;</xsl:variable>
        <xsl:variable name="lhquot">&#x201C;</xsl:variable>
        <xsl:variable name="rlquot">&#x201E;</xsl:variable>
        <xsl:text>"</xsl:text><xsl:value-of select="name(.)" />": "<xsl:value-of select="translate(translate(translate(translate(., $quot, $apos), $rhquot, $apos), $lhquot, $apos), $rlquot, $apos)" /><xsl:text>"</xsl:text>
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <!-- all other elements are 1:1 copys of their xml counterpart -->
    <xsl:template match="*">
        <xsl:choose>
            <xsl:when test="*">"<xsl:value-of select="name(.)" />": { <xsl:apply-templates /> }<xsl:if test="following-sibling::*">,</xsl:if></xsl:when>
            <xsl:otherwise>
                "<xsl:value-of select="name(.)" />": "<xsl:value-of select="." />"<xsl:if test="following-sibling::*">,</xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>