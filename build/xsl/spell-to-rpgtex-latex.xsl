<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />

<xsl:template match="/spell">
    <xsl:text>\DndSpellHeader&#xa;</xsl:text>
    <xsl:text>    {</xsl:text><xsl:value-of select="name" /><xsl:text>}&#xa;</xsl:text>
    <xsl:text>    {</xsl:text><xsl:value-of select="school-level-raw" /><xsl:text>}&#xa;</xsl:text>
    
    <xsl:text>    {</xsl:text><xsl:value-of select="time" /><xsl:text>}&#xa;</xsl:text>
    <xsl:text>    {</xsl:text><xsl:value-of select="range" /><xsl:text>}&#xa;</xsl:text>
    <xsl:text>    {</xsl:text><xsl:value-of select="components" />
    <xsl:if test="material-components">
        <xsl:text> (</xsl:text>
        <xsl:value-of select="material-components"/>
        <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:text>}&#xa;</xsl:text>
    <xsl:text>    {</xsl:text><xsl:value-of select="duration" /><xsl:text>}&#xa;</xsl:text>
    <xsl:value-of select="description/text" />
    <xsl:if test="description/text-full != description/text">
        <xsl:text>&#xa;% Gesamter Text: </xsl:text>
        <xsl:value-of select="description/text-full" />
        <xsl:text>&#xa;</xsl:text>
    </xsl:if>
    <xsl:text>&#xa;</xsl:text>
</xsl:template>

</xsl:stylesheet>
