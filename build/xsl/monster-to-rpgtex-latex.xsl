<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>

    <xsl:template match="/monster">\begin{DndMonster}[float*=b,width=\textwidth + 8pt]{<xsl:value-of select="name" />}
  \begin{multicols}{2}

    \DndMonsterType{<xsl:value-of select="size-type-raw" />, <xsl:value-of select="alignment" />}

    \DndMonsterBasics[
        armor-class = {<xsl:value-of select="armor-class/raw" />},
        hit-points  = {<xsl:value-of select="hit-points/raw" />},
        speed       = {<xsl:value-of select="speeds/raw" />}<xsl:text>&#xa;</xsl:text>
    <xsl:text>    ]</xsl:text>

    \DndMonsterAbilityScores[
        str = <xsl:value-of select="attributes/attribute/class[text()='str']/following::value" />,
        dex = <xsl:value-of select="attributes/attribute/class[text()='dex']/following::value" />,
        con = <xsl:value-of select="attributes/attribute/class[text()='con']/following::value" />,
        int = <xsl:value-of select="attributes/attribute/class[text()='int']/following::value" />,
        wis = <xsl:value-of select="attributes/attribute/class[text()='wis']/following::value" />,
        cha = <xsl:value-of select="attributes/attribute/class[text()='cha']/following::value" /><xsl:text>&#xa;</xsl:text>
    <xsl:text>    ]</xsl:text>
    <xsl:text>&#xa;&#xa;</xsl:text>
    <xsl:text>    \DndMonsterDetails[ &#xa;</xsl:text>
        <xsl:apply-templates select="saving-throws" />
        <xsl:apply-templates select="skills" />
        <xsl:apply-templates select="damage-vulnerabilitys" />
        <xsl:apply-templates select="damage-resistances" />
        <xsl:apply-templates select="damage-immunitys" />
        <xsl:apply-templates select="condition-immunitys" />
        <xsl:apply-templates select="senses" />
        <xsl:apply-templates select="languages" />
        <xsl:text>        challenge = </xsl:text><xsl:value-of select='challenge' /><xsl:text>&#xa;</xsl:text>
    <xsl:text>    ]</xsl:text>
    <xsl:text>&#xa;</xsl:text>
    
    <xsl:text>&#xa;    % Traits&#xa;</xsl:text>

    <xsl:apply-templates select="spellcasting-trait | spellcasting-innate-trait" />

    <xsl:apply-templates select="traits/trait" />
    
    <xsl:apply-templates select="actions | bonus-actions | legendary-actions | reactions" />
  \end{multicols}
\end{DndMonster}
</xsl:template>

<xsl:template match="damage-vulnerabilitys | damage-resistances | damage-immunitys | condition-immunitys">
    <xsl:variable name="node">
        <xsl:choose>
            <xsl:when test="substring-before(name(.), 'tys') != ''">
                <xsl:value-of select="concat(substring-before(name(.), 'tys'), 'ties')"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="name(.)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:text>        </xsl:text><xsl:value-of select="$node" /><xsl:text> = {</xsl:text><xsl:value-of select="raw" /><xsl:text>},&#xa;</xsl:text>
</xsl:template>

<xsl:template match="saving-throws | skills | senses | languages">
    <xsl:text>        </xsl:text>
    <xsl:value-of select="name(.)" />
    <xsl:text> = {</xsl:text>
    <xsl:value-of select="raw" />
    <xsl:text>},&#xa;</xsl:text>
</xsl:template>

<xsl:template match="trait">
    <xsl:text>&#xa;</xsl:text>
        \DndMonsterAction{<xsl:value-of select="name" />}
        <xsl:value-of select="value" />
</xsl:template>

<xsl:template match="actions">
    <xsl:text>&#xa;&#xa;</xsl:text>
    <xsl:text>    \DndMonsterSection{Aktionen}&#xa;</xsl:text>
    <xsl:text>&#xa;</xsl:text>
    <xsl:apply-templates select="action" />
</xsl:template>

<xsl:template match="action | bonus-action | reaction">
    <xsl:text>        \DndMonsterAction{</xsl:text>
    <xsl:value-of select="name" />
    <xsl:text>}&#xa;</xsl:text>
    <xsl:text>        </xsl:text><xsl:value-of select="value" />    
    <xsl:text>&#xa;</xsl:text>
    <xsl:text>&#xa;</xsl:text>

    <xsl:variable name="distance">
        <xsl:choose>
            <xsl:when test="substring-after(./value, 'Nah- oder Fernkampf') != ''">both</xsl:when>
            <xsl:when test="substring-after(./value, 'Nahkampf') != ''">melee</xsl:when>
            <xsl:when test="substring-after(./value, 'Fernampf') != ''">ranged</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>       
    </xsl:variable>

    <xsl:variable name="type">
        <xsl:choose>
            <xsl:when test="substring-after(./value, 'waffenangriff') != ''">weapon</xsl:when>
            <xsl:when test="substring-after(./value, 'Zauberangriff') != ''">spell</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>       
    </xsl:variable>
    
    <xsl:variable name="plus-dmg">
        <xsl:value-of select="substring-before(substring-after(value, 'schaden plus '), ') ')" />
    </xsl:variable>
    
    <xsl:variable name="or-dmg">
        <xsl:value-of select="substring-before(substring-after(substring-after(value, '. Treffer: '), 'schaden oder '), ')')" />
    </xsl:variable>

    <xsl:if test="$distance != '' and $type != ''">
<xsl:text>% Alternativ kannst du auch diese Form verwenden, ggf. ist sie nicht fehlerfrei:</xsl:text>
%   \DndMonsterAttack[
%      name=<xsl:value-of select="name" />,
%      distance=<xsl:value-of select="$distance" />,
%      type=<xsl:value-of select="$type" />,
%      mod=<xsl:value-of select="substring-after(substring-before(value, ' auf Treffer,'), 'angriff: ')" />,
%      reach=<xsl:value-of select="substring-after(substring-before(value, ' m,'), 'Reichweite ')" /> m,
%      targets=<xsl:value-of select="substring-before(substring-after(value, ' m, '), '. Treffer:')" />,
%      dmg=<xsl:value-of select="substring-before(substring-after(value, '. Treffer: '), ') ')" />),
%      dmg-type=<xsl:value-of select="substring-before(substring-after(substring-after(value, '. Treffer: '), ') '), 'schaden')" />,
%      plus-dmg=<xsl:if test="$plus-dmg != ''"><xsl:value-of select="$plus-dmg" />)</xsl:if>,
%      plus-dmg-type=<xsl:value-of select="substring-before(substring-after(substring-after(value, 'schaden plus '), ') '), 'schaden.')" />,
%      or-dmg=<xsl:if test="$or-dmg != ''"><xsl:value-of select="$or-dmg" />)</xsl:if>,
%      or-dmg-when=<xsl:value-of select="substring-before(substring-after(substring-after(value, 'schaden oder '), 'schaden '), '.')" />,
%      extra=<xsl:value-of select="substring-after(substring-after(value, '.'), '. ')" />
<xsl:text>%    ]&#xa;</xsl:text>
    </xsl:if>


</xsl:template>

<xsl:template match="bonusactions">
    \DndMonsterSection{Bonus-Aktionen}
    <xsl:apply-templates select="bonus-action" />
</xsl:template>

<xsl:template match="legendary-actions">
    \DndMonsterSection{Legend"are Aktionen}
    
    <xsl:value-of select="legendary-text"/>
    
    <xsl:text>&#xa;&#xa;    \begin{DndMonsterLegendaryActions}</xsl:text>
    <xsl:apply-templates select="legendary-action" />
    <xsl:text>    \end{DndMonsterLegendaryActions}&#xa;</xsl:text>
</xsl:template>

<xsl:template match="legendary-action">
        \DndMonsterLegendaryAction{<xsl:value-of select="name" />}{<xsl:value-of select="value" /><xsl:text>}&#xa;</xsl:text>
</xsl:template>

<xsl:template match="reactions">
    \DndMonsterSection{Reaktionen}
    <xsl:apply-templates select="reaction" />
</xsl:template>

<xsl:template match="spellcasting-trait">
    <xsl:text>&#xa;</xsl:text>
    <xsl:text>        \DndMonsterAction{Zauberwirken}&#xa;</xsl:text>
    <xsl:text>        </xsl:text><xsl:value-of select="./spellcasting-text" /><xsl:text>&#xa;</xsl:text>
    <xsl:text>        \begin{DndMonsterSpells}&#xa;</xsl:text>
    <xsl:text>            \DndMonsterSpellLevel{</xsl:text><xsl:apply-templates select="./spells/spell-list[level[text() = 'Zaubertricks']]/spell-reference" /><xsl:text>}&#xa;</xsl:text>
    <xsl:for-each select="./spells/spell-list/level[text() != 'Zaubertricks']/parent::*">
        <xsl:text>            \DndMonsterSpellLevel[</xsl:text><xsl:value-of select="level" />][<xsl:value-of select="slots" />]{<xsl:apply-templates select="spell-reference" /><xsl:text>}&#xa;</xsl:text>
    </xsl:for-each>
    <xsl:text>        \end{DndMonsterSpells}</xsl:text>
</xsl:template>

<xsl:template match="spellcasting-innate-trait">
    <xsl:text>&#xa;</xsl:text>
    <xsl:text>        \DndMonsterAction{Angeborenes Zauberwirken}&#xa;</xsl:text>
    <xsl:text>        </xsl:text><xsl:value-of select="./spellcasting-text" /><xsl:text>&#xa;</xsl:text>
    <xsl:text>        \begin{DndMonsterSpells}&#xa;</xsl:text>
    <xsl:for-each select="spells/spell-list">
        <xsl:text>            \DndInnateSpellLevel</xsl:text>
        <xsl:choose>
            <xsl:when test="frequency = 'Beliebig oft'" />
            <xsl:when test="substring-after(frequency, '1-mal') != ''">
                <xsl:text>[1]</xsl:text>
            </xsl:when>
            <xsl:when test="substring-after(frequency, '3-mal') != ''">
                <xsl:text>[3]</xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:text>{</xsl:text><xsl:apply-templates select="spell-reference" /><xsl:text>}&#xa;</xsl:text>
    </xsl:for-each>
    <xsl:text>        \end{DndMonsterSpells}</xsl:text>
</xsl:template>

<xsl:template match="spell-reference">
    <xsl:value-of select="name" /><xsl:if test="following-sibling::*">, </xsl:if>
</xsl:template>

</xsl:stylesheet>