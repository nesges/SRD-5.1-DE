<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:variable name="spell-links">true</xsl:variable>
    <xsl:variable name="spell-links-baseurl">https://openrpg.de/srd/5e/de/#</xsl:variable>
    <xsl:variable name="spell-links-extension"></xsl:variable>

<xsl:template name="attribute-format">
    <xsl:param name="type" />

    <xsl:text>|</xsl:text>
    <xsl:value-of select="substring(concat(attributes/attribute/class[text()=$type]/following::value, '  '), 1, 2)" />
    <xsl:text> (</xsl:text>
    <xsl:value-of select="substring(concat(attributes/attribute/class[text()=$type]/following::modifier, '  '), 1, 2)" />
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="/monster">
    <xsl:text>___&#xa;</xsl:text>
    <xsl:text>&gt; ## </xsl:text><xsl:value-of select="name" /><xsl:text>&#xa;</xsl:text>
    <xsl:text>&gt; *</xsl:text><xsl:value-of select="size-type-raw" />, <xsl:value-of select="alignment" /><xsl:text>*&#xa;</xsl:text>
    <xsl:text>&gt; ___&#xa;</xsl:text>
    <xsl:text>&gt; **Rüstungsklasse** </xsl:text><xsl:value-of select="armor-class/raw" /><xsl:text>  &#xa;</xsl:text>
    <xsl:text>&gt; **Trefferpunkte**  </xsl:text><xsl:value-of select="hit-points/raw" /><xsl:text>  &#xa;</xsl:text>
    <xsl:text>&gt; **Bewegungsrate**  </xsl:text><xsl:value-of select="speeds/raw" /><xsl:text>  &#xa;</xsl:text>
    <xsl:text>&gt; ___&#xa;</xsl:text>
    <xsl:text>&gt; |  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |&#xa;</xsl:text>
    <xsl:text>&gt; |:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|&#xa;</xsl:text>
    <xsl:text>&gt; </xsl:text>
    <xsl:call-template name="attribute-format">
        <xsl:with-param name="type">str</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="attribute-format">
        <xsl:with-param name="type">dex</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="attribute-format">
        <xsl:with-param name="type">con</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="attribute-format">
        <xsl:with-param name="type">int</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="attribute-format">
        <xsl:with-param name="type">wis</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="attribute-format">
        <xsl:with-param name="type">cha</xsl:with-param>
    </xsl:call-template>
    <xsl:text>|&#xa;</xsl:text>
    <xsl:text>&gt; ___&#xa;</xsl:text>
    <xsl:if test="saving-throws/raw"          >&gt; **Rettungswürfe**           <xsl:value-of select="saving-throws/raw" /><xsl:text>  &#xa;</xsl:text></xsl:if>
    <xsl:if test="skills/raw"                 >&gt; **Fertigkeiten**            <xsl:value-of select="skills/raw" /><xsl:text>  &#xa;</xsl:text></xsl:if>
    <xsl:if test="damage-resistances/raw"     >&gt; **Schadensresistenzen**     <xsl:value-of select="damage-resistances/raw" /><xsl:text>  &#xa;</xsl:text></xsl:if>
    <xsl:if test="damage-vulnerabilitys/raw"  >&gt; **Schadensanfälligkeiten**  <xsl:value-of select="damage-vulnerabilitys/raw" /><xsl:text>  &#xa;</xsl:text></xsl:if>
    <xsl:if test="damage-immunitys/raw"       >&gt; **Schadensimmunitäten**     <xsl:value-of select="damage-immunitys/raw" /><xsl:text>  &#xa;</xsl:text></xsl:if>
    <xsl:if test="condition-immunitys/raw"    >&gt; **Zustandsimmunitäten**     <xsl:value-of select="condition-immunitys/raw" /><xsl:text>  &#xa;</xsl:text></xsl:if>
    <xsl:text>&gt; **Sinne**                   </xsl:text><xsl:value-of select="senses/raw" /><xsl:text>  &#xa;</xsl:text>
    <xsl:text>&gt; **Sprachen**                </xsl:text><xsl:value-of select="languages/raw" /><xsl:text>  &#xa;</xsl:text>
    <xsl:text>&gt; **Herausforderung**         </xsl:text><xsl:value-of select="challenge" /> (<xsl:value-of select="xp" /> EP)<xsl:text>  &#xa;</xsl:text>
    <xsl:text>&gt; ___</xsl:text>
    <xsl:apply-templates select="//trait" />
    <xsl:apply-templates select="spellcasting-trait | spellcasting-innate-trait" />
    <xsl:apply-templates select="actions" />
    <xsl:apply-templates select="bonus-actions" />
    <xsl:apply-templates select="legendary-actions" />
    <xsl:apply-templates select="reactions" />
    <xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="actions">
    <xsl:text>&#xa;&gt; ### Aktionen</xsl:text><xsl:apply-templates select="./action" />
</xsl:template>

<xsl:template match="legendary-actions">
    <xsl:text>&#xa;&gt; ### Legendäre Aktionen&#xa;</xsl:text>
    <xsl:value-of select="./legendary-text" />
    <xsl:text>&#xa;&gt; </xsl:text>
    <xsl:apply-templates select="./legendary-action" />
</xsl:template>

<xsl:template match="bonus-actions">
    <xsl:text>&#xa;&gt; ### Bonus-Aktionen</xsl:text><xsl:apply-templates select="./bonus-action" />
</xsl:template>

<xsl:template match="reactions">
    <xsl:text>&#xa;&gt; ### Reaktionen</xsl:text><xsl:apply-templates select="./reaction" />
</xsl:template>

<xsl:template name="action-attack-format">
    <xsl:param name="type" />

    <xsl:text>*</xsl:text><xsl:value-of select="$type"/><xsl:text>*:</xsl:text>
    <xsl:value-of select="substring-before(substring-after(./value, concat($type, ':')), 'Treffer:')" />
    <xsl:text>*Treffer*:</xsl:text>
    <xsl:value-of select="substring-after(./value, 'Treffer:')" />
</xsl:template>

<xsl:template match="action">
    <xsl:text>&#xa;&gt; ***</xsl:text>
    <xsl:value-of select="./name" />
    <xsl:text>.*** </xsl:text>
    <xsl:choose>
        <xsl:when test="substring-after(./value, 'Nahkampfwaffenangriff:') != ''">
            <xsl:call-template name="action-attack-format">
                <xsl:with-param name="type">Nahkampfwaffenangriff</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:when test="substring-after(./value, 'Fernkampfwaffenangriff:') != ''">
            <xsl:call-template name="action-attack-format">
                <xsl:with-param name="type">Fernkampfwaffenangriff</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:when test="substring-after(./value, 'Nahkampf-Zauberangriff:') != ''">
            <xsl:call-template name="action-attack-format">
                <xsl:with-param name="type">Nahkampf-Zauberangriff</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:when test="substring-after(./value, 'Fernkampf-Zauberangriff:') != ''">
            <xsl:call-template name="action-attack-format">
                <xsl:with-param name="type">Fernkampf-Zauberangriff</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="./value" />
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#xa;&gt; </xsl:text>
</xsl:template>

<xsl:template match="trait | legendary-action | bonus-action | reaction">
    <xsl:text>&#xa;&gt; ***</xsl:text><xsl:value-of select="./name" />.*** <xsl:value-of select="./value" />
    <xsl:text>&#xa;&gt; </xsl:text>
</xsl:template>

<xsl:template match="spellcasting-trait">
    <xsl:text>&#xa;&gt; ***Zauberwirken.*** </xsl:text><xsl:value-of select="./spellcasting-text" />
    <xsl:text>&#xa;&gt; &#xa;</xsl:text>
    <xsl:apply-templates select="./spells" />
    <xsl:text>&#xa;&gt; </xsl:text>
</xsl:template>

<xsl:template match="spellcasting-innate-trait">
    <xsl:text>&#xa;&gt; ***Angeborenes Zauberwirken.*** </xsl:text><xsl:value-of select="./spellcasting-text" />
    <xsl:text>&#xa;&gt; &#xa;</xsl:text>
    <xsl:apply-templates select="./spells" />
    <xsl:text>&#xa;&gt;  </xsl:text></xsl:template>

<xsl:template match="spells">
    <xsl:apply-templates select="./spell-list" />
    <xsl:apply-templates select="spellcasting-note" />
</xsl:template>

<xsl:template match="spell-list">
    <xsl:choose>
        <xsl:when test="./level">
            <xsl:text>&gt; </xsl:text><xsl:value-of select="./level" /><xsl:if test="number(./level) = ./level">&amp;period; Grad</xsl:if> (<xsl:value-of select="./slots" /><xsl:if test="number(./level) = ./level"><xsl:choose><xsl:when test="./slots='1'"> Platz</xsl:when><xsl:otherwise> Plätze</xsl:otherwise></xsl:choose></xsl:if>): <xsl:apply-templates select="spell-reference" />
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>&gt; </xsl:text><xsl:value-of select="./frequency" />: <xsl:apply-templates select="spell-reference" />
        </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="./following-sibling::spell-list"><xsl:text>&lt;br&gt;</xsl:text><xsl:text>&#xa;</xsl:text></xsl:if>
</xsl:template>

<xsl:template match="spell-reference">
    <xsl:if test="$spell-links='true'">[</xsl:if>
    <xsl:text>*</xsl:text>
    <xsl:choose>
        <xsl:when test="substring-before(./name/text(), '*') != ''">
            <!-- replace * with &ast; -->
            <xsl:value-of select="concat(substring-before(./name/text(), '*'), '&amp;ast;')" />
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="./name" />
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>*</xsl:text>
    <xsl:if test="$spell-links='true'">](<xsl:value-of select="$spell-links-baseurl" /><xsl:value-of select="./id" /><xsl:value-of select="$spell-links-extension" />)</xsl:if>
    <xsl:if test="following-sibling::spell-reference">, </xsl:if>
</xsl:template>

<xsl:template match="spellcasting-note">
    <xsl:text>&#xa;&gt; &#xa;&gt; </xsl:text>
    <xsl:choose>
        <xsl:when test="substring-after(., '*') != ''">
            <!-- replace * with &ast; -->
            <xsl:value-of select="concat('&amp;ast;', substring-after(., '*'))" />        
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="." />
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
