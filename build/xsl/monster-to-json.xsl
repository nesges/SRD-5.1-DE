<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>

    <xsl:template match="/monster">{<xsl:apply-templates />}</xsl:template>

    <!-- array-type elements -->
    <xsl:template match="attributes | saving-throws | skills | damage-resistances | damage-vulnerabilitys | damage-immunitys | condition-immunitys | senses | languages | traits | actions | legendary-actions | bonus-actions | reactions | spells">"<xsl:value-of select="name(.)" />": [<xsl:apply-templates />]<xsl:if test="following-sibling::*">,</xsl:if></xsl:template>
    
    <!-- spells is an array element, but may be followed by spellcasting-note -->
    <xsl:template match="spells">"<xsl:value-of select="name(.)" />": [<xsl:apply-templates select="spell-list" />]
        <xsl:choose>
            <xsl:when test="following-sibling::*">,</xsl:when>
            <xsl:otherwise>
                <xsl:if test="spellcasting-note">, "spellcasting-note": "<xsl:value-of select="spellcasting-note"/>"</xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- children of array-type elements are separated by "," but you mustn't add a "," after the last element in json -->
    <xsl:template match="attribute">
        {<xsl:apply-templates />}<xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <!-- other children of array-type elements -->
    <xsl:template match="saving-throw | skill | damage-resistance | damage-vulnerability | damage-immunity | condition-immunity | sense | language | trait | action | legendary-action | bonus-action | reaction | spell-reference">
        <xsl:choose>
            <xsl:when test="*">{<xsl:apply-templates />}<xsl:if test="following-sibling::*">,</xsl:if></xsl:when>
            <xsl:otherwise>"<xsl:value-of select="." />"<xsl:if test="following-sibling::*">,</xsl:if></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="spell-list">{
        <!-- only in spellcasting-trait -->
        <xsl:if test="./level"><xsl:text>"level": "</xsl:text><xsl:value-of select="level" />",</xsl:if>
        <xsl:if test="./slots"><xsl:text>"slots": "</xsl:text><xsl:value-of select="slots" />",</xsl:if>
        <!-- only in spellcasting-innate-trait -->
        <xsl:if test="./frequency"><xsl:text>"frequency": "</xsl:text><xsl:value-of select="frequency" />",</xsl:if>
        <!-- in spellcasting-trait and spellcasting-innate-trait -->
        <xsl:text>"list": [</xsl:text><xsl:apply-templates select="spell-reference" />]
    }<xsl:if test="following-sibling::spell-list">,</xsl:if></xsl:template>
    
    <!-- ignore those elements -->
    <xsl:template match="raw | size-type-raw | legendary-text" />

    <!-- all other elements are 1:1 copys of their xml counterpart -->
    <xsl:template match="*">
        <xsl:choose>
            <xsl:when test="*">"<xsl:value-of select="name(.)" />": { <xsl:apply-templates /> }<xsl:if test="following-sibling::*">,</xsl:if></xsl:when>
            <xsl:otherwise>
                <!--xsl:if test=". != ''">"<xsl:value-of select="name(.)" />": "<xsl:value-of select="." />"<xsl:if test="following-sibling::*">,</xsl:if></xsl:if-->
                "<xsl:value-of select="name(.)" />": "<xsl:value-of select="." />"<xsl:if test="following-sibling::*">,</xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>