<?xml version="1.0"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:variable name="spell-links">true</xsl:variable>
    <xsl:variable name="spell-links-baseurl">https://openrpg.de/srd/5e/de/#</xsl:variable>
    <xsl:variable name="spell-links-extension"></xsl:variable>

<xsl:template match="/spell">
    <xsl:apply-templates select="name" />
    <xsl:text>*</xsl:text><xsl:value-of select="school-level-raw" /><xsl:text>*&#xa;</xsl:text>
    <xsl:text>___&#xa;</xsl:text>
    <xsl:text>- **Zeitaufwand:**   </xsl:text><xsl:value-of select="time"/><xsl:text>&#xa;</xsl:text>
    <xsl:text>- **Reichweite:**    </xsl:text><xsl:value-of select="range"/><xsl:text>&#xa;</xsl:text>
    <xsl:text>- **Komponenten:**   </xsl:text><xsl:value-of select="components" />
    <xsl:if test="material-components">
        <xsl:text> (</xsl:text>
        <xsl:value-of select="material-components"/>
        <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:text>&#xa;</xsl:text>
    <xsl:text>- **Wirkungsdauer:** </xsl:text><xsl:value-of select="duration"/><xsl:text>&#xa;</xsl:text>
    <xsl:text>&#xa;</xsl:text>
    <xsl:apply-templates select="description/html" />
    <xsl:text>&#xa;&#xa;</xsl:text>
</xsl:template>

<xsl:template match="name">
    <xsl:text>&#xa;#### </xsl:text><xsl:value-of select="." /><xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="html">
    <xsl:choose>
        <xsl:when test="substring-after(., 'Auf höheren Graden:') != ''">
            <xsl:value-of select="substring-before(., ' &lt;p&gt;&lt;span class=&quot;trait-name&quot;&gt;Auf höheren Graden:')"/>
            <xsl:text>&#xa;&#xa;***Auf höheren Graden:***</xsl:text>
            <xsl:value-of select="substring-before(substring-after(., 'Auf höheren Graden:&lt;/span&gt;'), '&lt;/p&gt;')" />
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
