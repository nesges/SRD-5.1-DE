#/bin/bash

. `dirname $0`/env.sh

LISTFILE="$DIR/../ID.txt"
> "$LISTFILE"

for F in "$WEB/magische-gegenstaende/a-z.html" "$WEB/magische-gegenstaende/intelligente-magische-gegenstaende.html"
do 
    grep '<div class="magicitem" id="' $F | sed -e 's/[\r\n]+//' | sed -e 's/<.*id="\(.*\)">.*/\1/' >> "$LISTFILE"
done

for F in "$WEB/monster/monster.html" "$WEB/monster/kreaturen.html" "$WEB/monster/nsc.html"
do 
    grep '<div class="monster" id="' $F | sed -e 's/[\r\n]+//' | sed -e 's/<.*id="\(.*\)">.*/\1/' >> "$LISTFILE"
done

for F in "$WEB/zauberwirken/beschreibungen-der-zauber.html"
do 
    grep '<div class="spell" id="' $F | sed -e 's/[\r\n]+//' | sed -e 's/<.*id="\(.*\)">.*/\1/' >> "$LISTFILE"
done

echo "ID list generated to $LISTFILE"