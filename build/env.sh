#!/bin/bash

#######################################################################
# CONFIGURATION
#######################################################################

XMLLINT=/usr/bin/xmllint
XSLTPROC=/usr/bin/xsltproc
JSONLINT=/usr/bin/jsonlint-php
JSONSCHEMA=/usr/bin/jsonschema
JQ=/usr/bin/jq
# TIDY=/usr/bin/tidy
BASEURL="https://openrpg.de/srd/5e/de"

#######################################################################
# PROGRAM
#######################################################################

DIR=`dirname $0`;
WEB=`readlink -f "$DIR/../web"`

# check required programs
if [[ ! -x $XMLLINT ]] || [[ ! -x $XSLTPROC ]]
then
    echo "$XMLLINT and $XSLTPROC are needed to run this program"
    echo "please see README.MD for requirements"
    exit
fi

# check optional programs
for OPTIONAL in "$JSONSCHEMA" "$JSONLINT" "$JQ"
do
    if [[ ! -x $OPTIONAL ]]
    then
        echo "please install $OPTIONAL (optional)"
    fi
done


# check directory permissions
if [[ ! -w $WEB ]]
then
    echo "can't write to $WEB"
    exit
fi

# check required files
for FILE in "$DIR/dtd/srd-51-de.dtd" "$DIR/xsl/monster-to-json.xsl" "$DIR/xsl/monster-to-rpgtex-latex.xsl" "$DIR/xsl/monster-to-homebrewery-markdown.xsl" "$DIR/xsl/magicitem-to-json.xsl" "$DIR/xsl/magicitem-to-homebrewery-markdown.xsl" "$DIR/xsl/spell-to-json.xsl" "$DIR/xsl/spell-to-homebrewery-markdown.xsl"
do
    if [[ ! -r "$FILE" ]]
    then
        echo "can't read $FILE"
        exit
    fi
done

# copy DTDs to web
cp "$DIR"/dtd/*.dtd "$WEB/"

# save original IFS
OIFS=$IFS