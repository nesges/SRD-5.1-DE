#/bin/bash

. `dirname $0`/env.sh

function lint {
    # tag xpath [optional]
    # uses global $HTML
    CONTENT=`$XMLLINT --xpath "$2" $HTML 2> /dev/null`
    if [[ $CONTENT ]] || [[ "$3" != "optional" ]]
    then
        echo "<$1>$CONTENT</$1>"
    fi
}

for F in "$WEB/zauberwirken/beschreibungen-der-zauber.html"
do 
    if [[ $1 ]]
    then
        SPELLS=$1
    else
        SPELLS=`grep '<div class="spell" id="' $F | sed -e 's/\r/\n/' | sed -e 's/<.*id="\(.*\)">/\1/'`
    fi

    for i in $SPELLS
    do 
        ERROR=0
        NAME=`xmllint --xpath "//*[@id='$i']/h5[1]/text()" $F`
        if [[ $NAME ]]
        then
            echo -n "$NAME "
                
            #######################################################################
            # HTML
            HTML="$WEB/zauberwirken/$i.html"
        
            echo '<!DOCTYPE html>' > "$HTML"
            echo '<html lang="de">' >> "$HTML"
            echo '    <head>' >> "$HTML"
            echo '        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />' >> "$HTML"
            echo '        <meta name="viewport" content="width=device-width, initial-scale=1" />' >> "$HTML"
            echo "        <title>$NAME :: SRD 5.1 de</title>" >> "$HTML"
            echo '        <link rel="stylesheet" href="'"$BASEURL"'/css/basic.css" />' >> "$HTML"
            echo '        <link rel="apple-touch-icon" sizes="180x180" href="'"$BASEURL"'/img/favicon/apple-touch-icon.png" />' >> "$HTML"
            echo '        <link rel="icon" type="image/png" sizes="32x32" href="'"$BASEURL"'/img/favicon/favicon-32x32.png" />' >> "$HTML"
            echo '        <link rel="icon" type="image/png" sizes="16x16" href="'"$BASEURL"'/img/favicon/favicon-16x16.png" />' >> "$HTML"
            echo '        <link rel="manifest" href="'"$BASEURL"'/img/favicon/site.webmanifest" />' >> "$HTML"
            echo '    </head>' >> "$HTML"
            echo '    <body>' >> "$HTML"
            echo '        <div id="copy" class="srd-51-de">' >> "$HTML"
            xmllint --xpath "//*[@id='$i']" $F | sed 's/^/            /' >> "$HTML"
            echo '        </div>' >> "$HTML"
            echo '    </body>' >> "$HTML"
            echo '</html>' >> "$HTML"
            
            if [[ -s $HTML ]]
            then
                echo -n "."
            else
                echo -n "0"
                ERROR=1
            fi

            #######################################################################
            # XML
            # using xmllint to do single xpath queries is extremely inefficient
            # maybe take some time to refactor XML generation
            XML="$WEB/zauberwirken/$i.xml"
                   
            echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' > "$XML"
            echo "<!--<!DOCTYPE spell SYSTEM '$BASEURL/srd-51-de.dtd'>-->" >> "$XML"
            echo "<spell>" >> "$XML"
            echo '    <WORK-IN-PROGRESS-WARNING>THIS IS A DEVELOPMENT VERSION. USE AT OWN RISK</WORK-IN-PROGRESS-WARNING>' >> "$XML"
            echo "    <id>$i</id>" >> "$XML"
            echo "    <src>`basename "$F"`</src>" >> "$XML"
            echo "    <name>$NAME</name>" >> "$XML"
            echo '    '`lint school-level-raw "//*[@id='$i']/*[@class='school'][1]/text()"` >> "$XML"
            echo '    '`lint school "substring-before(//*[@id='$i']/*[@class='school'][1]/text(), 'zauber ')" optional | perl -pe 's#(?<!Erkenntni)s(</school>)#$1#'` >> "$XML"
            echo '    '`lint school "substring-after(//*[@id='$i']/*[@class='school'][1]/text(), 'Zaubertrick der ')" optional` >> "$XML"
            echo '    '`lint school "substring-after(//*[@id='$i']/*[@class='school'][1]/text(), 'Zaubertrick des ')" optional` | sed 's#Banns#Bann#' >> "$XML"
            echo '    '`lint level "substring-before(substring-after(//*[@id='$i']/*[@class='school'][1]/text(), ' '), '.')" | sed 's#><#>0<#'` >> "$XML"
            echo '    '`lint time "substring-after(//*[@id='$i']/*[@class='time'][1]/text(), ' ')"` >> "$XML"
            echo '    '`lint range "substring-after(//*[@id='$i']/*[@class='range'][1]/text(), ' ')"` >> "$XML"
            echo '    '`lint components "substring-after(//*[@id='$i']/*[@class='components'][1]/text(), ' ')" | sed 's#M (.*\(</components>\)#M\1#'` >> "$XML"
            echo '    '`lint material-components "substring-before(substring-after(//*[@id='$i']/*[@class='components'][1]/text(), '('), ')')" optional` >> "$XML"
            echo '    '`lint duration "substring-after(//*[@id='$i']/*[@class='duration'][1]/text(), ' ')"` >> "$XML"
            echo '    '`lint concentration "substring-after(//*[@id='$i']/*[@class='duration'][1]/text(), 'Konzentration')" optional | sed 's#\(<concentration>\).*\(</concentration>\)#\1true\2#'` >> "$XML"
            echo '    '`lint ritual "substring-after(//*[@id='$i']/*[@class='school'][1]/text(), '(Ritual')" optional | sed 's#\(<ritual>\))\(</ritual>\)#\1true\2#'` >> "$XML"
            echo "    <description>" >> "$XML"
            echo "        <text>`$XMLLINT --xpath "//*[@id='$i']/*[@class='duration'][1]//following-sibling::p//text()" $HTML | perl -ne 's#\n+# #g; s#\s+# #g; print' 2>/dev/null`</text>" >> "$XML"
            echo "        <text-full>`$XMLLINT --xpath "//*[@id='$i']/*[@class='duration'][1]//following-sibling::*//text()" $HTML | perl -ne 's#\n+# #g; s#\s+# #g; print' 2>/dev/null`</text-full>" >> "$XML"
            echo "        <html>`$XMLLINT --xpath "//*[@id='$i']/*[@class='duration'][1]//following-sibling::*" $HTML 2>/dev/null | perl -ne 's#<#&lt;#g; s#>#&gt;#g; s#\n+# #g; s#\s+# #g; print'`</html>"  >> "$XML"
            echo "    </description>" >> "$XML"
            echo "</spell>" >> "$XML"

            # check validity of XML
            $XMLLINT --dtdvalid "$DIR/dtd/srd-51-de.dtd" --noblanks --noout "$XML"
            echo -n "."

            # pretty print XML
            XMLLINT_INDENT="    " $XMLLINT --format "$XML" > "$XML~"
            mv "$XML~" "$XML"

            if [[ -s $XML ]]
            then
                echo -n "."
            else
                echo -n "0"
                ERROR=1
            fi
            
            #######################################################################
            # JSON
            JSON="$WEB/zauberwirken/$i.json"
            
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/spell-to-json.xsl" "$XML" > "$JSON"
                echo -n "."
            else
                echo -n "x"
            fi
            
            # check validity of JSON
            if [[ -x $JSONLINT ]]
            then
                $JSONLINT "$JSON" > /dev/null
                echo -n "."
            else
                echo -n "x"
            fi
            
            
            # pretty print JSON
            if [[ -x $JQ ]]
            then
                $JQ . --indent 4 "$JSON" > "$JSON~"
                mv "$JSON~" "$JSON"
                if [[ -s $JSON ]]
                then
                    echo -n "."
                else
                    echo -n "0"
                    ERROR=1
                fi
            else
                echo -n "x"
            fi

            #######################################################################
            # MARKDOWN
            HOMEBREWERY="$WEB/zauberwirken/$i.homebrewery.md"
            GMBINDER="$WEB/zauberwirken/$i.gmbinder.md"
            
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/spell-to-homebrewery-markdown.xsl" "$XML" > "$HOMEBREWERY"
                $XSLTPROC "$DIR/xsl/spell-to-gmbinder-markdown.xsl" "$XML" > "$GMBINDER"
                echo -n "."
            else
                echo -n "x"
            fi

            #######################################################################
            # LATEX
            LATEX="$WEB/zauberwirken/$i.rpgtex.tex"
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/spell-to-rpgtex-latex.xsl" "$XML" > "$LATEX"
                echo -n "."
            else
                echo -n "x"
            fi

            #######################################################################

            if [[ $ERROR > 0 ]]
            then
                echo -e ' \u203C''ERROR''\u203C'
            else
                echo -e ' \u2713'
            fi
        fi
    done
done