<!ELEMENT compendium (bestiary | trove | spellbook)*>
    <!ELEMENT bestiary (monster+)>
    <!ELEMENT trove (magicitem+)>
    <!ELEMENT spellbook (spell+)>

    <!-- this element will be deleted at some point -->
    <!ELEMENT WORK-IN-PROGRESS-WARNING (#PCDATA)>

    <!ELEMENT monster (WORK-IN-PROGRESS-WARNING?, id, src, name, size-type-raw?, size, type, alignment, armor-class, hit-points, speeds, attributes, saving-throws?, skills?, damage-resistances?, damage-vulnerabilitys?, damage-immunitys?, condition-immunitys?, senses?, languages, challenge, xp, traits?, spellcasting-trait?, spellcasting-innate-trait?, actions?, legendary-actions?, bonus-actions?, reactions?)>

        <!ELEMENT id            (#PCDATA)>
        <!ELEMENT src           (#PCDATA)>
        <!ELEMENT name          (#PCDATA)>
        <!ELEMENT type          (#PCDATA)>
        <!ELEMENT size          (#PCDATA)>
        <!ELEMENT size-type-raw (#PCDATA)>
        <!ELEMENT alignment     (#PCDATA)>
    
        <!ELEMENT armor-class (raw?, value, info?)>
            <!ELEMENT raw       (#PCDATA)>
            <!ELEMENT value     (#PCDATA)>
            <!ELEMENT info      (#PCDATA)>
        
        <!ELEMENT hit-points (raw?, value, formula)>
            <!ELEMENT formula (#PCDATA)>
        
        <!ELEMENT speeds (raw?, walk, burrow?, climb?, fly?, swim?)>
            <!ELEMENT walk      (#PCDATA)>
            <!ELEMENT burrow    (#PCDATA)>
            <!ELEMENT climb     (#PCDATA)>
            <!ELEMENT fly       (#PCDATA)>
            <!ELEMENT swim      (#PCDATA)>
            
        <!ELEMENT attributes (attribute+)>
            <!ELEMENT attribute (class, value, modifier)>
                <!ELEMENT class     (#PCDATA)>
                <!ELEMENT modifier  (#PCDATA)>
        
        <!ELEMENT saving-throws (raw?, saving-throw+)>
            <!ELEMENT saving-throw (#PCDATA)>
        
        <!ELEMENT skills (raw?, skill+)>
            <!ELEMENT skill (#PCDATA)>
        
        <!ELEMENT damage-resistances (raw?, damage-resistance+)>
            <!ELEMENT damage-resistance (#PCDATA)>
        
        <!ELEMENT damage-vulnerabilitys (raw?, damage-vulnerability+)>
            <!ELEMENT damage-vulnerability (#PCDATA)>
        
        <!ELEMENT damage-immunitys (raw?, damage-immunity+)>
            <!ELEMENT damage-immunity (#PCDATA)>
            
        <!ELEMENT condition-immunitys (raw?, condition-immunity+)>
            <!ELEMENT condition-immunity (#PCDATA)>
            
        <!ELEMENT senses (raw?, sense+)>
            <!ELEMENT sense (#PCDATA)>
            
        <!ELEMENT languages (raw?, language+)>
            <!ELEMENT language (#PCDATA)>
        
        <!ELEMENT challenge  (#PCDATA)>
        <!ELEMENT xp         (#PCDATA)>
        
        <!ELEMENT traits (trait*)>
            <!ELEMENT trait (name, value)>
        
        <!ELEMENT spellcasting-trait (spellcasting-text, spells)>
            <!ELEMENT spellcasting-text (#PCDATA)>
            <!ELEMENT spells (spell-list+, spellcasting-note?)>
                <!ELEMENT spell-list ((level | frequency), slots?, spell-reference+)>
                    <!ELEMENT level             (#PCDATA)>
                    <!ELEMENT frequency         (#PCDATA)>
                    <!ELEMENT slots             (#PCDATA)>
                    <!ELEMENT spell-reference   (id, name)>
            <!ELEMENT spellcasting-note (#PCDATA)>
    
        <!ELEMENT spellcasting-innate-trait (spellcasting-text, spells)>
        
        <!ELEMENT actions (action+)>
            <!ELEMENT action (name, value)>
            
        <!ELEMENT legendary-actions (legendary-text?, legendary-action+)>
            <!ELEMENT legendary-text    (#PCDATA)>
            <!ELEMENT legendary-action  (name, value)>
            
        <!ELEMENT bonus-actions (bonus-action+)>
            <!ELEMENT bonus-action (name, value)>
        
        <!ELEMENT reactions (reaction+)>
            <!ELEMENT reaction (name, value)>

    
    <!ELEMENT magicitem (WORK-IN-PROGRESS-WARNING?, id, src, name, type, rarity, attunement?, description)>
    
        <!ELEMENT rarity (#PCDATA)>
        <!ELEMENT attunement (#PCDATA)>
        <!ELEMENT description (text|text-full|html)+>
            <!ELEMENT text      (#PCDATA)>
            <!ELEMENT text-full (#PCDATA)>
            <!ELEMENT html      (#PCDATA)>
        
    <!ELEMENT spell (WORK-IN-PROGRESS-WARNING?, id, src, name, school-level-raw?, school, level, time, range, components, material-components?, duration, concentration?, ritual?, description, description-higher-level?)>
    
        <!ELEMENT school-level-raw      (#PCDATA)>
        <!ELEMENT school                (#PCDATA)>
        <!ELEMENT time                  (#PCDATA)>
        <!ELEMENT range                 (#PCDATA)>
        <!ELEMENT components            (#PCDATA)>
        <!ELEMENT material-components   (#PCDATA)>
        <!ELEMENT duration              (#PCDATA)>
        <!ELEMENT concentration         (#PCDATA)>
        <!ELEMENT ritual                (#PCDATA)>
        
        <!ELEMENT description-higher-level (text|text-full|html)+>    