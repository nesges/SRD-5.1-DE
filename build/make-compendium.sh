#!/bin/bash

#######################################################################
# CONFIGURATION
#######################################################################

XMLLINT=/usr/bin/xmllint
BASEURL="https://openrpg.de/srd/5e/de"

#######################################################################
# PROGRAM
#######################################################################

DIR=`dirname $0`;
WEB=`readlink -f "$DIR/../web"`

#######################################################################
# Homebrewery Markdown

if [[ ! $1 ]] || [[ $1 == "homebrewery.md" ]]
then
    C="$WEB/compendium.homebrewery.md"
    >$C
    
    function pagebreak {
        if (( $PAGEBREAK < 1 ))
        then
            echo "{{pageNumber,auto}}" >> $C
            echo "{{footnote SRD 5.1 **de**}}" >> $C
            echo "\page" >> $C
            echo "--"
            NO=0
            GRAVITY=0
            PAGEBREAK=1
        fi
    }
    
    echo "::::" >> $C
    echo "<div class='wide'><center>" >> $C
    echo "" >> $C
    echo "# Kompendium 5e " >> $C
    echo "##### SRD 5.1 de &middot; openrpg.de &middot; dnddeutsch.de" >> $C
    echo ":" >> $C
    echo "Wertebl&ouml;cke f&uuml;r Monster, Kreaturen, Nichtspielercharaktere, Magische Gegenst&auml;nde und Zauberspr&uuml;che aus dem SRD 5.1 **de**, fertig formatiert f&uuml;r Homebrewery." >> $C
    echo "" >> $C
    echo "</center></div>" >> $C
    echo "::::::::::" >> $C
    
    
    echo "" >> $C
    echo "<div class='wide''><center>" >> $C
    echo "" >> $C
    echo "## Bestiarium" >> $C
    echo "" >> $C
    echo "Monster, Kreaturen und Nichtspielercharaktere" >> $C
    echo "" >> $C
    echo "</div></center>" >> $C
    echo "::::" >> $C
    echo "" >> $C
    echo "<div class='wide''></div>" >> $C
    echo "" >> $C

    
    PAGEBREAK=1
    NO=0
    GRAVITY=0
    
    for MD in "$WEB"/monster/monster-*.homebrewery.md
    do
        SIZE=`stat --format=%s $MD`
        (( GRAVITY=$GRAVITY+$SIZE ))
    
        echo "$MD $SIZE $GRAVITY"
    
        if (( $GRAVITY > 3000 )) || (( $NO == 2 )) || [[ $MD = "monster-plapperndes-hundertmaul.homebrewery.md" ]]
        then
            pagebreak
        fi
        
        if (( $SIZE > 2500 ))
        then
            cat $MD | sed 's/\({{monster,frame\)/\1,wide/' >> $C
            PAGEBREAK=0
            pagebreak
        else
            cat $MD >> $C
            PAGEBREAK=0
            ((NO=$NO+1))
        fi
    
        echo "" >> $C
        echo "" >> $C
    done

    PAGEBREAK=0
    GRAVITY=0
    pagebreak
    
    echo "<div class='wide''><center>" >> $C
    echo "" >> $C
    echo "## Schatzkammer" >> $C
    echo "" >> $C
    echo "Magische Gegenst&auml;nde" >> $C
    echo "" >> $C
    echo "</div></center>" >> $C
    echo "::::" >> $C
    echo "" >> $C
    echo "<div class='wide''></div>" >> $C
    echo "" >> $C
        
    for MD in "$WEB"/magicitem/magicitem-*.homebrewery.md
    do
        SIZE=`stat --format=%s $MD`
        (( GRAVITY=$GRAVITY+$SIZE ))
    
        echo "$MD $SIZE $GRAVITY"
    
        if (( $GRAVITY > 3000 ))
        then
            pagebreak
        fi
        
        cat $MD >> $C
        echo "::" >> $C
        PAGEBREAK=0
        
        if (( $SIZE > 2500 ))
        then
            pagebreak
        fi
        
        echo "" >> $C
        echo "" >> $C
    done

    PAGEBREAK=0
    GRAVITY=0
    pagebreak
    
    echo "<div class='wide''><center>" >> $C
    echo "" >> $C
    echo "## Zauberbuch" >> $C
    echo "" >> $C
    echo "Zauberspr&uuml;che" >> $C
    echo "" >> $C
    echo "</div></center>" >> $C
    echo "::::" >> $C
    echo "" >> $C
    echo "<div class='wide''></div>" >> $C
    echo "" >> $C


    for MD in "$WEB"/spell/spell-*.homebrewery.md
    do
        SIZE=`stat --format=%s $MD`
        (( GRAVITY=$GRAVITY+$SIZE ))
    
        echo "$MD $SIZE $GRAVITY"
    
        if (( $GRAVITY > 3750 ))
        then
            pagebreak
        fi
        
        cat $MD >> $C
        echo "::" >> $C
        PAGEBREAK=0
        
        if (( $SIZE > 3000 ))
        then
            pagebreak
        fi
        
        echo "" >> $C
        echo "" >> $C
    done

    PAGEBREAK=0
    pagebreak

    echo "<div class='wide' style='position:absolute;bottom:40px;width:100%;left:0'><center>" >> $C
    echo "" >> $C
    echo "**Kompendium 5e**<br>" >> $C
    echo "Inhalte lizensiert unter [CC-BY-4.0](https://openrpg.de/license/cc/by/4.0/de/)<br>" >> $C
    echo "`date +%Y-%d-%m` [SRD 5.1 **de**](https://codeberg.org/nesges/SRD-5.1-DE) &middot; [D3](https://dnddeutsch.de) &middot; [OpenRPG.de](https://openrpg.de)" >> $C
    echo "</center></div>" >> $C
    echo "" >> $C
    echo '<img src="https://openrpg.de/img/openrpg.de.title.svg" style="position:absolute; bottom:55px; left:40px; height:50px;">' >> $C
    echo '<img src="https://openrpg.de/img/OpenD3-logo.svg" style="position:absolute; bottom:55px; right:40px; height:50px;">' >> $C
    echo "" >> $C
    echo "{{pageNumber,auto}}" >> $C
    
    echo ""
    echo -e 'Homebrewery: \u2713'
    echo ""
    
    OK="$OK\nHomebrewery: \u2713"
fi

#######################################################################
# XML

if [[ ! $1 ]] || [[ $1 == "xml" ]]
then
    C="$WEB/compendium.xml"
    
    echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' > $C
    echo "<!--<!DOCTYPE compendium SYSTEM '$BASEURL/srd-51-de.dtd'>-->" >> $C
    echo '<compendium>' >> $C
    echo '<bestiary>' >> $C
    
    for XML in "$WEB"/monster/monster-*.xml
    do
        echo "$XML"
        tail -n +2 $XML | grep -v '<!--' | grep -v '<WORK-IN-PROGRESS-WARNING>' | grep -v '<!DOCTYPE' >> $C
    done
    
    echo '</bestiary>' >> $C
    echo '<trove>' >> $C
    
    for XML in "$WEB"/magicitem/magicitem-*.xml
    do
        echo "$XML"
        tail -n +2 $XML | grep -v '<!--' | grep -v '<WORK-IN-PROGRESS-WARNING>' | grep -v '<!DOCTYPE' >> $C
    done
    
    echo '</trove>' >> $C
    echo '<spellbook>' >> $C
    
    for XML in "$WEB"/spell/spell-*.xml
    do
        echo "$XML"
        tail -n +2 $XML | grep -v '<!--' | grep -v '<WORK-IN-PROGRESS-WARNING>' | grep -v '<!DOCTYPE' >> $C
    done
    
    echo '</spellbook>' >> $C
    echo '</compendium>' >> $C
    
    # pretty print XML
    XMLLINT_INDENT="    " $XMLLINT --format $C > $C~
    mv "$C~" "$C"
    
    # check validity of XML
    $XMLLINT --dtdvalid "$DIR/dtd/srd-51-de.dtd" --noblanks --noout $C
    
    echo ""
    echo -e 'XML: \u2713'
    echo ""
    
    OK="$OK\nXML: \u2713"
fi


#######################################################################
# TEX

if [[ ! $1 ]] || [[ $1 == "tex" ]]
then
    C="$WEB/compendium.rpgtex.tex"
    cat "$DIR/parts/pre.rpgtex.tex" > $C;
    
    for TEX in "$WEB"/monster/monster-*.rpgtex.tex
    do
        echo "$TEX"
        echo "% "`basename "$TEX"` >> "$C"
        echo "" >> "$C"
        cat "$TEX" >> "$C"
    done

    for TEX in "$WEB"/magicitem/magicitem-*.rpgtex.tex
    do
        echo "$TEX"
        echo "% "`basename "$TEX"` >> "$C"
        echo "" >> "$C"
        cat "$TEX" >> "$C"
    done

    for TEX in "$WEB"/spell/spell-*.rpgtex.tex
    do
        echo "$TEX"
        echo "% "`basename "$TEX"` >> "$C"
        echo "" >> "$C"
        cat "$TEX" >> "$C"
    done
    
    cat "$DIR/parts/post.rpgtex.tex" >> $C;
    
    echo ""
    echo -e 'TeX: \u2713'
    echo ""
    OK="$OK\nTeX: \u2713"
fi

if [[ ! $1 ]]
then
    echo -e $OK
fi