#/bin/bash

. `dirname $0`/env.sh

function lint {
    # tag xpath [optional]
    # uses global $HTML
    CONTENT=`$XMLLINT --xpath "$2" $HTML 2> /dev/null`
    if [[ $CONTENT ]] || [[ "$3" != "optional" ]]
    then
        echo "<$1>$CONTENT</$1>"
    fi
}

for F in "$WEB/magische-gegenstaende/a-z.html" "$WEB/magische-gegenstaende/intelligente-magische-gegenstaende.html"
do 
    if [[ $1 ]]
    then
        ITEMS=$1
    else
        ITEMS=`grep '<div class="magicitem" id="' $F | sed -e 's/\r/\n/' | sed -e 's/<.*id="\(.*\)">/\1/'`
    fi

    for i in $ITEMS
    do 
        ERROR=0
        NAME=`$XMLLINT --xpath "//*[@id='$i']/h5[1]/text()" $F  2>/dev/null`
        
        if [[ $NAME ]]
        then
            echo -n "$NAME "
            
            #######################################################################
            # HTML
            HTML="$WEB/magische-gegenstaende/$i.html"
            
            echo '<!DOCTYPE html>' > "$HTML"
            echo '<html lang="de">' >> "$HTML"
            echo '    <head>' >> "$HTML"
            echo '        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />' >> "$HTML"
            echo '        <meta name="viewport" content="width=device-width, initial-scale=1" />' >> "$HTML"
            echo "        <title>$NAME :: SRD 5.1 de</title>" >> "$HTML"
            echo '        <link rel="stylesheet" href="'"$BASEURL"'/css/basic.css" />' >> "$HTML"
            echo '        <link rel="apple-touch-icon" sizes="180x180" href="'"$BASEURL"'/img/favicon/apple-touch-icon.png" />' >> "$HTML"
            echo '        <link rel="icon" type="image/png" sizes="32x32" href="'"$BASEURL"'/img/favicon/favicon-32x32.png" />' >> "$HTML"
            echo '        <link rel="icon" type="image/png" sizes="16x16" href="'"$BASEURL"'/img/favicon/favicon-16x16.png" />' >> "$HTML"
            echo '        <link rel="manifest" href="'"$BASEURL"'/img/favicon/site.webmanifest" />' >> "$HTML"
            echo '    </head>' >> "$HTML"
            echo '    <body>' >> "$HTML"
            echo '        <div id="copy" class="srd-51-de">' >> "$HTML"
            $XMLLINT --xpath "//*[@id='$i']" $F  2>/dev/null | sed 's/^/            /' >> "$HTML"
            echo '        </div>' >> "$HTML"
            echo '    </body>' >> "$HTML"
            echo '</html>' >> "$HTML"
            
            if [[ -s $HTML ]]
            then
                echo -n "."
            else
                echo -n "0"
                ERROR=1
            fi
            
            #######################################################################
            # XML
            # using xmllint to do single xpath queries is extremely inefficient
            # maybe take some time to refactor XML generation
            XML="$WEB/magische-gegenstaende/$i.xml"
                   
            echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' > "$XML"
            echo "<!--<!DOCTYPE magicitem SYSTEM '$BASEURL/srd-51-de.dtd'>-->" >> "$XML"
            echo "<magicitem>" >> "$XML"
            echo '    <WORK-IN-PROGRESS-WARNING>THIS IS A DEVELOPMENT VERSION. USE AT OWN RISK</WORK-IN-PROGRESS-WARNING>' >> "$XML"
            echo "    <id>$i</id>" >> "$XML"
            echo "    <src>`basename "$F"`</src>" >> "$XML"
            echo "    <name>$NAME</name>" >> "$XML"
            echo '    '`lint type "substring-before(//*[@id='$i']/*[@class='magicitem-type'][1]/em/text(), ',')"` >> "$XML"
            echo '    '`lint rarity "substring-after(//*[@id='$i']/*[@class='magicitem-type'][1]/em/text(), ', ')" | perl -ne 's#\s+\(erfordert.*?\)##; print'` >> "$XML"
            echo '    '`lint attunement "substring-before(substring-after(//*[@id='$i']/*[@class='magicitem-type'][1]/em/text(), '('), ')')"` >> "$XML"
            echo "    <description>" >> "$XML"
            echo "        <text>`$XMLLINT --xpath "//*[@id='$i']/*[@class='magicitem-type'][1]//following-sibling::p//text()" $HTML | perl -ne 's#\n+# #g; s#\s+# #g; print' 2>/dev/null`</text>" >> "$XML"
            echo "        <text-full>`$XMLLINT --xpath "//*[@id='$i']/*[@class='magicitem-type'][1]//following-sibling::*//text()" $HTML | perl -ne 's#\n+# #g; s#\s+# #g; print' 2>/dev/null`</text-full>" >> "$XML"
            echo "        <html>`$XMLLINT --xpath "//*[@id='$i']/*[@class='magicitem-type'][1]//following-sibling::*" $HTML 2>/dev/null | perl -ne 's#<#&lt;#g; s#>#&gt;#g; s#\n+# #g; s#\s+# #g; print'`</html>"  >> "$XML"
            echo "    </description>" >> "$XML"
            echo "</magicitem>" >> "$XML"
            
            echo -n '.'
            
            # check validity of XML
            $XMLLINT --dtdvalid "$DIR/dtd/srd-51-de.dtd" --noblanks --noout "$XML"
            echo -n "."

            # pretty print XML
            XMLLINT_INDENT="    " $XMLLINT --format "$XML" > "$XML~"
            mv "$XML~" "$XML"
            
            if [[ -s $XML ]]
            then
                echo -n "."
            else
                echo -n "0"
            fi


            #######################################################################
            # JSON
            JSON="$WEB/magische-gegenstaende/$i.json"
            
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/magicitem-to-json.xsl" "$XML" > "$JSON"
                echo -n "."
            else
                echo -n "x"
            fi
            
            # check validity of JSON
            if [[ -x $JSONLINT ]]
            then
                $JSONLINT "$JSON" > /dev/null
                echo -n "."
            else
                echo -n "x"
            fi
            
            
            # pretty print JSON
            if [[ -x $JQ ]]
            then
                $JQ . --indent 4 "$JSON" > "$JSON~"
                mv "$JSON~" "$JSON"
                if [[ -s $JSON ]]
                then
                    echo -n "."
                else
                    echo -n "0"
                    ERROR=1
                fi
            else
                echo -n "x"
            fi

            #######################################################################
            # MARKDOWN
            HOMEBREWERY="$WEB/magische-gegenstaende/$i.homebrewery.md"
            GMBINDER="$WEB/magische-gegenstaende/$i.gmbinder.md"
            
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/magicitem-to-homebrewery-markdown.xsl" "$XML" > "$HOMEBREWERY"
                $XSLTPROC "$DIR/xsl/magicitem-to-gmbinder-markdown.xsl" "$XML" > "$GMBINDER"
                echo -n "."
            else
                echo -n "x"
            fi

            #######################################################################
            # LATEX
            LATEX="$WEB/magische-gegenstaende/$i.rpgtex.tex"
            if [[ -x $XSLTPROC ]]
            then
                $XSLTPROC "$DIR/xsl/magicitem-to-rpgtex-latex.xsl" "$XML" > "$LATEX"
                echo -n "."
            else
                echo -n "x"
            fi

            #######################################################################

            if [[ $ERROR > 0 ]]
            then
                echo -e ' \u203C''ERROR''\u203C'
            else
                echo -e ' \u2713'
            fi

        fi
    done
done