#!/usr/bin/php
<?
    // parses ocred copy of monster text
    $lastline=10147;
    $lines = explode("\n", file_get_contents('../html/SRD_CC_v5.1_DE.html'));
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        global $monster;
        print "\n\nMONSTER:\n\n";
        print_r($monster);
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });
     
    // Aboleth starts at line 10145
    for($line=$lastline; $line<count($lines); $line++) {
        if(preg_match('#<h5>(.*?)</h5>#', $lines[$line], $matches)) {
            $lines[$line]='';
            if($matches[1] == 'Aktionen') {
                $traittype = 'action';
            } else if($matches[1] == 'Bonusaktionen') {
                $traittype = 'bonus';
            } else if($matches[1] == 'Legendäre Aktionen') {
                $traittype = 'legendary';
                $monster->legendary_text = preg_replace('#</?p>#', '', $lines[$line+1]);
                $lines[++$line]='';
            } else {
                if(isset($monster)) {
                    for($l=$lastline; $l<$line; $l++) {
                        if(trim($lines[$l])) {
                            print $l." ".$lines[$l]."\n";
                        }
                    }
                    $lastline = $line;
                    dump_monster($monster);
                    
                    print "\n\n";
                    readline("next (start at line $line)");
                    print "\n\n";
                }
                // new monster
                $monster = new stdClass();
                $monster->name = $matches[1];
                $traittype = 'trait';
            }
        } else if(preg_match('#>((winzig|klein|mittelgroß|groß|riesig|gigantisch).*?),\s(.*?)<#i', $lines[$line], $matches)) {
            $lines[$line]='';
            $monster->type = $matches[1];
            $monster->alignment = $matches[3];
        } else if(preg_match('#>STR\s+GES\s+KON\s+INT\s+WEI\s+CHA<#i', $lines[$line], $matches)) {
            $lines[$line]='';
        } else if(preg_match('#>(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s*)<#i', $lines[$line], $matches)) {
            $lines[$line]='';
            $monster->attr['str'] = $matches[1];
            $monster->attr['ges'] = $matches[2];
            $monster->attr['kon'] = $matches[3];
            $monster->attr['int'] = $matches[4];
            $monster->attr['wei'] = $matches[5];
            $monster->attr['cha'] = $matches[6];
        } else if(preg_match('#>(Rüstungsklasse|Trefferpunkte|Bewegungsrate|Schadensresistenzen|Zustandsimmunitäten|Schadensanfälligkeiten|Rettungswürfe|Fertigkeiten|Sinne|Sprachen|Herausforderungsgrad)(\s*</span>)?\s*(.*?)<#', $lines[$line], $matches)) {
            $lines[$line]='';
            $monster->stat[$matches[1]] = $matches[3];
        } else if(preg_match('#>(.*?):(\s*</span>)?\s+(.*)<#', $lines[$line], $matches)) {
            $lines[$line]='';
            $matches[3] = strip_tags($matches[3]);
            $matches[3] = preg_replace('#(Nahkampfwaffenangriff|Fernkampfwaffenangriff):</span>#', '$1:', $matches[3]);
            $monster->trait[$traittype][strip_tags($matches[1])] = $matches[3];
        }
    }
    
    dump_monster($monster);
    
    function dump_monster($monster) {        
?>
<div class="monster" id="monster-<?= iconv("utf-8", "ascii//TRANSLIT", preg_replace('#\s+#', '-', strtolower($monster->name))) ?>">
    <h5><?= $monster->name ?></h5>
    <div class="type"><?= $monster->type ?></div>, <div class="alignment"><?= $monster->alignment ?></div>
    <div class="stat_name">Rüstungsklasse</div> <div class="stat_value"><?= $monster->stat['Rüstungsklasse'] ?></div>
    <div class="stat_name">Trefferpunkte</div> <div class="stat_value"><?= $monster->stat['Trefferpunkte'] ?></div>
    <div class="stat_name">Bewegungsrate</div> <div class="stat_value"><?= $monster->stat['Bewegungsrate'] ?></div>
    <div class="attributes">
        <div class="a">STR</div>
        <div class="a">GES</div>
        <div class="a">KON</div>
        <div class="a">INT</div>
        <div class="a">WEI</div>
        <div class="a">CHA</div>
        <div class="v"><?= $monster->attr['str'] ?></div>
        <div class="v"><?= $monster->attr['ges'] ?></div>
        <div class="v"><?= $monster->attr['kon'] ?></div>
        <div class="v"><?= $monster->attr['int'] ?></div>
        <div class="v"><?= $monster->attr['wei'] ?></div>
        <div class="v"><?= $monster->attr['cha'] ?></div>
    </div><?
        print "\n";

        foreach($monster->stat as $stat_name => $stat_value) {
            if(!in_array($stat_name, ['Rüstungsklasse', 'Trefferpunkte', 'Bewegungsrate'])) { 
                print '    <div class="stat_name">'.$stat_name.'</div> <div class="stat_value">'.$stat_value.'</div>'."\n";
            }
        }
    
        if(isset($monster->trait['trait']) && count($monster->trait['trait'])) {
            print "    <hr>\n";
            foreach($monster->trait['trait'] as $name => $value) {
                print '    <div class="trait_name">'.$name.'</div> <div class="trait_value">'.$value.'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['action']) && count($monster->trait['action'])) {
            print "    <hr>\n";
            print "    <h4>Aktionen</h4>\n";
            foreach($monster->trait['action'] as $name => $value) {
                print '    <div class="action_name">'.$name.'</div> <div class="action_value">'.$value.'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['bonus']) && count($monster->trait['bonus'])) {
            print "    <hr>\n";
            print "    <h4>Bonusaktionen</h4>\n";
            foreach($monster->trait['bonus'] as $name => $value) {
                print '    <div class="action_name">'.$name.'</div> <div class="action_value">'.$value.'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['legendary']) && count($monster->trait['legendary'])) {
            print "    <hr>\n";
            print "    <h4>Legendäre Aktionen</h4>\n";
            print "    <div class='legendary-text'>".$monster->legendary_text."</div>\n";
            foreach($monster->trait['legendary'] as $name => $value) {
                print '    <div class="action_name">'.$name.'</div> <div class="action_value">'.$value.'</div>'."\n";
            }
        }
    ?>
</div>

<?
    }

    foreach($lines as $line) {
        if(trim($line)) {
            print $line."\n";
        }
    }
?>