#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });

    $content = file_get_contents($argv[1]);

    $out = preg_replace_callback('#<h5>(.*)</h5>#', function($matches) { 
            return "</div>\n\n".'<div class="magicitem" id="'.srd_id('magicitem', $matches[1]).'">'."\n".'<h5>'.$matches[1].'</h5>'; 
        }, $content);
    
    $out = preg_replace('#</h5>\s*<p><span class="nameref">(.*?)</span></p>#s', '</h5>'."\n".'<p class="magicitem-type"><em>$1</em></p>', $out);

    print $out;
?>