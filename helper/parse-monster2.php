#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    // parses plaintext copy of monster text
    $lastline=1;
    $lines = explode("\n", file_get_contents($argv[1]));
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        global $monster;
        print "\n\nMONSTER:\n\n";
        print_r($monster);
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });
     
    // Aboleth starts at line 10145
    for($line=$lastline; $line<count($lines); $line++) {
        $lines[$line] = trim($lines[$line]);
        if($lines[$line] == 'Aktionen') {
            $traittype = 'action';
            $lines[$line]='';
        } else if($lines[$line] == 'Reaktionen') {
            $traittype = 'reaction';
            $lines[$line]='';
        } else if($lines[$line] == 'Bonusaktionen') {
            $traittype = 'bonus';
            $lines[$line]='';
        } else if($lines[$line] == 'Legendäre Aktionen') {
            $traittype = 'legendary';
            $monster->legendary_text = trim(preg_replace('#</?p>#', '', $lines[$line+1]));
            $lines[$line]='';
            $lines[++$line]='';
        } else if(preg_match('#^((winzig|klein|mittelgroß|groß|riesig|gigantisch).*?),\s(.*)#i', $lines[$line], $matches)) {
            if(isset($monster)) {
                dump_monster($monster);
                print "\n\n";
                for($l=$lastline; $l<$line-2; $l++) {
                    if(trim($lines[$l])) {
                        if(preg_match('#^<h#', $lines[$l])) {
                            print $lines[$l]."\n";
                        } else {
                            print "<p>".$lines[$l]."</p>\n";
                        }
                    }
                }
                $lastline = $line-2;

                if(isset($argv[2]) && $argv[2]=='--ask') {
                    print "\n\n";
                    $next = readline("next (start at line ".($line-2).")");
                    if(trim($next) == 'q') {
                        exit();
                    }
                }
                print "\n\n";
            }
            // new monster
            $monster = new stdClass();
            $lasttrait = '';
            $lasttrait_line = 0;
            $traittype = 'trait';
            
            $monster->name = trim($lines[$line-1]);
            $monster->type = trim($matches[1]);
            $monster->alignment = trim($matches[3]);
            $lines[$line-1]='';
            $lines[$line]='';
        } else if(preg_match('#^STR\s+GES\s+KON\s+INT\s+WEI\s+CHA\s*$#', $lines[$line], $matches)) {
            $lines[$line]='';
            if(preg_match('#^(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s+)(\d+\s\([+-]\d+\)\s*)$#i', $lines[$line+1], $matches)) {
                $lines[++$line]='';
                $monster->attr['str'] = trim($matches[1]);
                $monster->attr['ges'] = trim($matches[2]);
                $monster->attr['kon'] = trim($matches[3]);
                $monster->attr['int'] = trim($matches[4]);
                $monster->attr['wei'] = trim($matches[5]);
                $monster->attr['cha'] = trim($matches[6]);
            }
        } else if(preg_match('#^(Rüstungsklasse|Trefferpunkte|Bewegungsrate|Schadensresistenzen|Schadensimmunitäten|Zustandsimmunitäten|Schadensanfälligkeiten|Rettungswürfe|Fertigkeiten|Sinne|Sprachen|Herausforderungsgrad)\s+(.*)#', $lines[$line], $matches)) {
            $lines[$line]='';
            $monster->stat[trim($matches[1])] = trim($matches[2]);
        } else if(preg_match('#^(.*?):\s+(.*)$#', $lines[$line], $matches)) {
            $lines[$line]='';
            $matches[2] = strip_tags($matches[2]);
            $matches[2] = preg_replace('#(Nahkampfwaffenangriff|Fernkampfwaffenangriff|Nah- oder Fernkampfwaffenangriff):#', '$1:', $matches[2]);
            $lasttrait = trim(strip_tags($matches[1]));
            $lasttrait_line = $line;
            $monster->trait[$traittype][$lasttrait][] = trim($matches[2]);
        } else if(preg_match('#^Monster \(.*#', $lines[$line], $matches)) {
            $lines[$line]='<h3>'.$lines[$line].'</h3>';
        } else {
            if($lines[$line]) {
                if($lasttrait_line==$line-1) {
                    $monster->trait[$traittype][$lasttrait][] = $lines[$line];
                    $lines[$line]='';
                }
            }
        }
    }
    
    dump_monster($monster);
    
        function dump_monster($monster) {        
?>
<div class="monster" id="<?= srd_id('monster', $monster->name) ?>">
    <h5><?= $monster->name ?></h5>
    <div class="type"><?= $monster->type ?></div>, <div class="alignment"><?= $monster->alignment ?></div>
    <div class="stat_name">Rüstungsklasse</div> <div class="stat_value"><?= $monster->stat['Rüstungsklasse'] ?></div>
    <div class="stat_name">Trefferpunkte</div> <div class="stat_value"><?= $monster->stat['Trefferpunkte'] ?></div>
    <div class="stat_name">Bewegungsrate</div> <div class="stat_value"><?= $monster->stat['Bewegungsrate'] ?></div>
    <div class="attributes">
        <div class="a">STR</div>
        <div class="a">GES</div>
        <div class="a">KON</div>
        <div class="a">INT</div>
        <div class="a">WEI</div>
        <div class="a">CHA</div>
        <div class="v"><?= $monster->attr['str'] ?></div>
        <div class="v"><?= $monster->attr['ges'] ?></div>
        <div class="v"><?= $monster->attr['kon'] ?></div>
        <div class="v"><?= $monster->attr['int'] ?></div>
        <div class="v"><?= $monster->attr['wei'] ?></div>
        <div class="v"><?= $monster->attr['cha'] ?></div>
    </div><?
        print "\n";

        foreach($monster->stat as $stat_name => $stat_value) {
            if(!in_array($stat_name, ['Rüstungsklasse', 'Trefferpunkte', 'Bewegungsrate'])) { 
                print '    <div class="stat_name">'.$stat_name.'</div> <div class="stat_value">'.$stat_value.'</div>'."\n";
            }
        }
    
        if(isset($monster->trait['trait']) && count($monster->trait['trait'])) {
            print "    <hr>\n";
            foreach($monster->trait['trait'] as $name => $value) {
                print '    <div class="trait_name">'.$name.'</div> <div class="trait_value">'.join(' <br>', $value).'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['action']) && count($monster->trait['action'])) {
            print "    <hr>\n";
            print "    <h4>Aktionen</h4>\n";
            foreach($monster->trait['action'] as $name => $value) {
                print '    <div class="action_name">'.$name.'</div> <div class="action_value">'.join(' <br>', $value).'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['bonus']) && count($monster->trait['bonus'])) {
            print "    <hr>\n";
            print "    <h4>Bonusaktionen</h4>\n";
            foreach($monster->trait['bonus'] as $name => $value) {
                print '    <div class="action_name">'.$name.'</div> <div class="action_value">'.join(' <br>', $value).'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['reaction']) && count($monster->trait['reaction'])) {
            print "    <hr>\n";
            print "    <h4>Reaktionen</h4>\n";
            foreach($monster->trait['reaction'] as $name => $value) {
                print '    <div class="reaction_name">'.$name.'</div> <div class="reaction_value">'.join(' <br>', $value).'</div>'."\n";
            }
        }
        
        if(isset($monster->trait['legendary']) && count($monster->trait['legendary'])) {
            print "    <hr>\n";
            print "    <h4>Legendäre Aktionen</h4>\n";
            print "    <div class='legendary-text'>".$monster->legendary_text."</div>\n";
            foreach($monster->trait['legendary'] as $name => $value) {
                print '    <div class="action_name">'.$name.'</div> <div class="action_value">'.join(' <br>', $value).'</div>'."\n";
            }
        }
    ?>
</div>

<?
    }
?>