#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        //die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });

    $lines = explode("\n", file_get_contents($argv[1]));
    
    $links = explode("\n", file_get_contents("../html/spelllists-links.html"));
    foreach($links as $link) {
        if(preg_match('@<a class="spell" href="(#spell-.*?)">(.*?)</a>@', $link, $matches)) {
            $spellbook[$matches[2]] = $link;
        }
    }

    for($l=0; $l<count($lines); $l++) {
        if(preg_match('@<div class="trait_name">(Zaubertricks|[1-9]\. Grad).*?</div>\s*<div class="trait_value">(.*?)</div>@', $lines[$l], $matches)) {
            $spells = explode(', ', $matches[2]);
            foreach($spells as $spell) {
                $lines[$l] = preg_replace('#'.$spell.'#', $spellbook[$spell], $lines[$l]);
            }
        }
        
        if(preg_match('@<div class="innate_spell_list">(.*?)</div>@', $lines[$l], $matches)) {
            $spells = explode(', ', $matches[1]);
            foreach($spells as $spell) {
                $lines[$l] = preg_replace('#'.$spell.'#', $spellbook[$spell], $lines[$l]);
            }
        }
        
        print $lines[$l]."\n";
    }
?>