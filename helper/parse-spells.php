#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });

    $content = file_get_contents($argv[1]);

    if(preg_match('/spells.html/', $argv[1])) {
        print preg_replace_callback('#<h5>(.*)</h5>#', function($matches) { 
            return "</div>\n\n".'<div class="spell" id="'.srd_id('spell', $matches[1]).'">'."\n".'<h5>'.$matches[1].'</h5>'; 
        }, $content);
    } else if(preg_match('/spelllists.html/', $argv[1])) {
        $out = preg_replace_callback('#<p>(.*)</p>#', function($matches) { 
            return '<li><a href="#'.srd_id('spell', $matches[1]).'">'.$matches[1]."</a></li>";
        }, $content);
        $out = preg_replace('#(<h5>.*?</h5>)\s*#s', "</ul>\n\n$1\n<ul>\n", $out);
        $out = preg_replace('#</li>\s+</ul>#s', "</li>\n</ul>", $out);
        print $out;
    }
?>