#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });

    $lines = explode("\n", file_get_contents($argv[1]));

    $out = "";
    for($l=0; $l<count($lines); $l++) {
        if(preg_match('#<h([2-3]) id="(.*?)">(.*?)</h[2-3]>#', $lines[$l], $matches)) {
            if($matches[1]==2 || preg_match('/^monster-/', $matches[2])) {
                if($out) {
                    $out .= "</ul>\n</li>\n\n";
                }
            }
            $out .= '<li class="h'.$matches[1].'"><a href="#'.$matches[2].'">'.$matches[3].'</a>';
            if($matches[1]==2 || preg_match('/^monster-/', $matches[2])) {
                $out .= "\n<ul>\n";
            } else {
                $out .= '</li>'."\n";
            }
        } else if(preg_match('#<div class="(monster|spell|magicitem)" id="(.*?)">#', $lines[$l], $matches)) {
            if(preg_match('#<h5>(.*?)</h5>#', $lines[++$l], $matches2)) {
                $out .= '   <li class="'.$matches[1].'"><a href="#'.$matches[2].'">'.$matches2[1]."</a></li>\n";
            }
        }
    }
    
    $out = preg_replace('#<ul>\s*</ul>#', '', $out);

    $out = preg_replace('@(<li class="h3"><a href="#beschreibungen-der-zauber">.*?</a>)</li>@', "$1\n<ul>", $out);
    $out = preg_replace('@(<li class="h3"><a href="#fallen">Fallen</a></li>)@', "</ul>\n$1", $out);
    
    $out = preg_replace('@(<li class="h3"><a href="#das-a-bis-z-magischer-gegenstaende">.*?</a>)</li>@', "$1\n<ul>", $out);
    $out = preg_replace('@(<li class="h3"><a href="#intelligente-magische-gegenstaende">.*?</a></li>)@', "</ul>\n$1", $out);
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>SRD CC v5.1 DE HTML - Index</title>
</head>
<body>
<ul>
<?= $out ?>
</ul>
</li>
</ul>
</body>
</html>