#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });

    $content = file_get_contents($argv[1]);
    
    $links = explode("\n", file_get_contents("../html/magicitemslinks.html"));
    foreach($links as $link) {
        if(preg_match('@<a class="magicitem" href="(#magicitem-.*?)">(.*?)</a>@', $link, $matches)) {
            $milink = $matches[1];
            $miname = $matches[2];

            $content = preg_replace('#<span class="nameref">(\s*)'.$miname.'(\s*)</span>#', '$1'.$link.'$2', $content);
        }
    }
    
    print $content;
?>