#!/usr/bin/php
<?
    include("functions.php");
    
    if(!isset($argv[1]) || !file_exists($argv[1])) {
        die("file not found\n");
    }
    
    set_error_handler(function($errno, $errstr, $errfile, $errline) { 
        die ("\n\n\nERROR: $errno, $errstr, $errfile, $errline\n\n");
    });

    $lines = explode("\n", file_get_contents($argv[1]));
    for($l=0; $l<count($lines); $l++) {
        if(trim($lines[$l])=='<table>') {
            $tabletitle = "";
            for($b=1; $b<3; $b++) {
                if(preg_match('#<h[1-6]>(.*?)</h[1-6]>#', trim($lines[$l-$b]), $matches)) {
                    $tabletitle = trim($matches[1]);
                }
            }
            if($tabletitle) {
                $lines[$l] = '<table id="'.srd_id('table', $tabletitle).'">';
            }
        }
    }
    
    print join("\n", $lines);
?>