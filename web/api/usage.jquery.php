<? header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>API :: SRD 5.1 de</title>
    <script src="https://openrpg.de/srd/5e/de/lib/jquery/jquery.js"></script>
    <link rel="stylesheet" href="https://openrpg.de/srd/5e/de/css/basic.css?v=<?=filemtime('../css/basic.css')?>">
</head>
<body>
    <script src="https://openrpg.de/srd/5e/de/api/srd-51-de.jquery.js"></script>
    <p class="border border-3 border-primary p-3">
        Du liest entspannt einen Text, doch pl&ouml;tzlich taucht ein freundlicher <span class="srd-popup fw-bold" data-category="monster" data-name="goblin">Goblin</span> auf!<br>
        Eine liebevolle <span class="srd-popup fw-bold" data-category="monster" data-name="ork">Orkin</span> begleitet ihn. Klicke auf sie!
    </p>
</body>