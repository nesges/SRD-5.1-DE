<? include_once('config.php') ?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>API :: SRD 5.1 de</title>
    
    <script>const baseurl = '<?= $baseurl ?>';</script>
    
    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="../lib/jquery-ui/jquery-ui.min.css">

    <script src="../lib/popper/popper.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="../lib/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../lib/fontawesome/css/brands.min.css">
    <link rel="stylesheet" href="../lib/fontawesome/css/solid.min.css">

    <script src="../js/web.js?v=<?=filemtime('../js/web.js')?>"></script>
    <link rel="stylesheet" href="../css/web.css?v=<?=filemtime('../css/web.css')?>">

    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap.D3.min.css">
    
    <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../img/favicon/site.webmanifest">
    
    <script src="../js/api.js?v=<?=filemtime('../js/api.js')?>"></script>
    <link rel="stylesheet" href="../css/api.css?v=<?=filemtime('../css/api.css')?>">
</head>
<body id="top">
    <div class="container-fluid">
        <div class="row">
            <div class="col p-0">
                <nav class="navbar navbar-expand-sm bg-primary fixed-top navbar-dark" data-bs-theme="dark">
                    <div class="container-fluid">
                        <a class="navbar-brand" href="https://openrpg.de">
                        <? 
                            $svg = file_get_contents(dirname(__FILE__).'/../img/openrpg.de.title.svg');
                            $svg = preg_replace('#<!DOCTYPE svg.*?>#', '', $svg);
                            $svg = preg_replace('#<\?xml.*?\?>#', '', $svg);
                            print $svg;
                        ?>
                        </a>
                        
                        <a class="navbar-brand mr-1" href="..">SRD 5.1 <strong>de</strong></a><a class="navbar-brand ml-1" href="#">API</a>
                
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item"><a class="nav-link" href="#category">/category</a></li>
                                <li class="nav-item"><a class="nav-link" href="#name">/name</a></li>
                                <li class="nav-item"><a class="nav-link" href="#format">/format</a></li>
                                <li class="nav-item" title="Verwendung"><a class="nav-link" href="#verwendung"><i class="fa-solid fa-code"></i></a></li>
                                <li class="nav-item" title="Downloads"><a class="nav-link" href="#download"><i class="fa-solid fa-download"></i></a></li>
                            </ul>
                        </div>
                    </div>
                
                    <div class="d-none d-md-block">
                        <a href="https://dnddeutsch.de"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        	 height="30px" viewBox="0 0 538.628 305.486" enable-background="new 0 0 538.628 305.486"
                        	 xml:space="preserve">
                        <g>
                        	<path fill="white" fill-rule="evenodd" clip-rule="evenodd" d="M9.856,302.757c10.02-5.914,19.925-12.036,30.096-17.678
                        		c8.224-4.562,12.423-11.237,12.705-20.541c0.141-4.661,0.501-9.321,0.504-13.981c0.037-51.833,0.027-103.666,0.012-155.498
                        		c-0.001-2.83-0.225-5.66-0.239-8.491c-0.056-11.203-4.976-19.339-14.132-25.967C21.401,48.004,8.32,31.852,2.109,10.858
                        		C1.152,7.623,0.799,4.209,0,0c1.75,0.85,2.516,1.129,3.184,1.559c15.623,10.047,33.048,14.019,51.283,14.317
                        		c37.32,0.611,74.651,0.602,111.972,1.162c36.218,0.544,69.918,9.2,98.524,32.692c30.56,25.096,47.067,57.689,50.323,96.938
                        		c3.075,37.061-5.049,71.455-28.085,101.015c-24.342,31.234-56.904,49.165-96.292,53.927c-12.033,1.455-24.246,1.903-36.38,1.96
                        		c-46.332,0.218-92.665,0.088-138.998,0.088c-1.814,0-3.628,0-5.443,0C10.011,303.357,9.934,303.057,9.856,302.757z M133.471,86.287
                        		c0,49.678,0,98.65,0,147.793c1.692,0.117,3.005,0.3,4.316,0.285c12.815-0.147,25.691,0.4,38.434-0.653
                        		c25.678-2.122,44.852-18.023,52.177-42.708c6.012-20.261,6.041-40.762-0.015-61.005c-6.206-20.749-19.544-34.9-40.72-40.813
                        		C169.878,84.221,151.69,87.214,133.471,86.287z"/>
                        	<path fill="white" fill-rule="evenodd" clip-rule="evenodd" d="M396.969,181.757c0-17.435,0-34.323,0-51.927c6.273,0,12.233,0.216,18.173-0.046
                        		c11.854-0.522,23.063-2.956,32.389-11.135c13.613-11.939,11.94-36.343-8.125-41.994c-9.659-2.721-19.912-3.98-29.966-4.47
                        		c-10.258-0.499-20.635,0.393-30.901,1.323c-20.123,1.823-37.104,9.344-47.234,28.351c-1.349-0.352-2.413-0.677-3.498-0.902
                        		c-4.912-1.025-5.858-3.466-4.626-8.504c5.659-23.119,10.809-46.362,16.111-69.568c0.526-2.305,0.556-4.735,4.157-4.036
                        		c14.362,2.787,28.54-0.291,42.775-1.518c31.227-2.69,62.266-2.771,92.38,7.638c16.586,5.733,31.274,14.474,40.608,29.909
                        		c15.032,24.857,14.837,63.236-20.151,83.093c-5.899,3.348-12.066,6.222-19.028,9.785c2.376,0.8,3.975,1.483,5.642,1.879
                        		c43.688,10.354,60.614,48.808,49.771,88.119c-7.483,27.128-26.83,44.141-51.694,55.477c-22.803,10.396-46.928,13.329-71.7,11.929
                        		c-18.294-1.034-36.566-2.448-54.86-3.48c-6.25-0.353-12.537-0.056-19.496-0.056c0.341,0.53-0.055,0.153-0.168-0.295
                        		c-7.038-27.905-14.053-55.815-21.199-84.238c2.645-0.772,5.114-1.807,7.67-2.111c1.315-0.156,3.423,0.572,4.092,1.603
                        		c15.847,24.412,40.332,31.314,67.192,32.932c15.128,0.91,30.275,1.037,44.964-4.172c17.711-6.28,26.309-20.372,22.349-37.597
                        		c-2.825-12.288-11.65-18.447-22.876-22.104c-11.429-3.724-23.229-4.011-35.081-3.887
                        		C402.183,181.779,399.729,181.757,396.969,181.757z"/>
                        </g>
                        </svg></a>
                    </div>
                </nav>
            </div>
        </div>
        
        <div class="row">
            <div class="col pt-3">
                <div id="page">
                    <div class="container-fluid" id="content">
