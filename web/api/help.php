<? include('head.php') ?>

<h1 class="mb-5">SRD 5.1 <strong>de</strong> API Referenz</h1>

<p class="mt-5 mb-3">Diese API stellt Monster, Magische Gegenst&auml;nde und Zauberspr&uuml;che des <a href="..">SRD 5.1 <strong>de</strong></a> maschinenlesbar 
zur Verf&uuml;gung. Sowohl die API, als auch die verschiedenen Objekt-Repr&auml;sentationen sind derzeit in Entwicklung
und noch nicht finalisiert. Inhalte und Formate k&ouml;nnen sich jederzeit und ohne Ank&uuml;ndigung &auml;ndern. Der Quellcode 
des SRD 5.1 <strong>de</strong>, sowie dieser API, steht unter <a href="https://codeberg.org/nesges/SRD-5.1-DE">codeberg.org/nesges/SRD-5.1-DE</a> zur Verfügung.</p>

<h3 id="rest-api">REST-API</h3>
<div class="mt-4">
    <p>Alle Aufrufe an die API beginnen mit dem Basis-URL <tt><?= $baseurl ?></tt>. Der Basis-URL wird gefolgt von Pfadkomponenten:</p>
    
    <p class="code ml-3"><tt><?= $baseurl ?> <em>category</em> [ /<em>name</em> ] [ /<em>format</em> ]</tt></p>

    <p>Pfadkomponenten in [ eckigen Klammern ] sind optional, aber nicht austauschbar (die Reihenfolge muss eingehalten werden und nachgeordnete benötigen vorherige).</p>

    <h6 class="mt-5 mb-2">Probier's aus:</h6>

    <div class="input-group mb-3">
        <span class="input-group-text">/api/</span>
        <div class="form-floating">
            <select name="category" id="category" class="form-control form-select" aria-label="category">
                <option selected disabled></option>
                <?
                    foreach($categories as $category) {
                        print "<option>$category</option>";
                    }
                ?>
            </select>
            <label for="category">category</label>
        </div>
        <span class="input-group-text">/</span>
        <div class="form-floating">
            <input type="text" name="name" id="name" class="form-control autocomplete" autocomplete="off" aria-label="[ name ]">
            <label for="name">[ name ]</label>
        </div>
        <span class="input-group-text">/</span>
        <div class="form-floating">
            <select name="format" id="format" class="form-control form-select" aria-label="[ format ]">
                <option></option>
                <?
                    foreach($formats as $format) {
                        print "<option>$format</option>";
                    }
                ?>
            </select>
            <label for="format">[ format ]</label>
        </div>
    </div>
    
    <div id="url" class="mt-4 alert alert-success">
        <div class="py-4 position–relative">
             <i class="fa-solid fa-hand-point-right"></i>
             <a target="_blank" class="stretched–link" href="#">#</a>
             <span class="message"></span>
        </div>
    </div>
</div>

<h4 id="category"><i class="fa-regular fa-circle-question"></i> category</h4>
<div>
    <p><em>category</em> kann folgende Werte annehmen:</p>
    <ul>
            <?
                foreach($categories as $category) {
                    print "<li>$category</li>";
                }
            ?>
    </ul>
    
    <h6><i class="fa-solid fa-share"></i> Aufrufbeispiele:</h6>
    <p>
        <ul class="mb-0"><li>category=monster</li></ul>
        <a class="ml-5" href="<?= $baseurl ?>monster" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>monster</strong></a><br>
        <ul class="mt-2 mb-0"><li>category=spell</li></ul>
        <a class="ml-5" href="<?= $baseurl ?>spell" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>spell</strong></a><br>
        <ul class="mt-2 mb-0"><li>category=magicitem</li></ul>
        <a class="ml-5" href="<?= $baseurl ?>magicitem" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>magicitem</strong></a>
    </p>
    
    <h6><i class="fa-solid fa-reply"></i> Antwort (JSON):</h6>
    <pre class="code-json">{
      "query": "<?= $baseurl ?><em>category</em>",
      "status": {
        "type": "<em>statustype</em>",
        "messages: [ "message 1" ]
      },
      "result": {
        "category": <em>category</em>,
        "objects": [
          "<?= $baseurl ?><em>category</em>/<em>name-1</em>",
          "<?= $baseurl ?><em>category</em>/<em>name-2</em>",
          ...
        ]
      }
    }
    </pre>
    <ul>
        <li><tt><strong>query</strong></tt>: URL der Anforderung</li>
        <li><tt><strong>status</strong></tt>: Status der Antwort</li>
        <li><tt>status.<strong>type</strong></tt>: "ok" oder "error"</li>
        <li><tt>status.<strong>messages</strong></tt>: Fehlermeldungen und Warnhinweise</li>
        <li><tt>result.<strong>category</strong></tt>: Angeforderte Kategorie
        <li><tt>result.<strong>objects</strong></tt>: Liste von Objekten der angeforderten Kategorie <em>category</em></li>
        <li><tt>result.<strong>objects[0]</strong></tt>: URL</li>
    </ul>
</div>






<h4 id="name" style="margin-top:8rem"><i class="fa-regular fa-circle-question"></i> <span class="text-black-50">category/</span> name</h4>
<div>
    <p><em>name</em> kann der Name jedes Objektes unter der Kategorie <em>category</em> sein</p>
    
    <h6><i class="fa-solid fa-share"></i> Aufrufbeispiele:</h6>
    <p>
        <ul class="mb-0">
        <li>category=magicitem</li>
        <li>name=heiltrank</li>
        </ul>
        <a class="ml-5" href="<?= $baseurl ?>magicitem/heiltrank" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>magicitem</strong><wbr/>/<strong>heiltrank</strong></a><br>
        <ul class="mt-2 mb-0">
        <li>category=monster</li>
        <li>name=goblin</li>
        </ul>
        <a class="ml-5" href="<?= $baseurl ?>monster/goblin" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>monster</strong><wbr/>/<strong>goblin</strong></a><br>
        <ul class="mt-2 mb-0">
        <li>category=spell</li>
        <li>name=feuerpfeil</li>
        </ul>
        <a class="ml-5" href="<?= $baseurl ?>spell/feuerpfeil" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>spell</strong><wbr/>/<strong>feuerpfeil</strong></a><br>
    </p>
    
    <h6><i class="fa-solid fa-reply"></i> Antwort (JSON):</h6>
    <pre class="code-json">{
      "query": "<?= $baseurl ?><em>category</em>/<em>name</em>",
      "status": {
        "type": "<em>statustype</em>",
        "messages: [ "message 1" ]
      },
      "result": {
        "id": "<em>category</em>-<em>name</em>",
        "category": "<em>category</em>",
        "files": [
          [
            {
              "url": "<?= $baseurl ?><em>category</em>/<em>name</em>/<em>format 1</em>",
              "size": <em>filesize</em>,
              "mtime": "<em>filemtime YYYY-MM-DD HH:MI:SS</em>",
              "type": "<em>mime-type</em>",
              "format": "<em>format</em>"
            }, {
              "url": "<?= $baseurl ?><em>category</em>/<em>name</em>/<em>format 2</em>",
              "size": <em>filesize</em>,
              "mtime": "<em>filemtime YYYY-MM-DD HH:MI:SS</em>",
              "type": "<em>mime-type</em>",
              "format": "<em>format</em>"
            },
            ...
          ]
        ]
      }
    }
    </pre>
    <ul>
        <li><tt><strong>query</strong></tt>: URL der Anforderung</li>
        <li><tt><strong>status</strong></tt>: Status der Antwort</li>
        <li><tt>status.<strong>type</strong></tt>: "ok" oder "error"</li>
        <li><tt>status.<strong>messages</strong></tt>: Fehlermeldungen und Warnhinweise</li>
        <li><tt>result.<strong>id</strong></tt>: eindeutige Kennung des Objektes</li>
        <li><tt>result.<strong>category</strong></tt>: category des Objektes</li>
        <li><tt>result.<strong>files</strong></tt>: Liste von dem Objekt zugehörigen Files</li>
        <li><tt>result.files[0].<strong>url</strong></tt>: URL des Files</li>
        <li><tt>result.files[0].<strong>size</strong></tt>: Größe des Files in Byte</li>
        <li><tt>result.files[0].<strong>mtime</strong></tt>: Änderungszeitpunkt des Files im Format YYYY-MM-DD HH:MI:SS</li>
        <li><tt>result.files[0].<strong>type</strong></tt>: MIME-Type des Files</li>
        <li><tt>result.files[0].<strong>format</strong></tt>: format-Kennung zum Abruf des Files</li>
    </ul>
    
    
    <h6><i class="fa-solid fa-circle-info"></i> Anmerkungen zu den Namem:</h6>
    <p><strong><em>name</em></strong> entspricht dem Namen des Objekts im SRD, mit folgenden Anpassungen:</p>
    <ul>
        <li>konvertiert in Kleinbuchstaben</li>
        <li>Leerzeichen " " durch Bindestriche "-" ersetzt</li>
        <li>Umlaute ä ö ü ß durch ae oe ue sz ersetzt</li>
        <li>Sonderzeichen wie z.B. Klammern, Kommas etc. entfernt</li>
    </ul>
</div>



<h4 id="format" style="margin-top:8rem"><i class="fa-regular fa-circle-question"></i> <span class="text-black-50">category/name/</span> format</h4>
<div>
    <p><em>format</em> kann das Format jedes Files eines Objektes <em>name</em> unter der Kategorie <em>category</em> sein. Mögliche Werte sind:</p>
    
    <ul>
        <?
            foreach($formats as $format) {
                print "<li>$format</li>";
            }
        ?>
    </ul>
    
    <h6><i class="fa-solid fa-share"></i> Aufrufbeispiele:</h6>
    <p>
        <ul class="mb-0">
        <li>category=magicitem</li>
        <li>name=heiltrank</li>
        <li>format=html</li>
        </ul>
        <a class="ml-5" href="<?= $baseurl ?>magicitem/heiltrank/html" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>magicitem</strong><wbr/>/<strong>heiltrank</strong>/<strong>html</strong></a><br>
        <ul class="mt-2 mb-0">
        <li>category=monster<br>
        <li>name=goblin<br>
        <li>format=json<br>
        </ul>
        <a class="ml-5" href="<?= $baseurl ?>monster/goblin/json" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>monster</strong><wbr/>/<strong>goblin</strong>/<strong>json</strong></a><br>
        <ul class="mt-2 mb-0">
        <li>category=spell<br>
        <li>name=feuerpfeil<br>
        <li>format=xml<br>
        </ul>
        <a class="ml-5" href="<?= $baseurl ?>spell/feuerpfeil/xml" target="_blank"><i class="fa-solid fa-hand-point-right"></i> <?= $baseurl ?><wbr /><strong>spell</strong><wbr/>/<strong>feuerpfeil</strong>/<strong>xml</strong></a><br>
    </p>
    
    <h6><i class="fa-solid fa-reply"></i> Antwort (&ast;):</h6>
    
    <p>Inhalt des Files entsprechend des angeforderten <em>format</em> für das Objekt <em>name</em> unter der Kategorie <em>category</em>. Der Inhalt wird in seinem spezifischen MIME-Type ausgegeben.</p>
    
    <h6><i class="fa-solid fa-circle-info"></i> Anmerkungen zu den Formaten:</h6>
    <ul>
        <li><strong><em>html</em></strong> besteht im <tt>&lt;div id="copy"&gt;</tt> jeweils aus einer 1:1 Kopie des entsprechenden Abschnitts der <a href="..">Bigfile-Fassung</a> des SRD 5.1 de</li>
        <li><strong><em>xml</em></strong> validiert gegen die DTD <a href="../all.dtd">all.dtd</a></li>
        <li><strong><em>json</em></strong> entspricht strukturell weitestgehend <em>xml</em></li>
        <li><strong><em>gmbinder.md</em></strong> ist ein <a href="https://www.gmbinder.com/">GM Binder</a>-kompatibles Markdown-Format. Textpassagen von <em>magicitem</em> und <em>spell</em> können HTML enthalten</li>
        <li><strong><em>homebrewery.md</em></strong> ist ein <a href="https://homebrewery.naturalcrit.com/">Homebrewery</a>-kompatibles Markdown-Format. Textpassagen von <em>magicitem</em> und <em>spell</em> können HTML enthalten</li>
        <li><strong><em>rpgtex.tex</em></strong> ist LaTeX für <a href="https://github.com/rpgtex/DND-5e-LaTeX-Template">rpgtex/DND-5e-LaTeX-Template</a></li>
        <li><strong><em>fantasystatblocks.yaml</em></strong> ist YAML für das Obsidian-Plugin <a href="https://github.com/javalent/fantasy-statblocks">Fantasy Statblocks</a>. Nur für Monster. Für eine vollständige Übersetzung wird zustäzlich eine <a href="#download-fantasystatblocks">Layout-Datei</a> benötigt.</li>
    </ul>
</div>                                            




<h3 id="verwendung" style="margin-top:8rem !important"><i class="fa-solid fa-code pr-3"></i> Verwendung</h3>

<script src="<?= $baseurl ?>srd-51-de.jquery.js"></script>
<link rel="stylesheet" href="../css/basic.css">

<div>
    <h5 id="curl">Curl <span class="badge text-bg-primary float-end">CLI</span></h5>
    <p>Die Objekte der API können mit Curl direkt abgerufen werden:</p>
    <pre class="code">you@home:~# curl <?= $baseurl ?>monster/goblin
{
    "query": "https:\/\/openrpg.de\/srd\/5e\/de\/api\/monster\/goblin",
    "status": {
        "type": "ok"
    },
    "result": {
        ...</pre>
    <p>Accept-Header werden nicht verwendet</p>
    
    
    <h5 id="vanilla">Javascript <span class="badge text-bg-primary float-end">Javascript</span></h5>
    <p>Die Objekte der API können in Javascript ohne weitere Bibliotheken ("Vanilla JS") per XMLHttpRequest abgerufen werden:</p>
    <pre class="code">const xhr = new XMLHttpRequest();
xhr.open("GET", '<?= $baseurl ?>monster/goblin');
xhr.send();
xhr.onload = function(e) {
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
    	var response = JSON.parse(xhr.responseText);
    	console.log(response);
    }
}</pre>

    <h5 id="jquery">jQuery <span class="badge text-bg-primary float-end">jQuery</span></h5>
    <p>Die Objekte der API können mit <a href="https://jquery.com/">jQuery</a> z.B. per <a href="https://api.jquery.com/get/"><tt>$.get()</tt></a> oder <a href="https://api.jquery.com/load/"><tt>$(element).load()</tt></a> abgerufen werden:</p>
    <pre class="code">jQuery(document).ready(function($) {
    $.get('https://openrpg.de/srd/5e/de/api/monster/goblin/html', function(response) {
        console.log(response);
    })
})</pre>
    <p>oder:</p>
    <pre class="code">jQuery(document).ready(function($) {
    $("#response").load('<?= $baseurl ?>monster/goblin');
}</pre>
    
    <h5 id="srd-51-de-jquery-js">Popup mit srd-51-de.jquery.js <span class="badge text-bg-primary float-end">jQuery</span></h5>
    <p>Verwende jQuery, um ein Popup zu öffnen, das das <tt>format html</tt> des Objektes mit dem <tt>name goblin</tt> aus der <tt>category monster</tt> anzeigt:</p>
    
    <p class="border border-3 border-primary p-3">
        Du liest entspannt einen Text, doch pl&ouml;tzlich tauchen ein freundlicher <span class="srd-popup fw-bold" data-category="monster" data-name="goblin">Goblin</span> und
        eine nette <span class="fw-bold" onClick="srd_51_de_popupSRD(this)" data-category="monster" data-name="ork">Orkin</span> auf. Klicke auf sie!
    </p>

    <h6>Quellcode:</h6>
    <code class="code">
        &lt;script src="lib/jquery/jquery.js"&gt;&lt;/script&gt;<br>
        &lt;script src="js/srd-51-de.jquery.js"&gt;&lt;/script&gt;<br>
        &lt;link rel="stylesheet" href="css/basic.css"&gt;<br>
        <br>
        &lt;p&gt;Du liest entspannt einen Text, doch pl&ouml;tzlich tauchen ein freundlicher 
        <strong>&lt;span class="srd-popup fw-bold" data-category="monster" data-name="goblin"&gt;Goblin&lt;/span&gt;</strong> 
        und eine nette <strong>&lt;span class="fw-bold" onClick="srd_51_de_popupSRD(this)" data-category="monster" data-name="ork"&gt;Orkin&lt;/span&gt;</strong>
        auf. Klicke auf sie!&lt;/p&gt;
    </code>
    
    <h6>Implementierung:</h6>
    <ul>
        <li><strong>jQuery</strong>: <a href="https://jquery.com/">jQuery</a> wird benötigt und muss vor srd-51-de.jquery.js geladen werden.</li>
        <li><strong>srd-51-de.jquery.js</strong>: Das Kernstück ist <a  href="https://openrpg.de/srd/5e/de/api/srd-51-de.jquery.js">srd-51-de.jquery.js</a>. Du solltest das JavaScript auf deinen Webserver kopieren ggf. anpassen und von dort laden.</li>
        <li><strong>basic.css</strong>: Das Basis-Layout <a  href="https://openrpg.de/srd/5e/de/css/basic.css">basic.css</a> für Werteblöcke. Du solltest das Stylesheet auf deinen Webserver kopieren und von dort laden.</li>
        <li><strong>class=<em>srd-popup</em></strong>: Elemente mit der class <em>srd-popup</em> erhalten einen Click-Handler, der das Popup aufruft. Alternativ kann die Function <strong>srd_51_de_popupSRD(element)</strong> auf ein Element mit den oben beschriebenen data-Attributen aufgerufen werden.</li>
        <li><strong>data-category=<em>category</em></strong>: Das Attribut <tt>data-category</tt> muss die <em>category</em> des anzuzeigenden Objekts enthalten</li>
        <li><strong>data-name=<em>name</em></strong>: Das Attribut <tt>data-name</tt> muss den <em>name</em> des anzuzeigenden Objekts enthalten</li>
    </ul>
    
    <h5 id="srd-51-de-bootstrap-jquery-js">Popup mit srd-51-de.bootstrap.jquery.js <span class="badge text-bg-primary float-end">jQuery</span> <span class="badge text-bg-primary float-end mr-1">Bootstrap</span></h5>
    <p>Verwende Bootstrap und jQuery, um ein Popup zu öffnen, das das <tt>format html</tt> des Objektes mit dem <tt>name goblin</tt> aus der <tt>category monster</tt> anzeigt:</p>
        
    <p class="border border-3 border-primary p-3">
        Du liest entspannt einen Text, doch pl&ouml;tzlich tauchen ein freundlicher <span class="srd-popup fw-bold" data-category="monster" data-name="goblin">Goblin</span> und
        eine nette <span class="fw-bold" onClick="srd_51_de_popupSRD(this)" data-category="monster" data-name="ork">Orkin</span> auf. Klicke auf sie!
    </p>

    <h6>Quellcode:</h6>
    <code class="code">
        &lt;script src="lib/jquery/jquery.js"&gt;&lt;/script&gt;<br>
        &lt;script src="lib/bootstrap/js/bootstrap.min.js"&gt;&lt;/script&gt;<br>
        &lt;link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css"&gt;<br>
        &lt;script src="js/srd-51-de.bootstrap.jquery.js"&gt;&lt;/script&gt;<br>
        &lt;link rel="stylesheet" href="css/basic.css"&gt;<br>
        <br>
        &lt;p&gt;Du liest entspannt einen Text, doch pl&ouml;tzlich tauchen ein freundlicher 
        <strong>&lt;span class="srd-popup fw-bold" data-category="monster" data-name="goblin"&gt;Goblin&lt;/span&gt;</strong> 
        und eine nette <strong>&lt;span class="fw-bold" onClick="srd_51_de_popupSRD(this)" data-category="monster" data-name="ork"&gt;Orkin&lt;/span&gt;</strong>
        auf. Klicke auf sie!&lt;/p&gt;
    </code>
    
    <h6>Implementierung:</h6>
    <ul>
        <li><strong>jQuery</strong>: <a href="https://jquery.com/">jQuery</a> wird benötigt und muss vor srd-51-de.bootstrap.jquery.js geladen werden.</li>
        <li><strong>Bootstrap</strong>: <a href="https://getbootstrap.com/">Bootstrap</a> wird benötigt und muss vor srd-51-de.bootstrap.jquery.js geladen werden.</li>
        <li><strong>srd-51-de.bootstrap.jquery.js</strong>: Das Kernstück ist <a  href="https://openrpg.de/srd/5e/de/api/srd-51-de.bootstrap.jquery.js">srd-51-de.bootstrap.jquery.js</a>. Du solltest das JavaScript auf deinen Webserver kopieren ggf. anpassen und von dort laden.</li>
        <li><strong>basic.css</strong>: Das Basis-Layout <a  href="https://openrpg.de/srd/5e/de/css/basic.css">basic.css</a> für Werteblöcke. Du solltest das Stylesheet auf deinen Webserver kopieren und von dort laden.</li>
        <li><strong>class=<em>srd-popup</em></strong>: Elemente mit der class <em>srd-popup</em> erhalten einen Click-Handler, der das Popup aufruft. Alternativ kann die Function <strong>srd_51_de_popupSRD(element)</strong> auf ein Element mit den oben beschriebenen data-Attributen aufgerufen werden.</li>
        <li><strong>data-category=<em>category</em></strong>: Das Attribut <tt>data-category</tt> muss die <em>category</em> des anzuzeigenden Objekts enthalten</li>
        <li><strong>data-name=<em>name</em></strong>: Das Attribut <tt>data-name</tt> muss den <em>name</em> des anzuzeigenden Objekts enthalten</li>
    </ul>
    
</div>




<h3 id="download" style="margin-top:8rem !important"><i class="fa-solid fa-download pr-3"></i> Downloads</h3>
<div>
    <h5 id="download-popup">Popup-Integration</h5>
    <div>
        <ul>
            <li><a href="<?= $baseurl ?>srd-51-de.jquery.js"><i class="fa-solid fa-file-code"></i> srd-51-de.jquery.js</a>, siehe <a href="#srd-51-de-jquery-js">oben <i class="fa-solid fa-arrow-turn-up"></i></a></li>
            <li><a href="<?= $baseurl ?>srd-51-de.bootstrap.jquery.js"><i class="fa-solid fa-file-code"></i> srd-51-de.bootstrap.jquery.js</a>, siehe <a href="#srd-51-de-bootstrap-jquery-js">oben <i class="fa-solid fa-arrow-turn-up"></i></a></li>
            <li><a href="https://openrpg.de/srd/5e/de/css/basic.css"><i class="fa-solid fa-file-code"></i> basic.css</a> Basis-Stylesheet für Werteblöcke</li>
        </ul>
    </div>
    
    <h5 id="download-compendium">Compendium</h5>
    <div>
        <ul>
            <li><a href="../compendium.xml"><i class="fa-solid fa-file-code"></i> compendium.xml</a> enthält alle Objekte im XML–Format (<?= round(filesize('../compendium.xml')/1024/1024,2).' MB, '.date('Y-m-d H:i:s', filemtime('../compendium.xml'))?>)</li>
            <li><a href="../compendium.homebrewery.md"><i class="fa-solid fa-file-code"></i> compendium.homebrewery.md</a> enthält alle Objekte im Markdown–Format für <a href="https://homebrewery.naturalcrit.com/">Homebrewery</a> (<?= round(filesize('../compendium.homebrewery.md')/1024/1024,2).' MB, '.date('Y-m-d H:i:s', filemtime('../compendium.homebrewery.md'))?>)</li>
            <li><a href="../compendium.rpgtex.tex"><i class="fa-solid fa-file-code"></i> compendium.rpgtex.tex</a> enthält alle Objekte im <a href="https://github.com/rpgtex/DND-5e-LaTeX-Template">rpgtex/DND-5e-LaTeX-Template</a>-Format (<?= round(filesize('../compendium.rpgtex.tex')/1024/1024,2).' MB, '.date('Y-m-d H:i:s', filemtime('../compendium.rpgtex.tex'))?>)</li>
        </ul>
    </div>

    <h5 id="download-fantasystatblocks">Fantasy Statblocks</h5>
    <div>
        <ul>
            <li><a href="../download/fantasystatblocks/Basic-5e-Layout-DE.json"><i class="fa-solid fa-file-code"></i> Basic-5e-Layout-DE.json</a> enthält eine Übersetzung des Werteblock-<a href="https://github.com/javalent/fantasy-statblocks#layouts">Layouts für Fantasy Statblocks</a>. Wird zusätzlich zu <em>fantasystatblocks.yaml</em> benötigt, um Bezeichner wie Rüstungsklasse, Trefferpunkte etc. in <a href="https://obsidian.md/">Obsisian</a> in deutscher Übersetzung anzuzeigen. Zur Verfügung gestellt von <a href="https://mastodon.pnpde.social/@3Fragezeichen">@3Fragezeichen</a> (<?= round(filesize('../download/fantasystatblocks/Basic-5e-Layout-DE.json')/1024/1024,2).' MB, '.date('Y-m-d H:i:s', filemtime('../download/fantasystatblocks/Basic-5e-Layout-DE.json'))?>)</li>
        </ul>
    </div>
    
    <h5 id="quellcode">Quellcode</h5>
    <div>
        <ul>
            <li><a href="https://codeberg.org/nesges/SRD-5.1-DE">Repository SRD-5.1-DE auf Codeberg</a></li>
        </ul>
    </div>
</div>

</div>

<div id="computing"></div>

<? include('../foot.php') ?>