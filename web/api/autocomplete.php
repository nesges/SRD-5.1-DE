<?
    $q = strtolower(htmlspecialchars($_GET['term']));
    $c = strtolower(htmlspecialchars($_GET['category']));

    if($c) {
        $dirs = ['../'.$c ];
    } else {
        $dirs = ['../spell', '../magicitem', '../monster'];
    }

    $matches = [];
    
    foreach($dirs as $dir) {
        $handle = opendir($dir);
        while($file= readdir($handle)){
            if(preg_match('/^(spell|monster|magicitem)-.*?'.$q.'/', strtolower($file))){
                $name = preg_replace('/^(spell|monster|magicitem)-(.*?)\..*/', '$2', strtolower($file));
                $matches[$name]++;
            }
        }
        closedir($handle);
    }

    $names = array_keys($matches);
    sort($names);

    header("Content-type: application/json");
    print json_encode($names);
?>