// requires bootstrap, e.g.;
//      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
//      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
//
// requires jQuery, e.g.:
//      <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js"></script>

var popupSRD_modal;
const popupSRD_API_bootstrap = 'https://openrpg.de/srd/5e/de/api';

jQuery(document).ready(function(){
    const element = jQuery('<div id="srd-51-de-popup" class="srd-51-de-popup srd-51-de modal" tabindex="-1" data-backdrop="false">'
        + '   <div class="modal-dialog modal-lg">'
        + '       <div class="modal-content">'
        + '           <div class="modal-body"></div>'
        + '       </div>'
        + '   </div>'
        + '</div>');
    popupSRD_modal = new bootstrap.Modal(element);
    
    jQuery('.srd-popup').click(function(){
        srd_51_de_popup(this);
    });
});

function srd_51_de_popup(elem)  {
    const category = jQuery(elem).data('category');
    const name = jQuery(elem).data('name');
    
    jQuery('#srd-51-de-popup').html('<div class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div>');
    jQuery('.modal-body', '#srd-51-de-popup').load(popupSRD_API_bootstrap + '/' + category + '/' + name + '/html #copy', function(){
        popupSRD_modal.show();
    });
}