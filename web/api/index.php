<? 
    include_once('config.php');
    
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $basedir = '../';
    
    $response['query'] = $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $response['status']['type'] = "ok";
    
    // API
    $request = '';
    if(isset($_POST['request'])) {
        $request = htmlspecialchars($_POST['request']);
    } else if(isset($_GET['request'])) {
        // api/?request=monster-goblin
        $request = htmlspecialchars($_GET['request']);
    }
    
    $format = '';
    if(isset($_POST['format'])) {
        $format = htmlspecialchars($_POST['format']);
    } else if(isset($_GET['format'])) {
        $format = htmlspecialchars($_GET['format']);
    }
    if($format && !in_array($format, $formats)) {
        $response['status']['type'] = "error";
        $response['status']['message'][] = "unknown format $format";
    }
    
    $list = '';
    if(isset($_POST['list'])) {
        $list = htmlspecialchars($_POST['list']);
    } else if(isset($_GET['list'])) {
        $list = htmlspecialchars($_GET['list']);
    }
    if($list && !in_array($list, $categories)) {
        $response['status']['type'] = "error";
        $response['status']['message'][] = "unknown category $list";
    }

    if(!$request && !$list) {
        header("Content-type: text/html");
        include("help.php");
        exit;
    }

    // $response['debug']['request'] = $request;
    // $response['debug']['list'] = $list;
    // $response['debug']['GET'] = json_encode($_GET);
    // $response['debug']['POST'] = json_encode($_POST);

    // REQUEST    
    if($request) {
        // normalize
        $request = strtolower(trim($request));
        $request = preg_replace('/ü/', 'ue', $request);  
        $request = preg_replace('/ä/', 'ae', $request);
        $request = preg_replace('/ö/', 'oe', $request);
        $request = preg_replace('/ß/', 'sz', $request);
        $request = iconv("utf-8", "ascii//TRANSLIT", preg_replace('#\s+#', '-', $request));
        $request = preg_replace('/-+/', '-', $request);
        
        if(preg_match('/^(monster|magicitem|spell)-/', $request, $matches)) {
            $objectcategory = $matches[1];
        } else {
            if(file_exists($basedir.'monster/monster-'.$request.'.xml') || file_exists($basedir.'monster/monster-'.$request.'.html')) {
                $objectcategory = 'monster';
            } else if(file_exists($basedir.'magicitem/magicitem-'.$request.'.xml') || file_exists($basedir.'magicitem/magicitem-'.$request.'.html')) {
                $objectcategory = 'magicitem';
            } else if(file_exists($basedir.'spell/spell-'.$request.'.xml') || file_exists($basedir.'spell/spell-'.$request.'.html')) {
                $objectcategory = 'spell';
            } else {
                $objectcategory = 'unknown';
            }
            $request = $objectcategory.'-'.$request;
        }
        
        $objectfilename = $basedir.$objectcategory.'/'.$request;
        $objecturl = $baseurl.$objectcategory.'/'.preg_replace('/(monster|magicitem|spell)-/', '', $request, 1);
    
        if($format && in_array($format, $formats)) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET, POST');

            if(file_exists($objectfilename.'.'.$format)) {
                header("Content-type: ".mime_content_type($objectfilename.'.'.$format));
                $content = file_get_contents($objectfilename.'.'.$format);
                if($format == 'html') {
                    $content = preg_replace('#(</div>\s*</body>)#', '    <div class="copyright-notice"><a href="https://dnddeutsch.de"><img src="https://openrpg.de/img/d3.svg" style="height:1.4rem; float:left; filter:invert()"/></a><a  href="'.$baseurl.'">SRD 5.1 <strong>de</strong></a> zur Verfügung gestellt von <a href="https://openrpg.de">OpenRPG.de</a> unter <a href="https://openrpg.de/license/cc/by/4.0/de/">CC-BY 4.0 <img src="https://openrpg.de/img/cc.svg" style="height:1em"/><img src="https://openrpg.de/img/by.svg" style="height:1em"/></a>$1', $content);
                }
                print $content;
                exit;
            } else {
                $response['status']['type'] = "error";
                $response['status']['message'][] = basename($objectfilename).'.'.$format.' not found';
            }
        } else {
            $files = [];

            foreach($formats as $format) {
                if(file_exists($objectfilename.'.'.$format)) {
                    $files[] = [
                        'url' => $objecturl.'/'.$format,
                        'size' => filesize($objectfilename.'.'.$format),
                        'mtime' => date('Y-m-d H:i:s', filemtime($objectfilename.'.'.$format)),
                        'type' => mime_content_type($objectfilename.'.'.$format),
                        'format' => $format,
                    ];
                }
            }
            
            if(count($files)) {
                $response['result']['id'] = $request;
                $response['result']['category'] = $objectcategory;
                $response['result']['files'][] = $files;
            } else {
                $response['status']['type'] = "error";
                $response['status']['message'][] = "no files found";
            }
            
        }
    

    // LIST

    } else if($list) {
        $ids = [];
        if(is_dir('../'.$list)) {
            $handle = opendir('../'.$list);
            while($file= readdir($handle)){
                if(preg_match('/^('.$list.'-.*?)\..*/', $file, $matches)) {
                    $objecturl = $baseurl.preg_replace('/-/', '/', $matches[1], 1);
                    $urls[] = $objecturl;
                }
            }    
            closedir($handle);
            
            $response['result']['category'] = $list;
            $response['result']['objects'] = array_values(array_unique($urls));
        }
    }

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    header("Content-type: application/json");
    print json_encode($response, JSON_PRETTY_PRINT);
?>