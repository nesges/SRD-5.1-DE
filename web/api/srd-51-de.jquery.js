// requires jQuery, e.g.:
//      <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js"></script>

const popupSRD_API_jquery = 'https://openrpg.de/srd/5e/de/api';

jQuery(document).ready(function(){
    const element = jQuery('<div id="srd-51-de-popup" class="srd-51-de-jquery-standalone srd-51-de"></div>');
    element.hide();
    jQuery('body').append(element);
    
    jQuery('.srd-popup').click(function(){
        srd_51_de_popupSRD(this);
    });

    jQuery(window).click(function(){
        jQuery('body').removeClass('srd-51-de-backdrop');
        jQuery('#srd-51-de-popup').hide();
    });

    jQuery('#srd-51-de-popup').click(function(){
        event.stopPropagation();
    });
});

function srd_51_de_popupSRD(elem)  {
    const category = jQuery(elem).data('category');
    const name = jQuery(elem).data('name').replace(/\s+/g, '-').replace(/[()_,]/ig, '');

    jQuery('#srd-51-de-popup').html('loading..');
    jQuery('#srd-51-de-popup').load(popupSRD_API_jquery + '/' + category + '/' + name + '/html #copy', function(){
        jQuery('body').addClass('srd-51-de-backdrop');
        jQuery('#srd-51-de-popup').show();
    });
}
