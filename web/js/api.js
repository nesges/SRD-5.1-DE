jQuery(document).ready(function($) {
    $('#url').css('visibility', 'hidden');
     
    $('select[name="category"]').change(function(){
        $('.autocomplete').autocomplete({
            source: 'autocomplete.php?category=' + $(this).val(),
            close: function() { updateUrlPreview() }
        });
    });
    
    $('.autocomplete').autocomplete({
        source: 'autocomplete.php',
        close: function() { updateUrlPreview() }
    });
    
    $('select[name="category"], input[name=name], select[name="format"]').on('change', function(){
        updateUrlPreview();
    });
});

function updateUrlPreview() {
    const category = $('select[name="category"]').val();
    const name = $('input[name=name]').val();
    const format = $('select[name=format]').val();
    
    var url;
    var message;
    var error = 0;
    
    if(format=='fantasystatblocks.yaml' && category != 'monster') {
        url = ''
        error = 1;
        message = "Fantasy Statblocks kann <strong>nur Monster</strong> darstellen. Das YAML-Format ist nicht f&uuml;r die <strong>category " + category + "</strong> verf&uuml;gbar";
    } else {
        if(category) {
            url = baseurl;
            url += category;
            if(name) {
                url += '/' + name;
                if(format) {
                    url += '/' + format;
                }
            } else {
                if(format) {
                    error = 1;
                    message = "<em>format</em> h&auml;ngt von <em>name</em> ab. <em>"+format+"</em> wird deshalb nicht &uuml;bernommen.";
                }
            }
        }
    }

    $('#url .message').html(message ? (url!='' ? '<br>' : '') + message : '');
    $('#url a').html(url);
    if(!error) {
        $('#url a').attr('href', url);
        $('#url').addClass('alert-success');
        $('#url').removeClass('alert-warning');
    } else {
        $('#url a').attr('href', '#');
        $('#url').removeClass('alert-success');
        $('#url').addClass('alert-warning');
    }
    $('#url').removeClass('d�none');
    $('#url').css('visibility', 'visible');
}