var activeSection;
var waitSearch;

jQuery(document).ready(function($) {
    popupClickHandler();
    
    $('div.spell h5, div.magicitem h5, div.monster h5').click(function() {
        event.preventDefault();
        var href = $(this).parent('div').attr('id');
        var htmlfile = $(this).parent('div').attr('class') + '/' + $(this).parent('div').attr('id') + '.html';
        popup('#'+href, htmlfile);
    });
    
    $('#search').focus(function() {
        $('.search-highlight').removeClass('search-highlight');
    });
    $('#search').change(function() {
        clearTimeout(waitSearch);
        if($(this).val().length > 1) {
            search($(this).val());
        }
    });
    $('#search').keyup(function() {
        // trigger change when user stops typing
        clearTimeout(waitSearch);
        $('.searchspinner').removeClass('d-none');
        waitSearch = setTimeout(function() {
            $('.searchspinner').addClass('d-none');
            $('#search').change();
        }, 1000);
    });
    
    var scrollTimeout;
    // highlight active section in the top navigation
    $(window).on('resize scroll', function() {
        if(activeSection) {
            // remove highlights of all links that are not going to activeSection
            scrollTimeout = setTimeout(function() {
                $('.navbar [href!="#'+activeSection+'"]').removeClass('nav-highlight');
            },1000);
        }
        $('h2[id], h3[id]').each(function() {
            const id = $(this).attr('id');
            // if this has an id ..
            if(id) {
                // .. is visible ..
                if($(this).isInViewport()) {
                    // .. and is being linked to ..
                    if($('.navbar [href="#'+id+'"]').length) {
                        $('.navbar [href="#'+id+'"]').addClass('nav-highlight');
                        activeSection = id;
                        const anchor = $('<a href="#'+id+'">' + $(this).text() + '</a>');
                        anchor.click(function(){
                            offsetScroll(this);
                        });
                        $('#where-am-i').html(anchor);
                    }
                }
            } 
        });
    });
    
    // offset scroll position to account for fixed navbar
    $(':not(.spell):not(.monster):not(.magicitem)[href]').click(function(){    
        offsetScroll(this);
    });
});


function offsetScroll(elem) {
    const offset = $('nav').height() * 2;
    id = $(elem).attr('href');
    
    if(id!='#' && $(id).length) {
        event.preventDefault();
        window.scrollTo({top: $(id).offset().top - offset, behavior: 'smooth'});
    }
}

function popupClickHandler(selector='body') {
    $('a.spell, a.magicitem, a.monster', selector).click(function() {
        event.preventDefault();
        var href = $(this).attr('href');
        popup(href);
    });
}

var modal;

function popup(url, url2='') {
    if(modal) {
        modal.hide();
    }
    modal = new bootstrap.Modal('#popup');
    $('.modal-body', '#popup').html('<div class="centered"><img class="spinner w20" src="img/loading_spinner_w20.gif" alt="loading"></div>');
    $('.modal-footer .permalink-display', '#popup').html('<a href="'+url+'"><i class="fa-solid fa-link"></i> '+url+'</a>' + (url2 ? ' <a class="ml-3" href="'+url2+'"><i class="fa-solid fa-link"></i> '+url2+'</a>' : ''));
    modal.show();

    $('.modal-body', '#popup').load("index.php " + url, function() {
        popupClickHandler('#popup');
    })
}


function search(term) {
    $('.search-highlight').removeClass('search-highlight');

    const offcanvas = new bootstrap.Offcanvas('#offcanvasSearchResults');
    $('.offcanvas-title', '#offcanvasSearchResults').html('gesucht nach: ' + term);
    $('.offcanvas-body', '#offcanvasSearchResults').html('');
    
    // compare pretty stripped down textversions
    term = term.toLowerCase().replace(/[^a-z0-9_+-]/ig, '');
    
    var c = 0;
    $('div, span, li, p, td, th, h1, h2, h3, h4, h5:not(.offcanvas-title), h6').each(function(){
        const text = $(this).clone().children().remove().end().text();
        const comparetext = text.toLowerCase().replace(/[^a-z0-9_+-]/ig, '');
        if(comparetext.indexOf(term) != -1) {
            const result = $('<div class="searchresult">'+text+'</div>');
            result.prepend('<span class="badge rounded-pill bg-primary">'+(++c)+'</span> ');
            
            result.data('element', this);
            result.click(function() {
                $('.search-highlight').removeClass('search-highlight');
                $($(this).data('element')).addClass('search-highlight');
                
                const offset = $('nav').height() * 2;
                window.scrollTo({top: $($(this).data('element')).offset().top - offset, behavior: 'smooth'});
                event.preventDefault();
            });
            $('.offcanvas-body', '#offcanvasSearchResults').append(result);
        }
    });
    
    offcanvas.show();
}

$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};