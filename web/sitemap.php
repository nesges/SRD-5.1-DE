<?
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    $ignore = ['./navigation-sidebar.html', './navigation-top.html' ];
    $baseurl = $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST'].preg_replace('/sitemap\.(xml|php)/', '', $_SERVER['REQUEST_URI']);
    
    header("Content-type: application/xml");
    print '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    print '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
    
    $tree = traverse(".");
    usort($tree, function($a, $b) {
        if(substr_count($a, '/') > substr_count($b, '/')) {
            return 1;
        }
        if(substr_count($a, '/') < substr_count($b, '/')) {
            return -1;
        }
        return strnatcmp($a, $b);
    });
    foreach($tree as $file) {
        print "    <url>
        <loc>".preg_replace('#^./#', $baseurl, $file)."</loc>
        <lastmod>".date('Y-m-d', filemtime(getcwd().'/'.$file))."</lastmod>
    </url>\n";
    }    
    
    print '</urlset>';

    function traverse($dir, &$tree=[]) {
        global $ignore;
        
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if($file != '.' && $file != '..' && !is_link($dir.'/'.$file) && !in_array($dir.'/'.$file, $ignore)) {
                        if(is_dir($dir.'/'.$file)) {
                            traverse($dir.'/'.$file, $tree);
                        } else {
                            if(is_file($dir.'/'.$file) && preg_match('/\.(php|html)$/', $file)) {
                                $tree[] = $dir.'/'.$file;
                            }
                        }
                    }
                }
                closedir($dh);
            }
        }
        return $tree;
    }
?>