                    <div id="footer" class="footer-wrap">
                        <footer class="text-light p-4 mt-4 openrpg">
                            <p class="text-center"><strong><a href="https://openrpg.de">OpenRPG.de</a> ist ein Projekt von <a class="open-d3-logo" href="https://www.dnddeutsch.de">
                                     <?
                                         $svg = file_get_contents(dirname(__FILE__).'/img/OpenD3-logo.svg');
                                         $svg = preg_replace('#<!DOCTYPE svg.*?>#', '', $svg);
                                         $svg = preg_replace('#<\?xml.*?\?>#', '', $svg);
                                         $svg = preg_replace('#id="Layer_1"#', '', $svg);
                                         print $svg;
                                     ?>
                                 </a></strong>
                            </p>

                            <p style='margin-top:30px'><a href="https://dnddeutsch.de"><strong>D3</strong></a> ist die kompletteste Sammlung von deutschsprachigem D&D-5e-Kram der Welt! Die Fansite wird ohne Gewinnabsicht betrieben, kann vollst&auml;ndig kostenlos genutzt werden und hat keine Werbeeinblendungen. Zur Finanzierung der Betriebskosten ist <a href="https://www.patreon.com/dnddeutsch">eine Patreon-Seite</a> angeschlossen.</p>
                             				
                    		<div class="trennlinie"></div>
                    		
                    		<p>Dieses Werk enthält Material aus dem Systemreferenzdokument 5.1 („SRD 5.1&quot;) von Wizards of the Coast LLC, das unter <a class="d-none d-sm-none d-md-inline d-lg-inline d-xl-inline" href="https://dnd.wizards.com/de/resources/systems-reference-document">https://dnd.wizards.com/de/resources/systems-reference-document</a> <a class="d-inline d-sm-inline d-md-none d-lg-none d-xl-none" href="https://dnd.wizards.com/de/resources/systems-reference-document">systems-reference-document</a> verfügbar ist. Das SRD 5.1 ist lizenziert gemäß der Lizenz Creative Commons Namensnennung 4.0 International, die unter <a class="d-none d-sm-none d-md-inline d-lg-inline d-xl-inline" href="https://creativecommons.org/licenses/by/4.0/legalcode.de">https://creativecommons.org/licenses/by/4.0/legalcode.de</a> <a class="d-inline d-sm-inline d-md-none d-lg-none d-xl-none" href="https://creativecommons.org/licenses/by/4.0/legalcode.de">legalcode.de</a> verfügbar ist.</p>
                    		
                    		<div class="trennlinie"></div>
            	    		
                    		<p>Erstellt mit freier Software wie <a href="https://fontawesome.com/license">Font Awesome</a>,				
                    		<a href="https://www.php.net/">PHP</a>,
                    		<a href="https://jquery.com/">jQuery</a>,
                    		<a href="https://jqueryui.com/">jQuery UI</a>,
                    		<a href="https://game-icons.net/">Game-icons.net</a>,
                    		<a href="https://winscp.net/">WinSCP</a>,
                    		<a href="https://www.linux.org/">Linux</a>,
                    		<a href="https://www.putty.org/">PuTTY</a>,
                    		<a href="https://www.toptal.com/designers/subtlepatterns/">Subtle Patterns</a>,
                    		<a href="https://getbootstrap.com/">Bootstrap</a>
                    		und vielen anderen. Nanos gigantum humeris insidentes. Danke!</p>
                    		
                    		<div class="trennlinie"></div>
                    		
                    		<br>
                    		
                    		<div class="centered">
                    		    <a href="https://www.dnddeutsch.de/impressum/">Impressum</a> 
                    		    | <a href="https://www.dnddeutsch.de/impressum/#datenschutz">Datenschutzerkl&auml;rung</a>
                    		    | <a href="https://www.dnddeutsch.de/kontakt/">Kontakt</a> 
                    		    | <a href="https://www.dnddeutsch.de/ueber-d3/">&Uuml;ber D3</a>
                    		    | <a href="https://www.dnddeutsch.de/feeds"><i class='fa-solid fa-rss'></i></a> <a href="https://www.dnddeutsch.de/feeds">RSS</a>
                    		    | <a href="https://www.dnddeutsch.de/newsletter-anmeldung"><i class='fa-solid fa-envelope'></i></a> <a href="https://www.dnddeutsch.de/newsletter-anmeldung">Newsletter</a>
                    		    | <a href="https://mastodon.pnpde.social/@dnddeutsch"><i class='fa-brands fa-mastodon'></i></a> <a href="https://mastodon.pnpde.social/@dnddeutsch">Mastodon</a>
                    		</div>
                            <br>
                            <br>
                            <div class="centered">
                                <a href="https://www.patreon.com/dnddeutsch"><img width="125" height="35" style="background-color:white; padding:12.5px; border-radius:6px; width:150px; height:auto;" src="https://www.dnddeutsch.de/wp-content/uploads/2022/01/Patreon-Logo-2020-present-700x394-1-e1643699241272.png" alt="Unterst&uuml;tze D3 mit Patreon"></a>
                            </div>
                    	</footer>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</body>
</html>