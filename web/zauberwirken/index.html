<h2 id="zauberwirken">Zauberwirken</h2>
<p>Magie durchdringt Fantasy-Spielwelten. Meistens tritt sie in Form von Zaubern auf.</p>
<p>Dieser Abschnitt enthält die Regeln zum Zauberwirken. Verschiedene Charakterklassen haben unterschiedliche Arten, ihre Zauber zu lernen und vorzubereiten. Monster nutzen Magie auf einzigartige Weise. Unabhängig von ihrer Quelle folgen Zauber den hier aufgeführten Regeln.</p>
<h3 id="was-ist-ein-zauber">Was ist ein Zauber?</h3>
<p>Ein Zauber ist ein diskreter magischer Effekt, eine einzelne Verformung der magischen Energien, die das Multiversum in einer spezifischen Ausprägung durchdringen. Beim Wirken eines Zaubers manipuliert der Charakter kunstvoll die unsichtbaren Fasern urtümlicher Magie, die die Welt durchdringen, formt sie zu einem ganz bestimmten Muster, sieht, wie sie auf ganz bestimmte Weise vibrieren, und lässt sie dann wieder los, um den gewünschten Effekt zu erzielen - in den meisten Fällen geschieht all das innerhalb von Sekunden.</p>
<p>Zauber können vielseitige Werkzeuge, Waffen oder Schutzschilde sein. Sie können Schaden verursachen oder rückgängig machen, Zustände verursachen oder entfernen (siehe Anhang PH-A), Lebensenergie absaugen und Toten wieder Leben einhauchen.</p>
<p>Im Verlauf der Geschichte des Multiversums wurden unzählige Zauber erschaffen und viele von ihnen sind längst in Vergessenheit geraten. Einige sind möglicherweise in staubigen Zauberbüchern in uralten Ruinen niedergeschrieben oder im Geist einer toten Gottheit gefangen. Vielleicht werden sie eines Tages von einem Charakter wiederentdeckt, der über die nötige Macht und das Wissen verfügt.</p>
<h4>Zaubergrad</h4>
<p>Jeder Zauber hat einen Grad von 0 bis 9. Der Grad eines Zaubers ist ein allgemeiner Hinweis darauf, wie mächtig er ist. So gibt es das bescheidene (aber immer noch beeindruckende) <a class="spell" href="#spell-magisches-geschoss">Magisches Geschoss</a> des 1. Grades und den welterschütternden <a class="spell" href="#spell-wunsch">Wunsch</a> des 9. Grades. Zaubertricks - einfache, aber mächtige Zauber, die Charaktere quasi im Schlaf beherrschen - haben den 0. Grad. Je höher der Grad eines Zaubers ist, desto höher muss die Stufe des Zauberwirkers sein, damit dieser den Zauberspruch verwenden kann.</p>
<p>Zaubergrad und Charakterstufe stimmen nicht direkt überein. Für gewöhnlich kann ein Charakter einen Zauber des 9. Grades erst wirken, wenn er die 17. Stufe erreicht hat, nicht die 9. Stufe.</p>
<h4>Bekannte und vorbereitete Zauber</h4>
<p>Ehe ein Zauberwirker einen Spruch entfesseln kann, muss er diesen fest in seinem Geist verankern oder durch einen magischen Gegenstand Zugriff auf ihn haben. Mitglieder einiger Klassen, darunter Barden und Zauberer, besitzen eine begrenzte Anzahl von Zaubern, die ihnen immer zur Verfügung stehen. Dies gilt auch für viele magische Monster. Andere Zauberwirker wie Kleriker und Magier müssen ihre Zauber vorbereiten. Dieser Vorgang unterscheidet sich je nach Klasse und ist in deren Beschreibungen angegeben.</p>
<p>In jedem Fall hängt die Anzahl der Zauber, die ein Zauberwirker in seinem Verstand verankert haben kann, von der Charakterstufe ab.</p>
<h4>Zauberplätze</h4>
<p>Egal wie viele Zauber ein Zauberwirker kennt oder vorbereitet: Er kann nur eine bestimmte Anzahl an Zaubern wirken, bevor er rasten muss. Die Manipulation des magischen Grundgefüges und die Kanalisierung seiner Energie selbst in den einfachsten Zauber ist körperlich und mental sehr anstrengend, und auf höheren Graden steigen die Anforderungen noch weiter. Aus diesem Grund ist in der Aufstiegstabelle jeder zauberwirkenden Klasse angegeben, wie viele Zauberplätze eines jeden Grades dem Charakter auf seiner aktuellen Stufe zur Verfügung stehen. Beispielsweise besitzt Umara, eine Magierin der 3. Stufe, vier Zauberplätze des 1. Grades und zwei des 2. Grades.</p>
<p>Wenn ein Charakter einen Zauber wirkt, verbraucht er einen Platz für den Grad des Zaubers oder höher. Er „füllt&quot; quasi zuvor einen Platz mit dem entsprechenden Zauber. Du kannst dir einen Zauberplatz wie eine Aussparung einer bestimmten Größe vorstellen: kleine Plätze für Zauber des 1. Grades, etwas größere für Zauber höheren Grades. Ein Zauber des 1. Grades passt in einen Zauberplatz einer beliebigen Größe, ein Zauber des 9. Grades dagegen nur in einen Platz des 9. Grades. Wenn Umara also <a class="spell" href="#spell-magisches-geschoss">Magisches Geschoss</a> - einen Zauber 1. Grades - wirkt, verbraucht sie einen Zauberplatz des 1. Grades und hat noch drei Plätze übrig.</p>
<p>Nach einer langen Rast werden verbrauchte Zauberplätze wiederhergestellt.</p>
<p>Manche Charaktere und Monster haben Spezialfähigkeiten, mit denen sie Zauber wirken können, ohne Zauberplätze verbrauchen zu müssen. Dies gilt beispielsweise für einen Mönch, der dem Weg der vier Elemente folgt, einen Hexenmeister, der bestimmte schauerliche Anrufungen wählt, und einen Höllenschlundteufel aus den Neun Höllen.</p>
<h5>Einen Zauber auf einem höheren Grad wirken</h5>
<p>Wenn ein Zauberwirker einen Zauber mit einem Zauberplatz eines höheren Grades wirkt, gilt der höhere Grad. Wenn Umara zum Beispiel beim Wirken von <a class="spell" href="#spell-magisches-geschoss">Magisches Geschoss</a> einen Zauberplatz des 2. Grades nutzt, hat <a class="spell" href="#spell-magisches-geschoss">Magisches Geschoss</a> in diesem Zug den 2. Grad. Der Zauber dehnt sich gewissermaßen aus, um den zugewiesenen Zauberplatz auszufüllen.</p>
<p>Bestimmte Zauber wie etwa <a class="spell" href="#spell-magisches-geschoss">Magisches Geschoss</a> und <a class="spell" href="#spell-wunden-heilen">Wunden heilen</a> haben mächtigere Effekte, wenn sie auf einem höheren Grad gewirkt werden. Diese sind in der Beschreibung des Zaubers aufgeführt.</p>

<div class="boxed" id="box-in-ruestung-zaubern">
<h5>In Rüstung zaubern</h5>
<p>Da das Zauberwirken mentalen Fokus und präzise Gesten erfordert, musst du im Umgang mit der getragenen Rüstung geübt sein, damit du Zauber wirken kannst. Sonst bist du zu sehr abgelenkt und in deinen Bewegungen eingeschränkt.</p>
</div>

<h4>Zaubertricks</h4>
<p>Ein Zaubertrick ist ein Zauber, der jederzeit und ohne Zauberplatz gewirkt werden kann. Außerdem muss er nicht vorbereitet werden. Durch wiederholten Gebrauch hat sich der Zauber so fest ins Gedächtnis des Zauberwirkers eingebrannt und ihn mit der notwendigen Magie durchdrungen, dass er den Effekt jederzeit aufrufen kann. Der Zaubergrad eines Zaubertricks ist 0.</p>
<h4>Rituale</h4>
<p>Bestimmte Zauber haben den besonderen Zusatz „Ritual&quot;. Solche Zauber können nach den üblichen Regeln für das Zauberwirken oder als Ritual gewirkt werden. Das Wirken der Ritualversion eines Zaubers dauert zehn Minuten länger als sonst üblich und verbraucht keinen Zauberplatz. Dies bedeutet auch, dass die Ritualversion nicht auf einem höheren Grad gewirkt werden kann.</p>
<p>Damit ein Zauberwirker einen Zauber als Ritual wirken kann, muss er über das entsprechende Merkmal verfügen. Druiden und Kleriker besitzen beispielsweise ein solches Merkmal. Der Zauberwirker muss den Spruch außerdem vorbereitet oder in seiner Liste bekannter Zauber haben, es sei denn, das Ritual-Merkmal des Charakters gibt das anders an, etwa beim Magier.</p>

<h3 id="einen-zauber-wirken">Einen Zauber wirken</h3>
<p>Wenn ein Charakter einen Zauber wirkt, gelten unabhängig von der Charakterklasse und den Effekten des Zaubers die gleichen Grundregeln.</p>
<p>Jede Zauberbeschreibung beginnt mit einer Reihe von Informationen wie Name, Grad, Schule der Magie, Zeitaufwand, Reichweite, Komponenten und Wirkungsdauer. Der restliche Eintrag beschreibt den Effekt des Zaubers.</p>
<h4>Zeitaufwand</h4>
<p>Für die meisten Zauber ist nur eine Aktion erforderlich, aber bestimmte Zauber erfordern eine Bonusaktion, eine Reaktion oder einen viel größeren Zeitaufwand.</p>
<h5>Bonusaktion</h5>
<p>Ein Zauber, der als Bonusaktion gewirkt wird, ist besonders schnell. Du musst in deinem Zug eine Bonusaktion verwenden, um den Zauber zu wirken. Das geht allerdings nur, wenn du in dem Zug noch keine Bonusaktion verwendet hast. Außer einem Zaubertrick mit einem Zeitaufwand von einer Aktion kannst du in dem Zug keinen weiteren Zauber wirken.</p>
<h5>Reaktionen</h5>
<p>Manche Zauber können als Reaktion gewirkt werden. Das Wirken dieser Zauber dauert nur den Bruchteil einer Sekunde. Sie werden als Reaktion auf ein Ereignis gewirkt Wenn ein Zauber als Reaktion gewirkt werden kann, steht in der Zauberbeschreibung, wann genau das möglich ist.</p>
<h5>Längerer Zeitaufwand</h5>
<p>Bestimmte Zauber (beispielsweise solche, die als Ritual gewirkt werden) sind zeitaufwändiger: Das Wirken dauert mehrere Minuten oder sogar Stunden. Wenn du einen Zauber mit einem größeren Zeitaufwand als eine Aktion oder Reaktion wirkst, musst du in jedem Zug deine Aktion zum Wirken verwenden und während dieser Zeit deine Konzentration aufrechterhalten (siehe Konzentration unten). Wenn deine Konzentration unterbrochen wird, schlägt der Zauber fehl, aber der Zauberplatz wird nicht verbraucht. Wenn du den Zauber erneut wirken willst, musst du von vorne beginnen.</p>
<h4>Reichweite</h4>
<p>Das Ziel eines Zaubers muss sich innerhalb dessen Reichweite befinden. Bei einem Zauber wie <a class="spell" href="#spell-magisches-geschoss">Magisches Geschoss</a> ist das Ziel eine Kreatur. Bei einem Zauber wie <a class="spell" href="#spell-feuerball">Feuerball</a> ist das Ziel der Punkt im Raum, an dem der Feuerball explodiert.</p>
<p>Die meisten Zauber besitzen eine in Metern angegebene Reichweite. Bei manchen Zaubern muss die Zielkreatur (einschließlich des Wirkers) berührt werden. Andere Zauber, etwa <a class="spell" href="#spell-schild">Schild</a>, betreffen nur den Wirker. Diese Zauber haben die Reichweite „Selbst&quot;.</p>
<p>Zauber, deren Effekte kegel- oder linienförmig von dir ausgehen, haben ebenfalls die Reichweite „Selbst&quot;, da der Ursprung des Zaubereffekts bei dir liegen muss (siehe „Wirkungsbereiche&quot;).</p>
<p>Sobald ein Zauber gewirkt wurde, sind seine Effekte nicht mehr durch seine Reichweite beschränkt, sofern die Zauberbeschreibung nichts anderes angibt.</p>
<h4>Komponenten</h4>
<p>Die Komponenten eines Zaubers sind die physischen Voraussetzungen, die du erfüllen musst, um ihn wirken zu. Die Beschreibung jedes Zaubers gibt an, ob verbale Komponenten (V), Gesten (G) oder Materialkomponenten (M) erforderlich sind. Wenn eine oder mehrere Komponenten eines Zaubers fehlen, kannst du ihn nicht wirken.</p>
<h5>Verbal (V)</h5>
<p>Die meisten Zauber erfordern das Intonieren mystischer Worte. Die Worte selbst sind nicht die Quelle der Macht eines Zaubers. Vielmehr bringt die Kombination aus Lauten, Betonung und Resonanz die Fäden der Magie zum Schwingen. Ein Charakter, der geknebelt ist oder sich in einem Bereich der Stille befindet, wo etwa der Zauber Stille aktiv ist, kann keine Zauber mit verbalen Komponenten wirken.</p>
<h5>Geste (G)</h5>
<p>Das Wirken von Zaubern umfasst oft ausladende Gesten oder subtile Fingerbewegungen. Wenn ein Zauber eine Geste als Komponente erfordert, muss der Zauberwirker mindestens eine Hand frei bewegen können, um die entsprechenden Gesten auszuführen.</p>
<h5>Material (M)</h5>
<p>Zum Wirken mancher Zauber werden bestimmte Objekte benötigt, die in Klammern im Komponenteneintrag aufgeführt sind. Ein Charakter kann einen <span class="feature-name">Materialkomponentenbeutel </span>oder einen <span class="feature-name">Zauberfokus </span>(siehe .Ausrüstung&quot;) anstelle der für einen Zauber angegebenen Komponenten verwenden. Sind Kosten für eine Komponente angegeben, muss der Charakter diesen spezifischen Gegenstand besitzen.</p>
<p>Wenn in der Beschreibung eines Zaubers angegeben ist, dass die Materialkomponente durch den Zauber verbraucht wird, muss der Zauberwirker eine separate Komponente für jedes Wirken des Zaubers zur Hand haben.</p>
<p>Ein Zauberwirker benötigt mindestens eine freie Hand, um die Materialkomponenten eines Zaubers oder einen Zauberfokus zu verwenden. Dies kann jedoch dieselbe Hand sein, mit der er die Gesten ausführt.</p>
<h4>Wirkungsdauer</h4>
<p>Die Wirkungsdauer eines Zaubers gibt an, wie lange dessen Effekt andauert. Sie kann Runden, Minuten, Stunden oder sogar Jahre betragen. Manche Zauber sind aktiv, bis sie gebannt oder zerstört werden.</p>
<h5>Unmittelbar</h5>
<p>Der Effekt vieler Zauber tritt unmittelbar ein. Ein solcher Zauber verletzt, heilt, erzeugt oder verändert eine Kreatur oder ein Objekt auf eine Weise, die nicht gebannt werden kann, da die Magie nur einen Augenblick lang existiert.</p>
<h5>Konzentration</h5>
<p>Einige Zauber erfordern, dass du deine Konzentration aufrechterhältst, damit die Magie aktiv bleibt. Wenn du deine Konzentration verlierst, endet ein solcher Zauber.</p>
<p>Ist für die Aufrechterhaltung eines Zaubers Konzentration erforderlich, ist dies bei der Wirkungsdauer vermerkt. Zudem wird angegeben, wie lange du dich auf den Zauber konzentrieren kannst. Du kannst deine Konzentration jederzeit beenden - dazu ist keine Aktion notwendig.</p>
<p>Normale Aktivitäten wie Bewegungen und Angriffe stören die Konzentration nicht Die folgenden Faktoren können die Konzentration stören:</p>
<ul>
<li><span class="feature-name">Du wirkst einen anderen Zauber, der Konzentration erfordert.</span> Du verlierst die Konzentration auf einen Zauber, wenn du einen anderen wirkst, der Konzentration erfordert. Du kannst dich nicht auf zwei Zauber gleichzeitig konzentrieren.</li>
<li><span class="feature-name">Du nimmst Schaden. </span>Nimmst du Schaden, während du dich auf einen Zauber konzentrierst, musst du einen Konstitutionsrettungswurf ausführen, um deine Konzentration aufrechtzuerhalten. Der SG ist 10 oder die Hälfte des erlittenen Schadens, sofern dieser Wert höher ist. Wenn du aus mehreren Quellen Schaden nimmst, etwa durch einen Peil und den Feueratem eines Drachen, musst du für jede Schadensquelle einen eigenen Rettungswurf ausführen.</li>
<li><span class="feature-name">Du bist kampfunfähig oder tot. </span>Du verlierst deine Konzentration, wenn du kampfunfähig wirst oder stirbst.</li>
</ul>
<p>Der SL kann auch festlegen, dass unter bestimmten Umständen - etwa, wenn auf einem sturmgepeitschten Schiff eine Welle über dich hereinbricht - ein SG-10-Konstitutionsrettungswurf erforderlich ist, um die Konzentration aufrechtzuerhalten.</p>
<h4>Ziele</h4>
<p>Bei vielen Zaubern musst du mindestens ein Ziel auswählen. Die Beschreibung eines Zaubers gibt an, ob es sich dabei um Kreaturen, Objekte oder einen Wirkungsbereich (siehe unten) handelt.</p>
<p>Hat ein Zauber keine wahrnehmbare Wirkung, weiß eine Kreatur möglicherweise nicht, dass sie von einem Zauber betroffen wurde. Effekte wie Blitze sind offensichtlich, doch subtilerer Effekte, wie der Versuch, die Gedanken einer Kreatur zu lesen, bleiben normalerweise unbemerkt, sofern in der Beschreibung nichts anderes steht.</p>
<h5>Freie Sicht auf das Ziel</h5>
<p>Damit du etwas als Ziel wählen kannst, musst du es sehen können - es darf sich also nicht in vollständiger Deckung befinden.</p>
<p>Platzierst du einen Wirkungsbereich an einem Punkt, den du nicht sehen kannst, und liegt ein Hindernis wie eine Mauer zwischen dir und dem Punkt, wird der Ursprung auf der dir zugewandten Seite des Hindernisses platziert.</p>
<h5>Der Zauberwirker als Ziel</h5>
<p>Wenn das Ziel eines Zaubers eine Kreatur deiner Wahl ist, kannst du dich selbst als Ziel bestimmen, es sei denn, die Kreatur muss feindlich oder spezifisch eine andere Kreatur als du selbst sein. Wenn du dich im Wirkungsbereich eines Zaubers befindest, den du gewirkt hast, dann kannst du dich selbst als Ziel wählen.</p>

<h4>Wirkungsbereiche</h4>
<p>Zauber wie <a class="spell" href="#spell-brennende-haende">Brennende Hände</a> und <a class="spell" href="#spell-kaeltekegel">Kältekegel</a> decken eine Fläche ab. Dadurch können sie mehrere Kreaturen auf einmal treffen.</p>
<p>Die Beschreibung eines Zaubers enthält den Wirkungsbereich, der üblicherweise eine von fünf Formen hat: Kegel, Kugel, Linie, Würfel oder Zylinder. Jeder Wirkungsbereich hat einen Ursprung, den Punkt, von dem die Energie des Zaubers ausgeht. Die Regeln für jede Form legen fest, wo sich der Ursprung befindet. Üblicherweise ist der Ursprung ein Punkt im Raum. Manche Zauber haben jedoch einen Bereich, dessen Ursprung eine Kreatur oder ein Gegenstand ist.</p>
<p>Der Effekt eines Zaubers breitet sich in geraden Linien vom Ursprung aus. Lässt sich keine gerade Linie vom Ursprung zu einem Punkt innerhalb des Wirkungsbereichs ziehen, betrifft der Zauber diesen Punkt nicht. Eine solche Linie kann nur von einem Hindernis blockiert werden, das vollständige Deckung bietet.</p>
<h5>Kegel</h5>
<p>Ein Kegel breitet sich von seinem Ursprung in eine von dir gewählte Richtung aus. Die Breite eines Kegels an einem bestimmten Punkt entlang seiner Länge entspricht dem Abstand dieses Punkts vom Ursprung. Die maximale Länge des Kegels entspricht seinem Wirkungsbereich.</p>
<p>Der Ursprung eines Kegels ist nicht im Wirkungsbereich des Zaubers enthalten, es sei denn, du gibst dies ausdrücklich an.</p>
<h5>Linie</h5>
<p>Eine Linie verläuft vom Ursprung gerade bis zu ihrer maximalen Länge und deckt einen Bereich entsprechend ihrer Breite ab.</p>
<p>Der Ursprung einer Linie ist nicht im Wirkungsbereich des Zaubers enthalten, es sei denn, du gibst dies ausdrücklich an.</p>
<h5>Sphäre</h5>
<p>Du entscheidest, wo der Ursprung einer Sphäre liegt. Die Sphäre breitet sich dann von diesem Punkt aus. Die Größe der Sphäre wird durch ihren Radius in Metern beschrieben.</p>
<p>Der Ursprung der Sphäre ist im Wirkungsbereich des Zaubers enthalten.</p>
<h5>Würfel</h5>
<p>Du legst den Ursprung eines Würfels fest. Er liegt dabei an einem beliebigen Punkt auf einer der Seiten des Würfels. Die Größe des Würfels wird durch dessen Kantenlänge beschrieben.</p>
<p>Der Ursprung eines Würfels ist nicht im Wirkungsbereich des Zaubers enthalten, es sei denn, du gibst dies ausdrücklich an.</p>
<h5>Zylinder</h5>
<p>Der Ursprung eines Zylinders ist der Mittelpunkt eines Kreises mit einem bestimmten Radius, der in der Beschreibung des Zaubers angegeben ist. Der Kreis muss sich entweder auf dem Boden oder auf der Höhe des Zaubereffekts befinden. Der Effekt eines Zaubers breitet sich in einem Zylinder in geraden Linien vom Ursprung zum Umfang des Kreises aus, der die Basis des Zylinders bildet. Danach schießt die Energie von der Basis nach oben oder von der Oberseite nach unten, und zwar in einer Entfernung, die der Höhe des Zylinders entspricht.</p>
<p>Der Ursprung des Zylinders ist im Wirkungsbereich des Zaubers enthalten.</p>
<h4>Rettungswürfe</h4>
<p>Die Beschreibung vieler Zauber gibt an, dass das Ziel einen Rettungswurf ausführen kann, um den Effekt ganz oder teilweise zu verhindern. Der Zauber gibt das Attribut an, das das Ziel für den Rettungswurf nutzt, und er legt fest, was bei einem Erfolg oder Misserfolg geschieht.</p>
<p>Der SG, um einem deiner Zauber zu widerstehen, beträgt 8 + dein Zauberwirken-Attributsmodifikator + dein Übungsbonus + etwaige besondere Modifikatoren.</p>
<h4>Angriffswürfe</h4>
<p>Bei manchen Zaubersprüchen muss der Zauberwirker einen Angriffswurf ausführen, um festzustellen, ob der Zauber das Ziel trifft. Dein Angriffsbonus mit Zauberangriffen entspricht deinem Zauberwirken-Attributsmodifikator + deinem Übungsbonus.</p>
<p>Bei den meisten Zaubern, die Angriffswürfe benötigen, handelt es sich um Fernkampfangriffe. Bedenke, dass du bei Fernkampfangriffen im Nachteil bist, wenn du dich im Abstand von bis zu 1,5 Metern von einer feindlich gesinnten Kreatur befindest, dich sehen kann und kampffähig ist.</p>

<div class="boxed" id="box-die-schulen-der-magie">
<h5>Die Schulen der Magie</h5>
<p>Magische Akademien ordnen Zauber in acht Kategorien, welche sie Schulen der Magie nennen. Gelehrte, besonders Magier, wenden diese Kategorien auf alle Zauber an. Sie glauben, dass jegliche Magie letztendlich auf die gleiche Art und Weise funktioniert, ob sie nun aus striktem Studium stammt oder von einer Gottheit verliehen wurde.</p>
<p>Die Schulen der Magie helfen dabei, Zauber zu beschreiben. Für sie gelten keine eigenen Spielregeln, auch wenn sich einige Regeln auf sie beziehen.</p>
<p><strong>Bannzauber</strong> sind von Natur aus schützend, obwohl sie auch offensiv eingesetzt werden können. Sie erschaffen magische Barrieren, negieren schädliche Effekte, verletzen Eindringlinge oder verbannen Kreaturen auf andere Ebenen der Existenz.</p>
<p><strong>Beschwörungszauber</strong> transportieren Gegenstände oder Kreaturen von einem Ort zum anderen. Einige Zauber beschwören Kreaturen oder Gegenstände an die Seite des Zauberwirkers, während er sich mit anderen an einen entfernten Ort teleportieren kann. Einige Beschwörungszauber erschaffen Gegenstände oder Effekte aus dem Nichts.</p>
<p><strong>Erkenntniszauber</strong> offenbaren Informationen: lange vergessene Geheimnisse, Blicke in die Zukunft, die Position versteckter Gegenstände, die Wahrheit hinter Illusionen, oder Visionen weit entfernter Personen und Orte.</p>
<p><strong>Hervorrufungszauber</strong> manipulieren magische Energie, um den gewünschten Effekt zu erzeugen. Einige erschaffen Feuer oder Blitze. Andere kanalisieren positive Energie, um Wunden zu heilen.</p>
<p><strong>Illusionszauber</strong> täuschen die Sinne und verwirren die Gedanken anderer. Sie lassen Kreaturen Dinge sehen, die gar nicht da sind, oder Dinge übersehen, die es wirklich gibt. Sie rufen Phantomlaute oder Erinnerungen hervor, die nie geschehen sind. Einige Illusionszauber erschaffen Trugbilder, die jede Kreatur sehen kann, aber die tückischsten Illusionszauber pflanzen ein Bild direkt in den Verstand einer Kreatur.</p>
<p><strong>Nekromantiezauber</strong> lenken die Energien von Leben und Tod. Diese Zauber können zusätzliche Reserven an Lebenskraft gewähren, einer anderen Kreatur die Lebensenergie entziehen, Untote erschaffen oder sogar Tote wieder zum Leben erwecken.</p>
<p>Untote mit Nekromantiezaubern wie <a class="spell" href="#spell-tote-beleben">Tote beleben</a> zu erschaffen ist keine gute Tat und nur böse Zauberwirker wenden diese Zauber regelmäßig an.</p>
<p><strong>Verwandlungszauber</strong> verändern die Eigenschaften von Kreaturen, Gegenständen oder der Umgebung. Sie können einen Gegner in eine harmlose Kreatur verwandeln, einen Verbündeten stärken, einen Gegenstand auf Befehl des Zauberwirkers bewegen oder die Heilungsfähigkeiten einer Kreatur beschleunigen, damit sie sich von ihren Wunden erholt.</p>
<p><strong>Verzauberungszauber</strong> beeinflussen den Verstand einer anderen Person und manipulieren oder kontrollieren ihr Verhalten. Diese Zauber können Feinde dazu bringen, den Zauberwirker als Freund zu sehen, Kreaturen zu einer bestimmten Aktion zwingen oder sogar eine andere Kreatur wie eine Marionette zu kontrollieren.</p>
</div>

<h4>Magische Effekte kombinieren</h4>
<p>Die Effekte verschiedener Zauber addieren sich, wenn sich ihre Wirkungsdauer überschneidet. Im Gegensatz dazu werden die Effekte des gleichen Zaubers, der mehrmals gewirkt wird, nicht kombiniert. Stattdessen wird der stärkste Effekt - zum Beispiel der höchste Bonus - dieser Zauber angewendet, solange sich ihre Wirkungsdauer überschneidet.</p>
<p>Wirken zum Beispiel zwei Kleriker <a class="spell" href="#spell-segnen">Segnen</a> auf dasselbe Ziel, erhält der Charakter nur einmal den Vorteil des Zaubers. Er erhält keine zwei Bonuswürfel.</p>