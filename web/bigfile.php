<!DOCTYPE html>
<html lang="de">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SRD 5.1 de</title>
    
    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/popper/popper.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="lib/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/brands.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/solid.min.css">
    
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap.D3.min.css">
    
    <script src="js/web.js?v=<?=filemtime('js/web.js')?>"></script>
    <link rel="stylesheet" href="css/web.css?v=<?=filemtime('css/web.css')?>">
    
    <script src="js/bootstrap-dropdown-multilevel.js?v=<?=filemtime('js/bootstrap-dropdown-multilevel.js')?>"></script>
    <link rel="stylesheet" href="css/bootstrap-dropdown-multilevel.css?v=<?=filemtime('css/bootstrap-dropdown-multilevel.css')?>">
    
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
</head>
<body id="top">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col p-0">
                <? 
                    $openrpg_de_branding = true;
                    include("navigation-top.html");
                ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col pt-3">
                <div id="where-am-i"></div>
                <div id="page">
                    <div class="container-fluid" id="content">
                        <div class="sidebar-toggle m-3 text-white" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavigation" aria-controls="offcanvasNavigation">
                            <i class="fa-solid fa-angles-left"></i>
                        </div>
                        
                        <h1>Systemreferenzdokument 5.1 <strong>de</strong></h1>
                        <p class="text-center">f&uuml;r <i class="fa-brands fa-d-and-d"></i> 5e</p>
                        
                        <p>Diese Website enth&auml;lt alle Regeln, die von <a href="https://www.wizards.com">Wizards of the Coast</a> im <a href="https://dnd.wizards.com/de/resources/systems-reference-document">SRD 5.1</a> unter den Bedingungen der <a href="https://creativecommons.org/licenses/by/4.0/legalcode.de">CC BY-Lizenz</a> zur Verf&uuml;gung gestellt werden.</p>
                        
                        <? include("rechtliche-informationen.html") ?>
                                               
                        <p class="text-center mt-5" title="Creative Commons Namensnennung 4.0 International">
                            <a href="https://creativecommons.org/licenses/by/4.0/legalcode.de"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                	 width="64px" height="64px" viewBox="5.5 -3.5 64 64" enable-background="new 5.5 -3.5 64 64" xml:space="preserve">
                                <g>
                                	<circle fill="#FFFFFF" cx="37.785" cy="28.501" r="28.836"/>
                                	<path d="M37.441-3.5c8.951,0,16.572,3.125,22.857,9.372c3.008,3.009,5.295,6.448,6.857,10.314
                                		c1.561,3.867,2.344,7.971,2.344,12.314c0,4.381-0.773,8.486-2.314,12.313c-1.543,3.828-3.82,7.21-6.828,10.143
                                		c-3.123,3.085-6.666,5.448-10.629,7.086c-3.961,1.638-8.057,2.457-12.285,2.457s-8.276-0.808-12.143-2.429
                                		c-3.866-1.618-7.333-3.961-10.4-7.027c-3.067-3.066-5.4-6.524-7-10.372S5.5,32.767,5.5,28.5c0-4.229,0.809-8.295,2.428-12.2
                                		c1.619-3.905,3.972-7.4,7.057-10.486C21.08-0.394,28.565-3.5,37.441-3.5z M37.557,2.272c-7.314,0-13.467,2.553-18.458,7.657
                                		c-2.515,2.553-4.448,5.419-5.8,8.6c-1.354,3.181-2.029,6.505-2.029,9.972c0,3.429,0.675,6.734,2.029,9.913
                                		c1.353,3.183,3.285,6.021,5.8,8.516c2.514,2.496,5.351,4.399,8.515,5.715c3.161,1.314,6.476,1.971,9.943,1.971
                                		c3.428,0,6.75-0.665,9.973-1.999c3.219-1.335,6.121-3.257,8.713-5.771c4.99-4.876,7.484-10.99,7.484-18.344
                                		c0-3.543-0.648-6.895-1.943-10.057c-1.293-3.162-3.18-5.98-5.654-8.458C50.984,4.844,44.795,2.272,37.557,2.272z M37.156,23.187
                                		l-4.287,2.229c-0.458-0.951-1.019-1.619-1.685-2c-0.667-0.38-1.286-0.571-1.858-0.571c-2.856,0-4.286,1.885-4.286,5.657
                                		c0,1.714,0.362,3.084,1.085,4.113c0.724,1.029,1.791,1.544,3.201,1.544c1.867,0,3.181-0.915,3.944-2.743l3.942,2
                                		c-0.838,1.563-2,2.791-3.486,3.686c-1.484,0.896-3.123,1.343-4.914,1.343c-2.857,0-5.163-0.875-6.915-2.629
                                		c-1.752-1.752-2.628-4.19-2.628-7.313c0-3.048,0.886-5.466,2.657-7.257c1.771-1.79,4.009-2.686,6.715-2.686
                                		C32.604,18.558,35.441,20.101,37.156,23.187z M55.613,23.187l-4.229,2.229c-0.457-0.951-1.02-1.619-1.686-2
                                		c-0.668-0.38-1.307-0.571-1.914-0.571c-2.857,0-4.287,1.885-4.287,5.657c0,1.714,0.363,3.084,1.086,4.113
                                		c0.723,1.029,1.789,1.544,3.201,1.544c1.865,0,3.18-0.915,3.941-2.743l4,2c-0.875,1.563-2.057,2.791-3.541,3.686
                                		c-1.486,0.896-3.105,1.343-4.857,1.343c-2.896,0-5.209-0.875-6.941-2.629c-1.736-1.752-2.602-4.19-2.602-7.313
                                		c0-3.048,0.885-5.466,2.658-7.257c1.77-1.79,4.008-2.686,6.713-2.686C51.117,18.558,53.938,20.101,55.613,23.187z"/>
                                </g>
                                </svg><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                	 width="64px" height="64px" viewBox="5.5 -3.5 64 64" enable-background="new 5.5 -3.5 64 64" xml:space="preserve">
                                <g>
                                	<circle fill="#FFFFFF" cx="37.637" cy="28.806" r="28.276"/>
                                	<g>
                                		<path d="M37.443-3.5c8.988,0,16.57,3.085,22.742,9.257C66.393,11.967,69.5,19.548,69.5,28.5c0,8.991-3.049,16.476-9.145,22.456
                                			C53.879,57.319,46.242,60.5,37.443,60.5c-8.649,0-16.153-3.144-22.514-9.43C8.644,44.784,5.5,37.262,5.5,28.5
                                			c0-8.761,3.144-16.342,9.429-22.742C21.101-0.415,28.604-3.5,37.443-3.5z M37.557,2.272c-7.276,0-13.428,2.553-18.457,7.657
                                			c-5.22,5.334-7.829,11.525-7.829,18.572c0,7.086,2.59,13.22,7.77,18.398c5.181,5.182,11.352,7.771,18.514,7.771
                                			c7.123,0,13.334-2.607,18.629-7.828c5.029-4.838,7.543-10.952,7.543-18.343c0-7.276-2.553-13.465-7.656-18.571
                                			C50.967,4.824,44.795,2.272,37.557,2.272z M46.129,20.557v13.085h-3.656v15.542h-9.944V33.643h-3.656V20.557
                                			c0-0.572,0.2-1.057,0.599-1.457c0.401-0.399,0.887-0.6,1.457-0.6h13.144c0.533,0,1.01,0.2,1.428,0.6
                                			C45.918,19.5,46.129,19.986,46.129,20.557z M33.042,12.329c0-3.008,1.485-4.514,4.458-4.514s4.457,1.504,4.457,4.514
                                			c0,2.971-1.486,4.457-4.457,4.457S33.042,15.3,33.042,12.329z"/>
                                	</g>
                                </g>
                                </svg></a>
                        </p>
                        
                        <h4 class="mt-5">API, Quellcode und Mitarbeit</h4>
                        
                        <p><strong>API.</strong> Unter <a href="https://openrpg.de/srd/5e/de/api/">openrpg.de/srd/5e/de/api</a> steht eine API zur Verfügung, die Monster, Magische Gegenstände und Zaubersprüche des SRD 5.1 de maschinenlesbar zur Verfügung stellt.</p>
                        
                        <p><strong>Quellcode.</strong> Das Projekt-Repository findest du auf Codeberg: <a href="https://codeberg.org/nesges/SRD-5.1-DE">nesges/SRD-5.1-DE</a>.</p>
                        
                        <p><strong>Mitarbeit.</strong> Mitarbeit ist willkommen! Für kleinere Korrekturen und Ergänzungen sind <a href="https://codeberg.org/nesges/SRD-5.1-DE/pulls">Pull Requests</a> und <a href="https://codeberg.org/nesges/SRD-5.1-DE/issues">Issues</a> der geringste Aufwand, daher bitte nach Möglichkeit diese benutzen. Wenn du dich im größeren Umfang einbringen möchtest, dann setze dich <a href="https://dnddeutsch.de/kontakt">mit mir in Verbindung</a>.
                        
                        <? ob_start() ?>
                    
                        <? include("voelker/index.html") ?>
                        <? include("voelker/elf.html") ?>
                        <? include("voelker/halbling.html") ?>
                        <? include("voelker/mensch.html") ?>
                        <? include("voelker/zwerg.html") ?>
                        <? include("voelker/drachenbluetige.html") ?>
                        <? include("voelker/gnom.html") ?>
                        <? include("voelker/halbelf.html") ?>
                        <? include("voelker/halbork.html") ?>
                        <? include("voelker/tiefling.html") ?>
                        
                        <h2 id="klassen">Klassen</h2>
                        
                        <? include("klassen/barbar.html") ?>
                        <? include("klassen/barde.html") ?>
                        <? include("klassen/druide.html") ?>
                        <? include("klassen/hexenmeister.html") ?>
                        <? include("klassen/kaempfer.html") ?>
                        <? include("klassen/kleriker.html") ?>
                        <? include("klassen/magier.html") ?>
                        <? include("klassen/moench.html") ?>
                        <? include("klassen/paladin.html") ?>
                        <? include("klassen/schurke.html") ?>
                        <? include("klassen/waldlaeufer.html") ?>
                        <? include("klassen/zauberer.html") ?>
                        
                        <? include("nach-der-ersten-stufe/index.html") ?>
                        
                        <? include("ausruestung/index.html") ?>
                        
                        <? include("talente/index.html") ?>
                        
                        <? include("attributswerte-verwenden/index.html") ?>
                        
                        <? include("zauberwirken/index.html") ?>
                        <? include("zauberwirken/zauberlisten.html") ?>
                        <? include("zauberwirken/zauber-des-barden.html") ?>
                        <? include("zauberwirken/zauber-des-druiden.html") ?>
                        <? include("zauberwirken/zauber-des-hexenmeisters.html") ?>
                        <? include("zauberwirken/zauber-des-klerikers.html") ?>
                        <? include("zauberwirken/zauber-des-magiers.html") ?>
                        <? include("zauberwirken/zauber-des-paladins.html") ?>
                        <? include("zauberwirken/zauber-des-waldlaeufers.html") ?>
                        <? include("zauberwirken/zauber-des-zauberers.html") ?>
                        <? include("zauberwirken/beschreibungen-der-zauber.html") ?>
                        
                        <? include("fallen/index.html") ?>
                        <? include("krankheiten/index.html") ?>
                        <? include("wahnsinn/index.html") ?>
                        <? include("gegenstaende/index.html") ?>
                        <? include("gifte/index.html") ?>
                        
                        <? include("magische-gegenstaende/index.html") ?>
                        <? include("magische-gegenstaende/a-z.html") ?>
                        <? include("magische-gegenstaende/intelligente-magische-gegenstaende.html") ?>
                        
                        <? include("monster/index.html") ?>
                        <? include("monster/monster.html") ?>
                        
                        <? include("zustaende/index.html") ?>
                        <? include("pantheons/index.html") ?>
                        <? include("ebenen/index.html") ?>
                        
                        <? include("monster/kreaturen.html") ?>
                        
                        <? include("monster/nsc.html") ?>
                        
                        <h2>Verzeichnisse</h2>
                        
                        <? include("tabellenverzeichnis.html") ?>
                        <? include("kastenverzeichnis.html") ?>
                        
                        <? 
                            $html = ob_get_contents();
                            ob_end_clean();
                            
                            // just to make some heads explode
                            $html = preg_replace('#(d)er SL#i', '<span title="die Spielleitung">\1ie SL</span>', $html);
                            $html = preg_replace('#(d)en SL#i', '<span title="die Spielleitung">\1ie SL</span>', $html);
                            $html = preg_replace('#(d)em SL#i', '<span title="die Spielleitung">\1er SL</span>', $html);
                            $html = preg_replace('#(d)es SL#i', '<span title="die Spielleitung">\1er SL</span>', $html);
                            $html = preg_replace('#(d)ein SL#i', '<span title="die Spielleitung">\1eine SL</span>', $html);
                            
                            $html = preg_replace('#der Spielleiter#', 'die Spielleitung', $html);
                            $html = preg_replace('#deinem Spielleiter#', 'deiner Spielleitung', $html);
                            
                            $html = preg_replace('#(ein) Spieler#i', '\1e Spieler&ast;in', $html);
                            $html = preg_replace('#(d)er Spieler#i', '\1ie Spieler&ast;in', $html);
                            $html = preg_replace('#andere Spieler#i', 'andere Spieler&ast;innen', $html);
                            $html = preg_replace('#die Spieler#i', 'die Spielenden', $html);
                            
                            
                            $html = preg_replace('#(<table)#', '<div style="overflow:auto">\1', $html);
                            $html = preg_replace('#(</table>)#', '\1</div>', $html);
                            
                            // add link-icon to headlines with id
                            $html = preg_replace('#(<h([1-6])[^>]*id=(["\'])(.*?)\3[^>]*>.*?)(</h\2>)#', '\1&nbsp;<a href="#\4" class="permalink" title="Permalink zu dieser &Uuml;berschrift" data-bs-toggle="tooltip"><i class="fa-solid fa-link"></i></a>\5', $html);
                            $html = preg_replace('#(<div[^<]*id=(["\'])([^\2>]*)\2.*?<h5>.*?)(</h5>)#s', '\1&nbsp;<a href="#\3" class="permalink" title="Permalink zu diesem Abschnitt" data-bs-toggle="tooltip"><i class="fa-solid fa-link"></i></a>\4', $html);
                            
                            print $html;
                        ?>
                        
                        <? include("changelog.html") ?>
                    </div>
                    
    <? include("foot.php") ?>
    
    <? include("navigation-sidebar.html") ?>

    <div id="popup" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Schlie&szlig;en"></button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <div class="permalink-display text-left"></div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schlie&szlig;en</button>
                </div>
            </div>
        </div>
    </div>

    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasSearchResults" aria-labelledby="offcanvasSearchResultsLabel" data-bs-scroll="true" data-bs-backdrop="false">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasSearchResultsLabel">-</h5>
            <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Schlie&szlig;en"></button>
        </div>
        <div class="offcanvas-body">
        </div>
    </div>

    <div id="d20"></div>
</body>
</html>