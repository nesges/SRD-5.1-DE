::::
<div class='wide'><center>

# Monsterkompendium 5e 
##### SRD 5.1 de / openrpg.de / dnddeutsch.de
</center></div>
::::::::::::
{{monster,frame,wide
## Aboleth
*Große Aberration, rechtschaffen böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 135 (18W10+36)
**Speed**          :: 3 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|9  (-1)|15 (+2)|18 (+4)|15 (+2)|18 (+4)|
___
**Rettungswürfe**           :: Kon +6, Int +8, Wei +6
**Fertigkeiten**            :: Geschichte +12, Wahrnehmung +10
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 20
**Sprachen**                :: Telepathie auf 36 m, Tiefensprache
**Herausforderung**         :: 10 (5.900 EP)
___
***Amphibisch.*** Der Aboleth kann Luft und Wasser atmen.
:
***Schleimwolke.*** Unter Wasser ist der Aboleth von transformativem Schleim umgeben. Eine Kreatur, die den Aboleth berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, muss einen SG-14-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, so ist die Kreatur 1W4 Stunden lang krank. Die kranke Kreatur kann nur unter Wasser atmen.
:
***Sondierende Telepathie.*** Wenn eine Kreatur telepathisch mit dem Abolethen kommuniziert, erfährt der Aboleth die tiefsten Wünsche der Kreatur, sofern er sie sehen kann.
:
### Aktionen
***Mehrfachangriff.*** Der Aboleth führt drei Tentakelangriffe aus.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (3W6+5) Wuchtschaden.
:
***Tentakel.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 12 (2W6+5) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-14-Konstitutionsrettungswurf bestehen, oder es wird krank. Die Krankheit hat eine Minute lang keinen Effekt und kann von jeder Magie entfernt werden, die Krankheiten heilt. Nach einer Minute wird die Haut der kranken Kreatur durchscheinend und schleimig. Die Kreatur kann keine Trefferpunkte zurückerhalten, sofern sie sich nicht unter Wasser befindet, und die Krankheit kann nur durch Heilung oder einen anderen Heilzauber des 6. Grades oder höher entfernt werden. Wenn die Kreatur sich nicht im Wasser befindet, erleidet sie alle zehn Minuten 6 (1W12) Säureschaden, sofern ihre Haut nicht nass gemacht wird, bevor zehn Minuten vergangen sind.
:
***Versklaven (3-mal täglich).*** Der Aboleth zielt auf eine Kreatur im Abstand von bis zu neun Metern, die er sehen kann. Das Ziel muss einen SG-14-Weisheitsrettungswurf bestehen, oder sie ist vom Aboleth magisch bezaubert, bis der Aboleth stirbt oder sich auf eine andere Existenzebene als das Ziel begibt. Das bezauberte Ziel steht unter der Kontrolle des Abolethen und kann keine Reaktionen ausführen, und der Aboleth und das Ziel können über beliebige Distanzen telepathisch miteinander kommunizieren. Wenn das bezauberte Ziel Schaden erleidet, kann es den Rettungswurf wiederholen. Bei einem Erfolg endet der Effekt. Höchstens einmal alle 24 Stunden kann das Ziel den Rettungswurf ebenfalls wiederholen, wenn es mindestens 1,6 Kilometer vom Abolethen entfernt ist.
:
### Legendäre Aktionen
Der Aboleth kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Sie können jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Aboleth erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Psychische Absaugung (kostet 2 Aktionen).*** Eine vom Abolethen bezauberte Kreatur erleidet 10 (3W6) psychischen Schaden, und der Aboleth erhält Trefferpunkte in Höhe des Schadens zurück, den die Kreatur erleidet.
:
***Entdecken.*** Der Aboleth führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzstreich.*** Der Aboleth führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Adeliger
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 15 (Brustplatte)
**Trefferpunkte**  :: 9 (2W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|12 (+1)|11 (+0)|12 (+1)|14 (+2)|16 (+3)|
___
**Fertigkeiten**            :: Motiv erkennen +4, Täuschen +5, Überzeugen +5
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Zwei beliebige Sprachen
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Rapier.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W8+1) Stichschaden.
:
### Reaktionen
***Parieren.*** Der Adelige erhöht seine RK gegen einen Nahkampfangriff, der treffen würde, um 2. Dazu muss der Adelige den Angreifer sehen können und eine Nahkampfwaffe führen.
:
}}


{{monster,frame
## Adler
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 3 (1W6)
**Speed**          :: 3 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|15 (+2)|10 (+0)|2  (-4)|14 (+2)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfe Sicht.*** Der Adler ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Krallen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Akolyth
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 9 (2W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|10 (+0)|10 (+0)|10 (+0)|14 (+2)|11 (+0)|
___
**Fertigkeiten**            :: Heilkunde +4, Religion +2
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 1/4 (50 EP)
___
***Zauberwirken.*** Der Akolyth ist ein Zauberwirker der 1. Stufe. Sein Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 12, +4 auf Treffer mit Zauberangriffen). Der Akolyth hat folgende Klerikerzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Heilige Flamme*](https://openrpg.de/srd/5e/de/#spell-heilige-flamme)[*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Thaumaturgie*](https://openrpg.de/srd/5e/de/#spell-thaumaturgie)<br>
1&period; Grad (3 Plätze): [*Heiligtum*](https://openrpg.de/srd/5e/de/#spell-heiligtum)[*Segnen*](https://openrpg.de/srd/5e/de/#spell-segnen)[*Wunden heilen*](https://openrpg.de/srd/5e/de/#spell-wunden-heilen)
:
### Aktionen
***Knüppel.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Androsphinx
*Große Monstrosität, rechtschaffen neutral*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 199 (19W10+95)
**Speed**          :: 12 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|10 (+0)|20 (+5)|16 (+3)|18 (+4)|23 (+6)|
___
**Rettungswürfe**           :: Ges +6, Kon +11, Int +9, Wei +10
**Fertigkeiten**            :: Arkane Kunde +9, Religion +15, Wahrnehmung +10
**Schadensimmunitäten**     :: Psychisch; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Bezaubert, Verängstigt
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 20
**Sprachen**                :: Gemeinsprache, Sphinxisch
**Herausforderung**         :: 17 (18.000 EP)
___
***Magische Waffen.*** Die Waffenangriffe der Sphinx sind magisch.
:
***Undurchschaubar.*** Die Sphinx ist gegen alle Effekte immun, die ihre Emotionen oder Gedanken lesen würden, sowie gegen alle Erkenntniszauber, die sie zurückweist. Weisheitswürfe (Motiv erkennen) zum Bestimmen der Absichten oder der Aufrichtigkeit der Sphinx sind im Nachteil.
:
***Zauberwirken.*** Die Sphinx ist ein Zauberwirker der 12. Stufe. Ihr Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 18, +10 auf Treffer mit Zauberangriffen). Sie benötigt keine Materialkomponenten, um ihre Zauber zu wirken. Die Sphinx hat folgende Klerikerzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Heilige Flamme*](https://openrpg.de/srd/5e/de/#spell-heilige-flamme)[*Thaumaturgie*](https://openrpg.de/srd/5e/de/#spell-thaumaturgie)[*Verschonung der Sterbenden*](https://openrpg.de/srd/5e/de/#spell-verschonung-der-sterbenden)<br>
1&period; Grad (4 Plätze): [*Befehl*](https://openrpg.de/srd/5e/de/#spell-befehl)[*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)<br>
2&period; Grad (3 Plätze): [*Schwache Genesung*](https://openrpg.de/srd/5e/de/#spell-schwache-genesung)[*Zone der Wahrheit*](https://openrpg.de/srd/5e/de/#spell-zone-der-wahrheit)<br>
3&period; Grad (3 Plätze): [*Magie bannen*](https://openrpg.de/srd/5e/de/#spell-magie-bannen)[*Zungen*](https://openrpg.de/srd/5e/de/#spell-zungen)<br>
4&period; Grad (3 Plätze): [*Bewegungsfreiheit*](https://openrpg.de/srd/5e/de/#spell-bewegungsfreiheit)[*Verbannung*](https://openrpg.de/srd/5e/de/#spell-verbannung)<br>
5&period; Grad (2 Plätze): [*Flammenschlag*](https://openrpg.de/srd/5e/de/#spell-flammenschlag)[*Vollständige Genesung*](https://openrpg.de/srd/5e/de/#spell-vollstaendige-genesung)<br>
6&period; Grad (1 Platz): [*Heldenmahl*](https://openrpg.de/srd/5e/de/#spell-heldenmahl)
:
### Aktionen
***Mehrfachangriff.*** Die Sphinx führt zwei Klauenangriffe aus.
:
***Klauen.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 17 (2W10+6) Hiebschaden.
:
***Brüllen (3-mal täglich).*** Die Sphinx stößt ein magisches Brüllen aus. Jedes Mal, wenn sie brüllt, bevor sie eine lange Rast beendet, ist das Brüllen lauter, und der Effekt ist ein anderer wie unten beschrieben. Jede Kreatur im Abstand von bis zu 150 Metern von der Sphinx, die das Brüllen hören kann, muss einen Rettungswurf ausführen.
:
***Erstes Brüllen.*** Jede Kreatur, deren SG-18-Weisheitsrettungswurf scheitert, ist eine Minute lang verängstigt. Eine verängstigte Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
***Zweites Brüllen.*** Jede Kreatur, deren SG-18-Weisheitsrettungswurf scheitert, ist eine Minute lang taub und verängstigt. Eine verängstigte Kreatur ist gelähmt. Sie kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
***Drittes Brüllen.*** Jede Kreatur muss einen SG-18-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, so erleidet sie 44 (8W10) Schallschaden und wird umgestoßen. Bei einem erfolgreichen Rettungswurf erleidet die Kreatur halb so viel Schaden und wird nicht umgestoßen.
:
### Legendäre Aktionen
Die Sphinx kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Sie kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Die Sphinx erhält verbrauchte legendäre Aktionen am Anfang ihres Zugs zurück.
:
***Teleportieren (kostet 2 Aktionen).*** Die Sphinx teleportiert sich und sämtliche Ausrüstung, die sie trägt oder hält, magisch bis zu 36 Meter weit an eine freie Stelle, die sie sehen kann.
:
***Zauber wirken (kostet 3 Aktionen).*** Die Sphinx wirkt einen Zauber von ihrer Liste vorbereiteter Zauber, wobei sie wie gewöhnlich einen Zauberplatz verwendet.
:
***Klauenangriff.*** Die Sphinx führt einen Klauenangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Ankheg
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung), 11 in liegender Position
**Trefferpunkte**  :: 39 (6W10+6)
**Speed**          :: 9 m, Graben 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|11 (+0)|13 (+1)|1  (-5)|13 (+1)|6  (-2)|
___
**Sinne**                   :: Dunkelsicht 18 m, Erschütterungssinn 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden plus 3 (1W6) Säureschaden. Ist das Ziel eine höchstens große Kreatur, so wird sie gepackt (Rettungswurf-SG 13). Solange der Ankheg eine Kreatur gepackt hält, kann er nur diese Kreatur beißen und ist bei entsprechenden Angriffswürfen im Vorteil.
:
***Säure versprühen (Aufladung 6).*** Der Ankheg versprüht Säure in einem neun Meter langen und 1,5 Meter breiten Bereich, sofern er gerade keine Kreatur gepackt hat. Jede Kreatur in dieser Linie muss einen SG-13-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 10 (3W6) Säureschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Assassine
*Mittelgroßer Humanoide (jedes Volk), jede nichtgute Gesinnung*
___
**Rüstungsklasse** :: 15 (beschlagenes Leder)
**Trefferpunkte**  :: 78 (12W8+24)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|16 (+3)|14 (+2)|13 (+1)|11 (+0)|10 (+0)|
___
**Rettungswürfe**           :: Ges +6, Int +4
**Fertigkeiten**            :: Akrobatik +6, Heimlichkeit +9, Täuschen +3, Wahrnehmung +3
**Schadensresistenzen**     :: Gift
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Diebessprache und zwei weitere beliebige Sprachen
**Herausforderung**         :: 8 (3.900 EP)
___
***Attentat.*** Während seines ersten Zugs ist der Assassine bei Angriffswürfen gegen jede Kreatur, die noch keinen Zug ausgeführt hat, im Vorteil. Jeder Treffer des Assassinen gegen eine überraschte Kreatur ist kritisch.
:
***Entrinnen.*** Wenn der Assassine von einem Effekt profitiert, der es ihm ermöglicht, einen Geschicklichkeitsrettungswurf auszuführen, damit er nur den halben Schaden erleidet, so erleidet er stattdessen bei einem Erfolg gar keinen Schaden und bei Misserfolg nur den halben Schaden.
:
***Hinterhältiger Angriff.*** Einmal pro Zug bewirkt der Assassine zusätzlich 14 (4W6) Schaden, wenn er ein Ziel mit einem Waffenangriff trifft und beim Angriffswurf im Vorteil ist, oder wenn das Ziel sich im Abstand von bis zu 1,5 Metern von einem Verbündeten des Assassinen befindet, der nicht kampfunfähig ist, und der Assassine beim Angriffswurf nicht im Nachteil ist.
:
### Aktionen
***Mehrfachangriff.*** Der Assassine führt zwei Angriffe mit dem Kurzschwert aus.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden, und das Ziel muss einen SG-15-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 24 (7W6) Giftschaden, anderenfalls die Hälfte.
:
***Leichte Armbrust.*** *Fernkampfwaffenangriff*: +6 auf Treffer, Reichweite 24/96 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden, und das Ziel muss einen SG-15-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 24 (7W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Atterkopp
*Mittelgroße Monstrosität, neutral böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 44 (8W8+8)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|15 (+2)|13 (+1)|7  (-2)|12 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Überlebenskunst +3, Wahrnehmung +3
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Netzsinn.*** Bei Kontakt mit einem Netz weiß der Atterkopp den genauen Standort jeder anderen Kreatur im Kontakt mit demselben Netz.
:
***Netzwandler.*** Der Atterkopp ignoriert Bewegungseinschränkungen, die durch Netze verursacht werden.
:
***Spinnenklettern.*** Der Atterkopp kann ohne Attributswürfe schwierige Oberflächen erklimmen und sich kopfüber an Decken entlang bewegen.
:
### Aktionen
***Mehrfachangriff.*** Der Atterkopp führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 6 (1W8+2) Stichschaden plus 4 (1W8) Giftschaden. Das Ziel muss einen SG-11-Konstitutionsrettungswurf bestehen, oder es ist eine Minute lang vergiftet. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Hiebschaden.
:
***Netz (Aufladung 5–6).*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 9/18 m, eine höchstens große Kreatur. *Treffer*: Die Kreatur ist durch das Netz festgesetzt. Als Aktion kann die festgesetzte Kreatur einen SG-11-Stärkewurf ausführen. Bei einem Erfolg befreit sie sich aus dem Netz. Der Effekt endet auch, wenn das Netz zerstört wird. Das Netz besitzt eine RK von 10, 5 Trefferpunkte, ist anfällig für Feuerschaden und gegen Gift-, psychischen und Wuchtschaden immun.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Ausgewachsener blauer Drache
*Riesiger Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 225 (18W12+108)
**Speed**          :: 12 m, Fliegen 24 m, Graben 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|25 (+7)|10 (+0)|23 (+6)|16 (+3)|15 (+2)|19 (+4)|
___
**Rettungswürfe**           :: Ges +5, Kon +11, Wei +7, Cha +9
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +12
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 22
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 16 (15.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 18 (2W10+7) Stichschaden plus 5 (1W10) Blitzschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W6+7) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 16 (2W8+7) Wuchtschaden.
:
***Blitzodem (Aufladung 5–6).*** Der Drache atmet einen Blitzstrahl in einer 27 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in der Linie muss einen SG-19-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 66 (12W10) Blitzschaden, anderenfalls die Hälfte.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-17-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-20-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 14 (2W6+7) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener Bronzedrache
*Riesiger Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 212 (17W12+102)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|25 (+7)|10 (+0)|23 (+6)|16 (+3)|15 (+2)|19 (+4)|
___
**Rettungswürfe**           :: Ges +5, Kon +11, Wei +7, Cha +9
**Fertigkeiten**            :: Heimlichkeit +7, Motiv erkennen +12, Wahrnehmung +5
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 22
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 15 (13.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 18 (2W10+7) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W6+7) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 16 (2W8+7) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-17-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Blitzodem.*** Der Drache atmet einen Blitzstrahl in einer 27 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in der Linie muss einen SG-19-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 66 (12W10) Blitzschaden, anderenfalls die Hälfte.
:
***Odem der Abstoßung.*** Der Drache atmet abstoßende Energie in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-19-Stärkerettungswurf bestehen. Scheitert der Wurf, wird die Kreatur bis zu 18 Meter weit vom Drachen weggestoßen.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-20-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 14 (2W6+7) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener Golddrache
*Riesiger Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 256 (19W12+133)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|14 (+2)|25 (+7)|16 (+3)|15 (+2)|24 (+7)|
___
**Rettungswürfe**           :: Ges +8, Kon +13, Wei +8, Cha +13
**Fertigkeiten**            :: Heimlichkeit +8, Motiv erkennen +8, Überzeugen +13, Wahrnehmung +14
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 24
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 17 (18.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-21-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-21-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 66 (12W10) Feuerschaden, anderenfalls die Hälfte.
:
***Schwächender Odem.*** Der Drache atmet Gas in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-21-Stärkerettungswurf bestehen, oder sie ist eine Minute lang bei stärkebasierten Angriffswürfen, bei Stärkewürfen und Stärkerettungswürfen im Nachteil. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-22-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener grüner Drache
*Riesiger Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 207 (18W12+90)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|12 (+1)|21 (+5)|18 (+4)|15 (+2)|17 (+3)|
___
**Rettungswürfe**           :: Ges +6, Kon +10, Wei +7, Cha +8
**Fertigkeiten**            :: Heimlichkeit +6, Motiv erkennen +7, Täuschen +8, Überzeugen +8, Wahrnehmung +12
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 22
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 15 (13.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden plus 7 (2W6) Giftschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 15 (2W8+6) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-16-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Giftodem (Aufladung 5–6).*** Der Drache atmet giftiges Gas in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-18-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 56 (16W6) Giftschaden, anderenfalls die Hälfte.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-19-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 13 (2W6+6) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener Kupferdrache
*Riesiger Drache, chaotisch gut*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 184 (16W12+80)
**Speed**          :: 12 m, Klettern 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|12 (+1)|21 (+5)|18 (+4)|15 (+2)|17 (+3)|
___
**Rettungswürfe**           :: Ges +6, Kon +10, Wei +7, Cha +8
**Fertigkeiten**            :: Heimlichkeit +6, Täuschen +8, Wahrnehmung +12
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 22
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 14 (11.500 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 15 (2W8+6) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-16-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Säureodem.*** Der Drache atmet Säure in einer 18 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-18-Geschicklichkeitsret-tungswurf ausführen. Scheitert der Wurf, erleidet sie 54 (12W8) Säureschaden, anderenfalls die Hälfte.
:
***Bremsender Odem.*** Der Drache atmet Gas in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-18-Konstitutionsrettungswurf bestehen. Scheitert der Wurf, so kann die Kreatur keine Reaktionen einsetzen, ihre Bewegungsrate ist halbiert, und sie kann in ihrem Zug höchstens einen Angriff ausführen. Außerdem kann sie in ihrem Zug entweder eine Aktion oder eine Bonusaktion einsetzen, aber nicht beides. Diese Effekte halten eine Minute lang an. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-19-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 13 (2W6+6) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener Messingdrache
*Riesiger Drache, chaotisch gut*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 172 (15W12+75)
**Speed**          :: 12 m, Fliegen 24 m, Graben 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|10 (+0)|21 (+5)|14 (+2)|13 (+1)|17 (+3)|
___
**Rettungswürfe**           :: Ges +5, Kon +10, Wei +6, Cha +8
**Fertigkeiten**            :: Geschichte +7, Heimlichkeit +5, Überzeugen +8, Wahrnehmung +11
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 21
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 13 (10.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 15 (2W8+6) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-16-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einer 18 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-18-Geschicklichkeitsret-tungswurf ausführen. Scheitert der Wurf, erleidet sie 45 (13W6) Feuerschaden, anderenfalls die Hälfte.
:
***Schlafodem.*** Der Drache atmet Schlafgas in einem Kegel von 18 Metern aus. Jede Kreatur in dem Bereich muss einen SG-18-Konstitutionsrettungswurf bestehen, oder sie ist zehn Minuten lang bewusstlos. Dieser Effekt endet bei einer Kreatur, wenn sie Schaden erleidet oder jemand sie mit einer Aktion aufweckt.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-19-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 13 (2W6+6) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener roter Drache
*Riesiger Drache, chaotisch böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 256 (19W12+133)
**Speed**          :: 12 m, Klettern 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|10 (+0)|25 (+7)|16 (+3)|13 (+1)|21 (+5)|
___
**Rettungswürfe**           :: Ges +6, Kon +13, Wei +7, Cha +11
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +13
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 23
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 17 (18.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden plus 7 (2W6) Feuerschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Drache atmet Feuer in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-21-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 63 (18W6) Feuerschaden, anderenfalls die Hälfte.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-19-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-22-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener schwarzer Drache
*Riesiger Drache, chaotisch böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 195 (17W12+85)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|14 (+2)|21 (+5)|14 (+2)|13 (+1)|17 (+3)|
___
**Rettungswürfe**           :: Ges +7, Kon +10, Wei +6, Cha +8
**Fertigkeiten**            :: Heimlichkeit +7, Wahrnehmung +11
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 21
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 14 (11.500 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden plus 4 (1W8) Säureschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 15 (2W8+6) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-16-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Säureodem (Aufladung 5–6).*** Der Drache atmet Säure in einer 18 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-18-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 54 (12W8) Säureschaden, anderenfalls die Hälfte.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-19-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 13 (2W6+6) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener Silberdrache
*Riesiger Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 243 (18W12+126)
**Speed**          :: 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|10 (+0)|25 (+7)|16 (+3)|13 (+1)|21 (+5)|
___
**Rettungswürfe**           :: Ges +5, Kon +12, Wei +6, Cha +10
**Fertigkeiten**            :: Arkane Kunde +8, Geschichte +8, Heimlichkeit +5, Wahrnehmung +11
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 21
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 16 (15.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-18-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Kälteodem.*** Der Drache atmet einen eisigen Stoß in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-20-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 58 (13W8) Kälteschaden, anderenfalls die Hälfte.
:
***Lähmender Odem.*** Der Drache atmet lähmendes Gas in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-20-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang gelähmt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-21-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Ausgewachsener weißer Drache
*Riesiger Drache, chaotisch böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 200 (16W12+96)
**Speed**          :: 12 m, Fliegen 24 m, Graben 9 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|10 (+0)|22 (+6)|8  (-1)|12 (+1)|12 (+1)|
___
**Rettungswürfe**           :: Ges +5, Kon +11, Wei +6, Cha +6
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +11
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 21
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 13 (10.000 EP)
___
***Eisschritt.*** Der Drache kann sich über Eis bewegen und eisige Oberflächen erklimmen, ohne Attributswürfe ausführen zu müssen. Außerdem kostet ihn schwieriges Gelände, das aus Eis oder Schnee besteht, keine zusätzliche Bewegung.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden plus 4 (1W8) Kälteschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 15 (2W8+6) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-14-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Kälteodem (Aufladung 5–6).*** Der Drache atmet einen eisigen Stoß in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-19-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 54 (12W8) Kälteschaden, anderenfalls die Hälfte.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu drei Metern vom Drachen muss einen SG-19-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 13 (2W6+6) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Axtschnabel
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|12 (+1)|12 (+1)|2  (-4)|10 (+0)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Schnabel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Hiebschaden.
:
}}


{{monster,frame
## Azer
*Mittelgroßer Elementar, rechtschaffen neutral*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung, Schild)
**Trefferpunkte**  :: 39 (6W8+12)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|12 (+1)|15 (+2)|12 (+1)|13 (+1)|10 (+0)|
___
**Rettungswürfe**           :: Kon +4
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: Ignal
**Herausforderung**         :: 2 (450 EP)
___
***Beleuchtung.*** Der Azer spendet im Radius von drei Metern helles Licht und im Radius von weiteren drei Metern dämmriges Licht.
:
***Heiße Waffen.*** Wenn der Azer mit einer metallischen Nahkampfwaffe trifft, bewirkt diese zusätzlich 3 (1W6) Feuerschaden (im Angriff enthalten).
:
***Heißer Körper.*** Eine Kreatur, die den Azer berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, erleidet 5 (1W10) Feuerschaden.
:
### Aktionen
***Kriegshammer.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Wuchtschaden oder 8 (1W10+3) Wuchtschaden bei zweihändiger Führung und Nahkampfangriff, plus 3 (1W6) Feuerschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Balor
*Riesiger Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 262 (21W12+126)
**Speed**          :: 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|26 (+8)|15 (+2)|22 (+6)|20 (+5)|16 (+3)|22 (+6)|
___
**Rettungswürfe**           :: Str +14, Kon +12, Wei +9, Cha +12
**Schadensresistenzen**     :: Blitz, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 13
**Sprachen**                :: Abyssisch, Telepathie auf 36 m
**Herausforderung**         :: 19 (22.000 EP)
___
***Feueraura.*** Zu Beginn jedes Zugs des Balors erleidet jede Kreatur im Abstand von bis zu 1,5 Metern von ihm 10 (3W6) Feuerschaden, und brennbare Objekte in der Aura, die nicht getragen oder gehalten werden, gehen in Flammen auf. Eine Kreatur, die den Balor berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, erleidet 10 (3W6) Feuerschaden.
:
***Magieresistenz.*** Der Balor ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Balors sind magisch.
:
***Todeszuckungen.*** Wenn der Balor stirbt, explodiert er, und jede Kreatur im Abstand von bis zu neun Metern von ihm muss einen SG-20-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 70 (20W6) Feuerschaden, anderenfalls die Hälfte. Die Explosion setzt brennbare Objekte im Bereich in Brand, die nicht getragen oder gehalten werden, und sie zerstört die Waffen des Balors.
:
### Aktionen
***Mehrfachangriff.*** Der Balor führt zwei Angriffe aus: einen mit seinem Langschwert und einen mit seiner Peitsche.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 21 (3W8+8) Hiebschaden plus 13 (3W8) Blitzschaden. Wenn der Balor einen kritischen Treffer landet, kann er die Schadenswürfel dreimal statt zweimal werfen.
:
***Peitsche.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 9 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden plus 10 (3W6) Feuerschaden, und das Ziel muss einen SG-20-Stärkerettungswurf bestehen, oder es wird bis zu 7,5 Meter zum Balor gezogen.
:
***Teleportieren.*** Der Balor teleportiert sich und sämtliche Ausrüstung, die er trägt oder hält, magisch bis zu 36 Meter weit an eine freie Stelle, die er sehen kann.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Banditenhauptmann
*Mittelgroßer Humanoide (jedes Volk), jede nichtrechtschaffene Gesinnung*
___
**Rüstungsklasse** :: 15 (beschlagenes Leder)
**Trefferpunkte**  :: 65 (10W8+20)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|16 (+3)|14 (+2)|14 (+2)|11 (+0)|14 (+2)|
___
**Rettungswürfe**           :: Str +4, Ges +5, Wei +2
**Fertigkeiten**            :: Athletik +4, Täuschen +4
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Zwei beliebige Sprachen
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Hauptmann führt drei Nahkampfangriffe aus: zwei mit seinem Krummsäbel und einen mit seinem Dolch. Alternativ kann er zwei Fernkampfangriffe mit seinen Dolchen ausführen.
:
***Krummsäbel.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Hiebschaden.
:
***Dolch.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 5 (1W4+3) Stichschaden.
:
### Reaktionen
***Parieren.*** Der Hauptmann erhöht seine RK gegen einen Nahkampfangriff, der ihn treffen würde, um 2. Dazu muss der Hauptmann den Angreifer sehen können und eine Nahkampfwaffe führen.
:
}}


{{monster,frame
## Bandit
*Mittelgroßer Humanoide (jedes Volk), jede nicht-rechtschaffene Gesinnung*
___
**Rüstungsklasse** :: 12 (Lederrüstung)
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|12 (+1)|12 (+1)|10 (+0)|10 (+0)|10 (+0)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Krummsäbel.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Hiebschaden.
:
***Leichte Armbrust.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 24/96 m, ein Ziel. *Treffer*: 5 (1W8+1) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Bartteufel
*Mittelgroßer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 52 (8W8+16)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|15 (+2)|15 (+2)|9  (-1)|11 (+0)|11 (+0)|
___
**Rettungswürfe**           :: Str +5, Kon +4, Wei +2
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 3 (700 EP)
___
***Magieresistenz.*** Der Teufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Standfest.*** Der Teufel kann nicht verängstigt werden, solange er eine verbündete Kreatur im Abstand von bis zu neun Metern von ihm sehen kann.
:
***Teufelssicht.*** Die Dunkelsicht des Teufels wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Mehrfachangriff.*** Der Teufel führt zwei Angriffe aus: einen mit seinem Bart und einen mit seiner Glefe.
:
***Bart.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 6 (1W8+2) Stichschaden, und das Ziel muss einen SG-12-Konstitutionsrettungswurf bestehen, oder es ist eine Minute lang vergiftet. Auf diese Art vergiftete Ziele können außerdem keine Trefferpunkte zurückerhalten. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
***Glefe.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 8 (1W10+3) Hiebschaden. Wenn das Ziel kein Untoter und kein Konstrukt ist, muss es einen SG-12-Konstitutionsrettungswurf bestehen, oder es verliert zu Beginn jedes seiner Züge aufgrund einer infernalischen Wunde 5 (1W10) Trefferpunkte. Wann immer der Teufel das verwundete Ziel erneut mit diesem Angriff trifft, erhöht sich der Schaden durch die Wunde um 5 (1W10). Kreaturen können als Aktion versuchen, die Wunde mit einem erfolgreichen SG-12-Weisheitswurf (Heilkunde) zu verschließen. Die Wunde schließt sich auch, wenn das Ziel magisch geheilt wird.
:
}}


{{monster,frame
## Basilisk
*Mittelgroße Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 52 (8W8+16)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|8  (-1)|15 (+2)|2  (-4)|8  (-1)|7  (-2)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 3 (700 EP)
___
***Versteinernder Blick.*** Wenn eine Kreatur ihren Zug im Abstand von bis zu neun Metern vom Basilisken beginnt und die beiden einander sehen können, kann der Basilisk die Kreatur zu einem SG-12-Konstitutionsrettungswurf zwingen, sofern er nicht kampfunfähig ist. Scheitert der Wurf, so beginnt die Kreatur, magisch zu versteinern, und ist festgesetzt. Sie muss den Rettungswurf am Ende ihres nächsten Zugs wiederholen. Bei einem Erfolg endet der Effekt. Scheitert der Wurf, so bleibt die Kreatur versteinert, bis sie durch den Zauber Vollständige Genesung oder andere Magie befreit wird. Wenn die Kreatur nicht überrascht wird, kann sie die Augen abwenden, um den Rettungswurf zu Beginn ihres Zugs zu vermeiden. In diesem Fall kann sie den Basilisken bis zum Beginn ihres nächsten Zugs, wenn sie die Augen erneut abwenden kann, nicht sehen. Schaut sie den Basilisken in der Zwischenzeit an, so muss sie den Rettungswurf sofort ausführen.  Wenn der Basilisk im Abstand von bis zu neun Metern von sich in hellem Licht sein Spiegelbild sieht, hält er sich selbst für einen Rivalen und zielt mit seinem Blick auf sich.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Stichschaden plus 7 (2W6) Giftschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Baumhirte
*Riesige Pflanze, chaotisch gut*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 138 (12W12+60)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|8  (-1)|21 (+5)|12 (+1)|16 (+3)|12 (+1)|
___
**Schadensresistenzen**     :: Stichschaden, Wuchtschaden
**Schadensanfälligkeiten**  :: Feuer
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Druidisch, Elfisch, Gemeinsprache, Sylvanisch
**Herausforderung**         :: 9 (5.000 EP)
___
***Belagerungsmonster.*** Der Baumhirte fügt Gegenständen und Gebäuden doppelten Schaden zu.
:
***Falsches Erscheinungsbild.*** Solange der Baumhirte sich nicht bewegt, ist er nicht von einem gewöhnlichen Baum zu unterscheiden.
:
### Aktionen
***Mehrfachangriff.*** Der Baumhirte führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 16 (3W6+6) Wuchtschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +10 auf Treffer, Reichweite 18/54 m, ein Ziel. *Treffer*: 28 (4W10+6) Wuchtschaden.
:
***Bäume beleben (1-mal täglich).*** Der Baumhirte belebt magisch einen oder zwei Bäume im Abstand von bis zu 18 Metern, die er sehen kann. Diese Bäume haben die gleichen Spielwerte wie der Baumhirte, allerdings mit folgenden Abweichungen: Sie haben einen Intelligenz- und einen Charismawert von 1, können nicht sprechen und haben nur die Hieb-Aktionsmöglichkeit. Belebte Bäume agieren als Verbündete des Baumhirten. Sie bleiben einen Tag lang belebt oder bis sie sterben, der Baumhirte stirbt, er sich weiter als 36 Meter von ihnen entfernt oder sie als Bonusaktion wieder in normale Bäume verwandelt. In diesem Fall versuchen sie, sich wieder zu verwurzeln.
:
}}


{{monster,frame
## Behir
*Riesige Monstrosität, neutral böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 168 (16W12+64)
**Speed**          :: 15 m, Klettern 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|16 (+3)|18 (+4)|7  (-2)|14 (+2)|12 (+1)|
___
**Fertigkeiten**            :: Heimlichkeit +7, Wahrnehmung +6
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Dunkelsicht 27 m, Passive Wahrnehmung 16
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 11 (7.200 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Behir führt zwei Angriffe aus: einen Biss- und einen Umschlingen-Angriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 22 (3W10+6) Stichschaden.
:
***Umschlingen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, eine höchstens große Kreatur. *Treffer*: 17 (2W10+6) Wuchtschaden plus 17 (2W10+6) Hiebschaden. Das Ziel wird gepackt (Rettungswurf-SG 16), sofern der Behir nicht schon eine Kreatur umschlingt, und ist festgesetzt, solange es gepackt ist.
:
***Blitzodem (Aufladung 5–6).*** Der Behir atmet Blitze in einer sechs Meter langen und 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-16-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 66 (12W10) Blitzschaden, anderenfalls die Hälfte.
:
***Verschlucken.*** Der Behir führt einen Bissangriff gegen ein höchstens mittelgroßes Ziel aus, das er gepackt hält. Wenn der Angriff trifft, wird das Ziel außerdem verschluckt, und der Haltegriff endet. Verschluckte Ziele sind blind und festgesetzt, haben vollständige Deckung gegen Angriffe und andere Effekte von außerhalb des Behirs und erleiden zu Beginn jedes Zugs des Behirs 21 (6W6) Säureschaden. Der Behir kann nur jeweils eine Kreatur verschlucken. Erleidet der Behir durch die verschluckte Kreatur in einem einzigen Zug mindestens 30 Schaden, so muss er am Ende der Runde einen SG-16-Konstitutionsrettungswurf bestehen, oder er würgt die verschluckte Kreatur wieder aus. Diese findet sich dann in einem Bereich von 4,5 Metern um den Behir wieder und ist liegend. Stirbt der Behir, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie 4,5 Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Belebte Rüstung
*Mittelgroßes Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 33 (6W8+6)
**Speed**          :: 7,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|11 (+0)|13 (+1)|1  (-5)|3  (-4)|1  (-5)|
___
**Schadensimmunitäten**     :: Gift, Psychisch
**Zustandsimmunitäten**     :: Bezaubert, Blind, Erschöpft, Gelähmt, Taub, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 6
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Antimagische Empfindlichkeit.*** Die Rüstung ist im Bereich eines antimagischen Felds kampfunfähig. Wird sie Ziel des Zaubers Magie bannen, muss die Rüstung einen Konstitutionsrettungswurf gegen den Zauberrettungswurf-SG des Zauberwirkers bestehen, oder sie ist eine Minute lang bewusstlos.
:
***Falsches Erscheinungsbild.*** Solange die Rüstung sich nicht bewegt, ist sie nicht von einer gewöhnlichen Rüstung zu unterscheiden.
:
### Aktionen
***Mehrfachangriff.*** Der Rüstung führt zwei Nahkampfangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Wuchtschaden.
:
}}


{{monster,frame
## Berserker
*Mittelgroßer Humanoide (jedes Volk), jede chaotische Gesinnung*
___
**Rüstungsklasse** :: 13 (Fellrüstung)
**Trefferpunkte**  :: 67 (9W8+27)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|12 (+1)|17 (+3)|9  (-1)|11 (+0)|9  (-1)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 2 (450 EP)
___
***Unvorsichtig.*** Ab Beginn seines Zugs ist der Berserker bei allen Nahkampfwaffenangriffswürfen im Vorteil, doch Angriffswürfe gegen ihn sind ebenfalls im Vorteil. Dies gilt bis zum Beginn seines nächsten Zugs.
:
### Aktionen
***Zweihandaxt.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (1W12+3) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Blauer Drachennestling
*Mittelgroßer Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 52 (8W8+16)
**Speed**          :: 9 m, Fliegen 18 m, Graben 4,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|10 (+0)|15 (+2)|12 (+1)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Ges +2, Kon +4, Wei +2, Cha +4
**Fertigkeiten**            :: Heimlichkeit +2, Wahrnehmung +4
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 3 (700 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W10+3) Stichschaden plus 3 (1W6) Blitzschaden.
:
***Blitzodem (Aufladung 5–6).*** Der Drache atmet einen Blitzstrahl in einer neun Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in der Linie muss einen SG-12-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 22 (4W10) Blitzschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Blutfalke
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 7 (2W6)
**Speed**          :: 3 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|14 (+2)|10 (+0)|3  (-4)|14 (+2)|5  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Rudeltaktik.*** Der Falke ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfe Sicht.*** Der Falke ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Schnabel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Blutmücke
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 2 (1W4)
**Speed**          :: 3 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|4  (-3)|16 (+3)|11 (+0)|2  (-4)|8  (-1)|6  (-2)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Blutabsaugung.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 5 (1W4+3) Stichschaden, und die Blutmücke heftet sich an das Ziel an. Während sie angeheftet ist, greift die Blutmücke nicht an. Stattdessen verliert das Ziel zu Beginn jedes Zugs der Blutmücke 5 (1W4+3) Trefferpunkte aufgrund des Blutverlusts. Die Blutmücke kann sich lösen, indem sie 1,5 Meter ihrer Bewegungsrate verwendet. Das tut sie, nachdem sie 10 Trefferpunkte an Blut ausgesaugt hat oder wenn das Ziel stirbt. Eine Kreatur, auch das Ziel, kann eine Aktion verwenden, um die Blutmücke zu entfernen.
:
}}


{{monster,frame
## Braunbär
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 34 (4W10+12)
**Speed**          :: 12 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|16 (+3)|2  (-4)|13 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Scharfer Geruchssinn.*** Der Bär ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Bär führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W8+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Bronzedrachennestling
*Mittelgroßer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 32 (5W8+10)
**Speed**          :: 9 m, Fliegen 18 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|10 (+0)|15 (+2)|12 (+1)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Ges +2, Kon +4, Wei +2, Cha +4
**Fertigkeiten**            :: Heimlichkeit +2, Wahrnehmung +4
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 2 (450 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W10+3) Stichschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Blitzodem.*** Der Drache atmet einen Blitzstrahl in einer zwölf Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in der Linie muss einen SG-12-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 16 (3W10) Blitzschaden, anderenfalls die Hälfte.
:
***Odem der Abstoßung.*** Der Drache atmet abstoßende Energie in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-12-Stärkerettungswurf bestehen. Scheitert der Wurf, wird die Kreatur bis zu neun Meter weit vom Drachen weggestoßen.
:
}}


{{monster,frame
## Chimäre
*Große Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 114 (12W10+48)
**Speed**          :: 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|11 (+0)|19 (+4)|3  (-4)|14 (+2)|10 (+0)|
___
**Fertigkeiten**            :: Wahrnehmung +8
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 18
**Sprachen**                :: Versteht Drakonisch, aber kann nicht sprechen
**Herausforderung**         :: 6 (2.300 EP)
___
### Aktionen
***Mehrfachangriff.*** Die Chimäre führt drei Angriffe aus: einen Biss-, einen Hörner- und einen Klauenangriff. Ist ihr Feuerodem-Angriff verfügbar, kann sie den Odem anstatt Biss oder Hörner einsetzen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Stichschaden.
:
***Hörner.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (1W12+4) Wuchtschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Drachenkopf atmet Feuer in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-15-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 31 (7W8) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Chuul
*Große Aberration, chaotisch böse*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 93 (11W10+33)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|16 (+3)|5  (-3)|11 (+0)|5  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Versteht Tiefensprache, aber kann nicht sprechen
**Herausforderung**         :: 4 (1.100 EP)
___
***Amphibisch.*** Der Chuul kann Luft und Wasser atmen.
:
***Magie spüren.*** Der Chuul spürt beliebig oft Magie im Abstand von bis zu 36 Metern von sich. Ansonsten funktioniert dieses Merkmal wie der Zauber Magie entdecken, aber ist selbst nicht magisch.
:
### Aktionen
***Mehrfachangriff.*** Der Chuul führt zwei Zangenangriffe aus. Wenn der Chuul eine Kreatur gepackt hält, kann er auch einmal seine Tentakel einsetzen.
:
***Zange.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden. Das Ziel wird gepackt (Rettungswurf-SG 14), sofern es sich um eine höchstens große Kreatur handelt und der Chuul nicht schon zwei andere Kreaturen gepackt hält.
:
***Tentakel.*** Eine vom Chuul gepackte Kreatur muss einen SG-13-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang vergiftet. Bis zum Ende der Vergiftung ist das Ziel gelähmt. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Couatl
*Mittelgroßes celestisches Wesen, rechtschaffen gut*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 97 (13W8+39)
**Speed**          :: 9 m, Fliegen 27 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|20 (+5)|17 (+3)|18 (+4)|20 (+5)|18 (+4)|
___
**Rettungswürfe**           :: Kon +5, Wei +7, Cha +6
**Schadensresistenzen**     :: Gleißend
**Schadensimmunitäten**     :: Psychisch; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 15
**Sprachen**                :: Alle, Telepathie auf 36 m
**Herausforderung**         :: 4 (1.100 EP)
___
***Abgeschirmter Geist.*** Der Couatl ist gegen Ausspähung und alle Effekte immun, die seine Emotionen spüren, seine Gedanken lesen oder seinen Standort offenbaren würden.
:
***Magische Waffen.*** Die Waffenangriffe des Couatls sind magisch.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Couatls ist Charisma (Zauberrettungswurf-SG 14). Er kann folgende angeborene Zauber wirken, wobei er nur verbale Komponenten braucht:
:
Beliebig oft: [*Gedanken wahrnehmen*](https://openrpg.de/srd/5e/de/#spell-gedanken-wahrnehmen)[*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)<br>
Je 3-mal täglich: [*Heiligtum*](https://openrpg.de/srd/5e/de/#spell-heiligtum)[*Nahrung und Wasser erschaffen*](https://openrpg.de/srd/5e/de/#spell-nahrung-und-wasser-erschaffen)[*Schild*](https://openrpg.de/srd/5e/de/#spell-schild)[*Schutz vor Gift*](https://openrpg.de/srd/5e/de/#spell-schutz-vor-gift)[*Schwache Genesung*](https://openrpg.de/srd/5e/de/#spell-schwache-genesung)[*Segnen*](https://openrpg.de/srd/5e/de/#spell-segnen)[*Wunden heilen*](https://openrpg.de/srd/5e/de/#spell-wunden-heilen)<br>
Je 1-mal täglich: [*Ausspähung*](https://openrpg.de/srd/5e/de/#spell-ausspaehung)[*Traum*](https://openrpg.de/srd/5e/de/#spell-traum)[*Vollständige Genesung*](https://openrpg.de/srd/5e/de/#spell-vollstaendige-genesung)
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 8 (1W6+5) Stichschaden, und das Ziel muss einen SG-13-Konstitutionsrettungswurf bestehen, oder es ist 24 Stunden lang vergiftet. Bis zum Ende der Vergiftung ist das Ziel bewusstlos. Eine andere Kreatur kann eine Aktion verwenden, um das Ziel wachzurütteln.
:
***Umschlingen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, eine höchstens mittelgroße Kreatur. *Treffer*: 10 (2W6+3) Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 15). Ein gepacktes Ziel ist festgesetzt. Der Couatl kann solange kein weiteres Ziel umschlingen.
:
***Gestalt ändern.*** Der Couatl nimmt magisch die Gestalt eines Humanoiden oder eines Tieres an, dessen Herausforderungsgrad seinen eigenen nicht überschreitet, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Couatls). In anderer Gestalt behält der Couatl seine Spielwerte und seine Sprechfähigkeit bei, seine RK, Bewegungsmodi, Stärke, Geschicklichkeit und andere Aktionen werden durch die der neuen Gestalt ersetzt, und der Couatl erhält alle Spielwerte und Fähigkeiten (außer Klassenmerkmalen, legendären Aktionen und Hortaktionen), welche die neue Gestalt besitzt, der Couatl aber nicht. Wenn die neue Gestalt über einen Bissangriff verfügt, kann der Couatl seinen Biss in dieser Gestalt einsetzen.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Dachs
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 3 (1W4+1)
**Speed**          :: 6 m, Graben 1,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|4  (-3)|11 (+0)|12 (+1)|2  (-4)|12 (+1)|5  (-3)|
___
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfer Geruchssinn.*** Der Dachs ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden.
:
}}


{{monster,frame
## Dampf-Mephit
*Kleiner Elementar, neutral böse*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 21 (6W6)
**Speed**          :: 9 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|5  (-3)|11 (+0)|10 (+0)|11 (+0)|10 (+0)|12 (+1)|
___
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Aqual, Ignal
**Herausforderung**         :: 1/4 (50 EP)
___
***Angeborenes Zauberwirken (1-mal täglich).*** Der Mephit kann den angeborenen Zauber Verschwimmen wirken, ohne Materialkomponenten zu benötigen. Sein Attribut zum angeborenen Zauberwirken ist Charisma.
:
***Todesexplosion.*** Wenn der Mephit stirbt, vergeht er in einer Dampfwolke. Jede Kreatur im Abstand von bis zu 1,5 Metern vom Mephit muss einen SG-10-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 4 (1W8) Feuerschaden.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 2 (1W4) Hiebschaden plus 2 (1W4) Feuerschaden.
:
***Dampfodem (Aufladung 6).*** Der Mephit atmet siedenden Dampf in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-10-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 4 (1W8) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Deva
*Mittelgroßes celestisches Wesen, rechtschaffen gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 136 (16W8+64)
**Speed**          :: 9 m, Fliegen 27 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|18 (+4)|18 (+4)|17 (+3)|20 (+5)|20 (+5)|
___
**Rettungswürfe**           :: Wei +9, Cha +9
**Fertigkeiten**            :: Motiv erkennen +9, Wahrnehmung +9
**Schadensresistenzen**     :: Gleißend; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Verängstigt
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 19
**Sprachen**                :: Alle, Telepathie auf 36 m
**Herausforderung**         :: 10 (5.900 EP)
___
***Engelswaffen.*** Die Waffenangriffe des Devas sind magisch. Wenn der Deva mit einer Waffe trifft, bewirkt die Waffe zusätzlich 4W8 gleißenden Schaden (im Angriff enthalten).
:
***Magieresistenz.*** Der Deva ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Devas ist Charisma (Zauberrettungswurf-SG 17). Der Deva kann folgende angeborene Zauber wirken, wobei er nur verbale Komponenten braucht:
:
Beliebig oft: [*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)<br>
Je 1-mal täglich: [*Heiliges Gespräch*](https://openrpg.de/srd/5e/de/#spell-heiliges-gespraech)[*Tote erwecken*](https://openrpg.de/srd/5e/de/#spell-tote-erwecken)
:
### Aktionen
***Mehrfachangriff.*** Der Deva führt zwei Nahkampfangriffe aus.
:
***Streitkolben.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W6+4) Wuchtschaden plus 18 (4W8) gleißender Schaden.
:
***Gestalt ändern.*** Der Deva nimmt magisch die Gestalt eines Humanoiden eines oder Tieres an, dessen Herausforderungsgrad seinen eigenen nicht überschreitet, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Devas). In anderer Gestalt behält der Deva seine Spielwerte und seine Sprechfähigkeit bei, seine RK, Bewegungsmodi, Stärke, Geschicklichkeit und Spezialsinne werden durch die der neuen Gestalt ersetzt, und der Deva erhält alle Spielwerte und Fähigkeiten (außer Klassenmerkmalen, legendären Aktionen und Hortaktionen), welche die neue Gestalt besitzt, der Deva aber nicht.
:
***Heilende Berührung (3-mal täglich).*** Der Deva berührt eine andere Kreatur. Das Ziel erhält magisch 20 (4W8+2) Trefferpunkte zurück und wird von allen Flüchen, Krankheiten, Giften, von Blindheit und Taubheit befreit.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Dogge
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 5 (1W8+1)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|14 (+2)|12 (+1)|3  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Die Dogge ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-11-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Doppelgänger
*Mittelgroße Monstrosität (Gestaltwandler), neutral*
___
**Rüstungsklasse** :: 14
**Trefferpunkte**  :: 52 (8W8+16)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|18 (+4)|14 (+2)|11 (+0)|12 (+1)|14 (+2)|
___
**Fertigkeiten**            :: Motiv erkennen +3, Täuschen +6
**Zustandsimmunitäten**     :: Bezaubert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: Gemeinsprache
**Herausforderung**         :: 3 (700 EP)
___
***Gestaltwandler.*** Der Doppelgänger kann seine Aktion verwenden, um sich in einen kleinen oder mittelgroßen Humanoiden, den er gesehen hat, oder zurück in seine wahre Gestalt zu verwandeln. Seine Spielwerte sind abgesehen von der Größe in beiden Gestalten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Lauerjäger.*** Der Doppelgänger ist in der ersten Kampfrunde bei Angriffswürfen gegen jede Kreatur im Vorteil, die er überrascht hat.
:
***Überraschungsangriff.*** Wenn der Doppelgänger in der ersten Kampfrunde eine Kreatur überrascht und mit einem Angriff trifft, erleidet das Ziel zusätzlich 10 (3W6) Schaden durch den Angriff.
:
### Aktionen
***Mehrfachangriff.*** Der Doppelgänger führt zwei Nahkampfangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W6+4) Wuchtschaden.
:
***Gedanken lesen.*** Der Doppelgänger liest magisch die oberflächlichen Gedanken einer Kreatur im Abstand von bis zu 18 Metern von ihm. Der Zauber kann Barrieren durchdringen, wird aber von 90 Zentimetern Holz oder Erde, 60 Zentimetern Stein, fünf Zentimetern Metall (außer Blei) oder von einer dünnen Bleischicht blockiert. Solange das Ziel sich in Reichweite befindet, kann der Doppelgänger dessen Gedanken fortlaufend lesen, sofern seine Konzentration nicht unterbrochen wird (wie bei einem Zauber). Solange der Doppelgänger die Gedanken des Ziels liest, ist er bei Weisheits- (Motiv erkennen) und Charismawürfen (Täuschen, Einschüchtern und Überzeugen) gegen das Ziel im Vorteil.
:
}}


{{monster,frame
## Drachenschildkröte
*Gigantischer Drache, neutral*
___
**Rüstungsklasse** :: 20 (natürliche Rüstung)
**Trefferpunkte**  :: 341 (22W20+110)
**Speed**          :: 6 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|25 (+7)|10 (+0)|20 (+5)|10 (+0)|12 (+1)|12 (+1)|
___
**Rettungswürfe**           :: Ges +6, Kon +11, Wei +7
**Schadensresistenzen**     :: Feuer
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Aqual, Drakonisch
**Herausforderung**         :: 17 (18.000 EP)
___
***Amphibisch.*** Die Drachenschildkröte kann Luft und Wasser atmen.
:
### Aktionen
***Mehrfachangriff.*** Die Drachenschildkröte führt drei Angriffe aus: einen Biss- und zwei Klauenangriffe. Anstelle ihrer zwei Klauenangriffe kann sie einen Schwanzangriff ausführen.
:
***Biss.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 26 (3W12+7) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 16 (2W8+7) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 26 (3W12+7) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-20-Stärkerettungswurf bestehen, oder es wird bis zu drei Meter weit von der Drachenschildkröte weg- und umgestoßen.
:
***Dampfodem (Aufladung 5–6).*** Die Drachenschildkröte atmet siedenden Dampf in einem Kegel von 18 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-18-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 52 (15W6) Feuerschaden, anderenfalls die Hälfte. Unter Wasser zu sein gewährt keine Resistenz gegen diesen Schaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Dretch
*Kleiner Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 18 (4W6+4)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|11 (+0)|12 (+1)|5  (-3)|8  (-1)|3  (-4)|
___
**Schadensresistenzen**     :: Blitz, Feuer, Kälte
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: Abyssisch, Telepathie auf 18 m (nur mit Kreaturen, die Abyssisch verstehen)
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Dretch führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W6) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (2W4) Hiebschaden.
:
***Stinkwolke (1-mal täglich).*** Widerliches grünes Gas geht im Radius von drei Metern vom Dretch aus. Es breitet sich um Ecken aus, und sein Bereich ist leicht verschleiert. Das Gas bleibt eine Minute lang bestehen, oder bis ein starker Wind es verteilt. Alle Kreaturen, die ihren Zug in dem Bereich beginnen, müssen einen SG-11-Konstitutionsrettungswurf bestehen, oder sie sind bis zum Beginn ihres nächsten Zugs vergiftet. Auf diese Art vergiftete Ziele können keine Reaktionen ausführen, und sie können in ihrem Zug entweder eine Aktion oder eine Bonusaktion, aber nicht beides ausführen.
:
}}


{{monster,frame
## Drinne
*Große Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 123 (13W10+52)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|16 (+3)|18 (+4)|13 (+1)|14 (+2)|12 (+1)|
___
**Fertigkeiten**            :: Heimlichkeit +9, Wahrnehmung +5
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 15
**Sprachen**                :: Elfisch, Gemeinsprache der Unterreiche
**Herausforderung**         :: 6 (2.300 EP)
___
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist der Drinne bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
***Feenblut.*** Der Drinne ist bei Rettungswürfen gegen Bezauberung im Vorteil und kann nicht magisch zum Einschlafen gebracht werden.
:
***Netzwandler.*** Der Drinne ignoriert Bewegungsein-schränkungen, die durch Netze verursacht werden.
:
***Spinnenklettern.*** Der Drinne kann ohne Attributswürfe schwierige Oberflächen erklimmen und sich kopfüber an Decken entlang bewegen.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken der Drinne ist Weisheit (Zauberrettungswurf-SG 13): Der Drinne kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Tanzende Lichter*](https://openrpg.de/srd/5e/de/#spell-tanzende-lichter)<br>
Je 1-mal täglich: [*Dunkelheit*](https://openrpg.de/srd/5e/de/#spell-dunkelheit)[*Feenfeuer*](https://openrpg.de/srd/5e/de/#spell-feenfeuer)
:
### Aktionen
***Mehrfachangriff.*** Der Drinne führt entweder mit dem Langschwert oder mit dem Langbogen drei Angriffe aus. Er kann einen dieser Angriffe durch einen Bissangriff ersetzen.
:
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 2 (1W4) Stichschaden plus 9 (2W8) Giftschaden.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Hiebschaden oder 8 (1W10+3) Hiebschaden bei zweihändiger Führung.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +6 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden plus 4 (1W8) Giftschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Drow (Elf)
*Mittelgroßer Humanoide (Elf), neutral böse*
___
**Rüstungsklasse** :: 15 (Kettenhemd)
**Trefferpunkte**  :: 13 (3W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|14 (+2)|10 (+0)|11 (+0)|11 (+0)|12 (+1)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +2
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Elfisch, Gemeinsprache der Unterreiche
**Herausforderung**         :: 1/4 (50 EP)
___
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist der Drow bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
***Feenblut.*** Der Drow ist bei Rettungswürfen gegen den Zustand Bezaubert im Vorteil und kann nicht magisch zum Einschlafen gebracht werden.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Drow ist Charisma (Zauberrettungswurf-SG 11). Er kann die folgenden Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Tanzende Lichter*](https://openrpg.de/srd/5e/de/#spell-tanzende-lichter)<br>
Je 1-mal täglich: [*Dunkelheit*](https://openrpg.de/srd/5e/de/#spell-dunkelheit)[*Feenfeuer*](https://openrpg.de/srd/5e/de/#spell-feenfeuer)
:
### Aktionen
***Kurzschwert.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Handarmbrust.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 9/36 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden, und das Ziel muss einen SG-13-Konstitutionsrettungswurf bestehen, oder es ist eine Stunde lang vergiftet. Wenn der Rettungswurf um mindestens 5 Punkte scheitert, ist das Ziel außerdem bewusstlos, solange es auf diese Art vergiftet ist. Das Ziel erwacht, wenn es Schaden erleidet oder von einer anderen Kreatur als Aktion wachgerüttelt wird.
:
}}


{{monster,frame
## Druide
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 11 (16 mit Rindenhaut)
**Trefferpunkte**  :: 27 (5W8+5)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|12 (+1)|13 (+1)|12 (+1)|15 (+2)|11 (+0)|
___
**Fertigkeiten**            :: Heilkunde +4, Naturkunde +3, Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: Druidisch und zwei weitere beliebige Sprachen
**Herausforderung**         :: 2 (450 EP)
___
***Zauberwirken.*** Der Druide ist ein Zauberwirker der 4. Stufe. Sein Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 12, +4 auf Treffer mit Zauberangriffen). Er hat folgende Druidenzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Druidenkunst*](https://openrpg.de/srd/5e/de/#spell-druidenkunst)[*Flammen erzeugen*](https://openrpg.de/srd/5e/de/#spell-flammen-erzeugen)[*Shillelagh*](https://openrpg.de/srd/5e/de/#spell-shillelagh)<br>
1&period; Grad (4 Plätze): [*Donnerwoge*](https://openrpg.de/srd/5e/de/#spell-donnerwoge)[*Lange Schritte*](https://openrpg.de/srd/5e/de/#spell-lange-schritte)[*Mit Tieren sprechen*](https://openrpg.de/srd/5e/de/#spell-mit-tieren-sprechen)[*Verstricken*](https://openrpg.de/srd/5e/de/#spell-verstricken)<br>
2&period; Grad (3 Plätze): [*Rindenhaut*](https://openrpg.de/srd/5e/de/#spell-rindenhaut)[*Tierbote*](https://openrpg.de/srd/5e/de/#spell-tierbote)
:
### Aktionen
***Kampfstab.*** *Nahkampfwaffenangriff*: +2 auf Treffer (+4 auf Treffer mit Shillelagh), Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W6) Wuchtschaden, 4 (1W8) Wuchtschaden bei zweihändiger Führung oder 6 (1W8+2) Wuchtschaden mit Shillelagh.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Dryade
*Mittelgroßes Feenwesen, neutral*
___
**Rüstungsklasse** :: 11 (16 mit Rindenhaut)
**Trefferpunkte**  :: 22 (5W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|12 (+1)|11 (+0)|14 (+2)|15 (+2)|18 (+4)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Elfisch, Sylvanisch
**Herausforderung**         :: 1 (200 EP)
___
***Baumwandeln.*** Einmal in jedem ihrer Züge kann die Dryade drei Meter ihrer Bewegungsrate verwenden, um magisch in einen lebendigen Baum innerhalb ihrer Reichweite hinein- und aus einem anderen lebendigen Baum im Abstand von bis zu 18 Metern vom ersten Baum herauszutreten. Sie erscheint dann an einer freien Stelle im Abstand von bis zu 1,5 Metern vom zweiten Baum. Beide Bäume müssen mindestens groß sein.
:
***Magieresistenz.*** Die Dryade ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Mit Tieren und Pflanzen sprechen.*** Die Dryade kann sich mit Tieren und Pflanzen verständigen, als spräche sie ihre Sprache.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken der Dryade ist Charisma (Zauberrettungswurf-SG 14). Die Dryade kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Druidenkunst*](https://openrpg.de/srd/5e/de/#spell-druidenkunst)<br>
Je 3-mal täglich: [*Gute Beeren*](https://openrpg.de/srd/5e/de/#spell-gute-beeren)[*Verstricken*](https://openrpg.de/srd/5e/de/#spell-verstricken)<br>
Je 1-mal täglich: [*Rindenhaut*](https://openrpg.de/srd/5e/de/#spell-rindenhaut)[*Shillelagh*](https://openrpg.de/srd/5e/de/#spell-shillelagh)[*Spurloses Gehen*](https://openrpg.de/srd/5e/de/#spell-spurloses-gehen)
:
### Aktionen
***Knüppel.*** *Nahkampfwaffenangriff*: +2 auf Treffer (+6 auf Treffer mit Shillelagh), Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Wuchtschaden oder 8 (1W8+4) Wuchtschaden mit Shillelagh.
:
***Feenbezauberung.*** Die Dryade zielt auf einen Humanoiden oder ein Tier im Abstand von bis zu neun Metern von ihr, den oder das sie sehen kann. Wenn das Ziel die Dryade sehen kann, muss es einen SG-14-Weisheitsrettungswurf bestehen, oder es ist magisch bezaubert. Die bezauberte Kreatur betrachtet die Dryade als vertrauenswürdigen Freund, den sie beschützt und auf dessen Rat sie hört. Obwohl das Ziel nicht von der Dryade kontrolliert wird, steht es ihren Forderungen und Aktionen so entgegenkommend wie möglich gegenüber. Jedes Mal, wenn die Dryade oder ihre Verbündeten dem Ziel Schaden zufügen, kann es den Rettungswurf wiederholen und den Effekt bei einem Erfolg beenden. Anderenfalls hält der Effekt 24 Stunden lang an, oder bis die Dryade stirbt, sich auf eine andere Existenzebene als die des Ziels begibt oder den Effekt als Bonusaktion beendet. Bei einem erfolgreichen Rettungswurf des Ziels ist dieses 24 Stunden lang gegen die Feenbezauberung der Dryade immun. Die Dryade kann höchstens einen Humanoiden und bis zu drei Tiere zugleich bezaubern.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Dschinni
*Großer Elementar, chaotisch gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 161 (14W10+84)
**Speed**          :: 9 m, Fliegen 27 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|15 (+2)|22 (+6)|15 (+2)|16 (+3)|20 (+5)|
___
**Rettungswürfe**           :: Ges +6, Wei +7, Cha +9
**Schadensimmunitäten**     :: Blitz, Schall
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 13
**Sprachen**                :: Aural
**Herausforderung**         :: 11 (7.200 EP)
___
***Elementarer Untergang.*** Wenn der Dschinni stirbt, löst sein Körper sich in eine warme Brise auf, und es bleibt nur die Ausrüstung zurück, die der Dschinni getragen oder gehalten hat.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Dschinnis ist Charisma (Zauberrettungswurf-SG 17, +9 auf Treffer mit Zauberangriffen). Der Dschinni kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Donnerwoge*](https://openrpg.de/srd/5e/de/#spell-donnerwoge)[*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)<br>
Je 3-mal täglich: [*Nahrung und Wasser erschaffen (kann Wein statt Wasser erschaffen)*](https://openrpg.de/srd/5e/de/#spell-nahrung-und-wasser-erschaffen)[*Windwandeln*](https://openrpg.de/srd/5e/de/#spell-windwandeln)[*Zungen*](https://openrpg.de/srd/5e/de/#spell-zungen)<br>
Je 1-mal täglich: [*Ebenenwechsel*](https://openrpg.de/srd/5e/de/#spell-ebenenwechsel)[*Elementar beschwören (nur Luftelementar)*](https://openrpg.de/srd/5e/de/#spell-elementar-beschwoeren)[*Erschaffung*](https://openrpg.de/srd/5e/de/#spell-erschaffung)[*Gasförmige Gestalt*](https://openrpg.de/srd/5e/de/#spell-gasfoermige-gestalt)[*Mächtiges Trugbild*](https://openrpg.de/srd/5e/de/#spell-maechtiges-trugbild)[*Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)
:
### Aktionen
***Mehrfachangriff.*** Der Dschinni führt drei Angriffe mit dem Krummsäbel aus.
:
***Krummsäbel.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W6+5) Hiebschaden plus 3 (1W6) Blitz- oder Schallschaden (nach Wahl des Dschinnis).
:
***Wirbelwind erzeugen.*** Um einen Punkt im Abstand von bis zu 36 Metern vom Dschinni, den er sehen kann, bildet sich magisch ein neun Meter hoher, drei Meter breiter Zylinder aus wirbelnder Luft. Der Wirbelwind bleibt bestehen, solange der Dschinni sich konzentriert (wie bei einem Zauber). Jede Kreatur außer dem Dschinni, die in den Wirbelwind gerät, muss einen SG-18-Stärkerettungswurf bestehen, oder sie ist im Zylinder festgesetzt. Der Dschinni kann als Aktion den Wirbelwind bis zu 18 Meter weit bewegen. Vom Wirbelwind festgesetzte Kreaturen bewegen sich mit diesem. Der Wirbelwind endet, sobald der Dschinni ihn nicht mehr sehen kann. Eine Kreatur kann ihre Aktion verwenden, um zu versuchen, eine vom Wirbelwind festgesetzte Kreatur einschließlich sich selbst zu befreien. Dazu ist ein erfolgreicher SG-18-Stärkewurf erforderlich. Die Kreatur ist dann nicht mehr festgesetzt und bewegt sich an die dem Wirbelwind nächste Stelle.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Duergar
*Mittelgroßer Humanoide (Zwerg), rechtschaffen böse*
___
**Rüstungsklasse** :: 16 (Schuppenpanzer, Schild)
**Trefferpunkte**  :: 26 (4W8+8)
**Speed**          :: 7,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|11 (+0)|14 (+2)|11 (+0)|10 (+0)|9  (-1)|
___
**Schadensresistenzen**     :: Gift
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache der Unterreiche, Zwergisch
**Herausforderung**         :: 1 (200 EP)
___
***Duergar-Widerstandskraft.*** Der Duergar ist bei Rettungswürfen gegen den Zustand Vergiftet, gegen Zauber und Illusionen sowie beim Widerstehen gegen die Zustände Bezaubert und Gelähmt im Vorteil.
:
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist der Duergar bei Angriffs-, und Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Nachteil.
:
### Aktionen
***Vergrößern (wird nach kurzer oder langer Rast aufgeladen).*** Der Duergar wächst auf magische Art und bleibt eine Minute lang groß, ebenso alles, was er trägt oder hält. Vergrößert ist der Duergar groß, sein Schadenswürfel bei stärkebasierten Waffenangriffen wird verdoppelt (in Angriffe eingeschlossen), und er ist bei Stärkewürfen und Stärkerettungswürfen im Vorteil. Wenn der Duergar nicht genügend Platz hat, um groß zu werden, nimmt er die maximal mögliche Größe an.
:
***Kriegspicke.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden oder 11 (2W8+2) Stichschaden, wenn vergrößert.
:
***Wurfspeer.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m oder Reichweite 9/36 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden oder 9 (2W6+2) Stichschaden, wenn vergrößert.
:
***Unsichtbarkeit (wird nach kurzer oder langer Rast aufgeladen).*** Der Duergar ist bis zu eine Stunde lang auf magische Art unsichtbar, bis er angreift, einen Zauber wirkt, Vergrößern einsetzt, oder bis seine Konzentration unterbrochen wird (wie bei einem Zauber). Ausrüstung, die der Duergar trägt oder hält, ist ebenfalls unsichtbar.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Düstermantel
*Kleine Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 22 (5W6+5)
**Speed**          :: 3 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|12 (+1)|13 (+1)|2  (-4)|10 (+0)|5  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +3
**Sinne**                   :: Blindsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Echolot.*** Der Düstermantel kann seine Blindsicht nicht verwenden, solange er taub ist.
:
***Falsches Erscheinungsbild.*** Solange der Düstermantel sich nicht bewegt, ist er nicht von einer Höhlenformation wie einem Stalaktiten oder Stalagmiten zu unterscheiden.
:
### Aktionen
***Zermalmen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 6 (1W6+3) Wuchtschaden, und der Düstermantel heftet sich an das Ziel an. Wenn das Ziel höchstens mittelgroß und der Düstermantel beim Angriffswurf im Vorteil ist, umgibt er beim Anheften dessen Kopf. Das Ziel ist blind und kann nicht atmen, solange der Düstermantel auf diese Art angeheftet ist. Solange der Düstermantel an das Ziel angeheftet ist, kann er keine andere Kreatur als das Ziel angreifen, ist jedoch bei seinen Angriffswürfen im Vorteil. Die Bewegungsrate des angehefteten Düstermantels wird zu 0. Er bewegt sich mit dem Ziel und kann keine Boni auf seine Bewegungsrate nutzen. Die Kreatur kann den Düstermantel mit einem erfolgreichen SG-13-Stärkewurf als Aktion abschütteln. In seinem Zug kann der Düstermantel sich vom Ziel lösen, indem er 1,5 Meter seiner Bewegungsrate verwendet.
:
***Aura der Dunkelheit (1-mal täglich).*** Magische Dunkelheit geht im Radius von 4,5 Metern vom Düstermantel aus. Sie bewegt sich mit ihm und reicht auch um Ecken. Die Dunkelheit bleibt bis zu zehn Minuten bestehen, solange der Düstermantel sich konzentriert (wie auf einen Zauber). Dunkelsicht kann diese Dunkelheit nicht durchdringen, und kein natürliches Licht kann sie erhellen. Wenn sich der Wirkbereich der Dunkelheit mit dem eines Lichts überschneidet, das durch einen Zauber des höchstens 2. Grades erzeugt wurde, so wird der lichterzeugende Zauber gebannt.
:
}}


{{monster,frame
## Eber
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|11 (+0)|12 (+1)|2  (-4)|9  (-1)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Sturmangriff.*** Wenn der Eber sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Hauerangriff trifft, erleidet das Ziel zusätzlich 3 (1W6) Hiebschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-11-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Unermüdlich (wird nach kurzer oder langer Rast aufgeladen).*** Wenn der Eber maximal 7 Schaden erleidet und seine Trefferpunkte dadurch auf 0 sinken würden, hat er stattdessen noch 1 Trefferpunkt.
:
### Aktionen
***Hauer.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Echsenmensch
*Mittelgroßer Humanoide (Echsenmensch), neutral*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung, Schild)
**Trefferpunkte**  :: 22 (4W8+4)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|10 (+0)|13 (+1)|7  (-2)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Überlebenskunst +5, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Atem anhalten.*** Der Echsenmensch kann 15 Minuten lang den Atem anhalten.
:
### Aktionen
***Mehrfachangriff.*** Der Echsenmensch führt zwei Nahkampfangriffe aus, jeden mit einer anderen Waffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Dornenschild.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Schwerer Knüppel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Wuchtschaden.
:
***Wurfspeer.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m oder Reichweite 9/36 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
}}


{{monster,frame
## Eidechse
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 2 (1W4)
**Speed**          :: 6 m, Klettern 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|11 (+0)|10 (+0)|1  (-5)|8  (-1)|3  (-4)|
___
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +0 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Einhorn
*Großes celestisches Wesen, rechtschaffen gut*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 67 (9W10+18)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|14 (+2)|15 (+2)|11 (+0)|17 (+3)|16 (+3)|
___
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Gelähmt, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: Celestisch, Elfisch, Sylvanisch, Telepathie auf 18 m
**Herausforderung**         :: 5 (1.800 EP)
___
***Magieresistenz.*** Das Einhorn ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Einhorns sind magisch.
:
***Sturmangriff.*** Wenn das Einhorn sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Hornangriff trifft, erleidet das Ziel zusätzlich 9 (2W8) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-15-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Einhorns ist Charisma (Zauberrettungswurf-SG 14). Es kann folgende angeborene Zauber wirken, ohne Komponenten zu benötigen:
:
Beliebig oft: [*Druidenkunst*](https://openrpg.de/srd/5e/de/#spell-druidenkunst)[*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)[*Spurloses Gehen*](https://openrpg.de/srd/5e/de/#spell-spurloses-gehen)<br>
Je 1-mal täglich: [*Gefühle besänftigen*](https://openrpg.de/srd/5e/de/#spell-gefuehle-besaenftigen)[*Gutes und Böses bannen*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-bannen)[*Verstricken*](https://openrpg.de/srd/5e/de/#spell-verstricken)
:
### Aktionen
***Mehrfachangriff.*** Das Einhorn führt zwei Angriffe aus: einen Huf- und einen Hornangriff.
:
***Horn.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W8+4) Stichschaden.
:
***Hufe.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.
:
***Heilende Berührung (3-mal täglich).*** Das Einhorn berührt eine andere Kreatur mit seinem Horn. Das Ziel erhält magisch 11 (2W8+2) Trefferpunkte zurück. Außerdem entfernt die Berührung alle Krankheiten und neutralisiert alle Gifte, die auf das Ziel wirken.
:
***Teleportieren (1-mal täglich).*** Das Einhorn teleportiert sich selbst und bis zu drei bereitwillige Kreaturen im Abstand von bis zu 1,5 Metern von ihm, die es sehen kann, zusammen mit jeder Ausrüstung, die getragen oder gehalten wird, magisch an einen Ort im Radius von 1,6 Kilometern, den das Einhorn kennt.
:
### Legendäre Aktionen
Das Einhorn kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Es kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Das Einhorn erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Schimmernder Schild (kostet 2 Aktionen).*** Das Einhorn erzeugt um sich selbst oder eine andere Kreatur im Abstand von bis zu 18 Metern von ihm, die es sehen kann, ein schimmerndes magisches Feld. Das Ziel erhält bis zum Ende des nächsten Zugs des Einhorns einen Bonus von +2 auf seine RK.
:
***Selbstheilung (kostet 3 Aktionen).*** Das Einhorn erhält magisch 11 (2W8+2) Trefferpunkte zurück.
:
***Hufe.*** Das Einhorn führt einen Angriff mit seinen Hufen aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Eisbär
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 42 (5W10+15)
**Speed**          :: 12 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|10 (+0)|16 (+3)|2  (-4)|13 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Scharfer Geruchssinn.*** Der Bär ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Bär führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (1W8+5) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W6+5) Hiebschaden.
:
}}


{{monster,frame
## Eisengolem
*Großes Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 20 (natürliche Rüstung)
**Trefferpunkte**  :: 210 (20W10+100)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|24 (+7)|9  (-1)|20 (+5)|3  (-4)|11 (+0)|1  (-5)|
___
**Schadensimmunitäten**     :: Feuer, Gift, Psychisch; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Adamant
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht die Sprache seines Schöpfers, aber kann nicht sprechen
**Herausforderung**         :: 16 (15.000 EP)
___
***Feuerabsorption.*** Der Golem erleidet keinen Schaden durch Feuer, sondern gewinnt Trefferpunkte in Höhe des Feuerschadens zurück.
:
***Magieresistenz.*** Der Golem ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Golems sind magisch.
:
***Unveränderliche Form.*** Der Golem ist gegen alle Zauber und Effekte, die seine Gestalt ändern würden, immun.
:
### Aktionen
***Mehrfachangriff.*** Der Golem führt zwei Nahkampfangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 20 (3W8+7) Wuchtschaden.
:
***Schwert.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 23 (3W10+7) Hiebschaden.
:
***Giftodem (Aufladung 6).*** Der Golem atmet giftiges Gas in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-19-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 45 (10W8) Giftschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Eis-Mephit
*Kleiner Elementar, neutral böse*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 21 (6W6)
**Speed**          :: 9 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|7  (-2)|13 (+1)|10 (+0)|9  (-1)|11 (+0)|12 (+1)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +2
**Schadensanfälligkeiten**  :: Feuer, Wucht
**Schadensimmunitäten**     :: Gift, Kälte
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Aqual, Aural
**Herausforderung**         :: 1/2 (100 EP)
___
***Angeborenes Zauberwirken (1-mal täglich).*** Der Mephit kann den angeborenen Zauber Nebelwolke wirken, ohne Materialkomponenten zu benötigen. Sein Attribut zum angeborenen Zauberwirken ist Charisma.
:
***Falsches Erscheinungsbild.*** Solange der Mephit sich nicht bewegt, ist er nicht von einem gewöhnlichen Eissplitter zu unterscheiden.
:
***Todesexplosion.*** Wenn der Mephit stirbt, vergeht er in einer Explosion scharfer Eissplitter. Jede Kreatur im Abstand von bis zu 1,5 Metern von ihm muss einen SG-10-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 4 (1W8) Hiebschaden, anderenfalls die Hälfte.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 3 (1W4+1) Hiebschaden plus 2 (1W4) Kälteschaden.
:
***Frostodem (Aufladung 6).*** Der Mephit atmet eisige Luft in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-10-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 5 (2W4) Kälteschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Eisteufel
*Großer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 180 (19W10+76)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|14 (+2)|18 (+4)|18 (+4)|15 (+2)|18 (+4)|
___
**Rettungswürfe**           :: Ges +7, Kon +9, Wei +7, Cha +9
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift, Kälte
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 14 (11.500 EP)
___
***Magieresistenz.*** Der Teufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Teufelssicht.*** Die Dunkelsicht des Teufels wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Mehrfachangriff.*** Der Teufel führt drei Angriffe aus: einen Biss-, einen Klauen- und einen Schwanzangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W6+5) Stichschaden plus 10 (3W6) Kälteschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W4+5) Hiebschaden plus 10 (3W6) Kälteschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 12 (2W6+5) Wuchtschaden plus 10 (3W6) Kälteschaden.
:
***Eiswand (Aufladung 6).*** Der Teufel bildet auf einer festen Oberfläche im Abstand von bis zu 18 Metern, die er sehen kann, magisch eine undurchsichtige Eiswand. Die Wand ist 30 Zentimeter dick, bis zu neun Meter lang und drei Meter hoch, oder sie bildet eine halbkugelförmige Glocke von bis zu sechs Metern Durchmesser. Wenn die Wand erscheint, wird jede Kreatur in ihrem Bereich auf dem kürzesten Weg aus ihr herausgeschoben. Die Kreatur kann auswählen, auf welche Seite der Wand sie gelangen möchte, sofern sie nicht kampfunfähig ist. Dann führt die Kreatur einen SG-17-Geschicklichkeitsrettungswurf aus. Scheitert der Wurf, erleidet sie 35 (10W6) Kälteschaden, anderenfalls die Hälfte. Die Wand bleibt eine Minute lang bestehen, oder bis der Teufel kampfunfähig wird oder stirbt. Sie kann beschädigt und durchbrochen werden. Jeder Abschnitt von drei Metern besitzt eine RK von 5, 30 Trefferpunkte, ist anfällig für Feuerschaden und gegen Gift-, Kälte-, nekrotischen und psychischen Schaden sowie Säureschaden immun. Wenn ein Abschnitt zerstört wird, hinterlässt er in seinem Bereich eine eisige Luftschicht. Wann immer eine Kreatur in ihrem Zug bereitwillig oder unfreiwillig eine Bewegung durch die eisige Luft beendet, muss sie einen SG-17-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 17 (5W6) Kälteschaden, anderenfalls die Hälfte. Die eisige Luft löst sich auf, wenn der letzte Rest der Eiswand verschwindet.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Elch
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 13 (2W10+2)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|10 (+0)|12 (+1)|2  (-4)|10 (+0)|6  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Sturmangriff.*** Wenn der Elch sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Rammbock-Angriff trifft, erleidet das Ziel zusätzlich 7 (2W6) Schaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine liegende Kreatur. *Treffer*: 8 (2W4+3) Wuchtschaden.
:
***Rammbock.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Wuchtschaden.
:
}}


{{monster,frame
## Elefant
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 76 (8W12+24)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|9  (-1)|17 (+3)|3  (-4)|11 (+0)|6  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 4 (1.100 EP)
___
***Trampel-Sturmangriff.*** Wenn der Elefant sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Zerfleischen-Angriff trifft, muss das Ziel einen SG-12-Stärkerettungswurf bestehen, oder es wird umgestoßen. Gegen ein liegendes Ziel kann der Elefant als Bonusaktion einen Stampfen-Angriff ausführen.
:
### Aktionen
***Stampfen.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, eine liegende Kreatur. *Treffer*: 22 (3W10+6) Wuchtschaden.
:
***Zerfleischen.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 19 (3W8+6) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Erdelementar
*Großer Elementar, neutral*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 126 (12W10+60)
**Speed**          :: 9 m, Graben 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|8  (-1)|20 (+5)|5  (-3)|10 (+0)|5  (-3)|
___
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensanfälligkeiten**  :: Schall
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bewusstlos, Erschöpft, Gelähmt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Erschütterungssinn 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Terral
**Herausforderung**         :: 5 (1.800 EP)
___
***Belagerungsmonster.*** Der Elementar fügt Gegenständen und Gebäuden doppelten Schaden zu.
:
***Erdgleiten.*** Der Elementar kann sich durch nichtmagisches, unbearbeitetes Erd- und Steinmaterial graben. Dabei beeinträchtigt er das Material nicht, durch das er sich bewegt.
:
### Aktionen
***Mehrfachangriff.*** Der Elementar führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 14 (2W8+5) Wuchtschaden.
:
}}


{{monster,frame
## Erinnye
*Mittelgroßer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (Ritterrüstung)
**Trefferpunkte**  :: 153 (18W8+72)
**Speed**          :: 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|16 (+3)|18 (+4)|14 (+2)|14 (+2)|18 (+4)|
___
**Rettungswürfe**           :: Ges +7, Kon +8, Wei +6, Cha +8
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 12 (8.400 EP)
___
***Höllische Waffen.*** Die Waffenangriffe der Erynnie sind magisch und bewirken bei einem Treffer zusätzlich 13 (3W8) Giftschaden (im Angriff enthalten).
:
***Magieresistenz.*** Die Erinnye ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Die Erinnye führt drei Angriffe aus.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W8+4) Hiebschaden oder 9 (1W10+4) Hiebschaden bei zweihändiger Führung, plus 13 (3W8) Giftschaden.  Reaktionen
:
***Parieren.*** Die Erinnye erhöht ihre RK gegen einen Nahkampfangriff, der sie treffen würde, um 4. Dazu muss die Erinnye den Angreifer sehen können und eine Nahkampfwaffe führen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Erwachter Baum
*Riesige Pflanze, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 59 (6W12+14)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|6  (-2)|15 (+2)|10 (+0)|10 (+0)|7  (-2)|
___
**Schadensresistenzen**     :: Stichschaden, Wuchtschaden
**Schadensanfälligkeiten**  :: Feuer
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine seinem Schöpfer bekannte Sprache
**Herausforderung**         :: 2 (450 EP)
___
***Falsches Erscheinungsbild.*** Solange der Baum sich nicht bewegt, ist er nicht von einem gewöhnlichen Baum zu unterscheiden.
:
### Aktionen
***Hieb.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 14 (3W6+4) Wuchtschaden.
:
}}


{{monster,frame
## Erwachter Busch
*Kleine Pflanze, gesinnungslos*
___
**Rüstungsklasse** :: 9
**Trefferpunkte**  :: 10 (3W6)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|8  (-1)|11 (+0)|10 (+0)|10 (+0)|6  (-2)|
___
**Schadensresistenzen**     :: Stich
**Schadensanfälligkeiten**  :: Feuer
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine seinem Schöpfer bekannte Sprache
**Herausforderung**         :: 0 (10 EP)
___
***Falsches Erscheinungsbild.*** Solange der Busch sich nicht bewegt, ist er nicht von einem gewöhnlichen Busch zu unterscheiden.
:
### Aktionen
***Harken.*** *Nahkampfwaffenangriff*: +1 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 (1W4-1) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Erzmagier
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 12 (15 mit Magierrüstung)
**Trefferpunkte**  :: 99 (18W8+18)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|14 (+2)|12 (+1)|20 (+5)|15 (+2)|16 (+3)|
___
**Rettungswürfe**           :: Int +9, Wei +6
**Fertigkeiten**            :: Arkane Kunde +13, Geschichte +13
**Schadensresistenzen**     :: Schaden durch Zauber; Hieb, Stich und Wucht durch nichtmagische Angriffe (durch Steinhaut)
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Sechs beliebige Sprachen
**Herausforderung**         :: 12 (8.400 EP)
___
***Magieresistenz.*** Der Erzmagier ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Zauberwirken.*** Der Erzmagier ist ein Zauberwirker der 18. Stufe. Sein Attribut zum Zauberwirken ist Intelligenz (Zauberrettungswurf-SG 17, +9 auf Treffer mit Zauberangriffen). Der Erzmagier kann beliebig oft Selbstverkleidung und Unsichtbarkeit wirken und hat folgende Magierzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Feuerpfeil*](https://openrpg.de/srd/5e/de/#spell-feuerpfeil)[*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Magierhand*](https://openrpg.de/srd/5e/de/#spell-magierhand)[*Schockgriff*](https://openrpg.de/srd/5e/de/#spell-schockgriff)[*Taschenspielerei*](https://openrpg.de/srd/5e/de/#spell-taschenspielerei)<br>
1&period; Grad (4 Plätze): [*Identifizieren*](https://openrpg.de/srd/5e/de/#spell-identifizieren)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Magierrüstung&ast;*](https://openrpg.de/srd/5e/de/#spell-magierruestung)[*Magisches Geschoss*](https://openrpg.de/srd/5e/de/#spell-magisches-geschoss)<br>
2&period; Grad (3 Plätze): [*Gedanken wahrnehmen*](https://openrpg.de/srd/5e/de/#spell-gedanken-wahrnehmen)[*Nebelschritt*](https://openrpg.de/srd/5e/de/#spell-nebelschritt)[*Spiegelbilder*](https://openrpg.de/srd/5e/de/#spell-spiegelbilder)<br>
3&period; Grad (3 Plätze): [*Blitz*](https://openrpg.de/srd/5e/de/#spell-blitz)[*Fliegen*](https://openrpg.de/srd/5e/de/#spell-fliegen)[*Gegenzauber*](https://openrpg.de/srd/5e/de/#spell-gegenzauber)<br>
4&period; Grad (3 Plätze): [*Feuerschild*](https://openrpg.de/srd/5e/de/#spell-feuerschild)[*Steinhaut&ast;*](https://openrpg.de/srd/5e/de/#spell-steinhaut)[*Verbannung*](https://openrpg.de/srd/5e/de/#spell-verbannung)<br>
5&period; Grad (3 Plätze): [*Ausspähung*](https://openrpg.de/srd/5e/de/#spell-ausspaehung)[*Energiewand*](https://openrpg.de/srd/5e/de/#spell-energiewand)[*Kältekegel*](https://openrpg.de/srd/5e/de/#spell-kaeltekegel)<br>
6&period; Grad (1 Platz): [*Kugel der Unverwundbarkeit*](https://openrpg.de/srd/5e/de/#spell-kugel-der-unverwundbarkeit)<br>
7&period; Grad (1 Platz): [*Teleportieren*](https://openrpg.de/srd/5e/de/#spell-teleportieren)<br>
8&period; Grad (1 Platz): [*Gedankenleere&ast;*](https://openrpg.de/srd/5e/de/#spell-gedankenleere)<br>
9&period; Grad (1 Platz): [*Zeitstopp*](https://openrpg.de/srd/5e/de/#spell-zeitstopp)
:
&ast;Der Erzmagier wirkt diese Zauber vor dem Kampf auf sich selbst.
:
### Aktionen
***Dolch.*** *Fernkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Ettin
*Großer Riese, chaotisch böse*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 85 (10W10+30)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|8  (-1)|17 (+3)|6  (-2)|10 (+0)|8  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Orkisch, Riesisch
**Herausforderung**         :: 4 (1.100 EP)
___
***Wachsam.*** Wenn einer der Köpfe des Ettins schläft, ist der andere Kopf wach.
:
***Zwei Köpfe.*** Der Ettin ist bei Weisheitswürfen (Wahrnehmung) und bei Rettungswürfen gegen die Zustände Betäubt, Bewusstlos, Bezaubert, Blind, Taub und Verängstigt im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Ettin führt zwei Angriffe aus: einen mit seiner Streitaxt und einen mit seinem Morgenstern.
:
***Morgenstern.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W8+5) Stichschaden.
:
***Streitaxt.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W8+5) Hiebschaden.
:
}}


{{monster,frame
## Eule
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 1,5 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|13 (+1)|8  (-1)|2  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +3
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Die Eule ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Sicht basieren, im Vorteil.
:
***Vorbeifliegen.*** Die Eule provoziert keine Gelegenheitsangriffe, wenn sie die Reichweite eines Feindes verlässt.
:
### Aktionen
***Krallen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Eulenbär
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 59 (7W10+21)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|12 (+1)|17 (+3)|3  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 3 (700 EP)
___
***Scharfe Sicht und scharfer Geruchssinn.*** Der Eulenbär ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Eulenbär führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W8+5) Hiebschaden.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 10 (1W10+5) Stichschaden.
:
}}


{{monster,frame
## Falke
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 3 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|5  (-3)|16 (+3)|8  (-1)|2  (-4)|14 (+2)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfe Sicht.*** Der Falke ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Krallen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Feengeist
*Winziges Feenwesen, neutral gut*
___
**Rüstungsklasse** :: 15 (Lederrüstung)
**Trefferpunkte**  :: 2 (1W4)
**Speed**          :: 3 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|18 (+4)|10 (+0)|14 (+2)|13 (+1)|11 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +8, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Elfisch, Gemeinsprache, Sylvanisch
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Langschwert.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Hiebschaden.
:
***Kurzbogen.*** *Fernkampfwaffenangriff*: +6 auf Treffer, Reichweite 12/48 m, ein Ziel. *Treffer*: 1 Stichschaden, und das Ziel muss einen SG-10-Konstitutionsret-tungswurf bestehen, oder es ist eine Minute lang vergiftet. Wenn der Rettungswurf höchstens 5 Punkte ergibt, wird das vergiftete Ziel ebenso lange bewusstlos, oder bis es Schaden erleidet oder eine andere Kreatur eine Aktion verwendet, um es wachzurütteln.
:
***Herzsicht.*** Der Feengeist berührt eine Kreatur und erkennt magisch deren aktuellen emotionalen Zustand. Wenn ein SG-10-Charismarettungswurf des Ziels scheitert, erkennt der Feengeist außerdem dessen Gesinnung. Bei Celestischen, Unholden und Untoten scheitert der Rettungswurf automatisch.
:
***Unsichtbarkeit.*** Der Feengeist wird magisch unsichtbar, bis er angreift, einen Zauber wirkt oder seine Konzentration endet (wie bei einem Zauber). Ausrüstung, die der Feengeist trägt oder hält, ist ebenfalls unsichtbar.
:
}}


{{monster,frame
## Feuerelementar
*Großer Elementar, neutral*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 102 (12W10+36)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|17 (+3)|16 (+3)|6  (-2)|10 (+0)|7  (-2)|
___
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Bewusstlos, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Ignal
**Herausforderung**         :: 5 (1.800 EP)
___
***Beleuchtung.*** Der Elementar spendet im Radius von neun Metern helles Licht und im Radius von weiteren neun Metern dämmriges Licht.
:
***Feuergestalt.*** Der Elementar kann sich durch enge Bereiche mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen. Eine Kreatur, die den Elementar berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, erleidet 5 (1W10) Feuerschaden. Außerdem kann der Elementar in den Bereich einer feindlich gesinnten Kreatur eindringen und dort stoppen. Wenn er in seinem Zug erstmals in den Bereich der Kreatur gelangt, erleidet diese 5 (1W10) Feuerschaden und fängt Feuer. Sie erleidet zu Beginn jedes ihrer Züge 5 (1W10) Feuerschaden, bis eine Kreatur eine Aktion verwendet, um das Feuer zu löschen.
:
***Wasserempfindlichkeit.*** Je 1,5 Meter, die der Elementar sich in Wasser bewegt, oder je 4 Liter Wasser, die auf ihn gegossen werden, erleidet er 1 Kälteschaden.
:
### Aktionen
***Mehrfachangriff.*** Der Elementar führt zwei Berührungsangriffe aus.
:
***Berührung.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Feuerschaden. Wenn das Ziel eine Kreatur oder ein brennbares Objekt ist, fängt es Feuer. Es erleidet zu Beginn jedes seiner Züge 5 (1W10) Feuerschaden, bis eine Kreatur eine Aktion verwendet, um das Feuer zu löschen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Feuerriese
*Riesiger Riese, rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (Ritterrüstung)
**Trefferpunkte**  :: 162 (13W12+78)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|25 (+7)|9  (-1)|23 (+6)|10 (+0)|14 (+2)|13 (+1)|
___
**Rettungswürfe**           :: Ges +3, Kon +10, Cha +5
**Fertigkeiten**            :: Athletik +11, Wahrnehmung +6
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Passive Wahrnehmung 16
**Sprachen**                :: Riesisch
**Herausforderung**         :: 9 (5.000 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Riese führt zwei Angriffe mit dem Großschwert aus.
:
***Zweihandschwert.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 28 (6W6+7) Hiebschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +11 auf Treffer, Reichweite 18/72 m, ein Ziel. *Treffer*: 29 (4W10+7) Wuchtschaden.
:
}}


{{monster,frame
## Fledermaus
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 1,5 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|15 (+2)|8  (-1)|2  (-4)|12 (+1)|4  (-3)|
___
**Sinne**                   :: Blindsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Echolot.*** Die Fledermaus kann ihre Blindsicht nicht verwenden, solange sie taub ist.
:
***Scharfes Gehör.*** Die Fledermaus ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +0 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 1 Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Fledermausschwarm
*Mittelgroßer Schwarm winziger Tiere, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 22 (5W8)
**Speed**          :: 0 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|5  (-3)|15 (+2)|10 (+0)|2  (-4)|12 (+1)|4  (-3)|
___
**Schadensresistenzen**     :: Hieb, Stich, Wucht
**Zustandsimmunitäten**     :: Betäubt, Bezaubert, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Versteinert
**Sinne**                   :: Blindsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Echolot.*** Der Schwarm kann seine Blindsicht nicht verwenden, solange er taub ist.
:
***Scharfes Gehör.*** Der Schwarm ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör basieren, im Vorteil.
:
***Schwarm.*** Der Schwarm kann den Bereich einer anderen Kreatur besetzen und umgekehrt. Er kann sich durch jede Öffnung bewegen, die groß genug für eine winzige Fledermaus ist. Der Schwarm kann keine Trefferpunkte zurückerhalten und keine temporären Trefferpunkte erhalten.
:
### Aktionen
***Bisse.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 0 m, eine Kreatur im Bereich des Schwarms. *Treffer*: 5 (2W4) Stichschaden oder 2 (1W4) Stichschaden, wenn der Schwarm höchstens die Hälfte seiner Trefferpunkte hat.
:
}}


{{monster,frame,wide
## Fleischgolem
*Mittelgroßes Konstrukt, neutral*
___
**Rüstungsklasse** :: 9
**Trefferpunkte**  :: 93 (11W8+44)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|9  (-1)|18 (+4)|6  (-2)|10 (+0)|5  (-3)|
___
**Schadensimmunitäten**     :: Blitz, Gift; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Adamant
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht die Sprache seines Schöpfers, aber kann nicht sprechen
**Herausforderung**         :: 5 (1.800 EP)
___
***Abneigung gegen Feuer.*** Wenn der Golem Feuerschaden erleidet, ist er bis zum Ende seines nächsten Zugs bei Angriffswürfen und Attributswürfen im Nachteil.
:
***Berserker.*** Wenn der Golem seinen Zug mit höchstens 40 Trefferpunkten beginnt, würfle mit einem W6. Bei einer 6 gerät der Golem in Kampfrausch. Im Kampfrausch greift der Golem die Kreatur an, die ihm am nächsten ist und die er sehen kann. Befindet sich keine Kreatur in Angriffsreichweite, greift der Golem ein Objekt an, vorzugsweise eines, das kleiner ist als er selbst. Der Kampfrausch hält an, bis der Golem zerstört wird oder alle Trefferpunkte zurückgewinnt. Der Schöpfer des Golems kann versuchen, ihn durch strenges, überzeugendes Zureden aus dem Kampfrausch zu holen, sofern er sich höchstens 18 Meter vom Golem entfernt befindet. Der Golem muss seinen Schöpfer dazu hören können, und dieser muss eine Aktion einen SG-15-Charismawurf (Überzeugen) ausführen. Bei einem Erfolg ist der Kampfrausch des Golems beendet. Erleidet der Golem Schaden, solange er höchstens 40 Trefferpunkte hat, kann er wieder in Kampfrausch geraten.
:
***Blitzabsorption.*** Der Golem erleidet durch Blitze keinen Schaden, sondern gewinnt Trefferpunkte in Höhe des Blitzschadens zurück.
:
***Magieresistenz.*** Der Golem ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Golems sind magisch.
:
***Unveränderliche Form.*** Der Golem ist gegen alle Zauber und Effekte, die seine Gestalt ändern würden, immun.
:
### Aktionen
***Mehrfachangriff.*** Der Golem führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Wuchtschaden.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Fliegende Schlange
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14
**Trefferpunkte**  :: 5 (2W4)
**Speed**          :: 9 m, Fliegen 18 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|4  (-3)|18 (+4)|11 (+0)|2  (-4)|12 (+1)|5  (-3)|
___
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Vorbeifliegen.*** Die Schlange provoziert keine Gelegenheitsangriffe, wenn sie die Reichweite eines Feindes verlässt.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden plus 7 (3W4) Giftschaden.
:
}}


{{monster,frame
## Fliegendes Schwert
*Kleines Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 17 (5W6)
**Speed**          :: 0 m, Fliegen 15 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|15 (+2)|11 (+0)|1  (-5)|5  (-3)|1  (-5)|
___
**Rettungswürfe**           :: Ges +4
**Schadensimmunitäten**     :: Gift, Psychisch
**Zustandsimmunitäten**     :: Bezaubert, Blind, Gelähmt, Liegend, Taub, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 7
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Antimagische Empfindlichkeit.*** Das Schwert ist im Bereich eines antimagischen Felds kampfunfähig. Wird es Ziel des Zaubers Magie bannen, muss das Schwert einen Konstitutionsrettungswurf gegen den Zauberrettungswurf-SG des Zauberwirkers bestehen, oder es ist eine Minute lang bewusstlos.
:
***Falsches Erscheinungsbild.*** Solange das Schwert sich nicht bewegt und nicht fliegt, ist es nicht von einem gewöhnlichen Schwert zu unterscheiden.
:
### Aktionen
***Langschwert.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W8+1) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Flimmerhund
*Mittelgroßes Feenwesen, rechtschaffen gut*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 22 (4W8+4)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|17 (+3)|12 (+1)|10 (+0)|13 (+1)|11 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Flimmerhundisch, versteht Sylvanisch, aber kann es nicht sprechen
**Herausforderung**         :: 1/4 (50 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Der Hund ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden.
:
***Teleportieren (Aufladung 4–6).*** Der Hund teleportiert sich und alle Ausrüstung, die er trägt oder hält, magisch bis zu zwölf Meter weit an eine freie Stelle, die er sehen kann. Vor oder nach dem Teleportieren kann der Hund einen Bissangriff ausführen.
:
}}


{{monster,frame
## Frosch
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 6 m, Schwimmen 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1  (-5)|13 (+1)|8  (-1)|1  (-5)|8  (-1)|3  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +1
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 0 (0 EP)
___
***Amphibisch.*** Der Frosch kann Luft und Wasser atmen.
:
***Stehender Sprung.*** Der Frosch kann mit oder ohne Anlauf bis zu drei Meter weit und bis zu 1,5 Meter hoch springen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Frostriese
*Riesiger Riese, neutral böse*
___
**Rüstungsklasse** :: 15 (zusammengewürfelte Rüstung)
**Trefferpunkte**  :: 138 (12W12+60)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|9  (-1)|21 (+5)|9  (-1)|10 (+0)|12 (+1)|
___
**Rettungswürfe**           :: Kon +8, Wei +3, Cha +4
**Fertigkeiten**            :: Athletik +9, Wahrnehmung +3
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Riesisch
**Herausforderung**         :: 8 (3.900 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Riese führt zwei Angriffe mit der Zweihandaxt aus.
:
***Zweihandaxt.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 25 (3W12+6) Hiebschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +9 auf Treffer, Reichweite 18/72 m, ein Ziel. *Treffer*: 28 (4W10+6) Wuchtschaden.
:
}}


{{monster,frame,wide
## Gallertwürfel
*Großer Schlick, gesinnungslos*
___
**Rüstungsklasse** :: 6
**Trefferpunkte**  :: 84 (8W10+40)
**Speed**          :: 4,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|3  (-4)|20 (+5)|1  (-5)|6  (-2)|1  (-5)|
___
**Zustandsimmunitäten**     :: Bezaubert, Blind, Erschöpft, Liegend, Taub, Verängstigt
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Schlickwürfel.*** Der Würfel nimmt seinen gesamten Bereich ein. Andere Kreaturen können in den Bereich gelangen, werden dabei jedoch vom Würfel eingehüllt und sind beim Rettungswurf im Nachteil. Kreaturen im Würfel sind sichtbar, haben jedoch vollständige Deckung. Eine Kreatur im Abstand von bis zu 1,5 Metern vom Würfel kann eine Aktion verwenden, um eine Kreatur oder ein Objekt aus dem Würfel herauszuziehen. Dazu muss sie einen SG-12-Stärkewurf bestehen. Die Kreatur, die den Versuch wagt, erleidet 10 (3W6) Säureschaden. Der Würfel kann nur entweder eine große Kreatur oder bis zu vier höchstens mittelgroße Kreaturen zugleich enthalten.
:
***Transparent.*** Auch ein nicht getarnter Würfel kann nur mit einem erfolgreichen SG-15 Weisheitswurf (Wahrnehmung) entdeckt werden, sofern er sich weder bewegt noch angreift. Eine Kreatur, die in den Bereich des Würfels gelangt, ohne diesen zu bemerken, wird vom Würfel überrascht.
:
### Aktionen
***Scheinfuß.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 10 (3W6) Säureschaden.
:
***Einhüllen.*** Der Würfel nutzt seine Bewegungsrate. Dabei kann er in den Bereich von höchstens großen Kreaturen gelangen. In diesem Fall muss die betroffene Kreatur einen SG-12-Geschicklichkeitsrettungswurf ausführen. Bei einem erfolgreichen Rettungswurf kann die Kreatur wählen, 1,5 Meter zurück oder neben den Würfel geschoben zu werden. Wählt sie, nicht geschoben zu werden, gilt der Rettungswurf als gescheitert. Scheitert der Wurf, so gelangt der Würfel in den Bereich der Kreatur. Diese erleidet 10 (3W6) Säureschaden und wird eingehüllt. Die eingehüllte Kreatur kann nicht atmen, ist festgesetzt und erleidet zu Beginn jedes Zugs des Würfels 21 (6W6) Säureschaden. Bewegt sich der Würfel, so bewegt die eingehüllte Kreatur sich mit ihm. Eine eingehüllte Kreatur kann sich mit einer Aktion und einem SG-12-Stärkewurf zu befreien versuchen. Bei einem Erfolg entkommt die Kreatur und gelangt in einen Bereich ihrer Wahl im Abstand von bis zu 1,5 Metern vom Würfel.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Gargyl
*Mittelgroßer Elementar, chaotisch böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 52 (7W8+21)
**Speed**          :: 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|11 (+0)|16 (+3)|6  (-2)|11 (+0)|7  (-2)|
___
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Adamant
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Erschöpft, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Terral
**Herausforderung**         :: 2 (450 EP)
___
***Falsches Erscheinungsbild.*** Solange der Gargyl sich nicht bewegt, ist er nicht von einer leblosen Statue zu unterscheiden.
:
### Aktionen
***Mehrfachangriff.*** Der Gargyl führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Hiebschaden.
:
}}


{{monster,frame
## Geier
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 5 (1W8+1)
**Speed**          :: 3 m, Fliegen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|7  (-2)|10 (+0)|13 (+1)|2  (-4)|12 (+1)|4  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Rudeltaktik.*** Der Geier ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfe Sicht und scharfer Geruchssinn.*** Der Geier ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Schnabel.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Geisternaga
*Große Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 75 (10W10+20)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|17 (+3)|14 (+2)|16 (+3)|15 (+2)|16 (+3)|
___
**Rettungswürfe**           :: Ges +6, Kon +5, Wei +5, Cha +6
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Abyssisch, Gemeinsprache
**Herausforderung**         :: 8 (3.900 EP)
___
***Wiederbelebung.*** Wenn der Naga stirbt, wird er nach 1W6 Tagen wieder lebendig und erhält all seine Trefferpunkte zurück. Nur der Zauber Wunsch kann verhindern, dass dieses Merkmal wirkt.
:
***Zauberwirken.*** Der Naga ist ein Zauberwirker der 10. Stufe. Sein Attribut zum Zauberwirken ist Intelligenz (Zauberrettungswurf-SG 14, +6 auf Treffer mit Zauberangriffen), und er braucht nur verbale Komponenten, um seine Zauber zu wirken. Er hat die folgenden Magierzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Einfache Illusion*](https://openrpg.de/srd/5e/de/#spell-einfache-illusion)[*Kältestrahl*](https://openrpg.de/srd/5e/de/#spell-kaeltestrahl)[*Magierhand*](https://openrpg.de/srd/5e/de/#spell-magierhand)<br>
1&period; Grad (4 Plätze): [*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Person bezaubern*](https://openrpg.de/srd/5e/de/#spell-person-bezaubern)[*Schlaf*](https://openrpg.de/srd/5e/de/#spell-schlaf)<br>
2&period; Grad (3 Plätze): [*Gedanken wahrnehmen*](https://openrpg.de/srd/5e/de/#spell-gedanken-wahrnehmen)[*Person festhalten*](https://openrpg.de/srd/5e/de/#spell-person-festhalten)<br>
3&period; Grad (3 Plätze): [*Blitz*](https://openrpg.de/srd/5e/de/#spell-blitz)[*Wasser atmen*](https://openrpg.de/srd/5e/de/#spell-wasser-atmen)<br>
4&period; Grad (3 Plätze): [*Dimensionstür*](https://openrpg.de/srd/5e/de/#spell-dimensionstuer)[*Plage*](https://openrpg.de/srd/5e/de/#spell-plage)<br>
5&period; Grad (2 Plätze): [*Person beherrschen*](https://openrpg.de/srd/5e/de/#spell-person-beherrschen)
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 7 (1W6+4) Stichschaden, und das Ziel muss einen SG-13-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 31 (7W8) Giftschaden, anderenfalls die Hälfte.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Geist
*Mittelgroßer Untoter, jede Gesinnung*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 45 (10W8)
**Speed**          :: 0 m, Fliegen 12 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|7  (-2)|13 (+1)|10 (+0)|10 (+0)|12 (+1)|17 (+3)|
___
**Schadensresistenzen**     :: Blitz, Feuer, Säure, Schall; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift, Kälte, Nekrotisch
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: Alle zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 4 (1.100 EP)
___
***Ätherische Sicht.*** Der Geist kann 18 Meter weit in die Ätherebene blicken, wenn er sich auf der materiellen Ebene befindet, sowie umgekehrt.
:
***Körperlose Bewegung.*** Der Geist kann sich durch andere Kreaturen und durch Gegenstände bewegen, als wären sie schwieriges Gelände. Er erleidet 5 (1W10) Energieschaden, wenn er seinen Zug in einem Gegenstand beendet.
:
### Aktionen
***Vernichtende Berührung.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 17 (4W6+3) nekrotischer Schaden.
:
***Ätherische Gestalten.*** Der Geist begibt sich von der materiellen Ebene aus auf die Ätherebene oder umgekehrt. Er ist auf der materiellen Ebene sichtbar, solange er sich an der Äthergrenze befindet, sowie umgekehrt, aber er kann weder etwas auf der anderen Ebene beeinflussen noch von etwas auf der anderen Ebene beeinflusst werden.
:
***Grauenhaftes Antlitz.*** Alle Kreaturen - außer Untoten - im Abstand von bis zu 18 Metern vom Geist, die ihn sehen können, müssen einen SG-13-Weisheitsrettungswurf bestehen, oder sie sind eine Minute lang verängstigt. Wenn der Rettungswurf um mindestens 5 Punkte scheitert, altert das Ziel zusätzlich um 1W4×10 Jahre. Ein verängstigtes Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Verängstigt-Zustand bei einem Erfolg beenden. Wenn der Rettungswurf erfolgreich ist oder der Effekt endet, ist das Ziel 24 Stunden lang immun gegen das grauenhafte Antlitz des Geists. Der Alterungseffekt kann mit dem Zauber Vollständige Genesung rückgängig gemacht werden, allerdings nur in den ersten 24 Stunden, nachdem er aufgetreten ist.
:
***Inbesitznahme (Aufladung 6).*** Ein Humanoide im Abstand von bis zu 1,5 Metern vom Geist, den der Geist sehen kann, muss einen SG-13-Charismarettungswurf bestehen, oder der Geist ergreift Besitz von ihm. In dem Fall verschwindet der Geist, und das Ziel ist kampfunfähig und verliert die Kontrolle über seinen Körper. Der Geist kontrolliert jetzt den Körper des Ziels, nimmt dem Ziel aber nicht das Bewusstsein. Der Geist kann nicht das Ziel von Angriffen, Zaubern oder anderen Effekten werden - außer solchen, die Untote vertreiben. Er behält Gesinnung, Intelligenz, Weisheit und Charisma sowie seine Immunität gegen Bezaubert und Verängstigt bei. Ansonsten gelten die Werte des besessenen Ziels. Wissen, Klassenmerkmale und Übungen des Ziels sind jedoch nicht verfügbar. Die Inbesitznahme bleibt bestehen, bis die Trefferpunkte des besessenen Körpers auf 0 sinken, der Geist die Inbesitznahme als Bonusaktion beendet oder er vertrieben bzw. von einem Effekt wie vom Zauber Gutes und Böses bannen exorziert wird. Wenn die Inbesitznahme endet, erscheint der Geist an einer freien Stelle im Abstand von bis zu 1,5 Metern vom Körper des Ziels wieder. Wenn der Rettungswurf erfolgreich ist oder die Inbesitznahme endet, ist das Ziel 24 Stunden lang gegen die Inbesitznahme des Geists immun.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Gemeiner
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 4 (1W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|10 (+0)|10 (+0)|10 (+0)|10 (+0)|10 (+0)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 0 (10 EP)
___
### Aktionen
***Knüppel.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Wuchtschaden.
:
}}


{{monster,frame
## Ghul
*Mittelgroßer Untoter, chaotisch böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 22 (5W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|15 (+2)|10 (+0)|7  (-2)|10 (+0)|6  (-2)|
___
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache
**Herausforderung**         :: 1 (200 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 9 (2W6+2) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Hiebschaden. Falls das Ziel eine Kreatur ist, aber kein Elf oder Untoter, muss es einen SG-10-Konstitutionsrettungswurf bestehen, um nicht eine Minute lang gelähmt zu werden. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Giftschlange
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 2 (1W4)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|16 (+3)|11 (+0)|1  (-5)|10 (+0)|3  (-4)|
___
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden, und das Ziel muss einen SG-10-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 5 (2W4) Giftschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Giftschlangenschwarm
*Mittelgroßer Schwarm winziger Tiere, gesinnungslos*
___
**Rüstungsklasse** :: 14
**Trefferpunkte**  :: 36 (8W8)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|18 (+4)|11 (+0)|1  (-5)|10 (+0)|3  (-4)|
___
**Schadensresistenzen**     :: Hieb, Stich, Wucht
**Zustandsimmunitäten**     :: Betäubt, Bezaubert, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Versteinert
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Schwarm.*** Der Schwarm kann den Raum einer anderen Kreatur besetzen und umgekehrt. Er kann sich durch jede Öffnung bewegen, die groß genug für eine winzige Schlange ist. Der Schwarm kann keine Trefferpunkte zurückerhalten und keine temporären Trefferpunkte erhalten.
:
### Aktionen
***Bisse.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 0 m, eine Kreatur im Bereich des Schwarms. *Treffer*: 7 (2W6) Stichschaden oder 3 (1W6) Stichschaden, wenn der Schwarm höchstens die Hälfte seiner Trefferpunkte hat. Das Ziel muss einen SG-10-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 14 (4W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Glabrezu
*Großer Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 157 (15W10+75)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|15 (+2)|21 (+5)|19 (+4)|17 (+3)|16 (+3)|
___
**Rettungswürfe**           :: Str +9, Kon +9, Wei +7, Cha +7
**Schadensresistenzen**     :: Blitz, Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 13
**Sprachen**                :: Abyssisch, Telepathie auf 36 m
**Herausforderung**         :: 9 (5.000 EP)
___
***Magieresistenz.*** Der Glabrezu ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Glabrezus ist Intelligenz (Zauberrettungswurf-SG 16). Der Glabrezu kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Dunkelheit*](https://openrpg.de/srd/5e/de/#spell-dunkelheit)[*Magie bannen*](https://openrpg.de/srd/5e/de/#spell-magie-bannen)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)<br>
Je 1-mal täglich: [*Fliegen*](https://openrpg.de/srd/5e/de/#spell-fliegen)[*Verwirrung*](https://openrpg.de/srd/5e/de/#spell-verwirrung)[*Wort der Macht: Betäubung*](https://openrpg.de/srd/5e/de/#spell-wort-der-macht-betaeubung)
:
### Aktionen
***Mehrfachangriff.*** Der Glabrezu führt vier Angriffe aus: zwei mit seinen Zangen und zwei mit seinen Fäusten. Alternativ führt er zwei Angriffe mit seinen Zangen aus und wirkt einen Zauber.
:
***Faust.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Wuchtschaden.
:
***Zange.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 16 (2W10+5) Wuchtschaden. Wenn das Ziel eine höchstens mittelgroße Kreatur ist, wird es gepackt (Rettungswurf-SG 15). Der Glabrezu hat zwei Zangen, die jeweils ein Ziel packen können.
:
}}


{{monster,frame
## Gladiator
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 16 (beschlagenes Leder, Schild)
**Trefferpunkte**  :: 112 (15W8+45)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|10 (+0)|12 (+1)|15 (+2)|
___
**Rettungswürfe**           :: Str +7, Ges +5, Kon +6
**Fertigkeiten**            :: Athletik +10, Einschüchtern +5
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 5 (1.800 EP)
___
***Rohling.*** Eine Nahkampfwaffe bewirkt einen zusätzlichen Würfel ihres Schadens, wenn der Gladiator damit trifft (im Angriff enthalten).
:
***Tapferkeit.*** Der Gladiator ist bei Rettungswürfen gegen den Zustand Verängstigt im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Gladiator führt drei Nahkampfangriffe oder zwei Fernkampfangriffe aus.
:
***Schildstoß.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 9 (2W4+4) Wuchtschaden. Wenn das Ziel eine höchstens mittelgroße Kreatur ist, muss es einen SG-15-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Speer.*** *Fernkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m und Reichweite 6/18 m, ein Ziel. *Treffer*: 11 (2W6+4) Stichschaden oder 13 (2W8+4) Stichschaden bei zweihändiger Führung und Nahkampfangriff.
:
### Reaktionen
***Parieren.*** Der Gladiator erhöht seine RK gegen einen Nahkampfangriff, der ihn treffen würde, um 3. Dazu muss der Gladiator den Angreifer sehen können und eine Nahkampfwaffe führen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Gnoll
*Mittelgroßer Humanoide (Gnoll), chaotisch böse*
___
**Rüstungsklasse** :: 15 (Fellrüstung, Schild)
**Trefferpunkte**  :: 22 (5W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|12 (+1)|11 (+0)|6  (-2)|10 (+0)|7  (-2)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Gnollisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Wüten.*** Wenn der Gnoll in seinem Zug die Trefferpunkte einer Kreatur mit einem Nahkampfangriff auf 0 verringert hat, kann er eine Bonusaktion ausführen, um bis zur Hälfte seiner Bewegungsrate zurückzulegen und einen Bissangriff auszuführen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W4+2) Stichschaden.
:
***Speer.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden oder 6 (1W8+2) Stichschaden bei zweihändiger Führung und Nahkampfangriff.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 5 (1W8+1) Stichschaden.
:
}}


{{monster,frame
## Gnom, Tiefengnom (Svirfneblin)
*Kleiner Humanoide (Gnom), neutral gut*
___
**Rüstungsklasse** :: 15 (Kettenhemd)
**Trefferpunkte**  :: 16 (3W6+6)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|14 (+2)|14 (+2)|12 (+1)|10 (+0)|9  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Nachforschungen +3, Wahrnehmung +2
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Gemeinsprache der Unterreiche, Gnomisch, Terral
**Herausforderung**         :: 1/2 (100 EP)
___
***Gnomische Gerissenheit.*** Der Gnom ist bei Intelligenz-, Weisheits- und Charismarettungswürfen gegen Magie im Vorteil.
:
***Steintarnung.*** Der Gnom ist bei Geschicklichkeitswürfen (Heimlichkeit), die er ausführt, um sich in steinigem Gelände zu verstecken, im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Gnoms ist Intelligenz (Zauberrettungswurf-SG 11). Der Gnom kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Unauffindbarkeit (nur auf sich selbst)*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)<br>
Je 1-mal täglich: [*Blindheit/Taubheit*](https://openrpg.de/srd/5e/de/#spell-blindheit-taubheit)[*Selbstverkleidung*](https://openrpg.de/srd/5e/de/#spell-selbstverkleidung)[*Verschwimmen*](https://openrpg.de/srd/5e/de/#spell-verschwimmen)
:
### Aktionen
***Kriegspicke.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
***Vergifteter Wurfpfeil.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 9/36 m, eine Kreatur. *Treffer*: 4 (1W4+2) Stichschaden, und das Ziel muss einen SG-12-Konstitutionsrettungswurf bestehen, oder es ist eine Minute lang vergiftet. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Goblin
*Kleiner Humanoide (Goblinoide), neutral böse*
___
**Rüstungsklasse** :: 15 (Lederrüstung, Schild)
**Trefferpunkte**  :: 7 (2W6)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|14 (+2)|10 (+0)|10 (+0)|8  (-1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +6
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: Gemeinsprache, Goblinisch
**Herausforderung**         :: 1/4 (50 EP)
___
***Behändes Entkommen.*** Der Goblin kann in jedem seiner Züge die Rückzugs- oder die Verstecken-Aktion als Bonusaktion ausführen.
:
### Aktionen
***Krummsäbel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Hiebschaden.
:
***Kurzbogen.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 24/96 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
}}


{{monster,frame
## Golddrachennestling
*Mittelgroßer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 60 (8W8+24)
**Speed**          :: 9 m, Fliegen 18 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|14 (+2)|17 (+3)|14 (+2)|11 (+0)|16 (+3)|
___
**Rettungswürfe**           :: Ges +4, Kon +5, Wei +2, Cha +5
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +4
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 3 (700 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (1W10+4) Stichschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-13-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 22 (4W10) Feuerschaden, anderenfalls die Hälfte.
:
***Schwächender Odem.*** Der Drache atmet Gas in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-13-Stärkerettungswurf bestehen, oder sie ist eine Minute lang bei stärkebasierten Angriffswürfen, bei Stärkewürfen und Stärkerettungswürfen im Nachteil. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Gorgone
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 114 (12W10+48)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|11 (+0)|18 (+4)|2  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Zustandsimmunitäten**     :: Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Trampel-Sturmangriff.*** Wenn der Gorgone sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im gleichen Zug mit einem Zerfleischen-Angriff trifft, muss das Ziel einen SG-16-Stärkerettungswurf bestehen, oder es wird umgestoßen. Gegen ein liegendes Ziel kann der Gorgone als Bonusaktion einen Angriff mit seinen Hufen ausführen.
:
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 16 (2W10+5) Wuchtschaden.
:
***Zerfleischen.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 18 (2W12+5) Stichschaden.
:
***Versteinernder Odem (Aufladung 5–6).*** Der Gorgone atmet versteinerndes Gas in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-13-Konstitutionsrettungswurf bestehen. Scheitert der Wurf, so beginnt das Ziel zu versteinern und ist festgesetzt. Das festgesetzte Ziel muss den Rettungswurf am Ende seines nächsten Zugs wiederholen. Bei einem Erfolg endet der Effekt. Bei einem Misserfolg ist das Ziel versteinert, bis es durch den Zauber Vollständige Genesung oder andere Magie befreit wird.
:
}}


{{monster,frame
## Grauschlick
*Mittelgroßer Schlick, gesinnungslos*
___
**Rüstungsklasse** :: 8
**Trefferpunkte**  :: 22 (3W8+9)
**Speed**          :: 3 m, Klettern 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|6  (-2)|16 (+3)|1  (-5)|6  (-2)|2  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +2
**Schadensresistenzen**     :: Feuer, Kälte, Säure
**Zustandsimmunitäten**     :: Bezaubert, Blind, Erschöpft, Liegend, Taub, Verängstigt
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Amorph.*** Der Schlick kann sich durch enge Bereiche mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen.
:
***Falsches Erscheinungsbild.*** Solange der Schlick sich nicht bewegt, ist er nicht von einer öligen Pfütze oder einem nassen Felsblock zu unterscheiden.
:
***Metall korrodieren.*** Jede nichtmagische Waffe aus Metall, die den Schlick trifft, korrodiert. Wenn die Waffe Schaden bewirkt hat, erhält sie einen permanenten und kumulativen Malus von -1 auf Schadenswürfe. Sinkt ihr Malus auf -5, wird die Waffe zerstört. Nichtmagische Munition aus Metall, die den Schlick trifft, wird zerstört, nachdem sie Schaden bewirkt hat. Der Schlick kann sich in einer Runde durch fünf Zentimeter starkes nichtmagisches Metall fressen.
:
### Aktionen
***Scheinfuß.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Wuchtschaden plus 7 (2W6) Säureschaden, und wenn das Ziel eine nichtmagische Metallrüstung trägt, verrostet diese teilweise und erleidet einen permanenten und kumulativen Malus von -1 auf die RK, die sie bietet. Die Rüstung wird zerstört, wenn der Malus ihre RK auf 10 verringert.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Greif
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 59 (7W10+21)
**Speed**          :: 9 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|2  (-4)|13 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +5
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 15
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Scharfe Sicht.*** Der Greif ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Greif führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W8+4) Stichschaden.
:
}}


{{monster,frame
## Grick
*Mittelgroße Monstrosität, neutral*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 27 (6W8)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|14 (+2)|11 (+0)|3  (-4)|14 (+2)|5  (-3)|
___
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Steintarnung.*** Der Grick ist bei Geschicklichkeitswürfen (Heimlichkeit), die er ausführt, um sich in steinigem Gelände zu verstecken, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Grick führt einen Tentakelangriff aus. Wenn dieser Angriff trifft, kann der Grick einen Angriff mit dem Schnabel gegen dasselbe Ziel ausführen.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Tentakel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (2W6+2) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Grimlock
*Mittelgroßer Humanoide (Grimlock), neutral böse*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|12 (+1)|12 (+1)|9  (-1)|8  (-1)|6  (-2)|
___
**Fertigkeiten**            :: Athletik +5, Heimlichkeit +3, Wahrnehmung +3
**Zustandsimmunitäten**     :: Blind
**Sinne**                   :: Blindsicht 9 m oder 3 m bei Taubheit (blind außerhalb des Radius), Passive Wahrnehmung 13
**Sprachen**                :: Gemeinsprache der Unterreiche
**Herausforderung**         :: 1/4 (50 EP)
___
***Blinde Sinne.*** Der Grimlock kann seine Blindsicht nicht verwenden, solange er taub ist und nicht riechen kann.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Grimlock ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
***Steintarnung.*** Der Grimlock ist bei Geschicklichkeitswürfen (Heimlichkeit), die er ausführt, um sich in steinigem Gelände zu verstecken, im Vorteil.
:
### Aktionen
***Knöcherner Dornenknüppel.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W4+3) Wuchtschaden plus 2 (1W4) Stichschaden.
:
}}


{{monster,frame
## Grottenschrat
*Mittelgroßer Humanoide (Goblinoide), chaotisch böse*
___
**Rüstungsklasse** :: 16 (Fellrüstung, Schild)
**Trefferpunkte**  :: 27 (5W8+5)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|14 (+2)|13 (+1)|8  (-1)|11 (+0)|9  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Überlebenskunst +2
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache, Goblinisch
**Herausforderung**         :: 1 (200 EP)
___
***Rohling.*** Eine Nahkampfwaffe bewirkt einen zusätzlichen Würfel ihres Schadens, wenn der Grottenschrat damit trifft (im Angriff enthalten).
:
***Überraschungsangriff.*** Wenn der Grottenschrat in der ersten Kampfrunde eine Kreatur überrascht und mit einem Angriff trifft, erleidet das Ziel zusätzlich 7 (2W6) Schaden durch den Angriff.
:
### Aktionen
***Morgenstern.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W8+2) Stichschaden.
:
***Wurfspeer.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m oder Reichweite 9/36 m, ein Ziel. *Treffer*: 9 (2W6+2) Stichschaden im Nahkampf oder 5 (1W6+2) Stichschaden im Fernkampf.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Grüner Drachennestling
*Mittelgroßer Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 38 (7W8+7)
**Speed**          :: 9 m, Fliegen 18 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|12 (+1)|13 (+1)|14 (+2)|11 (+0)|13 (+1)|
___
**Rettungswürfe**           :: Ges +3, Kon +3, Wei +2, Cha +3
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +4
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 2 (450 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W10+2) Stichschaden plus 3 (1W6) Giftschaden.
:
***Giftodem (Aufladung 5–6).*** Der Drache atmet giftiges Gas in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 21 (6W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame,wide
## Grüne Vettel
*Mittelgroßes Feenwesen, neutral böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 82 (11W8+33)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|12 (+1)|16 (+3)|13 (+1)|14 (+2)|14 (+2)|
___
**Fertigkeiten**            :: Arkane Kunde +3, Heimlichkeit +3, Täuschen +4, Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch, Gemeinsprache, Sylvanisch
**Herausforderung**         :: 3 (700 EP)
___
***Amphibisch.*** Die Vettel kann Luft und Wasser atmen.
:
***Stimmen nachahmen.*** Die Vettel kann Tierlaute und menschliche Stimmen nachahmen. Ein Kreatur, die diese Geräusche hört, erkennt sie als Imitation, sofern sie einen SG-14-Weisheitswurf (Motiv erkennen) besteht.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken der Vettel ist Charisma (Zauberrettungswurf-SG 12). Die Vettel kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Einfache Illusion*](https://openrpg.de/srd/5e/de/#spell-einfache-illusion)[*Gehässiger Spott*](https://openrpg.de/srd/5e/de/#spell-gehaessiger-spott)[*Tanzende Lichter*](https://openrpg.de/srd/5e/de/#spell-tanzende-lichter)
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Hiebschaden.
:
***Illusionäres Erscheinungsbild.*** Die Vettel hüllt sich und alles, was sie trägt oder hält, in eine magische Illusion, die sie wie eine andere Kreatur von ihrer ungefähren Größe sowie von humanoider Gestalt erscheinen lässt. Die Illusion endet, wenn die Vettel sie mit einer Bonusaktion beendet oder ihre Trefferpunkte auf 0 sinken. Die Veränderungen durch diese Illusion halten einer genauen körperlichen Untersuchung nicht stand. Beispiel: Die Vettel könnte sich illusionär glatte Haut verleihen, doch jemand, der sie berührt, würde ihre Falten spüren. Anderenfalls muss eine Kreatur eine Aktion ausführen, um die Illusion visuell zu untersuchen und einen SG-20-Intelligenzwurf (Nachforschungen) auszuführen. Bei einem Erfolg bemerkt die Kreatur die Tarnung der Vettel.
:
***Unsichtbare Passage.*** Die Vettel wird magisch unsichtbar, bis sie angreift oder ihre Konzentration endet (wie bei einem Zauber). Solange sie unsichtbar ist, hinterlässt sie keine physischen Spuren und kann nur durch Magie gefunden werden. Ausrüstung, die sie trägt oder hält, ist ebenfalls unsichtbar.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Gruftschrecken
*Mittelgroßer Untoter, neutral böse*
___
**Rüstungsklasse** :: 14 (beschlagenes Leder)
**Trefferpunkte**  :: 45 (6W8+18)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|14 (+2)|16 (+3)|10 (+0)|13 (+1)|15 (+2)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +3
**Schadensresistenzen**     :: Nekrotisch; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Erschöpft, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 3 (700 EP)
___
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist der Gruftschrecken bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
### Aktionen
***Mehrfachangriff.*** Der Gruftschrecken führt zwei Langschwert- oder zwei Langbogenangriffe aus. Anstelle eines Langschwertangriffs kann er Lebensentzug einsetzen.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Hiebschaden oder 7 (1W10+2) Hiebschaden bei zweihändiger Führung.
:
***Lebensentzug.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 5 (1W6+2) nekrotischer Schaden. Das Ziel muss einen SG-13-Konstitutionsrettungswurf bestehen, oder sein Trefferpunktemaximum wird um den Betrag des erlittenen Schadens verringert. Es bleibt verringert, bis das Ziel eine lange Rast beendet. Wenn das Trefferpunktemaximum durch diesen Effekt auf 0 sinkt, stirbt das Ziel. Humanoide, die durch diesen Angriff getötet werden, erheben sich 24 Stunden später als Zombies unter Kontrolle des Gruftschreckens, sofern sie nicht zuvor wiederbelebt oder ihre Leichen zerstört werden. Der Gruftschrecken kann höchstens zwölf Zombies zugleich unter seiner Kontrolle haben.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Grul
*Mittelgroßer Untoter, chaotisch böse*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 36 (8W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|17 (+3)|10 (+0)|11 (+0)|10 (+0)|8  (-1)|
___
**Schadensresistenzen**     :: Nekrotisch
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache
**Herausforderung**         :: 2 (450 EP)
___
***Gestank.*** Jede Kreatur, die ihren Zug im Abstand von bis zu 1,5 Metern vom Grul beginnt, muss einen SG-10-Konstitutionsrettungswurf bestehen, oder sie ist bis zum Beginn ihres nächsten Zugs vergiftet. Bei einem erfolgreichen Rettungswurf ist die Kreatur 24 Stunden lang gegen den Gestank des Gruls immun.
:
***Trutz gegen Vertreibung.*** Der Grul und alle Ghule im Abstand von bis zu neun Metern von ihm sind bei Rettungswürfen gegen Effekte, die Untote vertreiben, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 12 (2W8+3) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden. Wenn das Ziel kein Untoter ist, muss es einen SG-10-Konstitutionsrettungswurf bestehen, oder es ist eine Minute lang gelähmt. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Gynosphinx
*Große Monstrosität, rechtschaffen neutral*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 136 (16W10+48)
**Speed**          :: 12 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|18 (+4)|18 (+4)|18 (+4)|
___
**Fertigkeiten**            :: Arkane Kunde +12, Geschichte +12, Religion +8, Wahrnehmung +8
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Psychisch
**Zustandsimmunitäten**     :: Bezaubert, Verängstigt
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 18
**Sprachen**                :: Gemeinsprache, Sphinxisch
**Herausforderung**         :: 11 (7.200 EP)
___
***Magische Waffen.*** Die Waffenangriffe der Sphinx sind magisch.
:
***Undurchschaubar.*** Die Sphinx ist gegen alle Effekte immun, die ihre Emotionen oder Gedanken lesen würden, sowie gegen alle Erkenntniszauber, die sie zurückweist. Weisheitswürfe (Motiv erkennen) zum Bestimmen der Absichten oder der Aufrichtigkeit der Sphinx sind im Nachteil.
:
***Zauberwirken.*** Die Sphinx ist ein Zauberwirker der 9. Stufe. Ihr Attribut zum Zauberwirken ist Intelligenz (Zauberrettungswurf-SG 16, +8 auf Treffer mit Zauberangriffen). Sie benötigt keine Materialkomponenten, um ihre Zauber zu wirken. Die Sphinx hat folgende Magierzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Einfache Illusion*](https://openrpg.de/srd/5e/de/#spell-einfache-illusion)[*Magierhand*](https://openrpg.de/srd/5e/de/#spell-magierhand)[*Taschenspielerei*](https://openrpg.de/srd/5e/de/#spell-taschenspielerei)<br>
1&period; Grad (4 Plätze): [*Identifizieren*](https://openrpg.de/srd/5e/de/#spell-identifizieren)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Schild*](https://openrpg.de/srd/5e/de/#spell-schild)<br>
2&period; Grad (3 Plätze): [*Dunkelheit*](https://openrpg.de/srd/5e/de/#spell-dunkelheit)[*Einflüsterung*](https://openrpg.de/srd/5e/de/#spell-einfluesterung)[*Gegenstand aufspüren*](https://openrpg.de/srd/5e/de/#spell-gegenstand-aufspueren)<br>
3&period; Grad (3 Plätze): [*Fluch brechen*](https://openrpg.de/srd/5e/de/#spell-fluch-brechen)[*Magie bannen*](https://openrpg.de/srd/5e/de/#spell-magie-bannen)[*Zungen*](https://openrpg.de/srd/5e/de/#spell-zungen)<br>
4&period; Grad (3 Plätze): [*Mächtige Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-maechtige-unsichtbarkeit)[*Verbannung*](https://openrpg.de/srd/5e/de/#spell-verbannung)<br>
5&period; Grad (1 Platz): [*Sagenkunde*](https://openrpg.de/srd/5e/de/#spell-sagenkunde)
:
### Aktionen
***Mehrfachangriff.*** Die Sphinx führt zwei Klauenangriffe aus.
:
***Klauen.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Hiebschaden.
:
### Legendäre Aktionen
Die Sphinx kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Sie kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Die Sphinx erhält verbrauchte legendäre Aktionen am Anfang ihres Zugs zurück.
:
***Teleportieren (kostet 2 Aktionen).*** Die Sphinx teleportiert sich und sämtliche Ausrüstung, die sie trägt oder hält, magisch bis zu 36 Meter weit an eine freie Stelle, die sie sehen kann.
:
***Zauber wirken (kostet 3 Aktionen).*** Die Sphinx wirkt einen Zauber von ihrer Liste vorbereiteter Zauber, wobei sie wie gewöhnlich einen Zauberplatz verwendet.
:
***Klauenangriff.*** Die Sphinx führt einen Klauenangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Halbroter Drachenveteran
*Mittelgroßer Humanoide (Mensch), jede Gesinnung*
___
**Rüstungsklasse** :: 18 (Ritterrüstung)
**Trefferpunkte**  :: 65 (10W8+20)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|13 (+1)|14 (+2)|10 (+0)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Athletik +5, Wahrnehmung +2
**Schadensresistenzen**     :: Feuer
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 5 (1.800 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Veteran führt zwei Langschwertangriffe durch. Wenn er ein Kurzschwert gezogen hat, kann er auch einen Kurzschwertangriff ausführen.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Hiebschaden oder 8 (1W10+3) Hiebschaden bei zweihändiger Führung.
:
***Schwere Armbrust.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 30/120 m, ein Ziel. *Treffer*: 6 (1W10+1) Stichschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Veteran atmet Feuer in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-15-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 24 (7W6) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Harpyie
*Mittelgroße Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 38 (7W8+7)
**Speed**          :: 6 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|13 (+1)|12 (+1)|7  (-2)|10 (+0)|13 (+1)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache
**Herausforderung**         :: 1 (200 EP)
___
### Aktionen
***Mehrfachangriff.*** Die Harpyie führt zwei Angriffe aus: einen Klauenangriff und einen mit dem Knüppel.
:
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (2W4+1) Hiebschaden.
:
***Knüppel.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W4+1) Wuchtschaden.
:
***Verlockender Gesang.*** Die Harpyie singt eine magische Melodie. Alle Humanoiden und Riesen im Abstand von bis zu 90 Metern um die Harpyie, die das Lied hören können, müssen einen SG-11-Weisheitsrettungswurf bestehen, um nicht bis zum Ende des Lieds bezaubert zu sein. Die Harpyie muss in folgenden Zügen eine Bonusaktion verwenden, um weiterzusingen. Sie kann den Gesang jederzeit beenden. Der Gesang endet, wenn die Harpyie kampfunfähig ist. Solange ein Ziel von der Harpyie bezaubert ist, ist es kampfunfähig und ignoriert die Lieder anderer Harpyien. Wenn das bezauberte Ziel mehr als 1,5 Meter von der Harpyie entfernt ist, muss es sich in seinem Zug auf dem direktesten Weg auf die Harpyie zubewegen, um in einen Radius von bis zu 1,5 Meter um die Kreatur zu kommen. Das Ziel vermeidet keine Gelegenheitsangriffe, aber bevor es sich in schädigendes Gelände wie Lava oder eine Grube bewegt und wenn es Schaden durch eine Quelle mit Ausnahme der Harpyie erleidet, kann das Ziel den Rettungswurf wiederholen. Das bezauberte Ziel kann den Rettungswurf am Ende eines jeden seiner Züge wiederholen. Wenn der Rettungswurf erfolgreich ist, endet der Effekt für das Ziel. Ein Ziel, dem ein Rettungswurf gelingt, ist 24 Stunden lang gegen den Gesang der Harpyie immun.
:
}}


{{monster,frame
## Hezrou
*Großer Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 136 (13W10+65)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|17 (+3)|20 (+5)|5  (-3)|12 (+1)|13 (+1)|
___
**Rettungswürfe**           :: Str +7, Kon +8, Wei +4
**Schadensresistenzen**     :: Blitz, Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Abyssisch, Telepathie auf 36 m
**Herausforderung**         :: 8 (3.900 EP)
___
***Gestank.*** Jede Kreatur, die ihren Zug im Abstand von bis zu drei Metern vom Hezrou beginnt, muss einen SG-14-Konstitutionsrettungswurf bestehen, oder sie ist bis zum Beginn ihres nächsten Zugs vergiftet. Bei einem erfolgreichen Rettungswurf ist die Kreatur 24 Stunden lang gegen den Gestank des Hezrou immun.
:
***Magieresistenz.*** Der Hezrou ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Hezrou führt drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Hobgoblin
*Mittelgroßer Humanoide (Goblinoide), rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (Kettenpanzer, Schild)
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|12 (+1)|12 (+1)|10 (+0)|10 (+0)|9  (-1)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache, Goblinisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Kämpferischer Vorteil.*** Einmal pro Zug kann der Hobgoblin einer Kreatur, die er mit einem Waffenangriff trifft, zusätzlich 7 (2W6) Schaden zufügen, sofern diese Kreatur sich im Abstand von bis zu 1,5 Metern von einem Verbündeten des Hobgoblins befindet, der nicht kampfunfähig ist.
:
### Aktionen
***Langschwert.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W8+1) Hiebschaden oder 6 (1W10+1) Hiebschaden bei zweihändiger Führung.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 5 (1W8+1) Stichschaden.
:
}}


{{monster,frame
## Höllenhund
*Mittelgroßer Unhold, rechtschaffen böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 45 (7W8+14)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|12 (+1)|14 (+2)|6  (-2)|13 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +5
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 15
**Sprachen**                :: Versteht Infernalisch, aber kann nicht sprechen
**Herausforderung**         :: 3 (700 EP)
___
***Rudeltaktik.*** Der Hund ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Hund ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden plus 7 (2W6) Feuerschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Hund atmet Feuer in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-12-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 21 (6W6) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Höllenschlundteufel
*Großer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 300 (24W10+168)
**Speed**          :: 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|26 (+8)|14 (+2)|24 (+7)|22 (+6)|18 (+4)|24 (+7)|
___
**Rettungswürfe**           :: Ges +8, Kon +13, Wei +10
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 14
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 20 (25.000 EP)
___
***Aura der Furcht.*** Jede Kreatur, die dem Höllenschlund-teufel feindlich gesinnt ist und ihren Zug im Abstand von bis zu sechs Metern von ihm beginnt, muss einen SG-21-Weisheitsrettungswurf ausführen, sofern der Höllenschlundteufel nicht kampfunfähig ist. Scheitert der Wurf, so ist die Kreatur bis zum Anfang ihres nächsten Zugs verängstigt. Bei einem Erfolg ist die Kreatur 24 Stunden lang gegen die Aura der Furcht des Höllenschlundteufels immun.
:
***Magieresistenz.*** Der Höllenschlundteufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Höllenschlundteufels sind magisch.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Höllenschlundteufels ist Charisma (Zauberrettungswurf-SG 21). Der Höllenschlundteufel kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Feuerball*](https://openrpg.de/srd/5e/de/#spell-feuerball)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)<br>
Je 3-mal täglich: [*Feuerwand*](https://openrpg.de/srd/5e/de/#spell-feuerwand)[*Monster festhalten*](https://openrpg.de/srd/5e/de/#spell-monster-festhalten)
:
### Aktionen
***Mehrfachangriff.*** Der Höllenschlundteufel führt vier Angriffe aus: einen Biss-, einen Klauen-, einen Streitkolben- und einen Schwanzangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 22 (4W6+8) Stichschaden. Das Ziel muss einen SG-21-Konstitutionsrettungswurf bestehen, oder es ist vergiftet. Während es auf diese Art vergiftet ist, kann es keine Trefferpunkte zurückerhalten und erleidet zu Beginn jedes seiner Züge 21 (6W6) Giftschaden. Das vergiftete Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W8+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 24 (3W10+8) Wuchtschaden.
:
***Streitkolben.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W6+8) Wuchtschaden plus 21 (6W6) Feuerschaden.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Homunkulus
*Winziges Konstrukt, neutral*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 5 (2W4)
**Speed**          :: 6 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|4  (-3)|15 (+2)|11 (+0)|10 (+0)|10 (+0)|7  (-2)|
___
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht die Sprache seines Schöpfers, aber kann nicht sprechen
**Herausforderung**         :: 0 (10 EP)
___
***Telepathische Bindung.*** Solange sich der Homunkulus auf derselben Existenzebene wie sein Meister befindet, kann er seine Wahrnehmungen magisch übermitteln, und die beiden können telepathisch miteinander kommunizieren.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 1 Stichschaden, und das Ziel muss einen SG-10-Konstitutionsrettungswurf bestehen, oder es ist eine Minute lang vergiftet. Wenn der Rettungswurf um mindestens 5 Punkte scheitert, ist das Ziel stattdessen 5 (1W10) Minuten lang vergiftet und bewusstlos, solange es auf diese Art vergiftet ist.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Hornteufel
*Großer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 178 (17W10+85)
**Speed**          :: 6 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|17 (+3)|21 (+5)|12 (+1)|16 (+3)|17 (+3)|
___
**Rettungswürfe**           :: Str +10, Ges +7, Wei +7, Cha +7
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 13
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 11 (7.200 EP)
___
***Magieresistenz.*** Der Teufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Teufelssicht.*** Die Dunkelsicht des Teufels wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Mehrfachangriff.*** Der Teufel führt drei Nahkampfangriffe aus: zwei mit seiner Gabel und einen Schwanzangriff. Statt der Nahkampfangriffe kann er nach Belieben Flamme schleudern einsetzen.
:
***Gabel.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W8+6) Stichschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 10 (1W8+6) Stichschaden. Wenn das Ziel kein Untoter und kein Konstrukt ist, muss es einen SG-17-Konstitutionsrettungswurf bestehen, oder es verliert zu Beginn jedes seiner Züge aufgrund einer infernalischen Wunde 10 (3W6) Trefferpunkte. Wann immer der Teufel das verwundete Ziel erneut mit diesem Angriff trifft, erhöht sich der Schaden durch die Wunde um 10 (3W6). Kreaturen können als Aktion versuchen, die Wunde mit einem erfolgreichen SG-12-Weisheitswurf (Heilkunde) zu verschließen. Die Wunde schließt sich auch, wenn das Ziel magisch geheilt wird.
:
***Flamme schleudern.*** *Fernkampf-Zauberangriff*: +7 auf Treffer, Reichweite 45 m, ein Ziel. *Treffer*: 14 (4W6) Feuerschaden. Wenn das Ziel ein brennbares Objekt ist, das nicht getragen oder gehalten wird, fängt es Feuer.
:
}}


{{monster,frame
## Hügelriese
*Riesiger Riese, chaotisch böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 105 (10W12+40)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|8  (-1)|19 (+4)|5  (-3)|9  (-1)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Riesisch
**Herausforderung**         :: 5 (1.800 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Riese führt zwei Angriffe mit dem Zweihandknüppel aus.
:
***Zweihandknüppel.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 18 (3W8+5) Wuchtschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +8 auf Treffer, Reichweite 18/72 m, ein Ziel. *Treffer*: 21 (3W10+5) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Hyäne
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 5 (1W8+1)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|13 (+1)|12 (+1)|2  (-4)|12 (+1)|5  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Rudeltaktik.*** Die Hyäne ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer ihrer Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W6) Stichschaden.
:
}}


{{monster,frame
## Hydra
*Riesige Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 172 (15W12+75)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|12 (+1)|20 (+5)|2  (-4)|10 (+0)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +6
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 16
**Sprachen**                :: -
**Herausforderung**         :: 8 (3.900 EP)
___
***Atem anhalten.*** Die Hydra kann eine Stunde lang den Atem anhalten.
:
***Mehrere Köpfe.*** Die Hydra hat fünf Köpfe. Solange sie mehr als einen Kopf hat, ist die Hydra bei Rettungswürfen gegen die Zustände Betäubt, Bewusstlos, Bezaubert, Blind, Taub und Verängstigt im Vorteil. Wann immer die Hydra mindestens 25 Schaden in einem einzigen Zug erleidet, stirbt einer ihrer Köpfe. Sind alle Köpfe tot, so stirbt die Hydra. Am Ende ihres Zugs wachsen der Hydra für jeden Kopf, der seit ihrem letzten Zug zerstört wurde, jeweils zwei Köpfe nach, sofern die Hydra seitdem keinen Feuerschaden erlitten hat. Die Hydra erhält 10 Trefferpunkte für jeden Kopf zurück, der auf diese Art nachgewachsen ist.
:
***Reaktive Köpfe.*** Für jeden Kopf der Hydra über einen hinaus bekommt sie eine zusätzliche Reaktion, die sie nur zu Gelegenheitsangriffen einsetzen kann.
:
***Wachsam.*** Wenn die Hydra schläft, ist mindestens einer ihrer Köpfe wach.
:
### Aktionen
***Mehrfachangriff.*** Die Hydra führt so viele Bissangriffe aus, wie sie Köpfe hat
:
***Biss.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 10 (1W10+5) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Ifriti
*Großer Elementar, rechtschaffen böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 200 (16W10+112)
**Speed**          :: 12 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|12 (+1)|24 (+7)|16 (+3)|15 (+2)|16 (+3)|
___
**Rettungswürfe**           :: Int +7, Wei +6, Cha +7
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Ignal
**Herausforderung**         :: 11 (7.200 EP)
___
***Elementarer Untergang.*** Wenn der Ifriti stirbt, löst sein Körper sich in einen Feuerblitz und eine Rauchwolke auf, und es bleibt nur die Ausrüstung zurück, die der Ifriti getragen oder gehalten hat.
:
***Angeborenes Zauberwirken.*** Der Attribut zum angeborenen Zauberwirken des Ifriti ist Charisma (Zauberrettungswurf-SG 15, +7 auf Treffer mit Zauberangriffen). Der Ifriti kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)<br>
3-mal täglich: [*Vergrößern/Verkleinern*](https://openrpg.de/srd/5e/de/#spell-vergroessern-verkleinern)[*Zungen*](https://openrpg.de/srd/5e/de/#spell-zungen)<br>
Je 1-mal täglich: [*Ebenenwechsel*](https://openrpg.de/srd/5e/de/#spell-ebenenwechsel)[*Elementar beschwören (nur Feuerelementar)*](https://openrpg.de/srd/5e/de/#spell-elementar-beschwoeren)[*Feuerwand*](https://openrpg.de/srd/5e/de/#spell-feuerwand)[*Gasförmige Gestalt*](https://openrpg.de/srd/5e/de/#spell-gasfoermige-gestalt)[*Mächtiges Trugbild*](https://openrpg.de/srd/5e/de/#spell-maechtiges-trugbild)[*Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)
:
### Aktionen
***Mehrfachangriff.*** Der Ifriti führt zwei Angriffe mit dem Krummsäbel aus oder setzt zweimal seinen Flamme-schleudern-Angriff ein.
:
***Krummsäbel.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden plus 7 (2W6) Feuerschaden.
:
***Flamme schleudern.*** *Fernkampf-Zauberangriff*: +7 auf Treffer, Reichweite 36 m, ein Ziel. *Treffer*: 17 (5W6) Feuerschaden.
:
}}


{{monster,frame
## Insektenschwarm
*Mittelgroßer Schwarm winziger Tiere, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 22 (5W8)
**Speed**          :: 6 m, Klettern 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|13 (+1)|10 (+0)|1  (-5)|7  (-2)|1  (-5)|
___
**Schadensresistenzen**     :: Hieb, Stich, Wucht
**Zustandsimmunitäten**     :: Betäubt, Bezaubert, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Versteinert
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Schwarm.*** Der Schwarm kann den Bereich einer anderen Kreatur besetzen und umgekehrt. Er kann sich durch jede Öffnung bewegen, die groß genug für ein winziges Insekt ist. Der Schwarm kann keine Trefferpunkte zurückerhalten und keine temporären Trefferpunkte erhalten.
:
### Aktionen
***Bisse.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 0 m, ein Ziel im Bereich des Schwarms. *Treffer*: 10 (4W4) Stichschaden oder 5 (2W4) Stichschaden, wenn der Schwarm höchstens die Hälfte seiner Trefferpunkte hat.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Irrlicht
*Winziger Untoter, chaotisch böse*
___
**Rüstungsklasse** :: 19
**Trefferpunkte**  :: 22 (9W4)
**Speed**          :: 0 m, Fliegen 15 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1  (-5)|28 (+9)|10 (+0)|13 (+1)|14 (+2)|11 (+0)|
___
**Schadensresistenzen**     :: Feuer, Kälte, Nekrotisch, Säure, Schall; Wucht-, Stich- und Hiebschaden durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Blitz, Gift
**Zustandsimmunitäten**     :: Bewusstlos, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 2 (450 EP)
___
***Flüchtig.*** Der Irrlicht kann nichts tragen oder halten.
:
***Körperlose Bewegung.*** Das Irrlicht kann sich durch andere Kreaturen und Gegenstände bewegen, als wären sie schwieriges Gelände. Es erleidet 5 (1W10) Energieschaden, wenn es seinen Zug in einem Gegenstand beendet.
:
***Leben verzehren.*** Das Irrlicht kann als Bonusaktion eine Kreatur im Abstand von bis zu 1,5 Metern von ihm auswählen, die es sehen kann, solange die Kreatur zwar 0 Trefferpunkte hat, aber noch am Leben ist. Das Ziel muss gegen diese Magie einen SG-10-Konstitutionsrettungswurf bestehen, um nicht zu sterben. Wenn das Ziel stirbt, erhält das Irrlicht 10 (3W6) Trefferpunkte zurück.
:
***Variable Beleuchtung.*** Das Irrlicht spendet in einem Radius zwischen 1,5 und sechs Metern helles Licht und in einer weiteren Spanne gleich dem gewählten Radius dämmriges Licht. Das Irrlicht kann den Radius als Bonusaktion anpassen.
:
### Aktionen
***Schock.*** *Nahkampf-Zauberangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 9 (2W8) Blitzschaden.
:
***Unsichtbarkeit.*** Das Irrlicht und sein Licht werden auf magische Weise unsichtbar, bis es angreift oder Leben verzehren nutzt oder bis seine Konzentration endet (wie bei einem Zauber).
:
}}


{{monster,frame
## Jagdhai
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 45 (6W10+12)
**Speed**          :: 0 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|13 (+1)|15 (+2)|1  (-5)|10 (+0)|4  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Blindsicht 9 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Blutrausch.*** Der Hai ist bei Nahkampf-Angriffswürfen gegen Kreaturen, die nicht alle Trefferpunkte besitzen, im Vorteil.
:
***Wasser atmen.*** Der Hai kann nur unter Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Junger blauer Drache
*Großer Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 152 (16W10+64)
**Speed**          :: 12 m, Graben 6 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|10 (+0)|19 (+4)|14 (+2)|13 (+1)|17 (+3)|
___
**Rettungswürfe**           :: Ges +4, Kon +8, Wei +5, Cha +7
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +9
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 19
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 9 (5.000 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 16 (2W10+5) Stichschaden plus 5 (1W10) Blitzschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W6+5) Hiebschaden.
:
***Blitzodem (Aufladung 5–6).*** Der Drache atmet einen Blitzstrahl in an 18 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-16-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 55 (10W10) Blitzschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Junger Bronzedrache
*Großer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 142 (15W10+60)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|10 (+0)|19 (+4)|14 (+2)|13 (+1)|17 (+3)|
___
**Rettungswürfe**           :: Ges +3, Kon +7, Wei +4, Cha +6
**Fertigkeiten**            :: Heimlichkeit +4, Motiv erkennen +7, Wahrnehmung +3
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 17
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 8 (3.900 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 16 (2W10+5) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W6+5) Hiebschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Blitzodem.*** Der Drache atmet einen Blitzstrahl in einer 18 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-15-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 55 (10W10) Blitzschaden, anderenfalls die Hälfte.
:
***Odem der Abstoßung.*** Der Drache atmet abstoßende Energie in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-15-Stärkerettungswurf bestehen. Scheitert der Wurf, wird die Kreatur bis zu zwölf Meter weit vom Drachen weggestoßen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Junger Golddrache
*Großer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 178 (17W10+85)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|14 (+2)|21 (+5)|16 (+3)|13 (+1)|20 (+5)|
___
**Rettungswürfe**           :: Ges +6, Kon +9, Wei +5, Cha +9
**Fertigkeiten**            :: Heimlichkeit +6, Motiv erkennen +5, Überzeugen +9, Wahrnehmung +9
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 19
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 10 (5.900 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-17-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 55 (10W10) Feuerschaden, anderenfalls die Hälfte.
:
***Schwächender Odem.*** Der Drache atmet Gas in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-17-Stärkerettungswurf bestehen, oder sie ist eine Minute lang bei stärkebasierten Angriffswürfen, bei Stärkewürfen und Stärkerettungswürfen im Nachteil. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Junger grüner Drache
*Großer Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 136 (16W10+48)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|12 (+1)|17 (+3)|16 (+3)|13 (+1)|15 (+2)|
___
**Rettungswürfe**           :: Ges +4, Kon +6, Wei +4, Cha +5
**Fertigkeiten**            :: Heimlichkeit +4, Täuschen +5, Wahrnehmung +7
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 17
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 8 (3.900 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden plus 7 (2W6) Giftschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Giftodem (Aufladung 5–6).*** Der Drache atmet giftiges Gas in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-14-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 42 (12W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Junger Kupferdrache
*Großer Drache, chaotisch gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 119 (14W10+42)
**Speed**          :: 12 m, Klettern 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|12 (+1)|17 (+3)|16 (+3)|13 (+1)|15 (+2)|
___
**Rettungswürfe**           :: Ges +4, Kon +6, Wei +4, Cha +5
**Fertigkeiten**            :: Heimlichkeit +4, Täuschen +5, Wahrnehmung +7
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 17
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 7 (2.900 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Säureodem.*** Der Drache atmet Säure in einer zwölf Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-14-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 40 (9W8) Säureschaden, anderenfalls die Hälfte.
:
***Bremsender Odem.*** Der Drache atmet Gas in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-14-Konstitutionsrettungswurf bestehen. Scheitert der Wurf, so kann die Kreatur keine Reaktionen einsetzen, ihre Bewegungsrate ist halbiert, und sie kann in ihrem Zug höchstens einen Angriff ausführen. Außerdem kann sie in ihrem Zug entweder eine Aktion oder eine Bonusaktion einsetzen, aber nicht beides. Diese Effekte halten eine Minute lang an. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Junger Messingdrache
*Großer Drache, chaotisch gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 110 (13W10+39)
**Speed**          :: 12 m, Graben 6 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|17 (+3)|12 (+1)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Ges +3, Kon +6, Wei +3, Cha +5
**Fertigkeiten**            :: Heimlichkeit +3, Überzeugen +5, Wahrnehmung +6
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 16
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 6 (2.300 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einer zwölf Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-14-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 42 (12W6) Feuerschaden, anderenfalls die Hälfte.
:
***Schlafodem.*** Der Drache atmet Schlafgas in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-14-Konstitutionsrettungswurf bestehen, oder sie ist fünf Minuten lang bewusstlos. Dieser Effekt endet bei einer Kreatur, wenn sie Schaden erleidet oder jemand sie mit einer Aktion aufweckt.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Junger roter Drache
*Großer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 178 (17W10+85)
**Speed**          :: 12 m, Klettern 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|10 (+0)|21 (+5)|14 (+2)|11 (+0)|19 (+4)|
___
**Rettungswürfe**           :: Ges +4, Kon +9, Wei +4, Cha +8
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +8
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 18
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 10 (5.900 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden plus 3 (1W6) Feuerschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Drache atmet Feuer in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-17-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 56 (16W6) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Junger schwarzer Drache
*Großer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 127 (15W10+45)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|14 (+2)|17 (+3)|12 (+1)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Ges +5, Kon +6, Wei +3, Cha +5
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +6
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 16
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 7 (2.900 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden plus 4 (1W8) Säureschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Säureodem (Aufladung 5–6).*** Der Drache atmet Säure in einer neun Metern langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-14-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 49 (11W8) Säureschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Junger Silberdrache
*Großer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 168 (16W10+80)
**Speed**          :: 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|10 (+0)|21 (+5)|14 (+2)|11 (+0)|19 (+4)|
___
**Rettungswürfe**           :: Ges +4, Kon +9, Wei +4, Cha +8
**Fertigkeiten**            :: Arkane Kunde +6, Geschichte +6, Heimlichkeit +4, Wahrnehmung +8
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 18
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 9 (5.000 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W10+6) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W6+6) Hiebschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Kälteodem.*** Der Drache atmet in einem Kegel von neun Metern Länge eisige Luft aus. Jede Kreatur in diesem Bereich muss einen SG-17-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 54 (12W8) Kälteschaden, anderenfalls die Hälfte.
:
***Lähmender Odem.*** Der Drache atmet lähmendes Gas in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-17-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang gelähmt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Junger weißer Drache
*Großer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 133 (14W8+56)
**Speed**          :: 12 m, Graben 6 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|10 (+0)|18 (+4)|6  (-2)|11 (+0)|12 (+1)|
___
**Rettungswürfe**           :: Ges +3, Kon +7, Wei +3, Cha +4
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +6
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 9 m, Dunkelsicht 36 m, Passive Wahrnehmung 16
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 6 (2.300 EP)
___
***Eisschritt.*** Der Drache kann sich über Eis bewegen und eisige Oberflächen erklimmen, ohne Attributswürfe ausführen zu müssen. Außerdem kostet ihn schwieriges Gelände, das aus Eis oder Schnee besteht, keine zusätzliche Bewegung.
:
### Aktionen
***Mehrfachangriff.*** Der Drache führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden plus 4 (1W8) Kälteschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
***Kälteodem (Aufladung 5–6).*** Der Drache atmet in einem Kegel von neun Metern Länge eisige Luft aus. Jede Kreatur im Kegel muss einen SG-15-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 45 (10W8) Kälteschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Kamel
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 9
**Trefferpunkte**  :: 15 (2W10+4)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|8  (-1)|14 (+2)|2  (-4)|8  (-1)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Wuchtschaden.
:
}}


{{monster,frame
## Katze
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 2 (1W4)
**Speed**          :: 12 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|15 (+2)|10 (+0)|3  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfer Geruchssinn.*** Die Katze ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +0 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Kettenteufel
*Mittelgroßer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 85 (10W8+40)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|18 (+4)|11 (+0)|12 (+1)|14 (+2)|
___
**Rettungswürfe**           :: Kon +7, Wei +4, Cha +5
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 8 (3.900 EP)
___
***Magieresistenz.*** Der Teufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Teufelssicht.*** Die Dunkelsicht des Teufels wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Mehrfachangriff.*** Der Teufel führt zwei Angriffe mit seinen Ketten aus.
:
***Kette.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden. Das Ziel wird gepackt (Rettungswurf-SG 14), sofern der Teufel nicht schon eine Kreatur gepackt hält. Ein gepacktes Ziel ist festgesetzt und erleidet zu Beginn jedes seiner Züge 7 (2W6) Stichschaden.
:
***Ketten beleben (wird nach kurzer oder langer Rast aufgeladen).*** Bis zu vier Ketten im Abstand von bis zu 18 Metern vom Teufel, die er sehen kann, bekommen magisch rasiermesserscharfe Stacheln und werden unter der Kontrolle des Teufels belebt, sofern sie nicht getragen oder gehalten werden. Jede belebte Kette ist ein Objekt, besitzt eine RK von 20, 20 Trefferpunkte, ist gegen Stichschaden resistent und gegen psychischen Schaden sowie Schallschaden immun. Wenn der Teufel in seinem Zug Mehrfachangriff einsetzt, kann er jede belebte Kette verwenden, um einen zusätzlichen Kettenangriff auszuführen. Eine belebte Kette kann eine Kreatur packen, aber keine Angriffe ausführen, solange sie eine Kreatur gepackt hält. Sie kehrt zum unbelebten Zustand zurück, wenn ihre Trefferpunkte auf 0 verringert werden, wenn der Teufel kampfunfähig ist oder wenn er stirbt.  Reaktionen
:
***Beklemmende Maske.*** Wenn eine Kreatur, die der Teufel sehen kann, ihren Zug im Abstand von bis zu neun Metern vom Teufel beginnt, kann der Teufel das illusionäre Erscheinungsbild einer geliebten, aber verstorbenen Person oder aber eines erbitterten Feinds der Kreatur annehmen. Wenn die Kreatur den Teufel sehen kann, muss sie einen SG-14-Weisheitsrettungswurf bestehen, oder sie ist bis zum Ende ihres Zugs verängstigt.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Killerwal
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 90 (12W12+12)
**Speed**          :: 0 m, Schwimmen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|13 (+1)|3  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Blindsicht 36 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 3 (700 EP)
___
***Atem anhalten.*** Der Wal kann 30 Minuten lang den Atem anhalten.
:
***Echolot.*** Der Wal kann seine Blindsicht nicht verwenden, solange er taub ist.
:
***Scharfes Gehör.*** Der Wal ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 21 (5W6+4) Stichschaden.
:
}}


{{monster,frame
## Klingenteufel
*Mittelgroßer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 110 (13W8+52)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|17 (+3)|18 (+4)|12 (+1)|14 (+2)|14 (+2)|
___
**Rettungswürfe**           :: Str +6, Kon +7, Wei +5, Cha +5
**Fertigkeiten**            :: Motiv erkennen +5, Täuschen +5, Wahrnehmung +8
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 18
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 5 (1.800 EP)
___
***Klingenhaut.*** Der Klingenteufel fügt zu Beginn jedes seiner Züge jeder Kreatur, die er gepackt hält, 5 (1W10) Stichschaden zu.
:
***Magieresistenz.*** Der Teufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Teufelssicht.*** Die Dunkelsicht des Teufels wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Mehrfachangriff.*** Der Teufel führt drei Nahkampfangriffe aus: einen Schwanz- und zwei Klauenangriffe. Alternativ kann er zweimal Flamme schleudern einsetzen.
:
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Stichschaden.
:
***Flamme schleudern.*** *Fernkampf-Zauberangriff*: +5 auf Treffer, Reichweite 45 m, ein Ziel. *Treffer*: 10 (3W6) Feuerschaden. Wenn das Ziel ein brennbares Objekt ist, das nicht getragen oder gehalten wird, fängt es Feuer.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Knochenteufel
*Großer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 142 (15W10+60)
**Speed**          :: 12 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|16 (+3)|18 (+4)|13 (+1)|14 (+2)|16 (+3)|
___
**Rettungswürfe**           :: Int +5, Wei +6, Cha +7
**Fertigkeiten**            :: Motiv erkennen +6, Täuschen +7
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 12
**Sprachen**                :: Infernalisch, Telepathie auf 36 m
**Herausforderung**         :: 9 (5.000 EP)
___
***Magieresistenz.*** Der Teufel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Teufelssicht.*** Die Dunkelsicht des Teufels wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Mehrfachangriff.*** Der Teufel führt drei Angriffe aus: zwei Klauen- und einen Stachelangriff.
:
***Klauen.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 8 (1W8+4) Hiebschaden.
:
***Stachel.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 13 (2W8+4) Stichschaden plus 17 (5W6) Giftschaden, und das Ziel muss einen SG-14-Konstitutionsrettungswurf ausführen, oder es ist eine Minute lang vergiftet. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Kobold
*Kleiner Humanoide (Kobold), rechtschaffen böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 5 (2W6-2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|7  (-2)|15 (+2)|9  (-1)|8  (-1)|7  (-2)|8  (-1)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 8
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 1/8 (25 EP)
___
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist der Kobold bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
***Rudeltaktik.*** Der Kobold ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
### Aktionen
***Dolch.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden.
:
***Schleuder.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 9/36 m, ein Ziel. *Treffer*: 4 (1W4+2) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Krabbe
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 2 (1W4)
**Speed**          :: 6 m, Schwimmen 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|11 (+0)|10 (+0)|1  (-5)|8  (-1)|2  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +2
**Sinne**                   :: Blindsicht 9 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Amphibisch.*** Die Krabbe kann Luft und Wasser atmen.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +0 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Kraken
*Gigantische Monstrosität (Titan), chaotisch böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 472 (27W20+189)
**Speed**          :: 6 m, Schwimmen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|30 (+1)|11 (+0)|25 (+7)|22 (+6)|18 (+4)|20 (+5)|
___
**Rettungswürfe**           :: Str +17, Ges +7, Kon +14, Int +13, Wei +11
**Schadensimmunitäten**     :: Blitz; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Verängstigt, Gelähmt
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 14
**Sprachen**                :: Versteht Abyssisch, Celestisch, Infernalisch und Urtümlich, aber kann nicht sprechen, Telepathie auf 36 m
**Herausforderung**         :: 23 (50.000 EP)
___
***Amphibisch.*** Der Kraken kann Luft und Wasser atmen.
:
***Belagerungsmonster.*** Der Kraken fügt Gegenständen und Gebäuden doppelten Schaden zu.
:
***Bewegungsfreiheit.*** Der Kraken ignoriert schwieriges Gelände, und magische Effekte können seine Bewegungsrate nicht verringern und ihn nicht festsetzen. Er kann 1,5 Meter seiner Bewegungsrate verwenden, um aus nichtmagischen Fesseln zu entkommen oder sich zu befreien, falls er gepackt wurde.
:
### Aktionen
***Mehrfachangriff.*** Der Kraken führt drei Tentakelangriffe aus, die er jeweils durch einen Einsatz von Schleudern ersetzen kann.
:
***Biss.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 23 (3W8+10) Stichschaden. Wenn das Ziel eine höchstens große Kreatur ist, die vom Kraken gepackt wurde, wird sie von ihm verschluckt und ist nicht mehr gepackt. Verschluckte Kreaturen sind blind und festgesetzt, haben vollständige Deckung gegen Angriffe und andere Effekte von außerhalb des Kraken und erleiden zu Beginn jedes Zugs des Kraken 42 (12W6) Säureschaden. Erleidet der Kraken durch eine verschluckte Kreatur in einem einzigen Zug mindestens 50 Schaden, so muss er am Ende des Zugs einen SG-25-Konstitutionsrettungswurf bestehen, oder er würgt alle verschluckten Kreaturen wieder hoch. Diese befinden sich dann im Bereich von drei Metern um den Kraken und sind liegend. Stirbt der Kraken, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie 4,5 Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
***Tentakel.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 9 m, ein Ziel. *Treffer*: 20 (3W6+10) Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 18). Solange das Ziel gepackt bleibt, ist es festgesetzt. Der Kraken hat zehn Tentakel, die jeweils ein Ziel packen können.
:
***Gewittersturm.*** Der Kraken erzeugt magisch drei Blitzschläge, die jeweils ein Ziel im Abstand von bis zu 36 Metern vom Kraken treffen können, das der Kraken sehen kann. Ein Ziel muss einen SG-23-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet es 22 (4W10) Blitzschaden, anderenfalls die Hälfte.
:
***Schleudern.*** Ein höchstens großes Objekt, das der Kraken hält, oder eine höchstens große Kreatur, die er gepackt hält, wird bis zu 18 Meter weit in eine zufällige Richtung geschleudert und umgestoßen. Wenn ein geschleudertes Ziel mit einer festen Oberfläche kollidiert, erleidet es 3 (1W6) Wuchtschaden je drei Meter, die es geschleudert wurde. Kollidiert das Ziel mit einer anderen Kreatur, muss diese einen SG-18-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet den gleichen Schaden und wird umgestoßen.
:
### Legendäre Aktionen
Der Kraken kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Kraken erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Gewittersturm (kostet 2 Aktionen).*** Der Kraken setzt Gewittersturm ein.
:
***Tintenwolke (kostet 3 Aktionen).*** Unter Wasser stößt der Kraken eine Tintenwolke mit einem Radius von 18 Metern aus. Die Wolke breitet sich um Ecken aus und verschleiert den Bereich für Kreaturen außer dem Kraken komplett. Jede Kreatur außer dem Kraken, die ihren Zug dort beendet, muss einen SG-23-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 16 (3W10) Giftschaden, anderenfalls die Hälfte. Eine starke Strömung kann die Wolke auflösen. Anderenfalls verschwindet sie am Ende des nächsten Zugs des Kraken.
:
***Tentakelangriff oder Schleudern.*** Der Kraken führt einen Tentakelangriff aus oder setzt Schleudern ein.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Kreischer
*Mittelgroße Pflanze, gesinnungslos*
___
**Rüstungsklasse** :: 5
**Trefferpunkte**  :: 13 (3W8)
**Speed**          :: 0 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1  (-5)|1  (-5)|10 (+0)|1  (-5)|3  (-4)|1  (-5)|
___
**Zustandsimmunitäten**     :: Blind, Taub, Verängstigt
**Sinne**                   :: Blindsicht 9 m (blind außerhalb des Radius), Passive Wahrnehmung 6
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Falsches Erscheinungsbild.*** Solange der Kreischer sich nicht bewegt, ist er nicht von einem gewöhnlichen Pilz zu unterscheiden.  Reaktionen
:
***Kreischen.*** Wenn sich helles Licht oder eine Kreatur im Abstand von bis zu neun Metern vom Kreischer befindet, stößt dieser ein Kreischen aus, das bis zu 90 Meter weit zu hören ist. Nachdem die Störung sich außer Reichweite bewegt hat, kreischt der Kreischer noch 1W4 seiner Züge lang weiter.
:
}}


{{monster,frame
## Krokodil
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 6 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|10 (+0)|13 (+1)|2  (-4)|10 (+0)|5  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +2
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Atem anhalten.*** Das Krokodil kann 15 Minuten lang den Atem anhalten.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 7 (1W10+2) Stichschaden, und das Ziel wird gepackt (Rettungswurf-SG 12). Ein gepacktes Ziel ist festgesetzt. Das Krokodil kann solange kein weiteres Ziel beißen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Kultfanatiker
*Mittelgroßer Humanoide (jedes Volk), jede nichtgute Gesinnung*
___
**Rüstungsklasse** :: 13 (Lederrüstung)
**Trefferpunkte**  :: 33 (6W8+6)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|14 (+2)|12 (+1)|10 (+0)|13 (+1)|14 (+2)|
___
**Fertigkeiten**            :: Religion +2, Täuschen +4, Überzeugen +4
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 2 (450 EP)
___
***Dunkle Hingabe.*** Der Fanatiker ist bei Rettungswürfen gegen die Zustände Bezaubert und Verängstigt im Vorteil.
:
***Zauberwirken.*** Der Fanatiker ist ein Zauberwirker der 4. Stufe. Sein Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 11, +3 auf Treffer mit Zauberangriffen). Der Fanatiker hat folgende Klerikerzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Heilige Flamme*](https://openrpg.de/srd/5e/de/#spell-heilige-flamme)[*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Thaumaturgie*](https://openrpg.de/srd/5e/de/#spell-thaumaturgie)<br>
1&period; Grad (4 Plätze): [*Befehl*](https://openrpg.de/srd/5e/de/#spell-befehl)[*Schild des Glaubens*](https://openrpg.de/srd/5e/de/#spell-schild-des-glaubens)[*Wunden verursachen*](https://openrpg.de/srd/5e/de/#spell-wunden-verursachen)<br>
2&period; Grad (3 Plätze): [*Person festhalten*](https://openrpg.de/srd/5e/de/#spell-person-festhalten)[*Waffe des Glaubens*](https://openrpg.de/srd/5e/de/#spell-waffe-des-glaubens)
:
### Aktionen
***Mehrfachangriff.*** Der Fanatiker führt zwei Nahkampfangriffe aus.
:
***Dolch.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, eine Kreatur. *Treffer*: 4 (1W4+2) Stichschaden.
:
}}


{{monster,frame
## Kultist
*Mittelgroßer Humanoide (jedes Volk), jede nichtgute Gesinnung*
___
**Rüstungsklasse** :: 12 (Lederrüstung)
**Trefferpunkte**  :: 9 (2W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|12 (+1)|10 (+0)|10 (+0)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Religion +2, Täuschen +2
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 1/8 (25 EP)
___
***Dunkle Hingabe.*** Der Kultist ist bei Rettungswürfen gegen die Zustände Bezaubert und Verängstigt im Vorteil.
:
### Aktionen
***Krummsäbel.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W6+1) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Kupferdrachennestling
*Mittelgroßer Drache, chaotisch gut*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 22 (4W8+4)
**Speed**          :: 9 m, Klettern 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|12 (+1)|13 (+1)|14 (+2)|11 (+0)|13 (+1)|
___
**Rettungswürfe**           :: Ges +3, Kon +3, Wei +2, Cha +3
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +4
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 1 (200 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W10+2) Stichschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Säureodem.*** Der Drache atmet Säure in einer sechs Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-11-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 18 (4W8) Säureschaden, anderenfalls die Hälfte.
:
***Bremsender Odem.*** Der Drache atmet Gas in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-11-Konstitutionsrettungswurf bestehen. Scheitert der Wurf, so kann die Kreatur keine Reaktionen einsetzen, ihre Bewegungsrate ist halbiert, und sie kann in ihrem Zug höchstens einen Angriff ausführen. Außerdem kann sie in ihrem Zug entweder eine Aktion oder eine Bonusaktion einsetzen, aber nicht beides. Diese Effekte halten eine Minute lang an. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Lamia
*Große Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 97 (13W10+26)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|13 (+1)|15 (+2)|14 (+2)|15 (+2)|16 (+3)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Motiv erkennen +4, Täuschen +7
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Abyssisch, Gemeinsprache
**Herausforderung**         :: 4 (1.100 EP)
___
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken der Lamia ist Charisma (Zauberrettungswurf-SG 13). Sie kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Mächtiges Trugbild*](https://openrpg.de/srd/5e/de/#spell-maechtiges-trugbild)[*Selbstverkleidung (jede humanoide Gestalt)*](https://openrpg.de/srd/5e/de/#spell-selbstverkleidung)<br>
Je 3-mal täglich: [*Ausspähung*](https://openrpg.de/srd/5e/de/#spell-ausspaehung)[*Einflüsterung*](https://openrpg.de/srd/5e/de/#spell-einfluesterung)[*Person bezaubern*](https://openrpg.de/srd/5e/de/#spell-person-bezaubern)[*Spiegelbilder*](https://openrpg.de/srd/5e/de/#spell-spiegelbilder)<br>
1-mal täglich: [*Geas*](https://openrpg.de/srd/5e/de/#spell-geas)
:
### Aktionen
***Mehrfachangriff.*** Die Lamia führt zwei Angriffe aus: einen Klauenangriff und einen mit ihrem Dolch, oder Vergiftende Berührung.
:
***Dolch.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W4+3) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W10+3) Hiebschaden.
:
***Vergiftende Berührung.*** *Nahkampf-Zauberangriff*: +5 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: Das Ziel ist eine Stunde lang magisch verflucht. Bis der Fluch endet, ist das Ziel bei Weisheitsrettungswürfen und allen Attributswürfen im Nachteil.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Landhai
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 94 (9W10+45)
**Speed**          :: 12 m, Graben 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|11 (+0)|21 (+5)|2  (-4)|10 (+0)|5  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +6
**Sinne**                   :: Dunkelsicht 18 m, Erschütterungssinn 18 m, Passive Wahrnehmung 16
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Stehender Sprung.*** Der Landhai kann mit oder ohne Anlauf bis zu neun Meter weit und bis zu 4,5 Meter hoch springen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 30 (4W12+4) Stichschaden.
:
***Tödlicher Sprung.*** Wenn der Landhai als Teil seiner Bewegung mindestens 4,5 Meter weit springt, kann er diese Aktion verwenden, um in einem Bereich auf den Füßen zu landen, in dem sich mindestens eine andere Kreatur befindet. All diese Kreaturen müssen einen SG-16-Stärke- oder Geschicklichkeitsrettungswurf (nach Wahl des Ziels) bestehen, oder sie werden umgestoßen und erleiden 14 (3W6+4) Wuchtschaden plus 14 (3W6+4) Hiebschaden. Bei einem erfolgreichen Rettungswurf erleiden die Kreaturen nur die Hälfte des Schadens und werden nicht umgestoßen, sondern 1,5 Meter aus dem Bereich des Landhais an eine freie Stelle ihrer Wahl geschoben. Gibt es innerhalb der Reichweite keine freie Stelle, sind die Kreaturen stattdessen im Bereich des Landhais liegend.
:
}}


{{monster,frame
## Lehmgolem
*Großes Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 133 (14W8+56)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|20 (+5)|9  (-1)|18 (+4)|3  (-4)|8  (-1)|1  (-5)|
___
**Schadensimmunitäten**     :: Gift, Psychisch, Säure; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Adamant
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: Versteht die Sprache seines Schöpfers, aber kann nicht sprechen
**Herausforderung**         :: 9 (5.000 EP)
___
***Berserker.*** Wenn der Golem seinen Zug mit höchstens 60 Trefferpunkten beginnt, würfle mit einem W6. Bei einer 6 gerät der Golem in Kampfrausch. Im Kampfrausch greift der Golem die Kreatur an, die ihm am nächsten ist und die er sehen kann. Befindet sich keine Kreatur in Angriffsreichweite, greift der Golem ein Objekt an, vorzugsweise eines, das kleiner ist als er selbst. Der Kampfrausch hält an, bis der Golem zerstört wird oder alle Trefferpunkte zurückgewinnt.
:
***Magieresistenz.*** Der Golem ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Golems sind magisch.
:
***Säureabsorption.*** Der Golem erleidet keinen Schaden durch Säure, sondern gewinnt Trefferpunkte in Höhe des Säureschadens zurück.
:
***Unveränderliche Form.*** Der Golem ist gegen alle Zauber und Effekte, die seine Gestalt ändern würden, immun.
:
### Aktionen
***Mehrfachangriff.*** Der Golem führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 16 (2W10+5) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-15-Konstitutionsrettungswurf bestehen, oder sein Trefferpunktemaximum wird um den Betrag des erlittenen Schadens verringert. Wenn das Trefferpunktemaximum durch diesen Angriff auf 0 sinkt, stirbt das Ziel. Die Verringerung bleibt bestehen, bis sie durch den Zauber Vollständige Genesung oder andere Magie entfernt wird.
:
***Hast (Aufladung 5–6).*** Bis zum Ende seines nächsten Zugs erhält der Golem magisch einen Bonus von +2 auf seine RK, ist bei Geschicklichkeitsrettungswürfen im Vorteil und kann seinen Hiebangriff als Bonusaktion ausführen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Lemure
*Mittelgroßer Unhold (Teufel), rechtschaffen böse*
___
**Rüstungsklasse** :: 7
**Trefferpunkte**  :: 13 (3W8)
**Speed**          :: 4,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|5  (-3)|11 (+0)|1  (-5)|11 (+0)|3  (-4)|
___
**Schadensresistenzen**     :: Kälte
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Bezaubert, Verängstigt, Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht Diabolisch, aber kann nicht sprechen
**Herausforderung**         :: 0 (10 EP)
___
***Höllische Wiederbelebung.*** Ein Lemure, der in den Neun Höllen stirbt, ersteht mit allen Trefferpunkten in 1W10 Tagen wieder auf, sofern er nicht von einer Kreatur mit guter Gesinnung getötet wurde, auf die der Zauber Segnen gewirkt wurde, oder die Überreste des Lemuren mit Weihwasser besprenkelt werden.
:
***Teufelssicht.*** Die Dunkelsicht des Lemuren wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Faust.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Lich
*Mittelgroßer Untoter, jede böse Gesinnung*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 135 (18W8+54)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|16 (+3)|16 (+3)|20 (+5)|14 (+2)|16 (+3)|
___
**Rettungswürfe**           :: Kon +10, Int +12, Wei +9
**Fertigkeiten**            :: Arkane Kunde +18, Geschichte +12, Motiv erkennen +9, Wahrnehmung +9
**Schadensresistenzen**     :: Blitz, Kälte, Nekrotisch
**Schadensimmunitäten**     :: Gift; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 19
**Sprachen**                :: Gemeinsprache und bis zu fünf weitere Sprachen
**Herausforderung**         :: 21 (33.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Lichs scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
***Resistenz gegen Wandeln.*** Der Lich ist bei Rettungswürfen gegen Effekte, die Untote vertreiben, im Vorteil.
:
***Wiederbelebung.*** Wenn der Lich zerstört wird, jedoch über ein Seelengefäß verfügt, erhält er in 1W10 Tagen einen neuen Körper mit allen Trefferpunkten und wird wieder aktiv. Der neue Körper erscheint im Abstand von bis zu 1,5 Metern vom Seelengefäß.
:
***Zauberwirken.*** Der Lich ist ein Zauberwirker der 18. Stufe. Sein Attribut zum Zauberwirken ist Intelligenz (Zauberrettungswurf-SG 20, +12 auf Treffer mit Zauberangriffen). Der Lich hat folgende Magierzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Kältestrahl*](https://openrpg.de/srd/5e/de/#spell-kaeltestrahl)[*Magierhand*](https://openrpg.de/srd/5e/de/#spell-magierhand)[*Taschenspielerei*](https://openrpg.de/srd/5e/de/#spell-taschenspielerei)<br>
1&period; Grad (4 Plätze): [*Donnerwoge*](https://openrpg.de/srd/5e/de/#spell-donnerwoge)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Magisches Geschoss*](https://openrpg.de/srd/5e/de/#spell-magisches-geschoss)[*Schild*](https://openrpg.de/srd/5e/de/#spell-schild)<br>
2&period; Grad (3 Plätze): [*Gedanken wahrnehmen*](https://openrpg.de/srd/5e/de/#spell-gedanken-wahrnehmen)[*Säurepfeil*](https://openrpg.de/srd/5e/de/#spell-saeurepfeil)[*Spiegelbilder*](https://openrpg.de/srd/5e/de/#spell-spiegelbilder)[*Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)<br>
3&period; Grad (3 Plätze): [*Feuerball*](https://openrpg.de/srd/5e/de/#spell-feuerball)[*Gegenzauber*](https://openrpg.de/srd/5e/de/#spell-gegenzauber)[*Magie bannen*](https://openrpg.de/srd/5e/de/#spell-magie-bannen)[*Tote beleben*](https://openrpg.de/srd/5e/de/#spell-tote-beleben)<br>
4&period; Grad (3 Plätze): [*Dimensionstür*](https://openrpg.de/srd/5e/de/#spell-dimensionstuer)[*Plage*](https://openrpg.de/srd/5e/de/#spell-plage)<br>
5&period; Grad (3 Plätze): [*Ausspähung*](https://openrpg.de/srd/5e/de/#spell-ausspaehung)[*Todeswolke*](https://openrpg.de/srd/5e/de/#spell-todeswolke)<br>
6&period; Grad (1 Platz): [*Auflösung*](https://openrpg.de/srd/5e/de/#spell-aufloesung)[*Kugel der Unverwundbarkeit*](https://openrpg.de/srd/5e/de/#spell-kugel-der-unverwundbarkeit)<br>
7&period; Grad (1 Platz): [*Ebenenwechsel*](https://openrpg.de/srd/5e/de/#spell-ebenenwechsel)[*Finger des Todes*](https://openrpg.de/srd/5e/de/#spell-finger-des-todes)<br>
8&period; Grad (1 Platz): [*Monster beherrschen*](https://openrpg.de/srd/5e/de/#spell-monster-beherrschen)[*Wort der Macht: Betäubung*](https://openrpg.de/srd/5e/de/#spell-wort-der-macht-betaeubung)<br>
9&period; Grad (1 Platz): [*Wort der Macht: Tod*](https://openrpg.de/srd/5e/de/#spell-wort-der-macht-tod)
:
### Aktionen
***Lähmende Berührung.*** *Nahkampf-Zauberangriff*: +12 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 10 (3W6) Kälteschaden. Das Ziel muss einen SG-18-Konstitutionsrettungswurf bestehen, oder es ist eine Minute lang gelähmt. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Lich kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Lich erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Furchteinflößender Blick (kostet 2 Aktionen).*** Der Lich blickt eine Kreatur im Abstand von bis zu drei Metern von ihm an, die er sehen kann. Das Ziel muss einen SG-18-Weisheitsrettungswurf gegen diese Magie bestehen, oder es ist eine Minute lang verängstigt. Ein verängstigtes Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn der Rettungswurf erfolgreich ist oder der Effekt endet, ist das Ziel 24 Stunden lang gegen den Blick des Lichs immun.
:
***Lähmende Berührung (kostet 2 Aktionen).*** Der Lich setzt seine Lähmende Berührung ein.
:
***Lebensunterbrechung (kostet 3 Aktionen).*** Jede Kreatur im Abstand von bis zu sechs Metern vom Lich, die kein Untoter ist, muss einen SG-18-Konstitutionsrettungswurf gegen diese Magie ausführen. Scheitert der Wurf, erleidet sie 21 (6W6) nekrotischen Schaden, anderenfalls die Hälfte.
:
***Zaubertrick.*** Der Lich wirkt einen Zaubertrick.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Löwe
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 26 (4W10+4)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|15 (+2)|13 (+1)|3  (-4)|12 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Anspringen.*** Wenn der Löwe sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Klauenangriff trifft, muss das Ziel einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen. Wenn das Ziel liegt, kann der Löwe als Bonusaktion einen Bissangriff gegen es ausführen.
:
***Rudeltaktik.*** Der Löwe ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfer Geruchssinn.*** Der Löwe ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
***Sprung aus dem Lauf.*** Nach drei Metern Anlauf kann der Löwe bis zu 7,5 Meter weit springen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Luftelementar
*Großer Elementar, neutral*
___
**Rüstungsklasse** :: 15
**Trefferpunkte**  :: 90 (12W10+24)
**Speed**          :: 0 m, Fliegen 27 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|20 (+5)|14 (+2)|6  (-2)|10 (+0)|6  (-2)|
___
**Schadensresistenzen**     :: Blitz, Schall; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bewusstlos, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Aural
**Herausforderung**         :: 5 (1.800 EP)
___
***Luftgestalt.*** Der Elementar kann in den Bereich einer feindlich gesinnten Kreatur eindringen und dort stoppen. Er kann sich durch einen engen Bereich mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen.
:
### Aktionen
***Mehrfachangriff.*** Der Elementar führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W8+5) Wuchtschaden.
:
***Wirbelwind (Aufladung 4–6).*** Jede Kreatur im Bereich des Elementars muss einen SG-13-Stärkerettungswurf ausführen. Scheitert der Wurf, so erleidet das Ziel 15 (3W8+2) Wuchtschaden, wird bis zu sechs Meter weit in eine zufällige Richtung vom Elementar weggeschleudert und umgestoßen. Wenn ein geschleudertes Ziel mit einem Objekt wie einer Wand oder dem Boden kollidiert, erleidet es 3 (1W6) Wuchtschaden je drei Meter, die es geschleudert wurde. Kollidiert das Ziel mit einer anderen Kreatur, muss diese einen SG-13-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet den gleichen Schaden und wird umgestoßen. Ist der Rettungswurf erfolgreich, erleidet das Ziel den halben Wuchtschaden und wird weder weg- noch umgestoßen.
:
}}


{{monster,frame
## Magier
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 12 (15 mit Magierrüstung)
**Trefferpunkte**  :: 40 (9W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|9  (-1)|14 (+2)|11 (+0)|17 (+3)|12 (+1)|11 (+0)|
___
**Rettungswürfe**           :: Int +6, Wei +4
**Fertigkeiten**            :: Arkane Kunde +6, Geschichte +6
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: Vier beliebige Sprachen
**Herausforderung**         :: 6 (2.300 EP)
___
***Zauberwirken.*** Der Magier ist ein Zauberwirker der 9. Stufe. Sein Attribut zum Zauberwirken ist Intelligenz (Zauberrettungswurf-SG 14, +6 auf Treffer mit Zauberangriffen). Der Magier hat folgende Magierzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Feuerpfeil*](https://openrpg.de/srd/5e/de/#spell-feuerpfeil)[*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Magierhand*](https://openrpg.de/srd/5e/de/#spell-magierhand)[*Taschenspielerei*](https://openrpg.de/srd/5e/de/#spell-taschenspielerei)<br>
1&period; Grad (4 Plätze): [*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Magierrüstung*](https://openrpg.de/srd/5e/de/#spell-magierruestung)[*Magisches Geschoss*](https://openrpg.de/srd/5e/de/#spell-magisches-geschoss)[*Schild*](https://openrpg.de/srd/5e/de/#spell-schild)<br>
2&period; Grad (3 Plätze): [*Einflüsterung*](https://openrpg.de/srd/5e/de/#spell-einfluesterung)[*Nebelschritt*](https://openrpg.de/srd/5e/de/#spell-nebelschritt)<br>
3&period; Grad (3 Plätze): [*Feuerball*](https://openrpg.de/srd/5e/de/#spell-feuerball)[*Fliegen*](https://openrpg.de/srd/5e/de/#spell-fliegen)[*Gegenzauber*](https://openrpg.de/srd/5e/de/#spell-gegenzauber)<br>
4&period; Grad (3 Plätze): [*Eissturm*](https://openrpg.de/srd/5e/de/#spell-eissturm)[*Mächtige Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-maechtige-unsichtbarkeit)<br>
5&period; Grad (1 Platz): [*Kältekegel*](https://openrpg.de/srd/5e/de/#spell-kaeltekegel)
:
### Aktionen
***Dolch.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Magma-Mephit
*Kleiner Elementar, neutral böse*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 22 (5W6+5)
**Speed**          :: 9 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|12 (+1)|12 (+1)|7  (-2)|10 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +3
**Schadensanfälligkeiten**  :: Kälte
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Ignal, Terral
**Herausforderung**         :: 1/2 (100 EP)
___
***Angeborenes Zauberwirken (1-mal täglich).*** Der Mephit kann den angeborenen Zauber Metall erhitzen (Zauberrettungswurf-SG 10) wirken, ohne Materialkomponenten zu benötigen. Sein Attribut zum angeborenen Zauberwirken ist Charisma.
:
***Falsches Erscheinungsbild.*** Solange der Mephit sich nicht bewegt, ist er nicht von einem gewöhnlichen Magmahaufen zu unterscheiden.
:
***Todesexplosion.*** Wenn der Mephit stirbt, vergeht er in einer Lavaexplosion. Jede Kreatur im Abstand von bis zu 1,5 Metern von ihm muss einen SG-11-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 7 (2W6) Feuerschaden, anderenfalls die Hälfte.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 3 (1W4+1) Hiebschaden plus 2 (1W4) Feuerschaden.
:
***Feuerodem (Aufladung 6).*** Der Mephit atmet Feuer in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-11-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 7 (2W6) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Magmin
*Kleiner Elementar, chaotisch neutral*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 9 (2W6+2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|7  (-2)|15 (+2)|12 (+1)|8  (-1)|11 (+0)|10 (+0)|
___
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Ignal
**Herausforderung**         :: 1/2 (100 EP)
___
***Entflammte Beleuchtung.*** Als Bonusaktion kann der Magmin sich selbst in Flammen aufgehen lassen oder aber seine Flammen löschen. Brennend spendet der Magmin im Radius von drei Metern helles Licht und im Radius von weiteren drei Metern dämmriges Licht.
:
***Todesexplosion.*** Wenn der Magmin stirbt, explodiert er mit einem Ausbruch von Feuer und Magma. Jede Kreatur im Abstand von bis zu drei Metern von ihm muss einen SG-11-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 7 (2W6) Feuerschaden, anderenfalls die Hälfte. Brennbare Objekte in dem Bereich, die nicht getragen oder gehalten werden, fangen Feuer.
:
### Aktionen
***Berührung.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W6) Feuerschaden. Wenn das Ziel eine Kreatur oder ein brennbares Objekt ist, fängt es Feuer. Es erleidet am Ende jedes seiner Züge 3 (1W6) Feuerschaden, bis eine Kreatur eine Aktion verwendet, um das Feuer zu löschen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Mammut
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 126 (11W12+55)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|24 (+7)|9  (-1)|21 (+5)|3  (-4)|11 (+0)|6  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 6 (2.300 EP)
___
***Trampel-Sturmangriff.*** Wenn das Mammut sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Zerfleischen-Angriff trifft, muss das Ziel einen SG-18-Stärkerettungswurf bestehen, oder es wird umgestoßen. Gegen ein liegendes Ziel kann das Mammut als Bonusaktion einen Stampfen-Angriff ausführen.
:
### Aktionen
***Zerfleischen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 25 (4W8+7) Stichschaden.
:
***Stampfen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, eine liegende Kreatur. *Treffer*: 29 (4W10+7) Wuchtschaden.
:
}}


{{monster,frame
## Mantikor
*Große Monstrosität, rechtschaffen böse*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 68 (8W10+24)
**Speed**          :: 9 m, Fliegen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|16 (+3)|17 (+3)|7  (-2)|12 (+1)|8  (-1)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: Gemeinsprache
**Herausforderung**         :: 3 (700 EP)
___
***Schwanzstachel-Wachstum.*** Der Mantikor hat 24 Schwanzstacheln. Verbrauchte Stacheln wachsen nach, wenn der Mantikor eine lange Rast abschließt.
:
### Aktionen
***Mehrfachangriff.*** Der Mantikor führt drei Angriffe aus: einen mit seinem Biss und zwei mit seinen Klauen oder drei mit seinen Schwanzstacheln.
:
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Hiebschaden.
:
***Schwanzstacheln.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 30/60 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Mantler
*Große Aberration, chaotisch neutral*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 78 (12W10+12)
**Speed**          :: 3 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|15 (+2)|12 (+1)|13 (+1)|12 (+1)|14 (+2)|
___
**Fertigkeiten**            :: Heimlichkeit +5
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: Gemeinsprache der Unterreiche, Tiefensprache
**Herausforderung**         :: 8 (3.900 EP)
___
***Falsches Erscheinungsbild.*** Solange der Mantler sich nicht bewegt und seine Unterseite nicht exponiert ist, ist er nicht von einem dunklen Lederumhang zu unterscheiden.
:
***Lichtempfindlich.*** In hellem Licht ist der Mantler bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
***Schadensübertragung.*** Während der Mantler sich an eine Kreatur angeheftet hat, erleidet er nur die Hälfte des Schadens, der ihm zugefügt wird (abgerundet), und die Kreatur erleidet die andere Hälfte.
:
### Aktionen
***Mehrfachangriff.*** Der Mantler führt zwei Angriffe aus: einen Biss- und einen Schwanzangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 10 (2W6+3) Stichschaden, und wenn das Ziel groß oder kleiner ist, heftet der Mantler sich an es an. Wenn der Mantler gegen das Ziel im Vorteil ist, heftet er sich an dessen Kopf. Das Ziel ist blind und kann nicht atmen, solange der Mantler sich angeheftet hat. Ist der Mantler angeheftet, so kann er diesen Angriff nur gegen das Ziel ausführen und ist beim Angriffswurf im Vorteil. Der Mantler kann sich lösen, indem er 1,5 Meter seiner Bewegungsrate verwendet. Eine Kreatur, auch das Ziel, kann seine Aktion verwenden, um den Mantler mit einem erfolgreichen SG-16-Stärkewurf zu entfernen.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 7 (1W8+3) Hiebschaden.
:
***Phantome (wird nach kurzer oder langer Rast aufgeladen).*** Der Mantler erzeugt magisch drei illusorische Duplikate von sich selbst, sofern er sich nicht in hellem Licht befindet. Die Duplikate bewegen sich mit ihm und imitieren seine Aktionen. Dabei tauschen sie die Positionen so, dass es unmöglich ist nachzuverfolgen, welcher Mantler der reale ist. Wenn der Mantler sich in einen Bereich mit hellem Licht begibt, verschwinden die Duplikate. Wenn eine Kreatur mit einem Angriff oder Schadenszauber auf den Mantler zielt, solange noch mindestens ein Duplikat vorhanden ist, bestimmt sie durch einen Zufallswurf, ob sie auf den Mantler oder auf ein Duplikat zielt. Die Kreatur ist von diesem magischen Effekt nicht betroffen, wenn sie nicht sehen kann oder sich auf andere Sinne als Sicht verlässt. Duplikate haben die RK des Mantlers und verwenden seine Rettungswürfe. Wenn ein Duplikat von einem Angriff getroffen wird oder sein Rettungswurf gegen einen Schadenseffekt scheitert, verschwindet das Duplikat.
:
***Stöhnen.*** Jede Kreatur im Abstand von bis zu 18 Metern vom Mantler, die sein Stöhnen hören kann und keine Aberration ist, muss einen SG-13-Weisheitsrettungswurf bestehen, oder sie ist bis zum Ende des nächsten Zugs des Mantlers verängstigt. Wenn eine Kreatur ihren Rettungswurf besteht, ist sie 24 Stunden lang gegen das Stöhnen des Mantlers immun.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Marilith
*Großer Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 189 (18W10+90)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|20 (+5)|20 (+5)|18 (+4)|16 (+3)|20 (+5)|
___
**Rettungswürfe**           :: Str +9, Kon +10, Wei +8, Cha +10
**Schadensresistenzen**     :: Blitz, Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 13
**Sprachen**                :: Abyssisch, Telepathie auf 36 m
**Herausforderung**         :: 16 (15.000 EP)
___
***Magieresistenz.*** Der Marilith ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Marilithen sind magisch.
:
***Reaktiv.*** Der Marilith kann im Kampf in jedem Zug eine Reaktion ausführen.
:
### Aktionen
***Mehrfachangriff.*** Der Marilith führt sieben Angriffe aus: sechs mit seinen Langschwertern und einen Schwanzangriff.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 15 (2W10+4) Wuchtschaden. Wenn das Ziel höchstens mittelgroß ist, wird es gepackt (Rettungswurf-SG 19). Solange es gepackt bleibt, ist es festgesetzt. Der Marilith kann es automatisch mit dem Schwanz treffen, jedoch keine Schwanzangriffe gegen andere Ziele ausführen.
:
***Teleportieren.*** Der Marilith teleportiert sich und sämtliche Ausrüstung, die er trägt oder hält, magisch bis zu 36 Meter weit an eine freie Stelle, die er sehen kann.  Reaktionen
:
***Parieren.*** Der Marilith erhöht seine RK gegen einen Nahkampfangriff, der ihn treffen würde, um 5. Dazu muss der Marilith den Angreifer sehen können und eine Nahkampfwaffe führen.
:
}}


{{monster,frame
## Maultier
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|10 (+0)|13 (+1)|2  (-4)|10 (+0)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Lasttier.*** Das Maultier gilt hinsichtlich seiner Traglast als großes Tier.
:
***Sicherer Tritt.*** Die Maultier ist bei Stärke- und Geschicklichkeitsrettungswürfen gegen Effekte, die es umstoßen würden, im Vorteil.
:
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Medusa
*Mittelgroße Monstrosität, rechtschaffen böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 127 (17W8+51)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|15 (+2)|16 (+3)|12 (+1)|13 (+1)|15 (+2)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Motiv erkennen +4, Täuschen +5, Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Gemeinsprache
**Herausforderung**         :: 6 (2.300 EP)
___
***Versteinernder Blick.*** Die Medusa kann Kreaturen, die ihre Augen sehen können und den Zug im Abstand von bis zu neun Metern von ihr beginnen, zu einem SG-14-Konstitutionsrettungswurf zwingen, sofern die Medusa die Kreatur sehen kann und nicht kampfunfähig ist. Wenn der Rettungswurf um mindestens 5 Punkte scheitert, wird die Kreatur sofort versteinert. Anderenfalls beginnt die Kreatur nach einem gescheiterten Rettungswurf zu versteinern und ist festgesetzt. Die festgesetzte Kreatur muss den Rettungswurf am Ende ihres nächsten Zugs wiederholen. Scheitert der Wurf, wird sie versteinert. Bei einem Erfolg endet der Effekt. Die Versteinerung bleibt bestehen, bis die Kreatur durch den Zauber Vollständige Genesung oder andere Magie befreit wird.  Wenn die Kreatur nicht überrascht wird, kann sie zu Beginn ihres Zugs die Augen abwenden, um den Rettungswurf zu vermeiden. In diesem Fall kann sie die Medusa bis zum Beginn ihres nächsten Zugs, wenn sie die Augen erneut abwenden kann, nicht sehen. Schaut sie die Medusa in der Zwischenzeit an, so muss sie den Rettungswurf sofort ausführen.  Wenn die Medusa sich in einem Bereich mit hellem Licht auf einer polierten Oberfläche im Abstand von bis zu neun Metern von ihr reflektiert sieht, erleidet sie aufgrund ihres Fluchs den Effekt ihres eigenen Blicks.
:
### Aktionen
***Mehrfachangriff.*** Die Medusa führt entweder drei Nahkampfangriffe - einen mit ihrem Schlangenhaar und zwei mit ihrem Kurzschwert - oder zwei Fernkampfangriffe mit ihrem Langbogen aus.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Schlangenhaar.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W4+2) Stichschaden plus 14 (4W6) Giftschaden.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden plus 7 (2W6) Giftschaden.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Meervolk
*Mittelgroßer Humanoide (Meervolk), neutral*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 3 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|13 (+1)|12 (+1)|11 (+0)|11 (+0)|12 (+1)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Aqual, Gemeinsprache
**Herausforderung**         :: 1/8 (25 EP)
___
***Amphibisch.*** Der Meervolk-Humanoide kann Luft und Wasser atmen.
:
### Aktionen
***Speer.*** *Fernkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 3 (1W6) Stichschaden oder 4 (1W8) Stichschaden bei zweihändiger Führung und Nahkampfangriff.
:
}}


{{monster,frame
## Menschenaffe
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 19 (3W8+6)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|14 (+2)|14 (+2)|6  (-2)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Athletik +5, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Menschenaffe führt zwei Faustangriffe aus.
:
***Faust.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Wuchtschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 7,5/15 m, ein Ziel. *Treffer*: 6 (1W6+3) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Messingdrachennestling
*Mittelgroßer Drache, chaotisch gut*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 16 (3W8+3)
**Speed**          :: 9 m, Fliegen 18 m, Graben 4,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|10 (+0)|13 (+1)|10 (+0)|11 (+0)|13 (+1)|
___
**Rettungswürfe**           :: Ges +2, Kon +3, Wei +2, Cha +3
**Fertigkeiten**            :: Heimlichkeit +2, Wahrnehmung +4
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 1 (200 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W10+2) Stichschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einer sechs Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-11-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 14 (4W6) Feuerschaden, anderenfalls die Hälfte.
:
***Schlafodem.*** Der Drache atmet Schlafgas in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-11-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang bewusstlos. Dieser Effekt endet bei einer Kreatur, wenn sie Schaden erleidet oder jemand sie mit einer Aktion aufweckt.
:
}}


{{monster,frame
## Mimik
*Mittelgroße Monstrosität (Gestaltwandler), neutral*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 58 (9W8+18)
**Speed**          :: 4,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|12 (+1)|15 (+2)|5  (-3)|13 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +5
**Schadensimmunitäten**     :: Säure
**Zustandsimmunitäten**     :: Liegend
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Gestaltwandler.*** Der Mimik kann seine Aktion verwenden, um sich in einen Gegenstand oder zurück in seine wahre, amorphe Gestalt zu verwandeln. Seine Spielwerte sind in beiden Formen gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Falsches Erscheinungsbild (nur in Objektgestalt).*** Solange der Mimik sich nicht bewegt, ist er nicht von einem gewöhnlichen Objekt zu unterscheiden.
:
***Greifer.*** Der Mimik ist bei Angriffswürfen gegen jede Kreatur, die er gepackt hält, im Vorteil.
:
***Haftend (nur in Objektgestalt).*** Der Mimik bleibt an allem haften, was ihn berührt. Eine höchstens riesige Kreatur, die am Mimik haftet, wird auch von ihm gepackt (Rettungswurf-SG 13). Attributswürfe, um diesem Haltegriff zu entkommen, sind im Nachteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Stichschaden plus 4 (1W8) Säureschaden.
:
***Scheinfuß.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Wuchtschaden. Wenn der Mimik sich in Objektgestalt befindet, wird das Ziel Opfer seines Haftend-Merkmals.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Minotaurus
*Große Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 76 (9W10+27)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|11 (+0)|16 (+3)|6  (-2)|16 (+3)|9  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +7
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 17
**Sprachen**                :: Abyssisch
**Herausforderung**         :: 3 (700 EP)
___
***Erinnerung des Labyrinths.*** Der Minotaurus kann sich perfekt an jeden Weg erinnern, den er zurückgelegt hat.
:
***Sturmangriff.*** Wenn der Minotaurus sich mindestens drei Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Zerfleischen-Angriff trifft, erleidet das Ziel zusätzlich 9 (2W8) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-14-Stärkerettungswurf bestehen, oder es wird bis zu drei Meter weit weg- und umgestoßen.
:
***Unvorsichtig.*** Zu Beginn seines Zugs kann der Minotaurus entscheiden, bei allen Nahkampfwaffen-Angriffswürfen im Vorteil zu sein, die er in diesem Zug ausführt, doch dann sind Angriffswürfe gegen ihn bis zum Beginn seines nächsten Zugs ebenfalls im Vorteil.
:
### Aktionen
***Zerfleischen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Stichschaden.
:
***Zweihandaxt.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 17 (2W12+4) Hiebschaden.
:
}}


{{monster,frame
## Minotaurus-Skelett
*Großer Untoter, rechtschaffen böse*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 67 (9W10+18)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|11 (+0)|15 (+2)|6  (-2)|8  (-1)|5  (-3)|
___
**Schadensanfälligkeiten**  :: Wucht
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Erschöpft, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: Versteht Abyssisch, aber kann nicht sprechen
**Herausforderung**         :: 2 (450 EP)
___
***Sturmangriff.*** Wenn das Skelett sich mindestens drei Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Zerfleischen-Angriff trifft, erleidet das Ziel zusätzlich 9 (2W8) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-14-Stärkerettungswurf bestehen, oder es wird bis zu drei Meter weit weg- und umgestoßen.
:
### Aktionen
***Zerfleischen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Stichschaden.
:
***Zweihandaxt.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 17 (2W12+4) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Modernder Schlurfer
*Große Pflanze, gesinnungslos*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 136 (16W10+48)
**Speed**          :: 6 m, Schwimmen 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|8  (-1)|16 (+3)|5  (-3)|10 (+0)|5  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +2
**Schadensresistenzen**     :: Feuer, Kälte
**Schadensimmunitäten**     :: Blitz
**Zustandsimmunitäten**     :: Blind, Erschöpft, Taub
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Blitzabsorption.*** Der modernde Schlurfer erleidet durch Blitze keinen Schaden, sondern gewinnt Trefferpunkte in Höhe des Blitzschadens zurück.
:
### Aktionen
***Mehrfachangriff.*** Der modernde Schlurfer führt zwei Hiebangriffe aus. Wenn beide Angriffe ein höchstens mittelgroßes Ziel treffen, wird das Ziel gepackt (Rettungswurf-SG 14), und der modernde Schlurfer setzt bei ihm Einhüllen ein.
:
***Hieb.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Wuchtschaden.
:
***Einhüllen.*** Der modernde Schlurfer hüllt eine höchstens mittelgroße Kreatur ein, die er gepackt hält. Das eingehüllte Ziel ist blind, festgesetzt und kann nicht atmen. Es muss zu Beginn jedes Zugs des modernden Schlurfers einen SG-14-Konstitutionsrettungswurf bestehen, oder es erleidet 13 (2W8+4) Wuchtschaden. Bewegt sich der modernde Schlurfer, so bewegt das eingehüllte Ziel sich mit ihm. Der modernde Schlurfer kann nur jeweils eine Kreatur einhüllen.
:
}}


{{monster,frame
## Mumie
*Mittelgroßer Untoter, rechtschaffen böse*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 58 (9W8+18)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|8  (-1)|15 (+2)|6  (-2)|10 (+0)|12 (+1)|
___
**Rettungswürfe**           :: Wei +2
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensanfälligkeiten**  :: Feuer
**Schadensimmunitäten**     :: Gift, Nekrotisch
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 3 (700 EP)
___
### Aktionen
***Mehrfachangriff.*** Die Mumie kann ihr Fürchterliches Starren einsetzen und führt einen Angriff mit ihrer Verrottenden Faust aus.
:
***Verrottende Faust.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Wuchtschaden plus 10 (3W6) nekrotischer Schaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-12-Konstitutionsrettungswurf bestehen, oder es wird mit Mumienfäule verflucht. Das verfluchte Ziel kann keine Trefferpunkte zurückerhalten, und sein Trefferpunktemaximum verringert sich alle 24 Stunden um 10 (3W6). Reduziert der Fluch das Trefferpunktemaximum des Ziels auf 0, stirbt das Ziel und sein Körper zerfällt zu Staub. Die Fluch bleibt bestehen, bis er durch den Zauber Fluch brechen oder andere Magie entfernt wird.
:
***Fürchterliches Starren.*** Die Mumie zielt auf eine Kreatur im Abstand von bis zu 18 Metern von ihr, sofern sie diese sehen kann. Wenn das Ziel die Mumie sehen kann, muss es einen SG-11-Weisheitsrettungswurf gegen diese Magie ausführen, oder es ist bis zum Ende des nächsten Zugs der Mumie verängstigt. Wenn der Rettungswurf um mindestens fünf Punkte scheitert, ist das Ziel außerdem ebenso lange gelähmt. Bei einem erfolgreichen Rettungswurf ist das Ziel 24 Stunden lang gegen das Fürchterliche Starren aller Mumien (jedoch nicht Mumienfürsten) immun.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Mumienfürst
*Mittelgroßer Untoter, rechtschaffen böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 97 (13W8+39)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|10 (+0)|17 (+3)|11 (+0)|18 (+4)|16 (+3)|
___
**Rettungswürfe**           :: Kon +8, Int +5, Wei +9, Cha +8
**Fertigkeiten**            :: Geschichte +5, Religion +5
**Schadensanfälligkeiten**  :: Feuer
**Schadensimmunitäten**     :: Gift, Nekrotisch; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 15 (13.000 EP)
___
***Magieresistenz.*** Der Mumienfürst ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Wiederbelebung.*** Wenn der Mumienfürst zerstört wird, sein Herz jedoch noch intakt ist, erhält er in 24 Stunden einen neuen Körper mit allen Trefferpunkten und wird wieder aktiv. Der neue Körper erscheint im Abstand von bis zu 1,5 Metern vom Herzen des Mumienfürsten.
:
***Zauberwirken.*** Der Mumienfürst ist ein Zauberwirker der 10. Stufe. Sein Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 17, +9 auf Treffer mit Zauberangriffen). Der Mumienfürst hat folgende Klerikerzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Heilige Flamme*](https://openrpg.de/srd/5e/de/#spell-heilige-flamme)[*Thaumaturgie*](https://openrpg.de/srd/5e/de/#spell-thaumaturgie)<br>
1&period; Grad (4 Plätze): [*Befehl*](https://openrpg.de/srd/5e/de/#spell-befehl)[*Lenkendes Geschoss*](https://openrpg.de/srd/5e/de/#spell-lenkendes-geschoss)[*Schild des Glaubens*](https://openrpg.de/srd/5e/de/#spell-schild-des-glaubens)<br>
2&period; Grad (3 Plätze): [*Person festhalten*](https://openrpg.de/srd/5e/de/#spell-person-festhalten)[*Stille*](https://openrpg.de/srd/5e/de/#spell-stille)[*Waffe des Glaubens*](https://openrpg.de/srd/5e/de/#spell-waffe-des-glaubens)<br>
3&period; Grad (3 Plätze): [*Magie bannen*](https://openrpg.de/srd/5e/de/#spell-magie-bannen)[*Tote beleben*](https://openrpg.de/srd/5e/de/#spell-tote-beleben)<br>
4&period; Grad (3 Plätze): [*Hüter des Glaubens*](https://openrpg.de/srd/5e/de/#spell-hueter-des-glaubens)[*Weissagung*](https://openrpg.de/srd/5e/de/#spell-weissagung)<br>
5&period; Grad (2 Plätze): [*Ansteckung*](https://openrpg.de/srd/5e/de/#spell-ansteckung)[*Insektenplage*](https://openrpg.de/srd/5e/de/#spell-insektenplage)<br>
6&period; Grad (1 Platz): [*Leid*](https://openrpg.de/srd/5e/de/#spell-leid)
:
### Aktionen
***Mehrfachangriff.*** Der Mumienfürst kann sein Fürchterliches Starren einsetzen und führt einen Angriff mit seiner Verrottenden Faust aus.
:
***Verrottende Faust.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (3W6+4) Wuchtschaden plus 21 (6W6) nekrotischer Schaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-16-Konstitutionsrettungswurf bestehen, oder es wird mit Mumienfäule verflucht. Das verfluchte Ziel kann keine Trefferpunkte zurückerhalten, und sein Trefferpunktemaximum verringert sich alle 24 Stunden um 10 (3W6). Reduziert der Fluch das Trefferpunktemaximum des Ziels auf 0, stirbt das Ziel und sein Körper zerfällt zu Staub. Die Fluch bleibt bestehen, bis er durch den Zauber Fluch brechen oder andere Magie entfernt wird.
:
***Fürchterliches Starren.*** Der Mumienfürst zielt auf eine Kreatur im Abstand von bis zu 18 Metern von ihm, das er sehen kann. Wenn das Ziel den Mumienfürsten sehen kann, muss es einen SG-16-Weisheitsrettungswurf gegen diese Magie ausführen, oder es ist bis zum Ende des nächsten Zugs des Mumienfürsten verängstigt. Wenn der Rettungswurf um mindestens fünf Punkte scheitert, ist das Ziel außerdem ebenso lange gelähmt.  Bei einem erfolgreichen Rettungswurf ist das Ziel 24 Stunden lang gegen das Fürchterliche Starren aller Mumien und Mumienfürsten immun.
:
### Legendäre Aktionen
Der Mumienfürst kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Mumienfürst erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Lästerliches Wort (kostet 2 Aktionen).*** Der Mumienfürst spricht ein lästerliches Wort. Jede Kreatur im Abstand von bis zu drei Metern vom Mumienfürsten, die kein Untoter ist und das Wort hören kann, muss einen SG-16-Konstitutionsrettungswurf bestehen, oder sie ist bis zum Ende des nächsten Zugs des Mumienfürsten betäubt.
:
***Negative Energie kanalisieren (kostet 2 Aktionen).*** Der Mumienfürst entfesselt magisch negative Energie. Kreaturen im Abstand von bis zu 18 Metern vom Mumienfürsten, auch solche hinter Barrieren und um Ecken, können bis zum Ende des nächsten Zugs des Mumienfürsten keine Trefferpunkte zurückerhalten.
:
***Wirbelwind aus Sand (kostet 2 Aktionen).*** Der Mumienfürst verwandelt sich magisch in einen Wirbelwind aus Sand, bewegt sich bis zu 18 Meter weit und nimmt dann wieder seine normale Gestalt an. In Wirbelwindgestalt ist der Mumienfürst gegen alle Schadensarten immun, und er kann nicht gepackt, versteinert, umgestoßen, festgesetzt oder betäubt werden. Ausrüstung, die der Mumienfürst trägt oder hält, verbleibt in seinem Besitz.
:
***Angriff.*** Der Mumienfürst führt einen Angriff mit seiner Verrottenden Faust aus oder setzt sein Fürchterliches Starren ein.
:
***Blendender Staub.*** Der Mumienfürst wird magisch von blendendem Staub und Sand umwirbelt. Jede Kreatur im Abstand von bis zu 1,5 Metern vom Mumienfürsten muss einen SG-16-Konstitutionsrettungswurf bestehen, oder sie ist bis zum Ende ihres nächsten Zugs blind.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Nachtmahr
*Großer Unhold, neutral böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 68 (8W10+24)
**Speed**          :: 18 m, Fliegen 27 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|10 (+0)|13 (+1)|15 (+2)|
___
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: Versteht Abyssisch, Gemeinsprache und Infernalisch, aber kann nicht sprechen
**Herausforderung**         :: 3 (700 EP)
___
***Beleuchtung.*** Der Nachtmahr spendet im Radius von drei Metern helles Licht und im Radius von weiteren drei Metern dämmriges Licht.
:
***Feuerresistenz gewähren.*** Der Nachtmahr kann jedem, der ihn reitet, Resistenz gegen Feuerschaden gewähren.
:
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Wuchtschaden plus 7 (2W6) Feuerschaden.
:
***Ätherischer Schritt.*** Der Nachtmahr und bis zu drei bereitwillige Kreaturen im Abstand von bis zu 1,5 Metern von ihm betreten von der materiellen Ebene aus magisch die Ätherebene oder umgekehrt.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Nachtvettel
*Mittelgroßer Unhold, neutral böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 112 (15W8+45)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|16 (+3)|14 (+2)|16 (+3)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Motiv erkennen +6, Täuschen +7, Wahrnehmung +6
**Schadensresistenzen**     :: Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Zustandsimmunitäten**     :: Bezaubert
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 16
**Sprachen**                :: Abyssisch, Gemeinsprache, Infernalisch, Urtümlich
**Herausforderung**         :: 5 (1.800 EP)
___
***Magieresistenz.*** Die Vettel ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken der Vettel ist Charisma (Zauberrettungswurf-SG 14, +6 auf Treffer mit Zauberangriffen). Die Vettel kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Magisches Geschoss*](https://openrpg.de/srd/5e/de/#spell-magisches-geschoss)<br>
Je 2-mal täglich: [*Ebenenwechsel (nur auf sich selbst)*](https://openrpg.de/srd/5e/de/#spell-ebenenwechsel)[*Schlaf*](https://openrpg.de/srd/5e/de/#spell-schlaf)[*Schwächestrahl*](https://openrpg.de/srd/5e/de/#spell-schwaechestrahl)
:
### Aktionen
***Klauen (nur Vettel-Gestalt).*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Hiebschaden.
:
***Albtraumspuk (1-mal täglich).*** Von der Ätherebene aus berührt die Vettel magisch einen schlafenden Humanoiden auf der materiellen Ebene. Der Zauber Schutz vor Gut und Böse, der auf das Ziel gewirkt wurde, verhindert diesen Kontakt ebenso wie ein Schutzkreis. Solange der Kontakt fortbesteht, hat das Ziel grässliche Visionen. Wenn diese Visionen mindestens eine Stunde lang anhalten, hat das Ziel durch seine Rast keine Vorzüge, und sein Trefferpunktemaximum ist um 5 (1W10) verringert. Verringert dieser Effekt das Trefferpunktemaximum des Ziels auf 0, so stirbt das Ziel, und sofern es böser Gesinnung war, ist seine Seele im Seelenbeutel der Vettel gefangen. Das Trefferpunktemaximum des Ziels bleibt verringert, bis der Zauber Vollständige Genesung oder ähnliche Magie auf das Ziel gewirkt wird.
:
***Ätherische Gestalten.*** Die Vettel begibt sich von der materiellen Ebene aus magisch auf die Ätherebene oder umgekehrt. Sie muss dazu einen Herzstein besitzen.
:
***Gestalt ändern.*** Die Vettel verwandelt sich magisch in einen höchstens mittelgroßen weiblichen Humanoiden oder zurück in ihre wahre Gestalt. Ihre Spielwerte sind in jeder Gestalt gleich. Ausrüstung, die sie trägt oder hält, wird nicht verwandelt. Wenn sie stirbt, nimmt sie wieder ihre wahre Gestalt an.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Nalfeshnee
*Großer Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 184 (16W10+96)
**Speed**          :: 6 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|10 (+0)|22 (+6)|19 (+4)|12 (+1)|15 (+2)|
___
**Rettungswürfe**           :: Kon +11, Int +9, Wei +6, Cha +7
**Schadensresistenzen**     :: Blitz, Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Abyssisch, Telepathie auf 36 m
**Herausforderung**         :: 13 (10.000 EP)
___
***Magieresistenz.*** Der Nalfeshnee ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Nalfeshnee setzt Schreckensnimbus ein, sofern möglich. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 32 (5W10+5) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (3W6+5) Hiebschaden.
:
***Schreckensnimbus (Aufladung 5–6).*** Der Nalfeshnee strahlt auf magische Art schillernd buntes Licht aus. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Nalfeshnee, die das Licht sehen kann, muss einen SG-15-Weisheitsrettungswurf bestehen, oder es ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen den Schreckensnimbus des Nalfeshnees immun.
:
***Teleportieren.*** Der Nalfeshnee teleportiert sich und sämtliche Ausrüstung, die er trägt oder hält, magisch bis zu 36 Meter weit an eine freie Stelle, die er sehen kann.
:
}}


{{monster,frame
## Nashorn
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 45 (6W10+12)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|8  (-1)|15 (+2)|2  (-4)|12 (+1)|6  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Sturmangriff.*** Wenn das Nashorn sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Zerfleischen-Angriff trifft, erleidet das Ziel zusätzlich 9 (2W8) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-15-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
### Aktionen
***Zerfleischen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W8+5) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Ockergallerte
*Großer Schlick, gesinnungslos*
___
**Rüstungsklasse** :: 8
**Trefferpunkte**  :: 45 (6W10+12)
**Speed**          :: 3 m, Klettern 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|6  (-2)|14 (+2)|2  (-4)|6  (-2)|1  (-5)|
___
**Schadensresistenzen**     :: Säure
**Schadensimmunitäten**     :: Blitz, Hieb
**Zustandsimmunitäten**     :: Bezaubert, Blind, Erschöpft, Liegend, Taub, Verängstigt
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Amorph.*** Die Gallerte kann sich durch enge Bereiche mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen.
:
***Spinnenklettern.*** Die Gallerte kann an schwierigen Oberflächen klettern, auch kopfüber an der Decke, ohne einen Attributswurf ausführen zu müssen.
:
### Aktionen
***Scheinfuß.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (2W6+2) Wuchtschaden plus 3 (1W6) Säureschaden.  Reaktionen
:
***Teilen.*** Wenn eine mindestens mittelgroße Gallerte Blitz- oder Hiebschaden erleidet, teilt sie sich in zwei neue Gallerten, sofern sie mindestens 10 Trefferpunkte hat. Jede neue Gallerte hat halb so viele Trefferpunkte wie die ursprüngliche Gallerte (abgerundet). Neue Gallerten sind eine Größenkategorie kleiner als die ursprüngliche Gallerte.
:
}}


{{monster,frame
## Oger
*Großer Riese, chaotisch böse*
___
**Rüstungsklasse** :: 11 (Fellrüstung)
**Trefferpunkte**  :: 59 (7W10+21)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|8  (-1)|16 (+3)|5  (-3)|7  (-2)|7  (-2)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 8
**Sprachen**                :: Gemeinsprache, Riesisch
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Zweihandknüppel.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Wuchtschaden.
:
***Wurfspeer.*** *Fernkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m oder Reichweite 9/36 m, ein Ziel. *Treffer*: 11 (2W6+4) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Ogerzombie
*Großer Untoter, neutral böse*
___
**Rüstungsklasse** :: 8
**Trefferpunkte**  :: 85 (9W10+36)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|6  (-2)|18 (+4)|3  (-4)|6  (-2)|5  (-3)|
___
**Rettungswürfe**           :: Wei +0
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 8
**Sprachen**                :: Versteht Gemeinsprache und Riesisch, aber kann nicht sprechen
**Herausforderung**         :: 2 (450 EP)
___
***Untote Ausdauer.*** Wenn die Trefferpunkte des Zombies durch Schaden auf 0 sinken, muss der Zombie einen Konstitutionsrettungswurf mit SG 5 + erlittenem Schaden ausführen, sofern der Schaden nicht gleißend ist oder von einem kritischen Treffer stammt. Bei einem Erfolg behält der Zombie 1 Trefferpunkt.
:
### Aktionen
***Morgenstern.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Wuchtschaden.
:
}}


{{monster,frame
## Oktopus
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 3 (1W6)
**Speed**          :: 1,5 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|4  (-3)|15 (+2)|11 (+0)|3  (-4)|10 (+0)|4  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +2
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Atem anhalten.*** Der Oktopus kann 30 Minuten lang den Atem anhalten, wenn er nicht im Wasser ist.
:
***Unterwassertarnung.*** Der Oktopus ist bei Geschicklichkeitswürfen (Heimlichkeit) unter Wasser im Vorteil.
:
***Wasser atmen.*** Der Oktopus kann nur unter Wasser atmen.
:
### Aktionen
***Tentakel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 10). Der Oktopus kann seine Tentakel nicht gegen andere Ziele einsetzen, solange er ein Ziel gepackt hält.
:
***Tintenwolke (wird nach kurzer oder langer Rast aufgeladen).*** Eine Tintenwolke mit 1,5 Metern Radius breitet sich um den Oktopus aus, wenn dieser sich unter Wasser befindet. Der Bereich ist eine Minute lang komplett verschleiert, wobei eine starke Strömung die Tinte fortspülen kann. Nach Ausstoßen der Tinte kann der Oktopus die Spurt-Aktion als Bonusaktion ausführen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Oni
*Großer Riese, rechtschaffen böse*
___
**Rüstungsklasse** :: 16 (Kettenpanzer)
**Trefferpunkte**  :: 110 (13W10+39)
**Speed**          :: 9 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|11 (+0)|16 (+3)|14 (+2)|12 (+1)|15 (+2)|
___
**Rettungswürfe**           :: Ges +3, Kon +6, Wei +4, Cha +5
**Fertigkeiten**            :: Arkane Kunde +5, Täuschen +8, Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Gemeinsprache, Riesisch
**Herausforderung**         :: 7 (2.900 EP)
___
***Magische Waffen.*** Die Waffenangriffe des Onis sind magisch.
:
***Regeneration.*** Der Oni erhält zu Beginn seines Zugs 10 Trefferpunkte zurück, wenn er noch mindestens 1 Trefferpunkt hat.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Onis ist Charisma (Zauberrettungswurf-SG 13). Der Oni kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Dunkelheit*](https://openrpg.de/srd/5e/de/#spell-dunkelheit)[*Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)<br>
Je 1-mal täglich: [*Gasförmige Gestalt*](https://openrpg.de/srd/5e/de/#spell-gasfoermige-gestalt)[*Kältekegel*](https://openrpg.de/srd/5e/de/#spell-kaeltekegel)[*Person bezaubern*](https://openrpg.de/srd/5e/de/#spell-person-bezaubern)[*Schlaf*](https://openrpg.de/srd/5e/de/#spell-schlaf)
:
### Aktionen
***Mehrfachangriff.*** Der Oni führt zwei Angriffe aus, entweder mit seinen Klauen oder mit seiner Glefe.
:
***Klauen (nur Oni-Gestalt).*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W8+4) Hiebschaden.
:
***Glefe.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W10+4) Hiebschaden oder 9 (1W10+4) Hiebschaden in kleiner oder mittelgroßer Gestalt.
:
***Gestalt ändern.*** Der Oni verwandelt sich magisch in einen kleinen oder mittelgroßen Humanoiden oder zurück in seine wahre Gestalt. Seine Spielwerte sind abgesehen von der Größe in allen Gestalten gleich. Die einzige Ausrüstung, die sich ebenfalls verwandelt, ist seine Glefe. Sie schrumpft, sodass sie auch in humanoider Gestalt geführt werden kann. Wenn der Oni stirbt, nimmt er wieder seine wahre Gestalt an, und seine Glefe nimmt wieder ihre normale Größe an.
:
}}


{{monster,frame
## Ork
*Mittelgroßer Humanoide (Ork), chaotisch böse*
___
**Rüstungsklasse** :: 13 (Fellrüstung)
**Trefferpunkte**  :: 15 (2W8+6)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|12 (+1)|16 (+3)|7  (-2)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Einschüchtern +2
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Gemeinsprache, Orkisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Aggressiv.*** Als Bonusaktion kann sich der Ork bis zu seiner Bewegungsrate auf eine feindlich gesinnte Kreatur zubewegen, die er sehen kann.
:
### Aktionen
***Zweihandaxt.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (1W12+3) Hiebschaden.
:
***Wurfspeer.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m oder Reichweite 9/36 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Otyugh
*Große Aberration, neutral*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 114 (12W10+48)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|11 (+0)|19 (+4)|6  (-2)|13 (+1)|6  (-2)|
___
**Rettungswürfe**           :: Kon +7
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Otyughisch
**Herausforderung**         :: 5 (1.800 EP)
___
***Beschränkte Telepathie.*** Der Otyugh kann einfache Botschaften und Bilder magisch an jede Kreatur, die eine Sprache versteht, im Abstand von bis zu 36 Metern von ihm übertragen. Diese Telepathieform ermöglicht nicht, dass die empfangende Kreatur telepathisch antwortet.
:
### Aktionen
***Mehrfachangriff.*** Der Otyugh führt drei Angriffe aus: einen Biss- und zwei Tentakelangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W8+3) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-15-Konstitutionsrettungswurf gegen die Krankheit bestehen, oder es ist vergiftet, bis die Krankheit geheilt wird. Nach jeweils 24 Stunden muss das Ziel den Rettungswurf wiederholen. Scheitert der Wurf, wird sein Trefferpunktemaximum um 5 (1W10) verringert. Bei einem Erfolg wird die Krankheit geheilt. Wenn das Trefferpunktemaximum durch die Krankheit auf 0 sinkt, stirbt das Ziel. Das Trefferpunktemaximum des Ziels bleibt verringert, bis die Krankheit geheilt wird.
:
***Tentakel.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 7 (1W8+3) Wuchtschaden plus 4 (1W8) Stichschaden. Wenn das Ziel höchstens mittelgroß ist, wird es gepackt (Rettungswurf-SG 13) und ist festgesetzt, bis es befreit wird. Der Otyugh hat zwei Tentakel, die jeweils ein Ziel packen können.
:
***Tentakelhieb.*** Der Otyugh schmettert gepackte Kreaturen gegeneinander oder gegen eine feste Oberfläche. Jede Kreatur muss einen SG-14-Konstitutionsrettungswurf bestehen, oder sie erleidet 10 (2W6+3) Wuchtschaden und ist bis zum Ende des nächsten Zugs des Otyughs betäubt. Bei einem erfolgreichen Rettungswurf erleidet das Ziel den halben Wuchtschaden und ist nicht betäubt.
:
}}


{{monster,frame
## Panther
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 13 (3W8)
**Speed**          :: 15 m, Klettern 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|15 (+2)|10 (+0)|3  (-4)|14 (+2)|7  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Anspringen.*** Wenn der Panther sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Klauenangriff trifft, muss das Ziel einen SG-12-Stärkerettungswurf bestehen, oder es wird umgestoßen. Wenn das Ziel liegt, kann der Panther als Bonusaktion einen Bissangriff gegen es ausführen.
:
***Scharfer Geruchssinn.*** Die Panther ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Pavian
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 3 (1W6)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|14 (+2)|11 (+0)|4  (-3)|12 (+1)|6  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Rudeltaktik.*** Der Pavian ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +1 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 (1W4-1) Stichschaden.
:
}}


{{monster,frame
## Pegasus
*Großes celestisches Wesen, chaotisch gut*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 59 (7W10+21)
**Speed**          :: 18 m, Fliegen 27 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|10 (+0)|15 (+2)|13 (+1)|
___
**Rettungswürfe**           :: Ges +4, Wei +4, Cha +3
**Fertigkeiten**            :: Wahrnehmung +6
**Sinne**                   :: Passive Wahrnehmung 16
**Sprachen**                :: Versteht Celestisch, Elfisch, Gemeinsprache und Sylvanisch, aber kann nicht sprechen
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Pferdegreif
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 12 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|13 (+1)|13 (+1)|2  (-4)|12 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +5
**Sinne**                   :: Passive Wahrnehmung 15
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Scharfe Sicht.*** Der Pferdegreif ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Pferdegreif führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W10+3) Stichschaden.
:
}}


{{monster,frame
## Phasenspinne
*Große Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 32 (5W10+5)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|15 (+2)|12 (+1)|6  (-2)|10 (+0)|6  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +6
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 3 (700 EP)
___
***Ätherische Bewegung.*** Als Bonusaktion kann die Spinne sich von der materiellen Ebene aus auf die Ätherebene oder umgekehrt begeben.
:
***Netzwandler.*** Die Spinne ignoriert Bewegungseinschränkungen, die durch Netze verursacht werden.
:
***Spinnenklettern.*** Die Spinne kann an schwierigen Oberflächen klettern, auch kopfüber an der Decke, ohne Attributswürfe ausführen zu müssen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 7 (1W10+2) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 18 (4W8) Giftschaden, anderenfalls die Hälfte. Wenn die Trefferpunkte des Ziels durch den Giftschaden auf 0 sinken, ist das Ziel stabil, aber eine Stunde lang vergiftet sowie gelähmt, selbst wenn es Trefferpunkte zurückgewinnt.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Planetar
*Großes celestisches Wesen, rechtschaffen gut*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 200 (16W10+112)
**Speed**          :: 12 m, Fliegen 36 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|24 (+7)|20 (+5)|24 (+7)|19 (+4)|22 (+6)|25 (+7)|
___
**Rettungswürfe**           :: Kon +12, Wei +11, Cha +12
**Fertigkeiten**            :: Wahrnehmung +11
**Schadensresistenzen**     :: Gleißend; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Verängstigt
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 21
**Sprachen**                :: Alle, Telepathie auf 36 m
**Herausforderung**         :: 16 (15.000 EP)
___
***Engelswaffen.*** Die Waffenangriffe des Planetars sind magisch. Wenn der Planetar mit einer Waffe trifft, bewirkt die Waffe zusätzlich 5W8 gleißenden Schaden (im Angriff enthalten).
:
***Göttliches Bewusstsein.*** Der Planetar erkennt, wenn er eine Lüge hört.
:
***Magieresistenz.*** Der Planetar ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Planetars ist Charisma (Zauberrettungswurf-SG 20). Der Planetar kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)[*Unsichtbarkeit (nur auf sich selbst)*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)<br>
Je 3-mal täglich: [*Flammenschlag*](https://openrpg.de/srd/5e/de/#spell-flammenschlag)[*Gutes und Böses bannen*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-bannen)[*Klingenbarriere*](https://openrpg.de/srd/5e/de/#spell-klingenbarriere)[*Tote erwecken*](https://openrpg.de/srd/5e/de/#spell-tote-erwecken)<br>
Je 1-mal täglich: [*Heiliges Gespräch*](https://openrpg.de/srd/5e/de/#spell-heiliges-gespraech)[*Insektenplage*](https://openrpg.de/srd/5e/de/#spell-insektenplage)[*Wetterkontrolle*](https://openrpg.de/srd/5e/de/#spell-wetterkontrolle)
:
### Aktionen
***Mehrfachangriff.*** Der Planetar führt zwei Nahkampfangriffe aus.
:
***Zweihandschwert.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 21 (4W6+7) Hiebschaden plus 22 (5W8) gleißender Schaden.
:
***Heilende Berührung (4-mal täglich).*** Der Planetar berührt eine andere Kreatur. Das Ziel erhält magisch 30 (6W8+3) Trefferpunkte zurück und wird von allen Flüchen, Giften und Krankheiten sowie von Blindheit und Taubheit befreit.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Plapperndes Hundertmaul
*Mittelgroße Aberration, neutral*
___
**Rüstungsklasse** :: 9
**Trefferpunkte**  :: 67 (9W8+27)
**Speed**          :: 3 m, Schwimmen 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|8  (-1)|16 (+3)|3  (-4)|10 (+0)|6  (-2)|
___
**Zustandsimmunitäten**     :: Liegend
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Plappern.*** Das Hundertmaul plappert unzusammenhän-gend, solange es irgendeine Kreatur sehen kann und nicht kampfunfähig ist. Jede Kreatur, die ihren Zug im Abstand von bis zu sechs Metern vom Hundertmaul beginnt und das Plappern hören kann, muss einen SG-10-Weisheitsrettungswurf ausführen. Scheitert der Wurf, so kann die Kreatur bis zum Beginn ihres nächsten Zugs keine Reaktionen ausführen und würfelt mit einem W8, um ihre Handlungen in ihrem Zug zu bestimmen: Von 1 bis 4 tut die Kreatur nichts. Bei 5 oder 6 führt die Kreatur weder Aktionen noch Bonusaktionen aus und verwendet ihre gesamte Bewegungsrate, um sich in eine zufällig bestimmte Richtung zu bewegen. Bei 7 oder 8 führt die Kreatur einen Nahkampfangriff gegen eine zufällig bestimmte Kreatur in Reichweite aus. Ist ein solcher Angriff nicht möglich, so tut die Kreatur nichts.
:
***Ungewöhnlicher Boden.*** Der Boden im Radius von drei Metern um das Hundertmaul ist teigartiges schwieriges Gelände. Jede Kreatur, die ihren Zug in diesem Bereich beginnt, muss einen SG-10-Stärkerettungswurf bestehen, oder ihre Bewegungsrate ist bis zum Beginn ihres nächsten Zugs auf 0 verringert.
:
### Aktionen
***Mehrfachangriff.*** Das plappernde Hundertmaul führt einen Bissangriff aus und setzt Blendender Speichel ein, sofern möglich.
:
***Bisse.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 17 (5W6) Stichschaden. Wenn das Ziel höchstens mittelgroß ist, muss es einen SG-10-Stärkerettungswurf bestehen, oder es wird umgestoßen. Wird das Ziel durch diesen Schaden getötet, so wird es vom Hundertmaul absorbiert.
:
***Blendender Speichel (Aufladung 5–6).*** Das Hundertmaul speit einen chemischen Klumpen auf einen Punkt im Abstand von bis zu 4,5 Metern von ihm, den es sehen kann. Der Klumpen explodiert beim Auftreffen in einem blendenden Blitz. Jede Kreatur im Abstand von bis zu 1,5 Metern vom Blitz muss einen SG-13-Geschicklichkeitsrettungswurf bestehen, oder sie ist bis zum Ende des nächsten Zugs des Hundertmauls blind.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Plesiosaurier
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 68 (8W10+24)
**Speed**          :: 6 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|15 (+2)|16 (+3)|2  (-4)|12 (+1)|5  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Atem anhalten.*** Der Plesiosaurier kann eine Stunde lang den Atem anhalten.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 14 (3W6+4) Stichschaden.
:
}}


{{monster,frame
## Pony
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|10 (+0)|13 (+1)|2  (-4)|11 (+0)|7  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Priester
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 13 (Kettenhemd)
**Trefferpunkte**  :: 27 (5W8+5)
**Speed**          :: 7,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|10 (+0)|12 (+1)|13 (+1)|16 (+3)|13 (+1)|
___
**Fertigkeiten**            :: Heilkunde +7, Religion +4, Überzeugen +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Zwei beliebige Sprachen
**Herausforderung**         :: 2 (450 EP)
___
***Göttliche Eminenz.*** Als Bonusaktion kann der Priester einen Zauberplatz verwenden, um seine Nahkampf-Waffenangriffe bei einem Treffer magisch mit zusätzlich 10 (3W6) gleißendem Schaden zu versehen. Dieser Vorzug bleibt bis zum Ende des Zugs erhalten. Wenn der Priester einen Zauberplatz mindestens des 2. Grades verwendet, wird der Extraschaden für jeden Grad über dem ersten um 1W6 erhöht.
:
***Zauberwirken.*** Der Priester ist ein Zauberwirker der 5. Stufe. Sein Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 13, +5 auf Treffer mit Zauberangriffen). Der Priester hat folgende Klerikerzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Heilige Flamme*](https://openrpg.de/srd/5e/de/#spell-heilige-flamme)[*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Thaumaturgie*](https://openrpg.de/srd/5e/de/#spell-thaumaturgie)<br>
1&period; Grad (4 Plätze): [*Heiligtum*](https://openrpg.de/srd/5e/de/#spell-heiligtum)[*Lenkendes Geschoss*](https://openrpg.de/srd/5e/de/#spell-lenkendes-geschoss)[*Wunden heilen*](https://openrpg.de/srd/5e/de/#spell-wunden-heilen)<br>
2&period; Grad (3 Plätze): [*Schwache Genesung*](https://openrpg.de/srd/5e/de/#spell-schwache-genesung)[*Waffe des Glaubens*](https://openrpg.de/srd/5e/de/#spell-waffe-des-glaubens)<br>
3&period; Grad (2 Plätze): [*Magie bannen*](https://openrpg.de/srd/5e/de/#spell-magie-bannen)[*Wächtergeister*](https://openrpg.de/srd/5e/de/#spell-waechtergeister)
:
### Aktionen
***Streitkolben.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W6) Wuchtschaden.
:
}}


{{monster,frame
## Pseudodrache
*Winziger Drache, neutral gut*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 7 (2W4+2)
**Speed**          :: 4,5 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|15 (+2)|13 (+1)|10 (+0)|12 (+1)|10 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +3
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: Versteht Drakonisch und Gemeinsprache, aber kann nicht sprechen
**Herausforderung**         :: 1/4 (50 EP)
___
***Beschränkte Telepathie.*** Der Pseudodrache kann magisch einfache Ideen, Emotionen und Bilder telepathisch an jede Kreatur, die eine Sprache versteht, im Abstand von bis zu 30 Metern von ihm übermitteln.
:
***Magieresistenz.*** Der Pseudodrache ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Scharfe Sinne.*** Der Pseudodrache ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht, Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden.
:
***Stachel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W4+2) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf bestehen, oder es ist eine Stunde lang vergiftet. Wenn der Rettungswurf um mindestens 5 Punkte scheitert, wird das Ziel ebenso lange bewusstlos, oder bis es Schaden erleidet oder eine andere Kreatur eine Aktion verwendet, um es wachzurütteln.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Purpurwurm
*Gigantische Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 247 (15W20+90)
**Speed**          :: 15 m, Graben 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|28 (+9)|7  (-2)|22 (+6)|1  (-5)|8  (-1)|4  (-3)|
___
**Rettungswürfe**           :: Kon +11, Wei +4
**Sinne**                   :: Blindsicht 9 m, Erschütterungssinn 18 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 15 (13.000 EP)
___
***Tunnelbauer.*** Der Wurm kann sich mit halber Grabbewegungsrate durch massiven Fels graben. Dabei hinterlässt er einen Tunnel mit drei Metern Durchmesser.
:
### Aktionen
***Mehrfachangriff.*** Der Wurm führt zwei Angriffe aus: einen Biss- und einen Stachelangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 22 (3W8+9) Stichschaden. Wenn das Ziel eine höchstens große Kreatur ist, muss es einen SG-19-Geschicklichkeitsrettungswurf bestehen, oder es wird vom Wurm verschluckt. Verschluckte Kreaturen sind blind und festgesetzt, haben vollständige Deckung gegen Angriffe und andere Effekte von außerhalb des Wurms und erleiden zu Beginn jedes Zugs des Wurms 21 (6W6) Säureschaden. Erleidet der Wurm durch eine verschluckte Kreatur in einem einzigen Zug mindestens 30 Schaden, so muss er am Ende des Zugs einen SG-21-Konstitutionsrettungswurf bestehen, oder er würgt alle verschluckten Kreaturen wieder hoch. Diese befinden sich dann im Bereich von drei Metern um den Wurm und sind liegend. Stirbt der Wurm, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie sechs Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
***Schwanzstachel.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 19 (3W6+9) Stichschaden, und das Ziel muss einen SG-19-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 42 (12W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Quasit
*Winziger Unhold (Dämon, Gestaltwandler), chaotisch böse*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 7 (3W4)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|5  (-3)|17 (+3)|10 (+0)|7  (-2)|10 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +5
**Schadensresistenzen**     :: Blitz, Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: Abyssisch, Gemeinsprache
**Herausforderung**         :: 1 (200 EP)
___
***Gestaltwandler.*** Der Quasit kann seine Aktion verwenden, um sich in eine Tiergestalt wie eine Fledermaus (Bewegungsrate 3 m, Fliegen 12 m), eine Kröte (12 m, Schwimmen 12 m), einen Tausendfüßler (12 m, Klettern 12 m) oder zurück in seine wahre Gestalt zu verwandeln. Seine Spielwerte sind in allen Gestalten bis auf die angegebenen Bewegungsraten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Magieresistenz.*** Der Quasit ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
### Aktionen
***Klauen (Biss in Tiergestalt).*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W4+3) Stichschaden, und das Ziel muss einen SG-10-Konstitutionsrettungswurf bestehen, oder es erleidet 5 (2W4) Giftschaden und ist eine Minute lang vergiftet. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
***Unsichtbarkeit.*** Der Quasit wird magisch unsichtbar, bis er angreift, Verängstigen einsetzt, oder bis seine Konzentration endet (wie bei einem Zauber). Ausrüstung, die der Quasit trägt oder hält, ist ebenfalls unsichtbar.
:
***Verängstigen (1-mal täglich).*** Eine Kreatur nach Wahl des Quasiten im Abstand von bis zu sechs Metern von ihm muss einen SG-10-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen. Wenn der Quasit in Sichtlinie ist, so ist der Rettungswurf im Nachteil. Bei einem Erfolg endet der Effekt.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Quipper
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 0 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|16 (+3)|9  (-1)|1  (-5)|7  (-2)|2  (-4)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Blutrausch.*** Der Quipper ist bei Nahkampf-Angriffswürfen gegen Kreaturen, die nicht alle Trefferpunkte besitzen, im Vorteil.
:
***Wasser atmen.*** Der Quipper kann nur unter Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden.
:
}}


{{monster,frame
## Quippernschwarm
*Mittelgroßer Schwarm winziger Tiere, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 28 (8W8-8)
**Speed**          :: 0 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|16 (+3)|9  (-1)|1  (-5)|7  (-2)|2  (-4)|
___
**Schadensresistenzen**     :: Hieb, Stich, Wucht
**Zustandsimmunitäten**     :: Betäubt, Bezaubert, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Blutrausch.*** Der Schwarm ist bei Nahkampf-Angriffswürfen gegen Kreaturen, die nicht alle Trefferpunkte besitzen, im Vorteil.
:
***Schwarm.*** Der Schwarm kann den Bereich einer anderen Kreatur besetzen und umgekehrt. Er kann sich durch jede Öffnung bewegen, die groß genug für einen winzigen Quipper ist. Der Schwarm kann keine Trefferpunkte zurückerhalten und keine temporären Trefferpunkte erhalten.
:
***Wasser atmen.*** Der Schwarm kann nur unter Wasser atmen.
:
### Aktionen
***Bisse.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 0 m, eine Kreatur im Bereich des Schwarms. *Treffer*: 14 (4W6) Stichschaden oder 7 (2W6) Stichschaden, wenn der Schwarm höchstens die Hälfte seiner Trefferpunkte hat.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Rabe
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 3 m, Fliegen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|14 (+2)|8  (-1)|2  (-4)|12 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Stimmen nachahmen.*** Der Rabe kann einfache Geräusche imitieren, die er gehört hat, beispielsweise das Geflüster einer Person, Säuglingsgeschrei oder Tierlaute. Ein Kreatur, die diese Geräusche hört, erkennt sie als Imitation, sofern sie einen SG-10-Weisheitswurf (Motiv erkennen) besteht.
:
### Aktionen
***Schnabel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden.
:
}}


{{monster,frame
## Rabenschwarm Variante: Insektenschwarm
*Mittelgroßer Schwarm winziger Tiere, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 24 (7W8-7)
**Speed**          :: 3 m, Fliegen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|14 (+2)|8  (-1)|3  (-4)|12 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +5
**Schadensresistenzen**     :: Hieb, Stich, Wucht
**Zustandsimmunitäten**     :: Betäubt, Bezaubert, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Versteinert
**Sinne**                   :: Passive Wahrnehmung 15
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Schwarm.*** Der Schwarm kann den Raum einer anderen Kreatur besetzen und umgekehrt. Er kann sich durch jede Öffnung bewegen, die groß genug für einen winzigen Raben ist. Der Schwarm kann keine Trefferpunkte zurückerhalten und keine temporären Trefferpunkte erhalten.
:
### Aktionen
***Schnäbel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel im Bereich des Schwarms. *Treffer*: 7 (2W6) Stichschaden oder 3 (1W6) Stichschaden, wenn der Schwarm höchstens die Hälfte seiner Trefferpunkte hat.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Rakshasa
*Mittelgroßer Unhold, rechtschaffen böse*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 110 (13W8+52)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|17 (+3)|18 (+4)|13 (+1)|16 (+3)|20 (+5)|
___
**Fertigkeiten**            :: Motiv erkennen +8, Täuschen +10
**Schadensanfälligkeiten**  :: Stich durch magische Waffen, die von Kreaturen mit guter Gesinnung geführt werden
**Schadensimmunitäten**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: Gemeinsprache, Infernalisch
**Herausforderung**         :: 13 (10.000 EP)
___
***Beschränkte Magieimmunität.*** Der Rakshasa kann nicht von Zaubern bis einschließlich des 6. Grades beeinflusst oder erkannt werden, sofern er dies nicht wünscht. Er ist bei Rettungswürfen gegen alle anderen Zauber und magischen Effekte im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Rakshasas ist Charisma (Zauberrettungswurf-SG 18, +10 auf Treffer mit Zauberangriffen). Der Rakshasa kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Einfache Illusion*](https://openrpg.de/srd/5e/de/#spell-einfache-illusion)[*Gedanken wahrnehmen*](https://openrpg.de/srd/5e/de/#spell-gedanken-wahrnehmen)[*Magierhand*](https://openrpg.de/srd/5e/de/#spell-magierhand)[*Selbstverkleidung*](https://openrpg.de/srd/5e/de/#spell-selbstverkleidung)<br>
Je 3-mal täglich: [*Einflüsterung*](https://openrpg.de/srd/5e/de/#spell-einfluesterung)[*Mächtiges Trugbild*](https://openrpg.de/srd/5e/de/#spell-maechtiges-trugbild)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Person bezaubern*](https://openrpg.de/srd/5e/de/#spell-person-bezaubern)[*Unsichtbarkeit*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)<br>
Je 1-mal täglich: [*Ebenenwechsel*](https://openrpg.de/srd/5e/de/#spell-ebenenwechsel)[*Fliegen*](https://openrpg.de/srd/5e/de/#spell-fliegen)[*Person beherrschen*](https://openrpg.de/srd/5e/de/#spell-person-beherrschen)[*Wahrer Blick*](https://openrpg.de/srd/5e/de/#spell-wahrer-blick)
:
### Aktionen
***Mehrfachangriff.*** Der Rakshasa führt zwei Klauenangriffe aus.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (2W6+2) Hiebschaden, und das Ziel ist verflucht, wenn es sich um eine Kreatur handelt. Der magische Fluch wirkt, wann immer das Ziel eine kurze oder lange Rast einlegt, und plagt es mit grauenhaften Bildern und Träumen. Das verfluchte Ziel erhält durch kurze oder lange Rasten keinen Vorzug. Dieser Fluch hält an, bis er durch den Zauber Fluch brechen oder ähnliche Magie entfernt wird.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Ratte
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|11 (+0)|9  (-1)|2  (-4)|10 (+0)|4  (-3)|
___
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfer Geruchssinn.*** Die Ratte ist bei Weisheitswürfen (Wahrnehmung) im Vorteil, die auf Geruchssinn basieren.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +0 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden.
:
}}


{{monster,frame
## Rattenschwarm
*Mittelgroßer Schwarm winziger Tiere, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 24 (7W8-7)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|9  (-1)|11 (+0)|9  (-1)|2  (-4)|10 (+0)|3  (-4)|
___
**Schadensresistenzen**     :: Hieb, Stich, Wucht
**Zustandsimmunitäten**     :: Betäubt, Bezaubert, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Versteinert
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Scharfer Geruchssinn.*** Der Schwarm ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
***Schwarm.*** Der Schwarm kann den Raum einer anderen Kreatur besetzen und umgekehrt. Er kann sich durch jede Öffnung bewegen, die groß genug für eine winzige Ratte ist. Der Schwarm kann keine Trefferpunkte zurückerhalten und keine temporären Trefferpunkte erhalten.
:
### Aktionen
***Bisse.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 0 m, ein Ziel im Bereich des Schwarms. *Treffer*: 7 (2W6) Stichschaden oder 3 (1W6) Stichschaden, wenn der Schwarm höchstens die Hälfte seiner Trefferpunkte hat.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Reh
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 4 (1W8)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|16 (+3)|11 (+0)|2  (-4)|14 (+2)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W4) Stichschaden.
:
}}


{{monster,frame
## Reitpferd
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 13 (2W10+2)
**Speed**          :: 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|10 (+0)|12 (+1)|2  (-4)|11 (+0)|7  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (2W4+3) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Remorhaz
*Riesige Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 195 (17W12+85)
**Speed**          :: 9 m, Graben 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|24 (+7)|13 (+1)|21 (+5)|4  (-3)|10 (+0)|5  (-3)|
___
**Schadensimmunitäten**     :: Feuer, Kälte
**Sinne**                   :: Dunkelsicht 18 m, Erschütterungssinn 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 11 (7.200 EP)
___
***Heißer Körper.*** Eine Kreatur, die den Remorhaz berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, erleidet 10 (3W6) Feuerschaden.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +11 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 40 (6W10+7) Stichschaden plus 10 (3W6) Feuerschaden. Wenn das Ziel eine Kreatur ist, wird es gepackt (Rettungswurf-SG 17). Ein gepacktes Ziel ist festgesetzt. Der Remorhaz kann solange kein weiteres Ziel beißen.
:
***Verschlucken.*** Der Remorhaz führt einen Bissangriff gegen eine höchstens mittelgroße Kreatur aus, die er gepackt hält. Wenn der Angriff trifft, erleidet die Kreatur den Bissschaden und wird außerdem verschluckt, und der Haltegriff endet. Verschluckte Kreaturen sind blind und festgesetzt, haben vollständige Deckung gegen Angriffe und andere Effekte von außerhalb des Remorhaz und erleiden zu Beginn jedes Zugs des Remorhaz 21 (6W6) Säureschaden. Erleidet der Remorhaz durch eine verschluckte Kreatur in einem einzigen Zug mindestens 30 Schaden, so muss er am Ende des Zugs einen SG-15-Konstitutionsrettungswurf bestehen, oder er würgt alle verschluckten Kreaturen wieder hoch. Diese befinden sich dann im Bereich von drei Metern um den Remorhaz und sind liegend. Stirbt der Remorhaz, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie 4,5 Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
}}


{{monster,frame
## Riesenadler
*Großes Tier, neutral gut*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 26 (4W10+4)
**Speed**          :: 3 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|17 (+3)|13 (+1)|8  (-1)|14 (+2)|10 (+0)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: Riesenadlerisch, versteht Aural und Gemeinsprache, aber kann sie nicht sprechen
**Herausforderung**         :: 1 (200 EP)
___
***Scharfe Sicht.*** Der Adler ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Adler führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Krallen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenaffe
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 157 (15W12+60)
**Speed**          :: 12 m, Klettern 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|14 (+2)|18 (+4)|7  (-2)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Athletik +9, Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 7 (2.900 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Menschenaffe führt zwei Faustangriffe aus.
:
***Faust.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 22 (3W10+6) Wuchtschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +9 auf Treffer, Reichweite 15/30 m, ein Ziel. *Treffer*: 30 (7W6+6) Wuchtschaden.
:
}}


{{monster,frame
## Riesendachs
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 13 (2W8+4)
**Speed**          :: 9 m, Graben 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|10 (+0)|15 (+2)|2  (-4)|12 (+1)|5  (-3)|
___
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Scharfer Geruchssinn.*** Der Dachs ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Dachs führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (2W4+1) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Rieseneber
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 42 (5W10+15)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|10 (+0)|16 (+3)|2  (-4)|7  (-2)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Sturmangriff.*** Wenn der Eber sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Hauerangriff trifft, erleidet das Ziel zusätzlich 7 (2W6) Hiebschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Unermüdlich (wird nach kurzer oder langer Rast aufgeladen).*** Wenn der Eber maximal 10 Schaden erleidet und seine Trefferpunkte dadurch auf 0 sinken würden, hat er stattdessen noch 1 Trefferpunkt.
:
### Aktionen
***Hauer.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden.
:
}}


{{monster,frame
## Rieseneidechse
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|12 (+1)|13 (+1)|2  (-4)|10 (+0)|5  (-3)|
___
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenelch
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 42 (5W12+10)
**Speed**          :: 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|16 (+3)|14 (+2)|7  (-2)|14 (+2)|10 (+0)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: Riesenelchisch, versteht Elfisch, Gemeinsprache und Sylvanisch, aber kann sie nicht sprechen
**Herausforderung**         :: 2 (450 EP)
___
***Sturmangriff.*** Wenn der Elch sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Rammbock-Angriff trifft, erleidet das Ziel zusätzlich 7 (2W6) Schaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-14-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine liegende Kreatur. *Treffer*: 22 (4W8+4) Wuchtschaden.
:
***Rammbock.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.
:
}}


{{monster,frame
## Rieseneule
*Großes Tier, neutral*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 1,5 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|15 (+2)|12 (+1)|8  (-1)|13 (+1)|10 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +5
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 15
**Sprachen**                :: Rieseneulisch, versteht Elfisch, Gemeinsprache und Sylvanisch, aber kann sie nicht sprechen
**Herausforderung**         :: 1/4 (50 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Die Eule ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Sicht basieren, im Vorteil.
:
***Vorbeifliegen.*** Die Eule provoziert keine Gelegenheitsangriffe, wenn sie die Reichweite eines Feindes verlässt.
:
### Aktionen
***Krallen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (2W6+1) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenfeuerkäfer
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 4 (1W6+1)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|10 (+0)|12 (+1)|1  (-5)|7  (-2)|3  (-4)|
___
**Sinne**                   :: Blindsicht 9 m, Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Beleuchtung.*** Der Käfer spendet im Radius von drei Metern helles Licht und im Radius von weiteren drei Metern dämmriges Licht.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +1 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 2 (1W6-1) Hiebschaden.
:
}}


{{monster,frame
## Riesenfledermaus
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 22 (4W10)
**Speed**          :: 3 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|16 (+3)|11 (+0)|2  (-4)|12 (+1)|6  (-2)|
___
**Sinne**                   :: Blindsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Echolot.*** Die Fledermaus kann ihre Blindsicht nicht verwenden, solange sie taub ist.
:
***Scharfes Gehör.*** Die Fledermaus ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 5 (1W6+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenfrosch
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 18 (4W8)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|13 (+1)|11 (+0)|2  (-4)|10 (+0)|3  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +2
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Amphibisch.*** Der Frosch kann Luft und Wasser atmen.
:
***Stehender Sprung.*** Der Frosch kann mit oder ohne Anlauf bis zu sechs Meter weit und bis zu drei Meter hoch springen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden, und das Ziel wird gepackt (Rettungswurf-SG 11). Ein gepacktes Ziel ist festgesetzt. Der Frosch kann solange kein weiteres Ziel beißen.
:
***Verschlucken.*** Der Frosch führt einen Bissangriff gegen ein höchstens kleines Ziel aus, das er gepackt hält. Wenn der Angriff trifft, wird das Ziel verschluckt, und der Haltegriff endet. Ein verschlucktes Ziel ist blind und festgesetzt, hat vollständige Deckung gegen Angriffe und andere Effekte von außerhalb des Froschs und erleidet zu Beginn jedes Zugs des Froschs 5 (2W4) Säureschaden. Der Frosch kann jeweils nur ein Ziel verschlucken. Stirbt der Frosch, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie 1,5 Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
}}


{{monster,frame
## Riesengeier
*Großes Tier, neutral böse*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 22 (3W10+6)
**Speed**          :: 3 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|10 (+0)|15 (+2)|6  (-2)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Versteht Gemeinsprache, aber kann nicht sprechen
**Herausforderung**         :: 1 (200 EP)
___
***Rudeltaktik.*** Der Geier ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfe Sicht und scharfer Geruchssinn.*** Der Geier ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Geier führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Krallen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (2W6+2) Hiebschaden.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesengiftschlange
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|18 (+4)|13 (+1)|2  (-4)|10 (+0)|3  (-4)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 6 (1W4+4) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 10 (3W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Riesenhai
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 126 (11W12+55)
**Speed**          :: 0 m, Schwimmen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|11 (+0)|21 (+5)|1  (-5)|10 (+0)|5  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Blindsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Blutrausch.*** Der Hai ist bei Nahkampf-Angriffswürfen gegen Kreaturen, die nicht alle Trefferpunkte besitzen, im Vorteil.
:
***Wasser atmen.*** Der Hai kann nur unter Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 22 (3W10+6) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenhyäne
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 45 (6W10+12)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|14 (+2)|14 (+2)|2  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Wüten.*** Wenn die Hyäne in ihrem Zug die Trefferpunkte einer Kreatur mit einem Nahkampfangriff auf 0 verringert hat, kann sie eine Bonusaktion ausführen, um bis zur Hälfte ihrer Bewegungsrate zurückzulegen und einen Bissangriff auszuführen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Stichschaden.
:
}}


{{monster,frame
## Riesenkrabbe
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 13 (3W8)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|15 (+2)|11 (+0)|1  (-5)|9  (-1)|3  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +4
**Sinne**                   :: Blindsicht 9 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Amphibisch.*** Die Krabbe kann Luft und Wasser atmen.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 11). Die Krabbe hat zwei Scheren, von denen jede jeweils ein Ziel packen kann.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenkröte
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 39 (6W10+6)
**Speed**          :: 6 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|13 (+1)|13 (+1)|2  (-4)|10 (+0)|3  (-4)|
___
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Amphibisch.*** Die Kröte kann Luft und Wasser atmen.
:
***Stehender Sprung.*** Die Kröte kann mit oder ohne Anlauf bis zu sechs Meter weit und bis zu drei Meter hoch springen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W10+2) Stichschaden plus 5 (1W10) Giftschaden, und das Ziel wird gepackt (Rettungswurf-SG 13). Ein gepacktes Ziel ist festgesetzt. Die Kröte kann solange kein weiteres Ziel beißen.
:
***Verschlucken.*** Die Kröte führt einen Bissangriff gegen ein höchstens mittelgroßes Ziel aus, das sie gepackt hält. Wenn der Angriff trifft, wird das Ziel verschluckt, und der Haltegriff endet. Ein verschlucktes Ziel ist blind und festgesetzt, hat vollständige Deckung gegen Angriffe und andere Effekte von außerhalb der Kröte und erleidet zu Beginn jedes Zugs der Kröte 10 (3W6) Säureschaden. Die Kröte kann jeweils nur ein Ziel verschlucken. Stirbt die Kröte, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie 1,5 Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
}}


{{monster,frame
## Riesenkrokodil
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 85 (9W12+27)
**Speed**          :: 9 m, Schwimmen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|21 (+5)|9  (-1)|17 (+3)|2  (-4)|10 (+0)|7  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +5
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Atem anhalten.*** Das Krokodil kann 30 Minuten lang den Atem anhalten.
:
### Aktionen
***Mehrfachangriff.*** Das Krokodil führt zwei Angriffe aus: einen Biss- und einen Schwanzangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 21 (3W10+5) Stichschaden, und das Ziel wird gepackt (Rettungswurf-SG 16). Ein gepacktes Ziel ist festgesetzt. Das Krokodil kann solange kein weiteres Ziel beißen.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, ein Ziel, das nicht vom Krokodil gepackt wurde. *Treffer*: 14 (2W8+5) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-16-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenoktopus
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 52 (8W10+8)
**Speed**          :: 3 m, Schwimmen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|13 (+1)|13 (+1)|4  (-3)|10 (+0)|4  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Atem anhalten.*** Der Oktopus kann eine Stunde lang den Atem anhalten, wenn er nicht im Wasser ist.
:
***Unterwassertarnung.*** Der Oktopus ist bei Geschicklichkeitswürfen (Heimlichkeit) unter Wasser im Vorteil.
:
***Wasser atmen.*** Der Oktopus kann nur unter Wasser atmen.
:
### Aktionen
***Tentakel.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Wuchtschaden. Wenn das Ziel eine Kreatur ist, wird es gepackt (Rettungswurf-SG 16). Ein gepacktes Ziel ist festgesetzt. Der Oktopus kann seine Tentakel nicht gegen andere Ziele einsetzen.
:
***Tintenwolke (wird nach kurzer oder langer Rast aufgeladen).*** Eine Tintenwolke mit sechs Metern Radius breitet sich um den Oktopus aus, wenn dieser sich unter Wasser befindet. Der Bereich ist eine Minute lang komplett verschleiert, wobei eine starke Strömung die Tinte fortspülen kann. Nach Ausstoßen der Tinte kann der Oktopus die Spurt-Aktion als Bonusaktion ausführen.
:
}}


{{monster,frame
## Riesenratte Variante: Kranke Riesenratte
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 7 (2W6)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|7  (-2)|15 (+2)|11 (+0)|2  (-4)|10 (+0)|4  (-3)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Rudeltaktik.*** Die Ratte ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer ihrer Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfer Geruchssinn.*** Die Ratte ist bei Weisheitswürfen (Wahrnehmung) im Vorteil, die auf Geruchssinn basieren.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenseepferdchen
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 16 (3W10)
**Speed**          :: 0 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|15 (+2)|11 (+0)|2  (-4)|12 (+1)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Sturmangriff.*** Wenn das Seepferdchen sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Rammbock-Angriff trifft, erleidet das Ziel zusätzlich 7 (2W6) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-11-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Wasser atmen.*** Das Seepferdchen kann nur unter Wasser atmen.
:
### Aktionen
***Rammbock.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Wuchtschaden.
:
}}


{{monster,frame
## Riesenskorpion
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 52 (7W10+14)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|13 (+1)|15 (+2)|1  (-5)|9  (-1)|3  (-4)|
___
**Sinne**                   :: Blindsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 3 (700 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Skorpion führt drei Angriffe aus: zwei Klauen- und einen Stachelangriff.
:
***Klauen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 12). Der Skorpion hat zwei Klauen, die jeweils ein Ziel packen können.
:
***Stachel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 7 (1W10+2) Stichschaden, und das Ziel muss einen SG-12-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 22 (4W10) Giftschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenspinne
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 26 (4W10+4)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|16 (+3)|12 (+1)|2  (-4)|11 (+0)|4  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +7
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Netzsinn.*** Solange die Spinne in Kontakt mit einem Spinnennetz ist, weiß sie genau, wo sich andere Kreaturen aufhalten, die in Kontakt mit demselben Netz sind.
:
***Netzwandler.*** Die Spinne ignoriert Bewegungseinschränkungen, die durch Netze verursacht werden.
:
***Spinnenklettern.*** Die Spinne kann an schwierigen Oberflächen klettern, auch kopfüber an der Decke, ohne Attributswürfe ausführen zu müssen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 7 (1W8+3) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 9 (2W8) Giftschaden, anderenfalls die Hälfte. Wenn die Trefferpunkte des Ziels durch den Giftschaden auf 0 sinken, ist das Ziel stabil, aber eine Stunde lang vergiftet sowie gelähmt, selbst wenn es Trefferpunkte zurückgewinnt.
:
***Netz (Aufladung 5–6).*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 9/18 m, eine Kreatur. *Treffer*: Das Ziel ist durch das Netz festgesetzt. Als Aktion kann das festgesetzte Ziel einen SG-12-Stärkewurf ausführen. Bei einem Erfolg befreit es sich aus den Netzen. Das Netz kann auch angegriffen und zerstört werden (RK 10, 5 Trefferpunkte, anfällig für Feuerschaden, immun gegen Gift-, psychischen und Wuchtschaden).
:
}}


{{monster,frame
## Riesentausendfüßler
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 4 (1W6+1)
**Speed**          :: 9 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|5  (-3)|14 (+2)|12 (+1)|1  (-5)|7  (-2)|3  (-4)|
___
**Sinne**                   :: Blindsicht 9 m, Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W4+2) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf bestehen, oder es erleidet 10 (3W6) Giftschaden. Wenn die Trefferpunkte des Ziels durch den Giftschaden auf 0 sinken, ist das Ziel stabil, aber eine Stunde lang vergiftet sowie gelähmt, selbst wenn es Trefferpunkte zurückgewinnt.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenwespe
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 13 (3W8)
**Speed**          :: 3 m, Fliegen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|14 (+2)|10 (+0)|1  (-5)|10 (+0)|3  (-4)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
### Aktionen
***Stachel.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 5 (1W6+2) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 10 (3W6) Giftschaden, anderenfalls die Hälfte. Wenn die Trefferpunkte des Ziels durch den Giftschaden auf 0 sinken, ist das Ziel stabil, aber eine Stunde lang vergiftet sowie gelähmt, selbst wenn es Trefferpunkte zurückgewinnt.
:
}}


{{monster,frame
## Riesenwiesel
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 9 (2W8)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|16 (+3)|10 (+0)|4  (-3)|12 (+1)|5  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +3
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1/8 (25 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Das Wiesel ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W4+3) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenwolfsspinne
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 12 m, Klettern 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|16 (+3)|13 (+1)|3  (-4)|12 (+1)|4  (-3)|
___
**Fertigkeiten**            :: Heimlichkeit +7, Wahrnehmung +3
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Netzsinn.*** Solange die Spinne in Kontakt mit einem Spinnennetz ist, weiß sie genau, wo sich andere Kreaturen aufhalten, die in Kontakt mit demselben Netz sind.
:
***Netzwandler.*** Die Spinne ignoriert Bewegungseinschränkungen, die durch Netze verursacht werden.
:
***Spinnenklettern.*** Die Spinne kann an schwierigen Oberflächen klettern, auch kopfüber an der Decke, ohne Attributswürfe ausführen zu müssen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W6+1) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 7 (2W6) Giftschaden, anderenfalls die Hälfte. Wenn die Trefferpunkte des Ziels durch den Giftschaden auf 0 sinken, ist das Ziel stabil, aber eine Stunde lang vergiftet sowie gelähmt, selbst wenn es Trefferpunkte zurückgewinnt.
:
}}


{{monster,frame
## Riesenwürgeschlange
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 60 (8W12+8)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|14 (+2)|12 (+1)|1  (-5)|10 (+0)|3  (-4)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 11 (2W6+4) Stichschaden.
:
***Umschlingen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 13 (2W8+4) Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 16). Eine gepackte Kreatur ist festgesetzt. Die Schlange kann solange kein weiteres Ziel umschlingen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Riesenziege
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|11 (+0)|12 (+1)|3  (-4)|12 (+1)|6  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Sturmangriff.*** Wenn die Ziege sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Rammbock-Angriff trifft, erleidet das Ziel zusätzlich 5 (2W4) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Sicherer Tritt.*** Die Ziege ist bei Stärke- und Geschicklichkeitsrettungswürfen gegen Effekte, die sie umstoßen würden, im Vorteil.
:
### Aktionen
***Rammbock.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (2W4+3) Wuchtschaden.
:
}}


{{monster,frame
## Riffhai
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 22 (4W8+4)
**Speed**          :: 0 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|13 (+1)|13 (+1)|1  (-5)|10 (+0)|4  (-3)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Blindsicht 9 m, Passive Wahrnehmung 12
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Rudeltaktik.*** Der Hai ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Wasser atmen.*** Der Hai kann nur unter Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Ritter
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 18 (Ritterrüstung)
**Trefferpunkte**  :: 52 (8W8+16)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|11 (+0)|14 (+2)|11 (+0)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Kon +4, Wei +2
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 3 (700 EP)
___
***Tapferkeit.*** Der Ritter ist bei Rettungswürfen gegen den Zustand Verängstigt im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Ritter führt zwei Nahkampfangriffe aus.
:
***Zweihandschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden.
:
***Schwere Armbrust.*** *Fernkampfwaffenangriff*: +2 auf Treffer, Reichweite 30/120 m, ein Ziel. *Treffer*: 5 (1W10) Stichschaden.
:
***Führungsqualitäten (wird nach kurzer oder langer Rast aufgeladen).*** Der Ritter kann eine Minute lang einen speziellen Befehl oder eine Warnung aussprechen, wann immer eine Kreatur im Abstand von bis zu neun Metern von ihm, die er sehen kann und die ihm nicht feindlich gesinnt ist, einen Angriffs- oder Rettungswurf ausführt. Diese Kreatur kann ihrem Wurf dann einen W4 hinzufügen, sofern sie den Ritter hören und verstehen kann. Eine Kreatur kann nur von jeweils einem Führungsqualitäten-Würfel profitieren. Dieser Effekt endet, wenn der Ritter kampfunfähig ist.
:
### Reaktionen
***Parieren.*** Der Ritter erhöht seine RK gegen einen Nahkampfangriff, der ihn treffen würde, um 2. Dazu muss der Ritter den Angreifer sehen können und eine Nahkampfwaffe führen.
:
}}


{{monster,frame
## Roch
*Gigantische Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 248 (16W20+80)
**Speed**          :: 6 m, Fliegen 36 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|28 (+9)|10 (+0)|20 (+5)|3  (-4)|10 (+0)|9  (-1)|
___
**Rettungswürfe**           :: Ges +4, Kon +9, Wei +4, Cha +3
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 11 (7.200 EP)
___
***Scharfe Sicht.*** Der Roch ist bei Weisheitswürfen (Wahrnehmung), die auf Sicht basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Roch führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Krallen.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 23 (4W6+9) Hiebschaden, und das Ziel wird gepackt (Rettungswurf-SG 19). Ein gepacktes Ziel ist festgesetzt. Der Roch kann seine Krallen nicht gegen andere Ziele einsetzen.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +13 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 27 (4W8+9) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Rostmonster
*Mittelgroße Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 27 (5W8+5)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|12 (+1)|13 (+1)|2  (-4)|13 (+1)|6  (-2)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Eisenwitterung.*** Das Rostmonster kann eisenartiges Metall im Abstand von bis zu neun Metern von ihm wittern und lokalisieren.
:
***Metall verrosten.*** Jede nichtmagische Waffe aus Metall, die das Rostmonster trifft, korrodiert. Wenn die Waffe Schaden bewirkt hat, erhält sie einen permanenten und kumulativen Malus von -1 auf Schadenswürfe. Sinkt ihr Malus auf -5, wird die Waffe zerstört. Nichtmagische Munition aus Metall, die das Rostmonster trifft, wird zerstört, nachdem sie Schaden bewirkt hat.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W8+1) Stichschaden.
:
***Antennen.*** Das Rostmonster korrodiert ein Objekt aus eisenartigem Metall im Abstand von bis zu 1,5 Metern von ihm, das es sehen kann. Wenn das Objekt nicht getragen oder gehalten wird, zerstört die Berührung einen würfelförmigen Teil von 30 Zentimetern Kantenlänge davon. Wenn das Objekt von einer Kreatur getragen oder gehalten wird, kann diese einen SG-11-Geschicklichkeitsrettungswurf ausführen, um die Berührung des Rostmonsters zu vermeiden. Wenn das getragene oder gehaltene berührte Objekt entweder eine Metallrüstung oder ein Metallschild ist, erleidet es einen permanenten und kumulativen Malus von -1 auf die RK, die es gewährt. Rüstungen, deren RK auf 10 sinkt, und Schilde, deren Bonus auf +0 sinkt, werden zerstört. Wenn das berührte Objekt eine Metallwaffe ist, die gehalten wird, verrostet sie wie beim Merkmal Metall verrosten beschrieben.
:
}}


{{monster,frame
## Roter Drachennestling
*Mittelgroßer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 75 (10W8+30)
**Speed**          :: 9 m, Klettern 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|17 (+3)|12 (+1)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Ges +2, Kon +5, Wei +2, Cha +4
**Fertigkeiten**            :: Heimlichkeit +2, Wahrnehmung +4
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 4 (1.100 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (1W10+4) Stichschaden plus 3 (1W6) Feuerschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Drache atmet Feuer in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-13-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 24 (7W6) Feuerschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Säbelzahntiger
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 52 (7W10+14)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|14 (+2)|15 (+2)|3  (-4)|12 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Anspringen.*** Wenn der Tiger sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Klauenangriff trifft, muss das Ziel einen SG-14-Stärkerettungswurf bestehen, oder es wird umgestoßen. Wenn das Ziel liegt, kann der Tiger als Bonusaktion einen Bissangriff gegen es ausführen.
:
***Scharfer Geruchssinn.*** Die Tiger ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (1W10+5) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 12 (2W6+5) Hiebschaden.
:
}}


{{monster,frame
## Sahuagin
*Mittelgroßer Humanoide (Sahuagin), rechtschaffen böse*
___
**Rüstungsklasse** :: 12 (natürliche Rüstung)
**Trefferpunkte**  :: 22 (4W8+4)
**Speed**          :: 9 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|11 (+0)|12 (+1)|12 (+1)|13 (+1)|9  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +5
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 15
**Sprachen**                :: Sahuaginisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Blutrausch.*** Der Sahuagin ist bei Nahkampf-Angriffswürfen gegen Kreaturen, die nicht alle Trefferpunkte besitzen, im Vorteil.
:
***Eingeschränkte Amphibie.*** Der Sahuagin kann Luft und Wasser atmen, doch er muss sich mindestens einmal alle vier Stunden ganz im Wasser befinden, um nicht zu ersticken.
:
***Haifischtelepathie.*** Der Sahuagin kann magisch mithilfe beschränkter Telepathie alle Haie im Abstand von bis zu 36 Metern von ihm befehligen.
:
### Aktionen
***Mehrfachangriff.*** Der Sahuagin führt zwei Nahkampfangriffe aus: einen Biss- und einen Klauen- oder Speerangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W4+1) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W4+1) Hiebschaden.
:
***Speer.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden oder 5 (1W8+1) Stichschaden, wenn beidhändig geführt, um einen Nahkampfangriff auszuführen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Salamander
*Großer Elementar, neutral böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 90 (12W10+24)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|14 (+2)|15 (+2)|11 (+0)|10 (+0)|12 (+1)|
___
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensanfälligkeiten**  :: Kälte
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Ignal
**Herausforderung**         :: 5 (1.800 EP)
___
***Heiße Waffen.*** Jede Nahkampfwaffe aus Metall, die der Salamander führt, bewirkt bei einem Treffer zusätzlich 3 (1W6) Feuerschaden (im Angriff enthalten).
:
***Heißer Körper.*** Eine Kreatur, die den Salamander berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, erleidet 7 (2W6) Feuerschaden.
:
### Aktionen
***Mehrfachangriff.*** Der Salamander führt zwei Angriffe aus: einen Speer- und einen Schwanzangriff.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden plus 7 (2W6) Feuerschaden, und das Ziel wird gepackt (Rettungswurf-SG 14). Solange es gepackt bleibt, ist es festgesetzt. Der Salamander kann es automatisch mit dem Schwanz treffen, jedoch keine Schwanzangriffe gegen andere Ziele ausführen.
:
***Speer.*** *Fernkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 11 (2W6+4) Stichschaden oder 13 (2W8+4) Stichschaden bei zweihändiger Führung und Nahkampfangriff, plus 3 (1W6) Feuerschaden.
:
}}


{{monster,frame
## Satyr
*Mittelgroßes Feenwesen, chaotisch neutral*
___
**Rüstungsklasse** :: 14 (Lederrüstung)
**Trefferpunkte**  :: 31 (7W8)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|16 (+3)|11 (+0)|12 (+1)|10 (+0)|14 (+2)|
___
**Fertigkeiten**            :: Auftreten +6, Heimlichkeit +5, Wahrnehmung +2
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Elfisch, Gemeinsprache, Sylvanisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Magieresistenz.*** Der Satyr ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
### Aktionen
***Kurzbogen.*** *Fernkampfwaffenangriff*: +5 auf Treffer, Reichweite 24/96 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
***Rammbock.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (2W4+1) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Schakal
*Kleines Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 3 (1W6)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|15 (+2)|11 (+0)|3  (-4)|12 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Rudeltaktik.*** Der Schakal ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Schakal ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +1 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 (1W4-1) Stichschaden.
:
}}


{{monster,frame
## Schatten
*Mittelgroßer Untoter, chaotisch böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 16 (3W8+3)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|14 (+2)|13 (+1)|6  (-2)|10 (+0)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +4 (+6 in dämmrigem Licht oder Dunkelheit)
**Schadensresistenzen**     :: Blitz, Feuer, Kälte, Säure, Schall; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensanfälligkeiten**  :: Gleißend
**Schadensimmunitäten**     :: Gift, Nekrotisch
**Zustandsimmunitäten**     :: Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Amorph.*** Der Schatten kann sich durch enge Bereiche mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen.
:
***Heimlicher Schatten.*** In dämmrigem Licht oder bei Dunkelheit kann der Schatten die Verstecken-Aktion als Bonusaktion ausführen.
:
***Schwach im Sonnenlicht.*** Im Sonnenlicht ist der Schatten bei Angriffswürfen, Attributswürfen und Rettungswürfen im Nachteil.
:
### Aktionen
***Stärkeentzug.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 9 (2W6+2) nekrotischer Schaden, und der Stärkewert des Ziels ist um 1W4 verringert. Wenn die Stärke dadurch auf 0 sinkt, stirbt das Ziel. Anderenfalls bleibt die Stärke verringert, bis das Ziel eine kurze oder lange Rast beendet. Wenn ein Humanoide, der keine böse Gesinnung hat, durch diesen Angriff stirbt, erhebt sich 1W4 Stunden später ein neuer Schatten aus der Leiche.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Schildwächter
*Großes Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 142 (15W10+60)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|8  (-1)|18 (+4)|7  (-2)|10 (+0)|3  (-4)|
___
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht Befehle in allen Sprachen, aber kann nicht sprechen
**Herausforderung**         :: 7 (2.900 EP)
___
***Gebunden.*** Der Schildwächter ist magisch an ein Amulett gebunden. Solange der Wächter und sein Amulett sich auf derselben Existenzebene befinden, kann der Träger des Amuletts den Wächter telepathisch zu sich rufen, und der Wächter kennt Richtung und Abstand zum Amulett. Wenn der Wächter sich im Abstand von bis zu 18 Metern vom Träger des Amuletts befindet, wird die Hälfte des Schadens, den der Träger erleidet (aufgerundet), an den Wächter übertragen.
:
***Regeneration.*** Der Schildwächter erhält zu Beginn seines Zugs 10 Trefferpunkte zurück, wenn er über mindestens 1 Trefferpunkt verfügt.
:
***Zauberspeicher.*** Ein Zauberwirker, der das Amulett des Schildwächters trägt, kann den Wächter verwenden, um einen Zauber des höchstens 4. Grades zu speichern. Dazu muss der Träger den Zauber auf den Wächter wirken. Der Zauber hat dabei keinen Effekt, sondern wird im Wächter gespeichert. Wenn der Träger es befiehlt oder eine Situation eintritt, die vom Zauberwirker definiert wurde, wirkt der Wächter den gespeicherten Zauber mit allen vom ursprünglichen Zauberwirker festgelegten Parametern, ohne dass dazu Komponenten erforderlich sind. Wurde der Zauber gewirkt oder ein neuer Zauber gespeichert, geht jeder zuvor gespeicherte Zauber verloren.
:
### Aktionen
***Mehrfachangriff.*** Der Wächter führt zwei Faustangriffe aus.
:
***Faust.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.  Reaktionen
:
***Schild.*** Wenn eine Kreatur einen Angriff gegen den Träger des Wächteramuletts ausführt, gewährt der Wächter einen Bonus von +2 auf die RK des Trägers, sofern der Wächter sich im Abstand von bis zu 1,5 Metern vom Träger befindet.
:
}}


{{monster,frame
## Schläger
*Mittelgroßer Humanoide (jedes Volk), jede nichtgute Gesinnung*
___
**Rüstungsklasse** :: 11 (Lederrüstung)
**Trefferpunkte**  :: 32 (5W8+10)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|11 (+0)|14 (+2)|10 (+0)|10 (+0)|11 (+0)|
___
**Fertigkeiten**            :: Einschüchtern +2
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 1/2 (100 EP)
___
***Rudeltaktik.*** Der Schläger ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
### Aktionen
***Mehrfachangriff.*** Der Schläger führt zwei Nahkampfangriffe aus.
:
***Streitkolben.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 5 (1W6+2) Wuchtschaden.
:
***Schwere Armbrust.*** *Fernkampfwaffenangriff*: +2 auf Treffer, Reichweite 30/120 m, ein Ziel. *Treffer*: 5 (1W10) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Schreckenswolf
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 37 (5W10+10)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|15 (+2)|15 (+2)|3  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Rudeltaktik.*** Der Wolf ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Wolf ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
}}


{{monster,frame
## Schreckgespenst
*Mittelgroßer Untoter, chaotisch böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 22 (5W8)
**Speed**          :: 0 m, Fliegen 15 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1  (-5)|14 (+2)|11 (+0)|10 (+0)|10 (+0)|11 (+0)|
___
**Schadensresistenzen**     :: Blitz, Feuer, Kälte, Säure, Schall; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift, Nekrotisch
**Zustandsimmunitäten**     :: Bezaubert, Bewusstlos, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht alle zu Lebzeiten bekannten Sprachen, kann aber nicht sprechen
**Herausforderung**         :: 1 (200 EP)
___
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist das Schreckgespenst bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
***Körperlose Bewegung.*** Das Schreckgespenst kann sich durch andere Kreaturen und Gegenstände bewegen, als wären sie schwieriges Gelände. Es erleidet 5 (1W10) Energieschaden, wenn es seinen Zug in einem Gegenstand beendet.
:
### Aktionen
***Lebensentzug.*** *Nahkampf-Zauberangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 10 (3W6) nekrotischer Schaden. Das Ziel muss einen SG-10-Konstitutionsrettungswurf bestehen, oder sein Trefferpunktemaximum wird um den Betrag des erlittenen Schadens verringert. Es bleibt verringert, bis die Kreatur eine lange Rast beendet. Wenn das Trefferpunktemaximum durch diesen Effekt auf 0 sinkt, stirbt das Ziel.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Schreckhahn
*Kleine Monstrosität, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 27 (6W6+6)
**Speed**          :: 6 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|12 (+1)|12 (+1)|2  (-4)|13 (+1)|5  (-3)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 3 (1W4+1) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf gegen magische Versteinerung ausführen. Scheitert der Wurf, so beginnt die Kreatur zu versteinern und ist festgesetzt. Sie muss den Rettungswurf am Ende ihres nächsten Zugs wiederholen. Bei einem Erfolg endet der Effekt. Bei einem Misserfolg ist die Kreatur 24 Stunden lang versteinert.
:
}}


{{monster,frame
## Schwarzbär
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 19 (3W8+6)
**Speed**          :: 12 m, Klettern 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|10 (+0)|14 (+2)|2  (-4)|12 (+1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Scharfer Geruchssinn.*** Der Bär ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Bär führt zwei Angriffe aus: einen Biss- und einen Klauenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Schwarzer Blob
*Großer Schlick, gesinnungslos*
___
**Rüstungsklasse** :: 7
**Trefferpunkte**  :: 85 (10W10+30)
**Speed**          :: 6 m, Klettern 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|5  (-3)|16 (+3)|1  (-5)|6  (-2)|1  (-5)|
___
**Schadensimmunitäten**     :: Blitz, Hieb, Kälte, Säure
**Zustandsimmunitäten**     :: Bezaubert, Blind, Erschöpft, Liegend, Taub, Verängstigt
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 8
**Sprachen**                :: -
**Herausforderung**         :: 4 (1.100 EP)
___
***Amorph.*** Der Blob kann sich durch enge Bereiche mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen.
:
***Korrosive Form.*** Eine Kreatur, die den Blob berührt oder mit einem Nahkampfangriff trifft, während sie sich im Abstand von bis zu 1,5 Metern von ihm befindet, erleidet 4 (1W8) Säureschaden. Jede nichtmagische Waffe aus Metall oder Holz, die den Blob trifft, wird verätzt. Wenn die Waffe Schaden bewirkt hat, erhält sie einen permanenten und kumulativen Malus von -1 auf Schadenswürfe. Sinkt ihr Malus auf -5, wird die Waffe zerstört. Nichtmagische Munition aus Metall oder Holz, die den Blob trifft, wird zerstört, nachdem sie Schaden bewirkt hat. Der Blob kann sich in einer Runde durch fünf Zentimeter starkes nichtmagisches Holz oder Metall fressen.
:
***Spinnenklettern.*** Der Blob kann ohne Attributswürfe schwierige Oberflächen erklimmen und sich kopfüber an Decken entlang bewegen.
:
### Aktionen
***Scheinfuß.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Wuchtschaden plus 18 (4W8) Säureschaden. Außerdem wird nichtmagische Rüstung, die vom Ziel getragen wird, teilweise zersetzt und erleidet einen permanenten und kumulativen Malus von -1 auf die RK, die sie gewährt. Die Rüstung wird zerstört, wenn der Malus ihre RK auf 10 verringert.  Reaktionen
:
***Teilen.*** Wenn ein mindestens mittelgroßer Blob Blitz- oder Hiebschaden erleidet, teilt er sich in zwei neue Blobs, sofern er mindestens 10 Trefferpunkte hat. Jeder neue Blob hat halb so viele Trefferpunkte wie der ursprüngliche Blob (abgerundet). Neue Blobs sind eine Größenkategorie kleiner als der ursprüngliche Blob.
:
}}


{{monster,frame
## Schwarzer Drachennestling
*Mittelgroßer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 33 (6W8+6)
**Speed**          :: 9 m, Fliegen 18 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|14 (+2)|13 (+1)|10 (+0)|11 (+0)|13 (+1)|
___
**Rettungswürfe**           :: Ges +4, Kon +3, Wei +2, Cha +3
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +4
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 2 (450 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W10+2) Stichschaden plus 2 (1W4) Säureschaden.
:
***Säureodem (Aufladung 5–6).*** Der Drache atmet Säure in einer 4,5 Meter langen, 1,5 Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-11-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 22 (5W8) Säureschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Seeoger
*Große Monstrosität, chaotisch böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 45 (6W10+12)
**Speed**          :: 3 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|10 (+0)|15 (+2)|8  (-1)|10 (+0)|9  (-1)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Abyssisch, Aqual
**Herausforderung**         :: 2 (450 EP)
___
***Amphibisch.*** Der Seeoger kann Luft und Wasser atmen.
:
### Aktionen
***Mehrfachangriff.*** Der Seeoger führt zwei Angriffe aus: einen Biss- und einen Klauen- oder Harpunenangriff.
:
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W8+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (2W4+4) Hiebschaden.
:
***Harpune.*** *Fernkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 11 (2W6+4) Stichschaden. Wenn das Ziel eine höchstens riesige Kreatur ist, muss es einen Stärkevergleich mit dem Seeoger bestehen, oder es wird bis zu sechs Meter auf den Seeoger zugezogen.
:
}}


{{monster,frame
## Seepferdchen
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 0 m, Schwimmen 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1  (-5)|12 (+1)|8  (-1)|1  (-5)|10 (+0)|2  (-4)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 0 (0 EP)
___
***Wasser atmen.*** Das Seepferdchen kann nur unter Wasser atmen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Seevettel
*Mittelgroßes Feenwesen, chaotisch böse*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 52 (7W8+21)
**Speed**          :: 9 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|13 (+1)|16 (+3)|12 (+1)|12 (+1)|13 (+1)|
___
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 11
**Sprachen**                :: Aqual, Gemeinsprache, Riesisch
**Herausforderung**         :: 2 (450 EP)
___
***Amphibisch.*** Die Vettel kann Luft und Wasser atmen.
:
***Grässliches Erscheinungsbild.*** Jeder Humanoide, der seinen Zug im Abstand von bis zu neun Metern von der Vettel beginnt und die wahre Gestalt der Vettel sehen kann, muss einen SG-11-Weisheitsrettungswurf ausführen. Scheitert der Wurf, so ist die Kreatur eine Minute lang verängstigt. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen. Wenn die Vettel in Sichtlinie ist, so ist der Rettungswurf im Nachteil. Bei einem Erfolg endet der Effekt. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen das grässliche Erscheinungsbild der Vettel immun. Sofern das Ziel nicht überrascht wird oder die Enthüllung der wahren Gestalt der Vettel plötzlich erfolgt, kann das Ziel den Blick abwenden und den ersten Rettungswurf vermeiden. Wendet eine Kreatur den Blick ab, so ist sie bis zum Beginn ihres nächsten Zugs bei Angriffswürfen gegen die Vettel im Nachteil.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden.
:
***Illusionäres Erscheinungsbild.*** Die Vettel hüllt sich und alles, was sie trägt oder hält, in eine magische Illusion, die sie wie eine hässliche Kreatur von ihrer ungefähren Größe sowie von humanoider Gestalt erscheinen lässt. Der Effekt endet, wenn die Vettel ihn mit einer Bonusaktion beendet oder ihre Trefferpunkte auf 0 sinken. Die Veränderungen durch diese Illusion halten einer genauen körperlichen Untersuchung nicht stand. Beispiel: Die Vettel könnte erscheinen, als hätte sie keine Klauen, doch jemand, der ihre Hand berührt, würde ihre Klauen spüren. Anderenfalls muss eine Kreatur eine Aktion ausführen, um die Illusion visuell zu untersuchen und einen SG-20-Intelligenzwurf (Nachforschungen) auszuführen. Bei einem Erfolg bemerkt die Kreatur die Tarnung der Vettel.
:
***Todesstarren.*** Die Vettel zielt auf eine verängstigte Kreatur im Abstand von bis zu neun Metern von ihr, die sie sehen kann. Wenn das Ziel die Vettel sehen kann, muss es einen SG-11-Weisheitsrettungswurf gegen diese Magie bestehen, oder seine Trefferpunkte sinken auf 0.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Seiler
*Große Monstrosität, neutral böse*
___
**Rüstungsklasse** :: 20 (natürliche Rüstung)
**Trefferpunkte**  :: 93 (11W10+33)
**Speed**          :: 3 m, Klettern 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|8  (-1)|17 (+3)|7  (-2)|16 (+3)|6  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +6
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 16
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Falsches Erscheinungsbild.*** Solange der Seiler sich nicht bewegt, ist er nicht von einer gewöhnlichen Höhlenformation wie einem Stalagmiten zu unterscheiden.
:
***Greifende Tentakel.*** Der Seiler kann bis zu sechs Tentakel zugleich haben. Jedes Tentakel kann angegriffen werden (RK 20, 10 Trefferpunkte, gegen Gift- und psychischen Schaden immun). Das Zerstören eines Tentakels fügt dem Seiler keinen Schaden zu. Dieser kann in seinem nächsten Zug einen Ersatztentakel bilden. Ein Tentakel kann auch zerstört werden, indem eine Kreatur eine Aktion verwendet und einen SG-15-Stärkewurf gegen den Tentakel besteht.
:
***Spinnenklettern.*** Der Seiler kann ohne Attributswürfe schwierige Oberflächen erklimmen und sich kopfüber an Decken entlang bewegen.
:
### Aktionen
***Mehrfachangriff.*** Der Seiler führt vier Angriffe mit seinen Tentakeln aus, setzt Einholen ein und führt einen Bissangriff aus.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 22 (4W8+4) Stichschaden.
:
***Tentakel.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 15 m, eine Kreatur. *Treffer*: Das Ziel wird gepackt (Rettungswurf-SG 15). Solange das Ziel gepackt ist, ist es festgesetzt und bei Stärkewürfen und Stärkerettungswürfen im Nachteil, und der Seiler kann das betreffende Tentakel gegen kein anderes Ziel einsetzen.
:
***Einholen.*** Der Seiler zieht jede gepackte Kreatur bis zu 7,5 Meter weit direkt auf sich zu.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Silberdrachennestling
*Mittelgroßer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 45 (6W8+18)
**Speed**          :: 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|17 (+3)|12 (+1)|11 (+0)|15 (+2)|
___
**Rettungswürfe**           :: Ges +2, Kon +5, Wei +2, Cha +4
**Fertigkeiten**            :: Heimlichkeit +2, Wahrnehmung +4
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (1W10+4) Stichschaden.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Kälteodem.*** Der Drache atmet einen eisigen Stoß in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-13-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 18 (4W8) Kälteschaden, anderenfalls die Hälfte.
:
***Lähmender Odem.*** Der Drache atmet lähmendes Gas in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-13-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang gelähmt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Skelett
*Mittelgroßer Untoter, rechtschaffen böse*
___
**Rüstungsklasse** :: 13 (Rüstungsteile)
**Trefferpunkte**  :: 13 (2W8+4)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|14 (+2)|15 (+2)|6  (-2)|8  (-1)|5  (-3)|
___
**Schadensanfälligkeiten**  :: Wucht
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Erschöpft, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: Versteht alle zu Lebzeiten bekannten Sprachen, kann aber nicht sprechen
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Kurzschwert.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Kurzbogen.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 24/96 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Skorpion
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11 (natürliche Rüstung)
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|11 (+0)|8  (-1)|1  (-5)|8  (-1)|2  (-4)|
___
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
### Aktionen
***Stachel.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 1 Stichschaden, und das Ziel muss einen SG-9-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet das Ziel 4 (1W8) Giftschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Solar
*Großes celestisches Wesen, rechtschaffen gut*
___
**Rüstungsklasse** :: 21 (natürliche Rüstung)
**Trefferpunkte**  :: 243 (18W10+144)
**Speed**          :: 15 m, Fliegen 45 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|26 (+8)|22 (+6)|26 (+8)|25 (+7)|25 (+7)|30 (+1)|
___
**Rettungswürfe**           :: Int +14, Wei +14, Cha +17
**Fertigkeiten**            :: Wahrnehmung +14
**Schadensresistenzen**     :: Gleißend; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift, Nekrotisch
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Verängstigt, Vergiftet
**Sinne**                   :: Wahrer Blick 36 m, Passive Wahrnehmung 24
**Sprachen**                :: Alle, Telepathie auf 36 m
**Herausforderung**         :: 21 (33.000 EP)
___
***Engelswaffen.*** Die Waffenangriffe des Solars sind magisch. Wenn der Solar mit einer Waffe trifft, bewirkt die Waffe zusätzlich 6W8 gleißenden Schaden (im Angriff enthalten).
:
***Göttliches Bewusstsein.*** Der Solar erkennt, wenn er eine Lüge hört.
:
***Magieresistenz.*** Der Solar ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum Zauberwirken des Solars ist Charisma (Zauberrettungswurf-SG 25). Der Solar kann folgende angeborenen Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Gutes und Böses entdecken*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-entdecken)[*Unsichtbarkeit (nur auf sich selbst)*](https://openrpg.de/srd/5e/de/#spell-unsichtbarkeit)<br>
Je 3-mal täglich: [*Auferstehung*](https://openrpg.de/srd/5e/de/#spell-auferstehung)[*Gutes und Böses bannen*](https://openrpg.de/srd/5e/de/#spell-gutes-und-boeses-bannen)[*Klingenbarriere*](https://openrpg.de/srd/5e/de/#spell-klingenbarriere)<br>
Je 1-mal täglich: [*Heiliges Gespräch*](https://openrpg.de/srd/5e/de/#spell-heiliges-gespraech)[*Wetterkontrolle*](https://openrpg.de/srd/5e/de/#spell-wetterkontrolle)
:
### Aktionen
***Mehrfachangriff.*** Der Solar führt zwei Großschwertangriffe aus.
:
***Zweihandschwert.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 22 (4W6+8) Hiebschaden plus 27 (6W8) gleißender Schaden.
:
***Langbogen des Tötens.*** *Fernkampfwaffenangriff*: +13 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 15 (2W8+6) Stichschaden plus 27 (6W8) gleißender Schaden. Wenn das Ziel eine Kreatur ist, die höchstens 100 Trefferpunkte hat, muss es einen SG-15-Konstitutionsrettungswurf bestehen, oder es stirbt.
:
***Fliegendes Schwert.*** Der Solar lässt sein Großschwert an einer freien Stelle im Abstand von bis zu 1,5 Metern von ihm magisch schweben. Wenn der Solar das Schwert sehen kann, so kann er ihm als Bonusaktion mental befehlen, bis zu 15 Meter weit zu fliegen und entweder einen Angriff gegen ein Ziel auszuführen oder in die Hände des Solars zurückzukehren. Wird das schwebende Schwert Ziel eines Effekts, so wird angenommen, dass der Solar es hält. Das schwebende Schwert fällt zu Boden, wenn der Solar stirbt.
:
***Heilende Berührung (4-mal täglich).*** Der Solar berührt eine andere Kreatur. Das Ziel erhält magisch 40 (8W8+4) Trefferpunkte zurück und wird von allen Flüchen, Giften und Krankheiten sowie von Blindheit und Taubheit befreit.
:
### Legendäre Aktionen
Der Solar kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Solar erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Sengende Explosion (kostet 2 Aktionen).*** Der Solar stößt magische göttliche Energie aus. Jede Kreatur seiner Wahl im Radius von drei Metern muss einen SG-23-Geschicklichkeitsrettungswurf ausführen. Scheitert dieser, erleidet sie 14 (4W6) Feuerschaden plus 14 (4W6) gleißenden Schaden, anderenfalls die Hälfte.
:
***Blendender Blick (kostet 3 Aktionen).*** Der Solar zielt auf eine Kreatur im Abstand von bis zu neun Metern, die er sehen kann. Wenn das Ziel ihn sehen kann, muss es einen SG-15-Konstitutionsrettungswurf bestehen, oder es ist blind, bis Magie wie der Zauber Schwache Genesung die Blindheit entfernt.
:
***Teleportieren.*** Der Solar teleportiert sich und sämtliche Ausrüstung, die er trägt oder hält, magisch bis zu 36 Meter weit an eine freie Stelle, die er sehen kann.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Späher
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 13 (Lederrüstung)
**Trefferpunkte**  :: 16 (3W8+3)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|11 (+0)|14 (+2)|12 (+1)|11 (+0)|13 (+1)|11 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Naturkunde +4, Überlebenskunst +5, Wahrnehmung +5
**Sinne**                   :: Passive Wahrnehmung 15
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 1/2 (100 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Der Späher ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Sicht basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Späher führt zwei Nahkampfangriffe oder zwei Fernkampfangriffe aus.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
}}


{{monster,frame
## Spinne
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 6 m, Klettern 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|2  (-4)|14 (+2)|8  (-1)|1  (-5)|10 (+0)|2  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +4
**Sinne**                   :: Dunkelsicht 9 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Netzsinn.*** Solange die Spinne in Kontakt mit einem Spinnennetz ist, weiß sie genau, wo sich andere Kreaturen aufhalten, die in Kontakt mit demselben Netz sind.
:
***Netzwandler.*** Die Spinne ignoriert Bewegungseinschränkungen, die durch Netze verursacht werden.
:
***Spinnenklettern.*** Die Spinne kann an schwierigen Oberflächen klettern, auch kopfüber an der Decke, ohne Attributswürfe ausführen zu müssen.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 1 Stichschaden, und das Ziel muss einen SG-9-Konstitutionsrettungswurf bestehen, oder es erleidet 2 (1W4) Giftschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Spion
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 27 (6W8)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|15 (+2)|10 (+0)|12 (+1)|14 (+2)|16 (+3)|
___
**Fertigkeiten**            :: Fingerfertigkeit +4, Heimlichkeit +4, Motiv erkennen +4, Nachforschungen +5, Täuschen +5, Überzeugen +5, Wahrnehmung +6
**Sinne**                   :: Passive Wahrnehmung 16
**Sprachen**                :: Zwei beliebige Sprachen
**Herausforderung**         :: 1 (200 EP)
___
***Hinterhältiger Angriff (1-mal pro Zug).*** Der Spion bewirkt zusätzlich 7 (2W6) Schaden, wenn er ein Ziel mit einem Waffenangriff trifft und beim Angriffswurf im Vorteil ist, oder wenn das Ziel sich im Abstand von bis zu 1,5 Metern von einem Verbündeten des Spions befindet, der nicht kampfunfähig ist, und der Spion beim Angriffswurf nicht im Nachteil ist.
:
***Raffinierte Aktion.*** Der Spion kann in jedem seiner Züge die Rückzugs-, Spurt- oder die Verstecken-Aktion als Bonusaktion ausführen.
:
### Aktionen
***Mehrfachangriff.*** Der Spion führt zwei Nahkampfangriffe aus.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Handarmbrust.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 9/36 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
}}


{{monster,frame
## Stammeskrieger
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 12 (Fellrüstung)
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|11 (+0)|12 (+1)|8  (-1)|11 (+0)|8  (-1)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: Eine beliebige Sprache
**Herausforderung**         :: 1/8 (25 EP)
___
***Rudeltaktik.*** Der Stammeskrieger ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
### Aktionen
***Speer.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden oder 5 (1W8+1) Stichschaden, wenn beidhändig geführt, um einen Nahkampfangriff auszuführen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Staub-Mephit
*Kleiner Elementar, neutral böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 17 (5W6)
**Speed**          :: 9 m, Fliegen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|5  (-3)|14 (+2)|10 (+0)|9  (-1)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +2
**Schadensanfälligkeiten**  :: Feuer
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Aural, Terral
**Herausforderung**         :: 1/2 (100 EP)
___
***Angeborenes Zauberwirken (1-mal täglich).*** Der Mephit kann den angeborenen Zauber Schlaf wirken, ohne Materialkomponenten zu benötigen. Sein Attribut zum angeborenen Zauberwirken ist Charisma.
:
***Todesexplosion.*** Wenn der Mephit stirbt, vergeht er in einer Staubexplosion. Jede Kreatur im Abstand von bis zu 1,5 Metern von ihm muss einen SG-10-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang blind. Eine blinde Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Aktionen
***Klauen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 4 (1W4+2) Hiebschaden.
:
***Blendender Odem (Aufladung 6).*** Der Mephit atmet blendenden Staub in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-10-Geschicklichkeitsrettungswurf bestehen, oder sie ist eine Minute lang blind. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{monster,frame
## Steingolem
*Großes Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 178 (17W10+85)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|9  (-1)|20 (+5)|3  (-4)|11 (+0)|1  (-5)|
___
**Schadensimmunitäten**     :: Gift, Psychisch; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Adamant
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Gelähmt, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: Versteht die Sprache seines Schöpfers, aber kann nicht sprechen
**Herausforderung**         :: 10 (5.900 EP)
___
***Magieresistenz.*** Der Golem ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Magische Waffen.*** Die Waffenangriffe des Golems sind magisch.
:
***Unveränderliche Form.*** Der Golem ist gegen alle Zauber und Effekte, die seine Gestalt ändern würden, immun.
:
### Aktionen
***Mehrfachangriff.*** Der Golem führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 19 (3W8+6) Wuchtschaden.
:
***Verlangsamen (Aufladung 5–6).*** Der Golem zielt auf mindestens eine Kreatur im Abstand von bis zu drei Metern von ihm, die er sehen kann. Jedes Ziel muss einen SG-17-Weisheitsrettungswurf gegen diese Magie ausführen. Scheitert der Wurf, so kann das Ziel keine Reaktionen einsetzen, seine Bewegungsrate ist halbiert, und es kann in seinem Zug höchstens einen Angriff ausführen. Außerdem kann das Ziel in seinem Zug entweder eine Aktion oder eine Bonusaktion, aber nicht beides ausführen. Diese Effekte halten eine Minute lang an. Das Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Steinriese
*Riesiger Riese, neutral*
___
**Rüstungsklasse** :: 17 (natürliche Rüstung)
**Trefferpunkte**  :: 126 (11W12+55)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|23 (+6)|15 (+2)|20 (+5)|10 (+0)|12 (+1)|9  (-1)|
___
**Rettungswürfe**           :: Ges +5, Kon +8, Wei +4
**Fertigkeiten**            :: Athletik +12, Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Riesisch
**Herausforderung**         :: 7 (2.900 EP)
___
***Steintarnung.*** Der Riese ist bei Geschicklichkeitswürfen (Heimlichkeit), die er ausführt, um sich in steinigem Gelände zu verstecken, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Riese führt zwei Angriffe mit dem Zweihandknüppel aus.
:
***Zweihandknüppel.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 19 (3W8+6) Wuchtschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +9 auf Treffer, Reichweite 18/72 m, ein Ziel. *Treffer*: 28 (4W10+6) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-17-Stärkerettungswurf bestehen, oder es wird umgestoßen.  Reaktionen
:
***Felsen fangen.*** Wenn ein Fels oder ein ähnliches Objekt auf den Riesen geschleudert wird, kann dieser mit einem erfolgreichen SG-10-Geschicklichkeitsrettungswurf das Geschoss fangen, ohne Wuchtschaden zu erleiden.
:
}}


{{monster,frame
## Streitross
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 11
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|12 (+1)|13 (+1)|2  (-4)|12 (+1)|7  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 11
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
***Trampel-Sturmangriff.*** Wenn das Ross sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Hufangriff trifft, muss das Ziel einen SG-14-Stärkerettungswurf bestehen, oder es wird umgestoßen. Gegen ein liegendes Ziel kann das Ross als Bonusaktion einen weiteren Angriff mit seinen Hufen ausführen.
:
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Streitross-Skelett
*Großer Untoter, rechtschaffen böse*
___
**Rüstungsklasse** :: 13 (Rossharnisch-Teile)
**Trefferpunkte**  :: 22 (3W10+6)
**Speed**          :: 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|12 (+1)|15 (+2)|2  (-4)|8  (-1)|5  (-3)|
___
**Schadensanfälligkeiten**  :: Wucht
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Erschöpft, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 9
**Sprachen**                :: -
**Herausforderung**         :: 1/2 (100 EP)
___
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.  Sphinxen
:
}}


{{monster,frame
## Sturmriese
*Riesiger Riese, chaotisch gut*
___
**Rüstungsklasse** :: 16 (Schuppenpanzer)
**Trefferpunkte**  :: 230 (20W12+100)
**Speed**          :: 15 m, Schwimmen 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|29 (+9)|14 (+2)|20 (+5)|16 (+3)|18 (+4)|18 (+4)|
___
**Rettungswürfe**           :: Str +14, Kon +10, Wei +9, Cha +9
**Fertigkeiten**            :: Arkane Kunde +8, Athletik +14, Geschichte +8, Wahrnehmung +9
**Schadensresistenzen**     :: Kälte
**Schadensimmunitäten**     :: Blitz, Schall
**Sinne**                   :: Passive Wahrnehmung 19
**Sprachen**                :: Gemeinsprache, Riesisch
**Herausforderung**         :: 13 (10.000 EP)
___
***Amphibisch.*** Der Riese kann Luft und Wasser atmen.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Riesen ist Charisma (Zauberrettungswurf-SG 17). Der Riese kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Federfall*](https://openrpg.de/srd/5e/de/#spell-federfall)[*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Schweben*](https://openrpg.de/srd/5e/de/#spell-schweben)<br>
Je 3-mal täglich: [*Wasser atmen*](https://openrpg.de/srd/5e/de/#spell-wasser-atmen)[*Wetterkontrolle*](https://openrpg.de/srd/5e/de/#spell-wetterkontrolle)
:
### Aktionen
***Mehrfachangriff.*** Der Riese führt zwei Angriffe mit dem Großschwert aus.
:
***Zweihandschwert.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 30 (6W6+9) Hiebschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +14 auf Treffer, Reichweite 18/72 m, ein Ziel. *Treffer*: 35 (4W12+9) Wuchtschaden.
:
***Blitzschlag (Aufladung 5–6).*** Der Riese schleudert einen magischen Blitz auf einen Punkt im Abstand von bis zu 150 Metern von ihm, den er sehen kann. Jede Kreatur im Abstand von bis zu drei Metern um diesen Punkt muss einen SG-17-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 54 (12W8) Blitzschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Sukkubus/Inkubus
*Mittelgroßer Unhold (Gestaltwandler), neutral böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 66 (12W8+12)
**Speed**          :: 9 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|8  (-1)|17 (+3)|13 (+1)|15 (+2)|12 (+1)|20 (+5)|
___
**Fertigkeiten**            :: Heimlichkeit +7, Motiv erkennen +5, Täuschen +9, Überzeugen +9, Wahrnehmung +5
**Schadensresistenzen**     :: Blitz, Feuer, Gift, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 15
**Sprachen**                :: Abyssisch, Gemeinsprache, Infernalisch, Telepathie auf 18 m
**Herausforderung**         :: 4 (1.100 EP)
___
***Gestaltwandler.*** Der Unhold kann seine Aktion verwenden, um sich in einen kleinen oder mittelgroßen Humanoiden oder zurück in seine wahre Gestalt zu verwandeln. Ohne Flügel verliert der Unhold seine Flugbewegungsrate. Seine Spielwerte sind im Gegensatz zur Größe und Bewegungsrate in beiden Gestalten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Telepathische Bindung.*** Der Unhold ignoriert die Reichweiteneinschränkung seiner Telepathie, wenn er mit einer Kreatur kommuniziert, die er bezaubert hat. Die beiden müssen sich dazu nicht einmal auf derselben Existenzebene befinden.
:
### Aktionen
***Klauen (nur Unhold-Gestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Hiebschaden.
:
***Ätherische Gestalten.*** Der Unhold begibt sich von der materiellen Ebene aus magisch auf die Ätherebene oder umgekehrt.
:
***Bezaubern.*** Ein Humanoide im Abstand von bis zu neun Metern vom Unhold, den er sehen kann, muss einen SG-15-Weisheitsrettungswurf bestehen, oder er ist einen Tag lang magisch bezaubert. Das bezauberte Ziel gehorcht den verbal oder telepathisch übermittelten Befehlen des Unholds. Wenn das Ziel Schaden erleidet oder einen selbstmörderischen Befehl erhält, kann es den Rettungswurf wiederholen und den Effekt bei einem Erfolg beenden. Wenn der Rettungswurf des Ziels gegen den Effekt erfolgreich ist oder der Effekt auf es endet, ist das Ziel 24 Stunden lang gegen die Bezauberung des Unholds immun. Der Unhold kann nur jeweils ein Ziel bezaubern. Bezaubert er ein anderes Ziel, so endet der Effekt auf das vorige Ziel.
:
***Schwächender Kuss.*** Der Unhold küsst eine Kreatur, die bereitwillig ist oder von ihm bezaubert wurde. Das Ziel muss einen SG-15-Konstitutionsrettungswurf gegen diese Magie ausführen. Scheitert der Wurf, erleidet es 32 (5W10+5) psychischen Schaden, anderenfalls die Hälfte. Das Trefferpunktemaximum des Ziels ist um den Betrag des erlittenen Schadens verringert. Es bleibt verringert, bis das Ziel eine lange Rast beendet. Wenn das Trefferpunktemaximum durch diesen Effekt auf 0 sinkt, stirbt das Ziel.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Tarraske
*Gigantische Monstrosität (Titan), gesinnungslos*
___
**Rüstungsklasse** :: 25 (natürliche Rüstung)
**Trefferpunkte**  :: 676 (33W20+330)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|30 (+1)|11 (+0)|30 (+1)|3  (-4)|11 (+0)|11 (+0)|
___
**Rettungswürfe**           :: Int +5, Wei +9, Cha +9
**Schadensimmunitäten**     :: Feuer, Gift; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Zustandsimmunitäten**     :: Bezaubert, Gelähmt, Verängstigt, Vergiftet
**Sinne**                   :: Blindsicht 36 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 30 (155.000 EP)
___
***Belagerungsmonster.*** Die Tarraske fügt Gegenständen und Gebäuden doppelten Schaden zu.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn ihr Rettungswurf scheitert, kann die Tarraske den Wurf in einen Erfolg verwandeln.
:
***Magieresistenz.*** Die Tarraske ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Reflektierender Panzer.*** Wann immer die Tarraske Ziel des Zaubers Magisches Geschoss, eines Linienzaubers oder eines Zaubers wird, für den ein Fernkampfangriffswurf erforderlich ist, würfle mit einem W6. Bei 1 bis 5 ist die Tarraske nicht betroffen. Bei einer 6 ist die Tarraske nicht betroffen, und der Effekt wird auf den Zauberwirker zurückgeworfen, als ginge er von der Tarraske aus, sodass der Zauberwirker zum Ziel wird.
:
### Aktionen
***Mehrfachangriff.*** Die Tarraske kann ihre Furchterregende Präsenz einsetzen. Sie führt dann fünf Angriffe aus: einen Biss-, zwei Klauen-, einen Hörner- und einen Schwanzangriff. Anstelle des Bissangriffs kann sie Verschlucken einsetzen.
:
***Biss.*** *Nahkampfwaffenangriff*: +19 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 36 (4W12+10) Stichschaden. Wenn das Ziel eine Kreatur ist, wird es gepackt (Rettungswurf-SG 20). Ein gepacktes Ziel ist festgesetzt. Die Tarraske kann solange kein weiteres Ziel beißen.
:
***Hörner.*** *Nahkampfwaffenangriff*: +19 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 32 (4W10+10) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +19 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 28 (4W8+10) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +19 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 24 (4W6+10) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-20-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl der Tarraske, die sich im Abstand von bis zu 36 Metern von ihr befindet und sie bemerkt, muss einen SG-17-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen. Wenn die Tarraske in Sichtlinie ist, so ist der Rettungswurf im Nachteil. Bei einem Erfolg endet der Effekt. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz der Tarraske immun.
:
***Verschlucken.*** Die Tarraske führt einen Bissangriff gegen eine höchstens mittelgroße Kreatur aus, die sie gepackt hält. Wenn der Angriff trifft, erleidet das Ziel den Bissschaden und wird außerdem verschluckt, und der Haltegriff endet. Verschluckte Kreaturen sind blind und festgesetzt, haben vollständige Deckung gegen Angriffe und andere Effekte von außerhalb der Tarraske und erleiden zu Beginn jedes Zugs der Tarraske 56 (16W6) Säureschaden. Erleidet die Tarraske durch eine verschluckte Kreatur in einem einzigen Zug mindestens 60 Schaden, so muss sie am Ende des Zugs einen SG-20-Konstitutionsrettungswurf bestehen, oder sie würgt alle verschluckten Kreaturen wieder hoch. Diese befinden sich dann im Bereich von drei Metern um die Tarraske und sind liegend. Stirbt die Tarraske, so ist die verschluckte Kreatur nicht mehr festgesetzt und kann aus dem Kadaver entkommen, indem sie neun Meter ihrer Bewegungsrate verwendet. Anschließend ist sie liegend.
:
### Legendäre Aktionen
Die Tarraske kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Sie kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Die Tarraske erhält verbrauchte legendäre Aktionen am Anfang ihres Zugs zurück.
:
***Mampfen (kostet 2 Aktionen).*** Die Tarraske führt einen Bissangriff aus oder setzt Verschlucken ein.
:
***Angriff.*** Die Tarraske führt einen Klauen- oder einen Schwanzangriff aus.
:
***Bewegung.*** Die Tarraske nutzt bis zur Hälfte ihrer Bewegungsrate.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Teppich des Erstickens
*Großes Konstrukt, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 33 (6W10)
**Speed**          :: 3 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|14 (+2)|10 (+0)|1  (-5)|3  (-4)|1  (-5)|
___
**Schadensimmunitäten**     :: Gift, Psychisch
**Zustandsimmunitäten**     :: Bezaubert, Blind, Gelähmt, Liegend, Taub, Verängstigt, Vergiftet, Versteinert
**Sinne**                   :: Blindsicht 18 m (blind außerhalb des Radius), Passive Wahrnehmung 6
**Sprachen**                :: -
**Herausforderung**         :: 2 (450 EP)
___
***Antimagische Empfindlichkeit.*** Der Teppich ist im Bereich eines antimagischen Felds kampfunfähig. Wird er Ziel des Zaubers Magie bannen, muss der Teppich einen Konstitutionsrettungswurf gegen den Zauberrettungswurf-SG des Zauberwirkers bestehen, oder er ist eine Minute lang bewusstlos.
:
***Falsches Erscheinungsbild.*** Solange der Teppich sich nicht bewegt, ist er nicht von einem gewöhnlichen Teppich zu unterscheiden.
:
***Schadensübertragung.*** Solange der Teppich eine Kreatur gepackt hält, erleidet er nur die Hälfte des ihm zugefügten Schadens, und die gepackte Kreatur erleidet die andere Hälfte.
:
### Aktionen
***Ersticken.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, eine höchstens mittelgroße Kreatur. *Treffer*: Die Kreatur wird gepackt (Rettungswurf-SG 13). Solange das Ziel gepackt bleibt, ist es festgesetzt, blind und in Gefahr zu ersticken, und der Teppich kann kein anderes Ziel ersticken. Außerdem erleidet das Ziel zu Beginn jedes seiner Züge 10 (2W6+3) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Teufelchen
*Winziger Unhold (Teufel, Gestaltwandler), rechtschaffen böse*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 10 (3W4+3)
**Speed**          :: 6 m, Fliegen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|17 (+3)|13 (+1)|11 (+0)|12 (+1)|14 (+2)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Motiv erkennen +3, Täuschen +4, Überzeugen +4
**Schadensresistenzen**     :: Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Feuer, Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Gemeinsprache, Infernalisch
**Herausforderung**         :: 1 (200 EP)
___
***Gestaltwandler.*** Das Teufelchen kann seine Aktion verwenden, um sich in eine Tiergestalt wie einen Raben (6 m, Fliegen 18 m), eine Ratte (Bewegungsrate 6 m), eine Spinne (6 m, Klettern 6 m) oder zurück in seine wahre Gestalt zu verwandeln. Seine Spielwerte sind in allen Gestalten bis auf die angegebenen Bewegungsraten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Magieresistenz.*** Das Teufelchen ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
***Teufelssicht.*** Die Dunkelsicht des Teufelchens wird nicht durch magische Dunkelheit beeinträchtigt.
:
### Aktionen
***Stachel (Biss in Tiergestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W4+3) Stichschaden, und das Ziel muss einen SG-11-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 10 (3W6) Giftschaden, anderenfalls die Hälfte.
:
***Unsichtbarkeit.*** Das Teufelchen wird magisch unsichtbar, bis es angreift oder seine Konzentration endet (wie bei einem Zauber). Ausrüstung, die das Teufelchen trägt oder hält, ist ebenfalls unsichtbar.
:
}}


{{monster,frame
## Tiger
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 37 (5W10+10)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|15 (+2)|14 (+2)|3  (-4)|12 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +3
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Anspringen.*** Wenn der Tiger sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im selben Zug mit einem Klauenangriff trifft, muss das Ziel einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen. Wenn das Ziel liegt, kann der Tiger als Bonusaktion einen Bissangriff gegen es ausführen.
:
***Scharfer Geruchssinn.*** Die Tiger ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W10+3) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Todesalb
*Mittelgroßer Untoter, neutral böse*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 67 (9W8+27)
**Speed**          :: 0 m, Fliegen 18 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|6  (-2)|16 (+3)|16 (+3)|12 (+1)|14 (+2)|15 (+2)|
___
**Schadensresistenzen**     :: Blitz, Feuer, Kälte, Säure, Schall; Hieb, Stich und Wucht durch nichtmagische Angriffe ohne Silber
**Schadensimmunitäten**     :: Gift, Nekrotisch
**Zustandsimmunitäten**     :: Bezaubert, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 5 (1.800 EP)
___
***Empfindlich gegenüber Sonnenlicht.*** Im Sonnenlicht ist der Todesalb bei Angriffswürfen sowie bei Weisheitswürfen (Wahrnehmung), die Sicht erfordern, im Nachteil.
:
***Körperlose Bewegung.*** Der Todesalb kann sich durch andere Kreaturen und Gegenstände bewegen, als wären sie schwieriges Gelände. Er erleidet 5 (1W10) Energieschaden, wenn er seinen Zug in einem Gegenstand beendet.
:
### Aktionen
***Lebensentzug.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 21 (4W8+3) nekrotischer Schaden. Das Ziel muss einen SG-14-Konstitutionsrettungswurf bestehen, oder sein Trefferpunktemaximum wird um den Betrag des erlittenen Schadens verringert. Es bleibt verringert, bis das Ziel eine lange Rast beendet. Wenn das Trefferpunktemaximum durch diesen Effekt auf 0 sinkt, stirbt das Ziel.
:
***Schreckgespenst erschaffen.*** Der Todesalb zielt auf einen Humanoiden im Abstand von bis zu drei Metern von ihm, der höchstens seit einer Minute tot und gewaltsam gestorben ist. Der Geist des Ziels erhebt sich im Bereich seiner Leiche oder an der nächsten freien Stelle als Schreckgespenst. Das Schreckgespenst steht unter der Kontrolle des Todesalbs. Der Todesalb kann höchstens zwölf Schreckgespenster zugleich unter seiner Kontrolle haben.
:
}}


{{monster,frame
## Todeshund
*Mittelgroße Monstrosität, neutral böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 39 (6W8+12)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|14 (+2)|14 (+2)|3  (-4)|13 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +5
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 15
**Sprachen**                :: -
**Herausforderung**         :: 1 (200 EP)
___
***Zweiköpfig.*** Der Hund ist bei Weisheitswürfen (Wahrnehmung) und bei Rettungswürfen gegen die Zustände Betäubt, Bewusstlos, Bezaubert, Blind, Taub und Verängstigt im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Hund führt zwei Bissangriffe aus.
:
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-12-Konstitutionsrettungswurf gegen die Krankheit bestehen, oder es ist vergiftet, bis die Krankheit geheilt wird. Nach jeweils 24 Stunden muss die Kreatur den Rettungswurf wiederholen. Scheitert der Wurf, wird ihr Trefferpunktemaximum um 5 (1W10) verringert. Es bleibt verringert, bis die Krankheit geheilt ist. Wenn das Trefferpunktemaximum durch die Krankheit auf 0 sinkt, stirbt die Kreatur.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Triceratops
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 95 (10W12+30)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|22 (+6)|9  (-1)|17 (+3)|2  (-4)|11 (+0)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 5 (1.800 EP)
___
***Trampel-Sturmangriff.*** Wenn der Triceratops sich mindestens sechs Meter weit direkt auf eine Kreatur zubewegt und sie dann im gleichen Zug mit einem Zerfleischen-Angriff trifft, muss das Ziel einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen. Gegen ein liegendes Ziel kann der Triceratops als Bonusaktion einen Stampfen-Angriff ausführen.
:
### Aktionen
***Stampfen.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, eine liegende Kreatur. *Treffer*: 22 (3W10+6) Wuchtschaden.
:
***Zerfleischen.*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 24 (4W8+6) Stichschaden.
:
}}


{{monster,frame
## Troll
*Großer Riese, chaotisch böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 84 (8W10+40)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|13 (+1)|20 (+5)|7  (-2)|9  (-1)|7  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 12
**Sprachen**                :: Riesisch
**Herausforderung**         :: 5 (1.800 EP)
___
***Regeneration.*** Der Troll erhält zu Beginn seines Zugs 10 Trefferpunkte zurück. Wenn der Troll Feuer- oder Säureschaden erleidet, wirkt dieses Merkmal zu Beginn seines nächsten Zugs nicht. Der Troll stirbt nur, wenn er seinen Zug mit 0 Trefferpunkten beginnt und keine Trefferpunkte regeneriert.
:
***Scharfer Geruchssinn.*** Der Troll ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Troll führt drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W6+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Tyrannosaurus Rex
*Riesiges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 136 (13W12+52)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|25 (+7)|10 (+0)|19 (+4)|2  (-4)|12 (+1)|9  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 8 (3.900 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Tyrannosaurus führt zwei Angriffe aus: einen Biss- und einen Schwanzangriff. Er kann nicht beide Angriffe gegen dasselbe Ziel ausführen.
:
***Biss.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 33 (4W12+7) Stichschaden. Wenn das Ziel eine höchstens mittelgroße Kreatur ist, wird es gepackt (Rettungswurf-SG 17). Ein gepacktes Ziel ist festgesetzt. Der Tyrannosaurus kann solange kein weiteres Ziel beißen.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +10 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 20 (3W8+7) Wuchtschaden.
:
}}


{{monster,frame
## Unsichtbarer Pirscher
*Mittelgroßer Elementar, neutral*
___
**Rüstungsklasse** :: 14
**Trefferpunkte**  :: 104 (16W8+32)
**Speed**          :: 15 m, Fliegen 15 m (Schweben)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|19 (+4)|14 (+2)|10 (+0)|15 (+2)|11 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +10, Wahrnehmung +8
**Schadensresistenzen**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bewusstlos, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 18
**Sprachen**                :: Aural, versteht die Gemeinsprache, aber kann sie nicht sprechen
**Herausforderung**         :: 6 (2.300 EP)
___
***Einwandfreier Fährtenleser.*** Der Beschwörer weist dem Pirscher eine Beute zu. Der Pirscher kennt die Richtung und Entfernung zu seiner Beute, solange die beiden sich auf der gleichen Existenzebene befinden. Der Pirscher weiß auch, wo sich sein Beschwörer befindet.
:
***Unsichtbarkeit.*** Der Pirscher ist unsichtbar.
:
### Aktionen
***Mehrfachangriff.*** Der Pirscher führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Uralter blauer Drache
*Gigantischer Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 22 (natürliche Rüstung)
**Trefferpunkte**  :: 481 (26W20+208)
**Speed**          :: 12 m, Graben 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|29 (+9)|10 (+0)|27 (+8)|18 (+4)|17 (+3)|21 (+5)|
___
**Rettungswürfe**           :: Ges +7, Kon +15, Wei +10, Cha +12
**Fertigkeiten**            :: Heimlichkeit +7, Wahrnehmung +17
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 27
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 23 (50.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +16 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 20 (2W10+9) Stichschaden plus 11 (2W10) Blitzschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +16 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 16 (2W6+9) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +16 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 18 (2W8+9) Wuchtschaden.
:
***Blitzodem (Aufladung 5–6).*** Der Drache atmet einen Blitzstrahl in einer 36 Meter langen, drei Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-23-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 88 (16W10) Blitzschaden, anderenfalls die Hälfte.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-20-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-24-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 16 (2W6+9) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter Bronzedrache
*Gigantischer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 22 (natürliche Rüstung)
**Trefferpunkte**  :: 444 (24W20+192)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|29 (+9)|10 (+0)|27 (+8)|18 (+4)|17 (+3)|21 (+5)|
___
**Rettungswürfe**           :: Ges +7, Kon +15, Wei +10, Cha +12
**Fertigkeiten**            :: Heimlichkeit +10, Motiv erkennen +17, Wahrnehmung +7
**Schadensimmunitäten**     :: Blitz
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 27
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 22 (41.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +16 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 20 (2W10+9) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +16 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 16 (2W6+9) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +16 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 18 (2W8+9) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-20-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Blitzodem.*** Der Drache atmet einen Blitzstrahl in einer 36 Meter langen, drei Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-23-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 88 (16W10) Blitzschaden, anderenfalls die Hälfte.
:
***Odem der Abstoßung.*** Der Drache atmet abstoßende Energie in einem Kegel von neun Metern aus. Jede Kreatur in diesem Bereich muss einen SG-23-Stärkerettungswurf bestehen. Scheitert der Wurf, wird die Kreatur bis zu 18 Meter weit vom Drachen weggestoßen.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-24-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 16 (2W6+9) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter Golddrache
*Gigantischer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 22 (natürliche Rüstung)
**Trefferpunkte**  :: 546 (28W20+252)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|30 (+1)|14 (+2)|29 (+9)|18 (+4)|17 (+3)|28 (+9)|
___
**Rettungswürfe**           :: Ges +9, Kon +16, Wei +10, Cha +16
**Fertigkeiten**            :: Heimlichkeit +10, Motiv erkennen +17, Überzeugen +16, Wahrnehmung +9
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 27
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 24 (62.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 21 (2W10+10) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W6+10) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 19 (2W8+10) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-24-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-24-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 71 (13W10) Feuerschaden, anderenfalls die Hälfte.
:
***Schwächender Odem.*** Der Drache atmet Gas in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-24-Stärkerettungswurf bestehen, oder sie ist eine Minute lang bei stärkebasierten Angriffswürfen, bei Stärkewürfen und Stärkerettungswürfen im Nachteil. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-25-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 17 (2W6+10) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter grüner Drache
*Gigantischer Drache, rechtschaffen böse*
___
**Rüstungsklasse** :: 21 (natürliche Rüstung)
**Trefferpunkte**  :: 385 (22W20+154)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|12 (+1)|25 (+7)|20 (+5)|17 (+3)|19 (+4)|
___
**Rettungswürfe**           :: Ges +8, Kon +14, Wei +10, Cha +11
**Fertigkeiten**            :: Heimlichkeit +8, Motiv erkennen +10, Täuschen +11, Überzeugen +11, Wahrnehmung +17
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 27
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 22 (41.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden plus 10 (3W6) Giftschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 22 (4W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-19-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Giftodem (Aufladung 5–6).*** Der Drache atmet giftiges Gas in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-22-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 77 (22W6) Giftschaden, anderenfalls die Hälfte.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-23-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter Kupferdrache
*Gigantischer Drache, chaotisch gut*
___
**Rüstungsklasse** :: 21 (natürliche Rüstung)
**Trefferpunkte**  :: 350 (20W20+140)
**Speed**          :: 12 m, Klettern 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|12 (+1)|25 (+7)|20 (+5)|17 (+3)|19 (+4)|
___
**Rettungswürfe**           :: Ges +8, Kon +14, Wei +10, Cha +11
**Fertigkeiten**            :: Heimlichkeit +8, Täuschen +11, Wahrnehmung +17
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 27
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 21 (33.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-19-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Säureodem.*** Der Drache atmet Säure in einer 27 Meter langen, drei Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-22-Geschicklichkeits-rettungswurf ausführen. Scheitert der Wurf, erleidet sie 63 (14W8) Säureschaden, anderenfalls die Hälfte.
:
***Bremsender Odem.*** Der Drache atmet Gas in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-22-Konstitutionsrettungswurf bestehen. Scheitert der Wurf, so kann die Kreatur keine Reaktionen einsetzen, ihre Bewegungsrate ist halbiert, und sie kann in ihrem Zug höchstens einen Angriff ausführen. Außerdem kann sie in ihrem Zug entweder eine Aktion oder eine Bonusaktion einsetzen, aber nicht beides. Diese Effekte halten eine Minute lang an. Die Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-23-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter Messingdrache
*Gigantischer Drache, chaotisch gut*
___
**Rüstungsklasse** :: 20 (natürliche Rüstung)
**Trefferpunkte**  :: 297 (17W20+119)
**Speed**          :: 12 m, Graben 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|10 (+0)|25 (+7)|16 (+3)|15 (+2)|19 (+4)|
___
**Rettungswürfe**           :: Ges +6, Kon +13, Wei +8, Cha +10
**Fertigkeiten**            :: Geschichte +9, Heimlichkeit +6, Überzeugen +10, Wahrnehmung +14
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 24
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 20 (25.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-18-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Feuerodem.*** Der Drache atmet Feuer in einer 27 Meter langen, drei Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-21-Geschicklichkeits-rettungswurf ausführen. Scheitert der Wurf, erleidet sie 56 (16W6) Feuerschaden, anderenfalls die Hälfte.
:
***Schlafodem.*** Der Drache atmet Schlafgas in einem Kegel von 27 Metern aus. Jede Kreatur in dem Bereich muss einen SG-21-Konstitutionsrettungswurf bestehen, oder sie ist zehn Minuten lang bewusstlos. Dieser Effekt endet bei einer Kreatur, wenn sie Schaden erleidet oder jemand sie mit einer Aktion aufweckt.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-22-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter roter Drache
*Gigantischer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 22 (natürliche Rüstung)
**Trefferpunkte**  :: 546 (28W20+252)
**Speed**          :: 12 m, Klettern 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|30 (+1)|10 (+0)|29 (+9)|18 (+4)|15 (+2)|23 (+6)|
___
**Rettungswürfe**           :: Ges +7, Kon +16, Wei +9, Cha +13
**Fertigkeiten**            :: Heimlichkeit +7, Wahrnehmung +16
**Schadensimmunitäten**     :: Feuer
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 26
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 24 (62.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 21 (2W10+10) Stichschaden plus 14 (4W6) Feuerschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W6+10) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 19 (2W8+10) Wuchtschaden.
:
***Feuerodem (Aufladung 5–6).*** Der Drache atmet Feuer in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-24-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 91 (26W6) Feuerschaden, anderenfalls die Hälfte.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-21-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-25-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 17 (2W6+10) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter schwarzer Drache
*Gigantischer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 22 (natürliche Rüstung)
**Trefferpunkte**  :: 367 (21W20+147)
**Speed**          :: 12 m, Fliegen 24 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|14 (+2)|25 (+7)|16 (+3)|15 (+2)|19 (+4)|
___
**Rettungswürfe**           :: Ges +9, Kon +14, Wei +9, Cha +11
**Fertigkeiten**            :: Heimlichkeit +9, Wahrnehmung +16
**Schadensimmunitäten**     :: Säure
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 26
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 21 (33.000 EP)
___
***Amphibisch.*** Der Drache kann Luft und Wasser atmen.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden plus 9 (2W8) Säureschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +15 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-19-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Säureodem (Aufladung 5–6).*** Der Drache atmet Säure in einer 27 Meter langen, drei Meter breiten Linie aus. Jede Kreatur in dieser Linie muss einen SG-22-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 67 (15W8) Säureschaden, anderenfalls die Hälfte.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-23-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter Silberdrache
*Gigantischer Drache, rechtschaffen gut*
___
**Rüstungsklasse** :: 22 (natürliche Rüstung)
**Trefferpunkte**  :: 487 (25W20+225)
**Speed**          :: 12 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|30 (+1)|10 (+0)|29 (+9)|18 (+4)|15 (+2)|23 (+6)|
___
**Rettungswürfe**           :: Ges +7, Kon +16, Wei +9, Cha +13
**Fertigkeiten**            :: Arkane Kunde +11, Geschichte +11, Heimlichkeit +7, Wahrnehmung +16
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 26
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 23 (50.000 EP)
___
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 21 (2W10+10) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 17 (2W6+10) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +17 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 19 (2W8+10) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-21-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Gestalt ändern.*** Der Drache nimmt magisch die Gestalt eines Humanoiden oder Tieres an, dessen Herausforderungsgrad nicht höher ist als sein eigener, oder er verwandelt sich wieder in seine wahre Gestalt. Wenn er stirbt, nimmt er seine wahre Gestalt an. Ausrüstung, die er trägt oder hält, wird von der neuen Gestalt absorbiert oder getragen (nach Wahl des Drachen). In der neuen Gestalt behält der Drache Gesinnung, Trefferpunkte, Trefferwürfel, Sprechfähigkeit, Übung, legendäre Resistenzen, Hortaktionen, Intelligenz-, Weisheits- und Charismawerte sowie diese Aktion bei. Abgesehen davon werden Spielwerte und Fähigkeiten durch die der neuen Gestalt ersetzt. Klassenmerkmale oder legendäre Aktionen der neuen Gestalt sind davon ausgenommen.
:
***Odemwaffen (Aufladung 5–6).*** Der Drache setzt eine der folgenden Odemwaffen ein:
:
***Kälteodem.*** Der Drache atmet einen eisigen Stoß in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-24-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 67 (15W8) Kälteschaden, anderenfalls die Hälfte.
:
***Lähmender Odem.*** Der Drache atmet lähmendes Gas in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-24-Konstitutionsrettungswurf bestehen, oder sie ist eine Minute lang gelähmt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-25-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 17 (2W6+10) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Uralter weißer Drache
*Gigantischer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 20 (natürliche Rüstung)
**Trefferpunkte**  :: 333 (18W20+144)
**Speed**          :: 12 m, Fliegen 24 m, Graben 12 m, Schwimmen 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|26 (+8)|10 (+0)|26 (+8)|10 (+0)|13 (+1)|14 (+2)|
___
**Rettungswürfe**           :: Ges +6, Kon +14, Wei +7, Cha +8
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +13
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 18 m, Dunkelsicht 36 m, Passive Wahrnehmung 23
**Sprachen**                :: Drakonisch, Gemeinsprache
**Herausforderung**         :: 20 (25.000 EP)
___
***Eisschritt.*** Der Drache kann sich über Eis bewegen und eisige Oberflächen erklimmen, ohne Attributswürfe ausführen zu müssen. Außerdem kostet ihn schwieriges Gelände, das aus Eis oder Schnee besteht, keine zusätzliche Bewegung.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn der Rettungswurf des Drachen scheitert, kann dieser den Wurf in einen Erfolg verwandeln.
:
### Aktionen
***Mehrfachangriff.*** Der Drache kann seine Furchterregende Präsenz einsetzen. Dann führt er drei Angriffe aus: einen Biss- und zwei Klauenangriffe.
:
***Biss.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 4,5 m, ein Ziel. *Treffer*: 19 (2W10+8) Stichschaden plus 9 (2W8) Kälteschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 15 (2W6+8) Hiebschaden.
:
***Schwanz.*** *Nahkampfwaffenangriff*: +14 auf Treffer, Reichweite 6 m, ein Ziel. *Treffer*: 17 (2W8+8) Wuchtschaden.
:
***Furchterregende Präsenz.*** Jede Kreatur nach Wahl des Drachen, die sich im Abstand von bis zu 36 Metern von ihm befindet und ihn bemerkt, muss einen SG-16-Weisheitsrettungswurf bestehen, oder sie ist eine Minute lang verängstigt. Eine Kreatur kann den Rettungswurf am Ende jedes ihrer Züge wiederholen und den Effekt bei einem Erfolg beenden. Wenn eine Kreatur ihren Rettungswurf besteht oder der Effekt auf sie endet, ist sie 24 Stunden lang gegen die Furchterregende Präsenz des Drachen immun.
:
***Kälteodem (Aufladung 5–6).*** Der Drache atmet einen eisigen Stoß in einem Kegel von 27 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-22-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 72 (16W8) Kälteschaden, anderenfalls die Hälfte.
:
### Legendäre Aktionen
Der Drache kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Drache erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Flügelangriff (kostet 2 Aktionen).*** Der Drache schlägt mit den Flügeln. Jede Kreatur im Abstand von bis zu 4,5 Metern vom Drachen muss einen SG-22-Geschicklichkeitsrettungswurf bestehen, oder sie erleidet 15 (2W6+8) Wuchtschaden und wird umgestoßen. Der Drache kann dann bis zur Hälfte seiner Flugbewegungsrate fliegend zurücklegen.
:
***Entdecken.*** Der Drache führt einen Weisheitswurf (Wahrnehmung) aus.
:
***Schwanzangriff.*** Der Drache führt einen Schwanzangriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Vampirbrut
*Mittelgroßer Untoter, neutral böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 82 (11W8+33)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|16 (+3)|16 (+3)|11 (+0)|10 (+0)|12 (+1)|
___
**Rettungswürfe**           :: Ges +6, Wei +3
**Fertigkeiten**            :: Heimlichkeit +6, Wahrnehmung +3
**Schadensresistenzen**     :: Nekrotisch; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 13
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 5 (1.800 EP)
___
***Regeneration.*** Der Vampir erhält zu Beginn seines Zugs 10 Trefferpunkte zurück, wenn er mindestens 1 Trefferpunkt hat und sich weder im Sonnenlicht noch in fließendem Wasser befindet. Wenn der Vampir gleißenden Schaden oder Schaden durch Weihwasser erleidet, wirkt dieses Merkmal zu Beginn seines nächsten Zugs nicht.
:
***Spinnenklettern.*** Der Vampir kann ohne Attributswürfe schwierige Oberflächen erklimmen und sich kopfüber an Decken entlang bewegen.
:
***Vampirschwächen.*** Der Vampir hat folgende Makel: Holzpflock ins Herz: Der Vampir wird zerstört, wenn ihm eine Stichwaffe aus Holz ins Herz getrieben wird, während er sich kampfunfähig an seinem Ruheplatz befindet. Hyperempfindlich gegenüber Sonnenlicht: Der Vampir erleidet 20 gleißenden Schaden, wenn er seinen Zug im Sonnenlicht beginnt. Im Sonnenlicht ist er bei Angriffs- und Attributswürfen im Nachteil.
:
***Schaden durch fließendes Wasser.*** Der Vampir erleidet 20 Säureschaden, wenn er seinen Zug in fließendem Wasser beendet.
:
***Zutritt verwehren.*** Der Vampir kann keine Wohnstätte ohne die Einladung eines der Bewohner betreten.
:
### Aktionen
***Mehrfachangriff.*** Der Vampir führt zwei Angriffe aus, von denen nur einer ein Bissangriff sein kann.
:
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine bereitwillige oder vom Vampir gepackte oder kampfunfähige oder festgesetzte Kreatur. *Treffer*: 6 (1W6+3) Stichschaden plus 7 (2W6) nekrotischer Schaden. Das Trefferpunktemaximum des Ziels wird um den Betrag des erlittenen nekrotischen Schadens verringert, und der Vampir erhält Trefferpunkte in Höhe dieses Betrags. Das Trefferpunktemaximum bleibt verringert, bis das Ziel eine lange Rast beendet. Wenn das Trefferpunktemaximum durch diesen Effekt auf 0 sinkt, stirbt das Ziel.
:
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 8 (2W4+3) Hiebschaden. Anstatt Schaden zu bewirken, kann der Vampir das Ziel packen (Rettungswurf-SG 13).
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame,wide
## Vampir
*Mittelgroßer Untoter (Gestaltwandler), rechtschaffen böse*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 144 (17W8+68)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|18 (+4)|18 (+4)|17 (+3)|15 (+2)|18 (+4)|
___
**Rettungswürfe**           :: Ges +9, Wei +7, Cha +9
**Fertigkeiten**            :: Heimlichkeit +9, Wahrnehmung +7
**Schadensresistenzen**     :: Nekrotisch; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 17
**Sprachen**                :: Die zu Lebzeiten bekannten Sprachen
**Herausforderung**         :: 13 (10.000 EP)
___
***Gestaltwandler.*** Wenn der Vampir sich nicht im Sonnenlicht oder in fließendem Wasser befindet, kann er seine Aktion verwenden, um sich in eine winzige Fledermaus, eine mittelgroße Nebelwolke oder zurück in seine wahre Gestalt zu verwandeln. In Fledermausgestalt kann der Vampir nicht sprechen, seine Schrittbewegungsrate beträgt 1,5 Meter, und er hat eine Flugbewegungsrate von neun Metern. Seine Spielwerte sind abgesehen von Größe und Bewegungsrate unverändert. Alles, was er trägt, verwandelt sich mit ihm, jedoch nichts, was er hält. Wenn er stirbt, nimmt er seine wahre Gestalt an. In Nebelgestalt kann der Vampir keine Aktionen ausführen, nicht sprechen und nicht mit Objekten interagieren. Er hat kein Gewicht, eine Flugbewegungsrate von sechs Metern, kann schweben, in den Bereich einer feindlich gesinnten Kreatur eindringen und dort stoppen. Sofern Luft durch einen Bereich strömen kann, kann der Nebel diesen Bereich ebenfalls passieren, ohne sich zu quetschen. Er kann nicht durch Wasser gelangen. Er ist bei Rettungswürfen auf Stärke, Geschicklichkeit und Konstitution im Vorteil und gegen alle nichtmagischen Schadensarten außer Schaden durch Sonnenlicht immun.
:
***Legendäre Resistenz (3-mal täglich).*** Wenn sein Rettungswurf scheitert, kann der Vampir den Wurf in einen Erfolg verwandeln.
:
***Neblige Flucht.*** Wenn seine Trefferpunkte außerhalb seines Ruheplatzes auf 0 sinken, verwandelt sich der Vampir in eine Nebelwolke (wie beim Gestaltwandler-Merkmal), anstatt bewusstlos zu werden, sofern er sich weder im Sonnenlicht noch in fließendem Wasser befindet. Wenn er sich nicht verwandeln kann, wird er zerstört. Mit 0 Trefferpunkten in Nebelgestalt kann er sich nicht in seine Vampirgestalt zurückverwandeln und muss im Abstand von zwei Stunden seinen Ruheplatz erreichen, oder er wird zerstört. Erreicht er seinen Ruheplatz, so verwandelt er sich in seine Vampirgestalt zurück. Dann ist er gelähmt, bis er mindestens einen Trefferpunkt zurückerhält. Wenn er eine Stunde mit 0 Trefferpunkten an seinem Ruheplatz verbracht hat, erhält er 1 Trefferpunkt zurück.
:
***Regeneration.*** Der Vampir erhält zu Beginn seines Zugs 20 Trefferpunkte zurück, wenn er mindestens 1 Trefferpunkt hat und sich weder im Sonnenlicht noch in fließendem Wasser befindet. Wenn der Vampir gleißenden Schaden oder Schaden durch Weihwasser erleidet, wirkt dieses Merkmal zu Beginn seines nächsten Zugs nicht.
:
***Spinnenklettern.*** Der Vampir kann ohne Attributswürfe schwierige Oberflächen erklimmen und sich kopfüber an Decken entlang bewegen.
:
***Vampirschwächen.*** Der Vampir hat folgende Makel: Holzpflock ins Herz: Wenn dem Vampir eine Stichwaffe aus Holz ins Herz getrieben wird, während er sich kampfunfähig an seinem Ruheplatz befindet, ist der Vampir gelähmt, bis die Waffe entfernt wird. Hyperempfindlich gegenüber Sonnenlicht: Der Vampir erleidet 20 gleißenden Schaden, wenn er seinen Zug im Sonnenlicht beginnt. Im Sonnenlicht ist er bei Angriffs- und Attributswürfen im Nachteil.
:
***Schaden durch fließendes Wasser.*** Der Vampir erleidet 20 Säureschaden, wenn er seinen Zug in fließendem Wasser beendet.
:
***Zutritt verwehren.*** Der Vampir kann keine Wohnstätte ohne die Einladung eines der Bewohner betreten.
:
### Aktionen
***Mehrfachangriff (nur in Vampirgestalt).*** Der Vampir führt zwei Angriffe aus, von denen nur einer ein Bissangriff sein kann.
:
***Biss (nur in Fledermaus- oder Vampirgestalt).*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, eine bereitwillige oder vom Vampir gepackte oder kampfunfähige oder festgesetzte Kreatur. *Treffer*: 7 (1W6+4) Stichschaden plus 10 (3W6) nekrotischer Schaden. Das Trefferpunktemaximum des Ziels wird um den Betrag des erlittenen nekrotischen Schadens verringert, und der Vampir erhält Trefferpunkte in Höhe dieses Betrags. Das Trefferpunktemaximum bleibt verringert, bis das Ziel eine lange Rast beendet. Wenn das Trefferpunktemaximum durch diesen Effekt auf 0 sinkt, stirbt das Ziel. Ein Humanoide, der auf diese Art stirbt und dann in der Erde begraben wird, erhebt sich in der folgenden Nacht als Vampirbrut unter der Kontrolle des Vampirs.
:
***Waffenloser Angriff (nur in Vampirgestalt).*** *Nahkampfwaffenangriff*: +9 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 8 (1W8+4) Wuchtschaden. Anstatt Schaden zu bewirken, kann der Vampir das Ziel packen (Rettungswurf-SG 18).
:
***Bezaubern.*** Der Vampir zielt auf einen Humanoiden im Abstand von bis zu neun Metern von ihm, den er sehen kann. Wenn das Ziel den Vampir sehen kann, muss es einen SG-17-Weisheitsrettungswurf gegen diese Magie bestehen, oder es ist vom Vampir bezaubert. Das bezauberte Ziel betrachtet den Vampir als vertrauenswürdigen Freund, den es beschützt und auf dessen Rat es hört. Obwohl das Ziel nicht vom Vampir kontrolliert wird, steht es dessen Forderungen und Aktionen so entgegenkommend wie möglich gegenüber und ist ein bereitwilliges Ziel für den Bissangriff des Vampirs. Jedes Mal, wenn der Vampir oder seine Verbündeten dem Ziel Schaden zufügen, kann es den Rettungswurf wiederholen und den Effekt bei einem Erfolg beenden. Anderenfalls hält der Effekt 24 Stunden lang an, oder bis der Vampir stirbt, sich auf eine andere Existenzebene als die des Ziels begibt oder den Effekt als Bonusaktion beendet.
:
***Kinder der Nacht (1-mal täglich).*** Der Vampir ruft magisch 2W4 Schwärme von Fledermäusen oder Ratten herbei, sofern die Sonne noch nicht aufgegangen ist. Wenn er sich im Freien befindet, kann der Vampir stattdessen 3W6 Wölfe herbeirufen. Die herbeigerufenen Kreaturen treffen 1W4 Runden später ein. Sie handeln als Verbündete des Vampirs und gehorchen seinen gesprochenen Befehlen. Die Tiere bleiben eine Stunde lang bestehen, bis sie sterben oder der Vampir stirbt, oder bis der Vampir sie als Bonusaktion entlässt.
:
### Legendäre Aktionen
Der Vampir kann drei legendäre Aktionen entsprechend den unten aufgeführten Optionen ausführen. Er kann jeweils nur eine legendäre Aktionsmöglichkeit und nur am Ende des Zugs einer anderen Kreatur ausführen. Der Vampir erhält verbrauchte legendäre Aktionen am Anfang seines Zugs zurück.
:
***Biss (kostet 2 Aktionen).*** Der Vampir führt einen Bissangriff aus.
:
***Bewegung.*** Der Vampir nutzt seine Bewegungsrate, ohne Gelegenheitsangriffe zu provozieren.
:
***Waffenloser Angriff.*** Der Vampir führt einen waffenlosen Angriff aus.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Veteran
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 17 (Schienenpanzer)
**Trefferpunkte**  :: 58 (9W8+18)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|13 (+1)|14 (+2)|10 (+0)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Athletik +5, Wahrnehmung +2
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 3 (700 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Veteran führt zwei Langschwertangriffe durch. Wenn er ein Kurzschwert gezogen hat, kann er auch einen Kurzschwertangriff ausführen.
:
***Kurzschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Stichschaden.
:
***Langschwert.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Hiebschaden oder 8 (1W10+3) Hiebschaden bei zweihändiger Führung.
:
***Schwere Armbrust.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 30/120 m, ein Ziel. *Treffer*: 6 (1W10+1) Stichschaden.
:
}}


{{monster,frame
## Violetter Pilz
*Mittelgroße Pflanze, gesinnungslos*
___
**Rüstungsklasse** :: 5
**Trefferpunkte**  :: 18 (4W8)
**Speed**          :: 1,5 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|1  (-5)|10 (+0)|1  (-5)|3  (-4)|1  (-5)|
___
**Zustandsimmunitäten**     :: Blind, Taub, Verängstigt
**Sinne**                   :: Blindsicht 9 m (blind außerhalb des Radius), Passive Wahrnehmung 6
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Falsches Erscheinungsbild.*** Solange der violette Pilz sich nicht bewegt, ist er nicht von einem gewöhnlichen Pilz zu unterscheiden.
:
### Aktionen
***Mehrfachangriff.*** Der Pilz führt 1W4 Faulige-Berührung-Angriffe aus.
:
***Faulige Berührung.*** *Nahkampfwaffenangriff*: +2 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 4 (1W8) nekrotischer Schaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Vrock
*Großer Unhold (Dämon), chaotisch böse*
___
**Rüstungsklasse** :: 15 (natürliche Rüstung)
**Trefferpunkte**  :: 104 (11W10+44)
**Speed**          :: 12 m, Fliegen 18 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|15 (+2)|18 (+4)|8  (-1)|13 (+1)|8  (-1)|
___
**Rettungswürfe**           :: Ges +5, Wei +4, Cha +2
**Schadensresistenzen**     :: Blitz, Feuer, Kälte; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 36 m, Passive Wahrnehmung 11
**Sprachen**                :: Abyssisch, Telepathie auf 36 m
**Herausforderung**         :: 6 (2.300 EP)
___
***Magieresistenz.*** Der Vrock ist bei Rettungswürfen gegen Zauber und andere magische Effekte im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Vrock führt zwei Angriffe aus: einen Schnabel- und einen Klauenangriff.
:
***Krallen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 14 (2W10+3) Hiebschaden.
:
***Schnabel.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Stichschaden.
:
***Betäubendes Kreischen (1-mal täglich).*** Der Vrock stößt ein entsetzliches Kreischen aus. Jede Kreatur im Abstand von bis zu sechs Metern von ihm, die ihn hören kann und kein Dämon ist, muss einen SG-14-Konstitutionsrettungswurf bestehen, oder sie ist bis zum Ende des nächsten Zugs des Vrocks betäubt.
:
***Sporen (Aufladung 6).*** Eine Wolke giftiger Sporen breitet sich im Radius von 4,5 m um den Vrock aus. Die Sporen breiten sich um Ecken aus. Jede Kreatur in diesem Bereich muss einen SG-14-Konstitutionsrettungswurf bestehen, oder sie ist vergiftet. Auf diese Art vergiftete Kreaturen erleiden zu Beginn jedes ihrer Züge 5 (1W10) Giftschaden. Ein Ziel kann den Rettungswurf am Ende jedes seiner Züge wiederholen und den Effekt bei einem Erfolg beenden. Der Effekt wird auch durch den Konsum einer Phiole mit Weihwasser beendet.
:
}}


{{monster,frame
## Wache
*Mittelgroßer Humanoide (jedes Volk), jede Gesinnung*
___
**Rüstungsklasse** :: 16 (Kettenhemd, Schild)
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|12 (+1)|12 (+1)|10 (+0)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Eine beliebige Sprache (normalerweise Gemeinsprache)
**Herausforderung**         :: 1/8 (25 EP)
___
### Aktionen
***Speer.*** *Fernkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, ein Ziel. *Treffer*: 4 (1W6+1) Stichschaden oder 5 (1W8+1) Stichschaden, wenn beidhändig geführt, um einen Nahkampfangriff auszuführen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Wächternaga
*Große Monstrosität, rechtschaffen gut*
___
**Rüstungsklasse** :: 18 (natürliche Rüstung)
**Trefferpunkte**  :: 127 (15W10+45)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|18 (+4)|16 (+3)|16 (+3)|19 (+4)|18 (+4)|
___
**Rettungswürfe**           :: Ges +8, Kon +7, Int +7, Wei +8, Cha +8
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bezaubert, Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Celestisch, Gemeinsprache
**Herausforderung**         :: 10 (5.900 EP)
___
***Wiederbelebung.*** Wenn der Naga stirbt, wird er nach 1W6 Tagen wieder lebendig und erhält all seine Trefferpunkte zurück. Nur der Zauber Wunsch kann verhindern, dass dieses Merkmal wirkt.
:
***Zauberwirken.*** Der Naga ist ein Zauberwirker der 11. Stufe. Sein Attribut zum Zauberwirken ist Weisheit (Zauberrettungswurf-SG 16, +8 auf Treffer mit Zauberangriffen), und er braucht nur verbale Komponenten, um seine Zauber zu wirken. Er hat folgende Klerikerzauber vorbereitet:
:
Zaubertricks (beliebig oft): [*Ausbessern*](https://openrpg.de/srd/5e/de/#spell-ausbessern)[*Heilige Flamme*](https://openrpg.de/srd/5e/de/#spell-heilige-flamme)[*Thaumaturgie*](https://openrpg.de/srd/5e/de/#spell-thaumaturgie)<br>
1&period; Grad (4 Plätze): [*Befehl*](https://openrpg.de/srd/5e/de/#spell-befehl)[*Schild des Glaubens*](https://openrpg.de/srd/5e/de/#spell-schild-des-glaubens)[*Wunden heilen*](https://openrpg.de/srd/5e/de/#spell-wunden-heilen)<br>
2&period; Grad (3 Plätze): [*Gefühle besänftigen*](https://openrpg.de/srd/5e/de/#spell-gefuehle-besaenftigen)[*Person festhalten*](https://openrpg.de/srd/5e/de/#spell-person-festhalten)<br>
3&period; Grad (3 Plätze): [*Fluch*](https://openrpg.de/srd/5e/de/#spell-fluch)[*Hellsehen*](https://openrpg.de/srd/5e/de/#spell-hellsehen)<br>
4&period; Grad (3 Plätze): [*Bewegungsfreiheit*](https://openrpg.de/srd/5e/de/#spell-bewegungsfreiheit)[*Verbannung*](https://openrpg.de/srd/5e/de/#spell-verbannung)<br>
5&period; Grad (2 Plätze): [*Flammenschlag*](https://openrpg.de/srd/5e/de/#spell-flammenschlag)[*Geas*](https://openrpg.de/srd/5e/de/#spell-geas)<br>
6&period; Grad (1 Platz): [*Wahrer Blick*](https://openrpg.de/srd/5e/de/#spell-wahrer-blick)
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +8 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 8 (1W8+4) Stichschaden, und das Ziel muss einen SG-15-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 45 (10W8) Giftschaden, anderenfalls die Hälfte.
:
***Gift spucken.*** *Fernkampfwaffenangriff*: +8 auf Treffer, Reichweite 4,5/9 m, eine Kreatur. *Treffer*: Das Ziel muss einen SG-15-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 45 (10W8) Giftschaden, anderenfalls die Hälfte.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Wasserelementar
*Großer Elementar, neutral*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 114 (12W10+48)
**Speed**          :: 9 m, Schwimmen 27 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|14 (+2)|18 (+4)|5  (-3)|10 (+0)|8  (-1)|
___
**Schadensresistenzen**     :: Säure; Hieb, Stich und Wucht durch nichtmagische Angriffe
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Bewusstlos, Erschöpft, Festgesetzt, Gelähmt, Gepackt, Liegend, Vergiftet, Versteinert
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 10
**Sprachen**                :: Aqual
**Herausforderung**         :: 5 (1.800 EP)
___
***Einfrieren.*** Wenn der Elementar Kälteschaden erleidet, gefriert er teilweise. Seine Bewegungsrate ist bis zum Ende seines nächsten Zugs um sechs Meter verringert.
:
***Wassergestalt.*** Der Elementar kann in den Bereich einer feindlich gesinnten Kreatur eindringen und dort stoppen. Er kann sich durch einen engen Bereich mit einer Breite von nur 2,5 Zentimetern bewegen, ohne sich quetschen zu müssen.
:
### Aktionen
***Mehrfachangriff.*** Der Elementar führt zwei Hiebangriffe aus.
:
***Hieb.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Wuchtschaden.
:
***Überschutt (Aufladung 4–6).*** Jede Kreatur im Bereich des Elementars muss einen SG-15-Stärkerettungswurf ausführen. Scheitert der Wurf, so erleidet das Ziel 13 (2W8+4) Wuchtschaden. Wenn es eine höchstens große Kreatur ist, wird es außerdem gepackt (Rettungswurf-SG 14): Solange das Ziel gepackt bleibt, ist es festgesetzt und kann nicht atmen, es sei denn, es verfügt über Wasseratmung. Wenn der Rettungswurf erfolgreich ist, wird das Ziel aus dem Bereich des Elementars gestoßen. Der Elementar kann eine große Kreatur oder bis zu zwei höchstens mittelgroße Kreaturen zugleich gepackt halten. Zu Beginn jedes Zugs des Elementars erleidet jedes von ihm gepackte Ziel 13 (2W8+4) Wuchtschaden. Eine Kreatur im Abstand von bis zu 1,5 Metern vom Elementar kann eine Kreatur oder ein Objekt aus ihm herausziehen. Dazu ist eine Aktion erforderlich, um einen SG-14-Stärkewurf auszuführen, welcher erfolgreich sein muss.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Weißer Drachennestling
*Mittelgroßer Drache, chaotisch böse*
___
**Rüstungsklasse** :: 16 (natürliche Rüstung)
**Trefferpunkte**  :: 32 (5W8+10)
**Speed**          :: 9 m, Fliegen 18 m, Graben 4,5 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|14 (+2)|10 (+0)|14 (+2)|5  (-3)|10 (+0)|11 (+0)|
___
**Rettungswürfe**           :: Ges +2, Kon +4, Wei +2, Cha +2
**Fertigkeiten**            :: Heimlichkeit +2, Wahrnehmung +4
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Blindsicht 3 m, Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Drakonisch
**Herausforderung**         :: 2 (450 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W10+2) Stichschaden plus 2 (1W4) Kälteschaden.
:
***Kälteodem (Aufladung 5–6).*** Der Drache atmet einen eisigen Stoß in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-12-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 22 (5W8) Kälteschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Werbär
*Mittelgroßer Humanoide (Mensch, Gestaltwandler), neutral gut*
___
**Rüstungsklasse** :: 10 in humanoider Gestalt, 11 (natürliche Rüstung) in Bären- und hybrider Gestalt
**Trefferpunkte**  :: 135 (18W8+54)
**Speed**          :: 9 m (12 m, Klettern 9 m in Bären- oder hybrider Gestalt)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|17 (+3)|11 (+0)|12 (+1)|12 (+1)|
___
**Fertigkeiten**            :: Wahrnehmung +7
**Schadensimmunitäten**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe von Waffen ohne Silber
**Sinne**                   :: Passive Wahrnehmung 17
**Sprachen**                :: Gemeinsprache (kann in Bärengestalt nicht sprechen)
**Herausforderung**         :: 5 (1.800 EP)
___
***Gestaltwandler.*** Der Werbär kann seine Aktion verwenden, um sich in eine hybride Gestalt aus Bär und Humanoide oder in einen großen Bären zu verwandeln - oder um wieder seine wahre, humanoide Gestalt anzunehmen. Seine Spielwerte sind abgesehen von Größe und RK in beiden Gestalten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Scharfer Geruchssinn.*** Der Werbär ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** In Bärengestalt führt der Werbär zwei Klauenangriffe aus. In humanoider Gestalt führt er zwei Angriffe mit der Zweihandaxt aus. In hybrider Gestalt kann er wie ein Bär oder wie ein Humanoide angreifen.
:
***Biss (nur Bären- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 15 (2W10+4) Stichschaden. Wenn das Ziel ein Humanoide ist, muss es einen SG-14-Konstitutionsrettungswurf bestehen, um nicht mit Werbär-Lykanthropie verflucht zu werden.
:
***Klauen (nur Bären- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Hiebschaden.
:
***Zweihandaxt (nur humanoide oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (1W12+4) Hiebschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Wereber
*Mittelgroßer Humanoide (Mensch, Gestaltwandler), neutral böse*
___
**Rüstungsklasse** :: 10 in humanoider Gestalt, 11 (natürliche Rüstung) in Eber- und hybrider Gestalt
**Trefferpunkte**  :: 78 (12W8+24)
**Speed**          :: 9 m (12 m in Ebergestalt)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|10 (+0)|15 (+2)|10 (+0)|11 (+0)|8  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +2
**Schadensimmunitäten**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe von Waffen ohne Silber
**Sinne**                   :: Passive Wahrnehmung 12
**Sprachen**                :: Gemeinsprache (kann in Ebergestalt nicht sprechen)
**Herausforderung**         :: 4 (1.100 EP)
___
***Gestaltwandler.*** Der Wereber kann seine Aktion verwenden, um sich in eine hybride Gestalt aus Eber und Humanoide oder in einen Eber zu verwandeln - oder um wieder seine wahre, humanoide Gestalt anzunehmen. Seine Spielwerte sind abgesehen von der RK in beiden Gestalten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Sturmangriff (nur Eber- oder hybride Gestalt).*** Wenn der Wereber sich mindestens 4,5 Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit seinen Hauern trifft, erleidet das Ziel zusätzlich 7 (2W6) Hiebschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Unermüdlich (wird nach kurzer oder langer Rast aufgeladen).*** Wenn der Wereber maximal 14 Schaden erleidet und seine Trefferpunkte dadurch auf 0 sinken würden, hat er stattdessen noch 1 Trefferpunkt.
:
### Aktionen
***Mehrfachangriff (nur humanoide oder hybride Gestalt).*** Der Wereber führt zwei Angriffe aus, von denen nur einer mit den Hauern erfolgen kann.
:
***Hauer (nur Eber- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Hiebschaden. Wenn das Ziel ein Humanoide ist, muss es einen SG-12-Konstitutionsrettungswurf bestehen, um nicht mit Wereber-Lykanthropie verflucht zu werden.
:
***Zerfleischen (nur humanoide oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Wuchtschaden.
:
}}


{{monster,frame
## Werratte
*Mittelgroßer Humanoide (Mensch, Gestaltwandler), rechtschaffen böse*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 33 (6W8+6)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|10 (+0)|15 (+2)|12 (+1)|11 (+0)|10 (+0)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +2
**Schadensimmunitäten**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe von Waffen ohne Silber
**Sinne**                   :: Dunkelsicht 18 m (nur in Rattengestalt), Passive Wahrnehmung 12
**Sprachen**                :: Gemeinsprache (kann in Rattengestalt nicht sprechen)
**Herausforderung**         :: 2 (450 EP)
___
***Gestaltwandler.*** Die Werratte kann ihre Aktion verwenden, um sich in eine hybride Gestalt aus Ratte und Humanoide oder eine Riesenratte zu verwandeln - oder um wieder ihre wahre, humanoide Gestalt anzunehmen. Ihre Spielwerte sind abgesehen von der Größe in beiden Gestalten gleich. Ausrüstung, die sie trägt oder hält, wird nicht verwandelt. Wenn sie stirbt, nimmt sie ihre wahre Gestalt an.
:
***Scharfer Geruchssinn.*** Die Werratte ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff (nur humanoide oder hybride Gestalt).*** Die Werratte führt zwei Angriffe aus, von denen nur einer ein Biss sein darf.
:
***Biss (nur Ratten- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W4+2) Stichschaden. Wenn das Ziel ein Humanoide ist, muss es einen SG-11-Konstitutionsrettungswurf bestehen, um nicht mit Werratten-Lykanthropie verflucht zu werden.
:
***Kurzschwert (nur humanoide oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Handarmbrust (nur humanoide oder hybride Gestalt).*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 9/36 m, ein Ziel. *Treffer*: 5 (1W6+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame,wide
## Wertiger
*Mittelgroßer Humanoide (Mensch, Gestaltwandler), neutral*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 120 (16W8+48)
**Speed**          :: 9 m (12 m in Tigergestalt)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|15 (+2)|16 (+3)|10 (+0)|13 (+1)|11 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +5
**Schadensimmunitäten**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe von Waffen ohne Silber
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 15
**Sprachen**                :: Gemeinsprache (kann in Tigergestalt nicht sprechen)
**Herausforderung**         :: 4 (1.100 EP)
___
***Gestaltwandler.*** Der Wertiger kann seine Aktion verwenden, um sich in eine hybride Gestalt aus Tiger und Humanoide oder in einen Tiger zu verwandeln - oder um wieder seine wahre, humanoide Gestalt anzunehmen. Seine Spielwerte sind abgesehen von der Größe in beiden Gestalten gleich. Ausrüstung, die sie trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Anspringen (nur Tiger- oder hybride Gestalt).*** Wenn der Wertiger sich mindestens 4,5 Meter weit direkt auf eine Kreatur zubewegt und sie dann im gleichen Zug mit einem Klauenangriff trifft, muss das Ziel einen SG-14-Stärkerettungswurf bestehen, oder es wird umgestoßen. Wenn das Ziel liegt, kann der Wertiger als Bonusaktion einen Bissangriff gegen es ausführen.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Wertiger ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff (nur humanoide oder hybride Gestalt).*** In humanoider Gestalt führt der Wertiger zwei Angriffe mit dem Krummsäbel oder zwei Angriffe mit dem Langbogen aus. In hybrider Gestalt kann er wie ein Humanoide angreifen oder zwei Klauenangriffe ausführen.
:
***Biss (nur Tiger- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 8 (1W10+3) Stichschaden. Wenn das Ziel ein Humanoide ist, muss es einen SG-13-Konstitutionsrettungswurf bestehen, um nicht mit Wertiger-Lykanthropie verflucht zu werden.
:
***Klauen (nur Tiger- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (1W8+3) Hiebschaden.
:
***Krummsäbel (nur humanoide oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Hiebschaden.
:
***Langbogen (nur humanoide oder hybride Gestalt).*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
}}
{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page


{{monster,frame
## Werwolf
*Mittelgroßer Humanoide (Mensch, Gestaltwandler), chaotisch böse*
___
**Rüstungsklasse** :: 11 in humanoider Gestalt, 12 (natürliche Rüstung) in Wolfs- und hybrider Gestalt
**Trefferpunkte**  :: 58 (9W8+18)
**Speed**          :: 9 m (12 m in Wolfsgestalt)
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|13 (+1)|14 (+2)|10 (+0)|11 (+0)|10 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +4
**Schadensimmunitäten**     :: Hieb, Stich und Wucht durch nichtmagische Angriffe von Waffen ohne Silber
**Sinne**                   :: Passive Wahrnehmung 14
**Sprachen**                :: Gemeinsprache (kann in Wolfsgestalt nicht sprechen)
**Herausforderung**         :: 3 (700 EP)
___
***Gestaltwandler.*** Der Werwolf kann seine Aktion verwenden, um sich in eine hybride Gestalt aus Wolf und Humanoide oder in einen Wolf zu verwandeln - oder um wieder seine wahre, humanoide Gestalt anzunehmen. Seine Spielwerte sind abgesehen von der RK in beiden Gestalten gleich. Ausrüstung, die er trägt oder hält, wird nicht verwandelt. Wenn er stirbt, nimmt er seine wahre Gestalt an.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Werwolf ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Mehrfachangriff (nur humanoide oder hybride Gestalt).*** Der Werwolf führt zwei Angriffe aus: einen Biss- und einen Klauen- oder Speerangriff.
:
***Biss (nur Wolfs- oder hybride Gestalt).*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden. Wenn das Ziel ein Humanoide ist, muss es einen SG-12-Konstitutionsrettungswurf bestehen, um nicht mit Werwolf-Lykanthropie verflucht zu werden.
:
***Klauen (nur hybrider Gestalt).*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 7 (2W4+2) Hiebschaden.
:
***Speer (nur humanoide Gestalt).*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m oder Reichweite 6/18 m, eine Kreatur. *Treffer*: 5 (1W6+2) Stichschaden oder 6 (1W8+2) Stichschaden bei zweihändiger Führung und Nahkampfangriff.
:
}}


{{monster,frame
## Wiesel
*Winziges Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13
**Trefferpunkte**  :: 1 (1W4-1)
**Speed**          :: 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|3  (-4)|16 (+3)|8  (-1)|2  (-4)|12 (+1)|3  (-4)|
___
**Fertigkeiten**            :: Heimlichkeit +5, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Das Wiesel ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 1 Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Winterwolf
*Große Monstrosität, neutral böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 75 (10W10+20)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|13 (+1)|14 (+2)|7  (-2)|12 (+1)|8  (-1)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +5
**Schadensimmunitäten**     :: Kälte
**Sinne**                   :: Passive Wahrnehmung 15
**Sprachen**                :: Gemeinsprache, Riesisch, Winterwolfisch
**Herausforderung**         :: 3 (700 EP)
___
***Rudeltaktik.*** Der Wolf ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Wolf ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
***Schneetarnung.*** Der Wolf ist bei Geschicklichkeitswürfen (Heimlichkeit), die er ausführt, um sich in verschneitem Gelände zu verstecken, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-14-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
***Kälteodem (Aufladung 5–6).*** Der Wolf atmet eisigen Wind in einem Kegel von 4,5 Metern aus. Jede Kreatur in diesem Bereich muss einen SG-12-Geschicklichkeitsrettungswurf ausführen. Scheitert der Wurf, erleidet sie 18 (4W8) Kälteschaden, anderenfalls die Hälfte.
:
}}


{{monster,frame
## Wolf
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 11 (2W8+2)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|15 (+2)|12 (+1)|3  (-4)|12 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Heimlichkeit +4, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
***Rudeltaktik.*** Der Wolf ist bei Angriffswürfen gegen eine Kreatur im Vorteil, wenn sich mindestens einer seiner Verbündeten, der nicht kampfunfähig ist, im Abstand von bis zu 1,5 Metern von der Kreatur befindet.
:
***Scharfes Gehör und scharfer Geruchssinn.*** Der Wolf ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 7 (2W4+2) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-11-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Wolkenriese
*Riesiger Riese, neutral gut (50 %) oder neutral böse (50 %)*
___
**Rüstungsklasse** :: 14 (natürliche Rüstung)
**Trefferpunkte**  :: 200 (16W12+96)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|27 (+8)|10 (+0)|22 (+6)|12 (+1)|16 (+3)|16 (+3)|
___
**Rettungswürfe**           :: Kon +10, Wei +7, Cha +7
**Fertigkeiten**            :: Motiv erkennen +7, Wahrnehmung +7
**Sinne**                   :: Passive Wahrnehmung 17
**Sprachen**                :: Gemeinsprache, Riesisch
**Herausforderung**         :: 9 (5.000 EP)
___
***Scharfer Geruchssinn.*** Der Riese ist bei Weisheitswürfen (Wahrnehmung), die auf Geruchssinn basieren, im Vorteil.
:
***Angeborenes Zauberwirken.*** Das Attribut zum angeborenen Zauberwirken des Riesen ist Charisma. Der Riese kann folgende angeborene Zauber wirken, ohne Materialkomponenten zu benötigen:
:
Beliebig oft: [*Licht*](https://openrpg.de/srd/5e/de/#spell-licht)[*Magie entdecken*](https://openrpg.de/srd/5e/de/#spell-magie-entdecken)[*Nebelwolke*](https://openrpg.de/srd/5e/de/#spell-nebelwolke)<br>
Je 3-mal täglich: [*Federfall*](https://openrpg.de/srd/5e/de/#spell-federfall)[*Fliegen*](https://openrpg.de/srd/5e/de/#spell-fliegen)[*Nebelschritt*](https://openrpg.de/srd/5e/de/#spell-nebelschritt)[*Telekinese*](https://openrpg.de/srd/5e/de/#spell-telekinese)<br>
Je 1-mal täglich: [*Gasförmige Gestalt*](https://openrpg.de/srd/5e/de/#spell-gasfoermige-gestalt)[*Wetterkontrolle*](https://openrpg.de/srd/5e/de/#spell-wetterkontrolle)
:
### Aktionen
***Mehrfachangriff.*** Der Riese führt zwei Angriffe mit dem Morgenstern aus.
:
***Morgenstern.*** *Nahkampfwaffenangriff*: +12 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 21 (3W8+8) Stichschaden.
:
***Felsblock.*** *Fernkampfwaffenangriff*: +12 auf Treffer, Reichweite 18/72 m, ein Ziel. *Treffer*: 30 (4W10+8) Wuchtschaden.
:
}}


{{monster,frame
## Worg
*Große Monstrosität, neutral böse*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 26 (4W10+4)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|16 (+3)|13 (+1)|13 (+1)|7  (-2)|11 (+0)|8  (-1)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: Goblinisch, Worgisch
**Herausforderung**         :: 1/2 (100 EP)
___
***Scharfes Gehör und scharfer Geruchssinn.*** Der Worg ist bei Weisheitswürfen (Wahrnehmung), die auf Gehör oder Geruchssinn basieren, im Vorteil.
:
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +5 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 10 (2W6+3) Stichschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-13-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Würgeschlange
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 13 (2W10+2)
**Speed**          :: 9 m, Schwimmen 9 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|15 (+2)|14 (+2)|12 (+1)|1  (-5)|10 (+0)|3  (-4)|
___
**Sinne**                   :: Blindsicht 3 m, Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Biss.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 5 (1W6+2) Stichschaden.
:
***Umschlingen.*** *Nahkampfwaffenangriff*: +4 auf Treffer, Reichweite 1,5 m, eine Kreatur. *Treffer*: 6 (1W8+2) Wuchtschaden, und das Ziel wird gepackt (Rettungswurf-SG 14). Eine gepackte Kreatur ist festgesetzt. Die Schlange kann solange kein weiteres Ziel umschlingen.
:
}}


{{monster,frame
## Wyvern
*Großer Drache, gesinnungslos*
___
**Rüstungsklasse** :: 13 (natürliche Rüstung)
**Trefferpunkte**  :: 110 (13W10+39)
**Speed**          :: 6 m, Fliegen 24 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|19 (+4)|10 (+0)|16 (+3)|5  (-3)|12 (+1)|6  (-2)|
___
**Fertigkeiten**            :: Wahrnehmung +4
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 14
**Sprachen**                :: -
**Herausforderung**         :: 6 (2.300 EP)
___
### Aktionen
***Mehrfachangriff.*** Der Wyvern führt zwei Angriffe aus: einen Biss- und einen Stachelangriff. Fliegend kann er anstelle eines anderen Angriffs seine Klauen einsetzen.
:
***Biss.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 11 (2W6+4) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (2W8+4) Hiebschaden.
:
***Stachel.*** *Nahkampfwaffenangriff*: +7 auf Treffer, Reichweite 3 m, eine Kreatur. *Treffer*: 11 (2W6+4) Stichschaden. Das Ziel muss einen SG-15-Konstitutionsrettungswurf ausführen. Scheitert der Wurf, erleidet es 24 (7W6) Giftschaden, anderenfalls die Hälfte.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Xorn
*Mittelgroßer Elementar, neutral*
___
**Rüstungsklasse** :: 19 (natürliche Rüstung)
**Trefferpunkte**  :: 73 (7W8+42)
**Speed**          :: 6 m, Graben 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|17 (+3)|10 (+0)|22 (+6)|11 (+0)|10 (+0)|11 (+0)|
___
**Fertigkeiten**            :: Heimlichkeit +3, Wahrnehmung +6
**Schadensresistenzen**     :: Hieb und Stich durch nichtmagische Angriffe ohne Adamant
**Sinne**                   :: Dunkelsicht 18 m, Erschütterungssinn 18 m, Passive Wahrnehmung 16
**Sprachen**                :: Terral
**Herausforderung**         :: 5 (1.800 EP)
___
***Erdgleiten.*** Der Xorn kann sich durch nichtmagisches, unbearbeitetes Erd- und Steinmaterial graben. Dabei beeinträchtigt er das Material nicht, durch das er sich bewegt.
:
***Schatzgespür.*** Der Xorn kann Edelmetalle und Edelsteine im Abstand von bis zu 18 Metern von ihm wittern und lokalisieren.
:
***Steintarnung.*** Der Xorn ist bei Geschicklichkeitswürfen (Heimlichkeit), die er ausführt, um sich in steinigem Gelände zu verstecken, im Vorteil.
:
### Aktionen
***Mehrfachangriff.*** Der Xorn führt drei Klauenangriffe und einen Bissangriff aus.
:
***Biss.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 13 (3W6+3) Stichschaden.
:
***Klauen.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 6 (1W6+3) Hiebschaden.
:
}}


{{monster,frame
## Zentaur
*Große Monstrosität, neutral gut*
___
**Rüstungsklasse** :: 12
**Trefferpunkte**  :: 45 (6W10+12)
**Speed**          :: 15 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|14 (+2)|14 (+2)|9  (-1)|13 (+1)|11 (+0)|
___
**Fertigkeiten**            :: Athletik +6, Überlebenskunst +3, Wahrnehmung +3
**Sinne**                   :: Passive Wahrnehmung 13
**Sprachen**                :: Elfisch, Sylvanisch
**Herausforderung**         :: 2 (450 EP)
___
***Sturmangriff.*** Wenn der Zentaur sich mindestens neun Meter weit in gerader Linie auf ein Ziel zubewegt und es dann im selben Zug mit einem Pikenangriff trifft, erleidet das Ziel zusätzlich 10 (3W6) Stichschaden.
:
### Aktionen
***Mehrfachangriff.*** Zentauren führen zwei Angriffe aus: entweder einen mit dem Spieß und einen mit den Hufen oder zwei mit dem Langbogen.
:
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 11 (2W6+4) Wuchtschaden.
:
***Pike.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 3 m, ein Ziel. *Treffer*: 9 (1W10+4) Stichschaden.
:
***Langbogen.*** *Fernkampfwaffenangriff*: +4 auf Treffer, Reichweite 45/180 m, ein Ziel. *Treffer*: 6 (1W8+2) Stichschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Ziege
*Mittelgroßes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 4 (1W8)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|12 (+1)|10 (+0)|11 (+0)|2  (-4)|10 (+0)|5  (-3)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 0 (10 EP)
___
***Sicherer Tritt.*** Die Ziege ist bei Stärke- und Geschicklichkeitsrettungswürfen gegen Effekte, die sie umstoßen würden, im Vorteil.
:
***Sturmangriff.*** Wenn die Ziege sich mindestens sechs Meter weit direkt auf ein Ziel zubewegt und es dann im selben Zug mit einem Rammbock-Angriff trifft, erleidet das Ziel zusätzlich 2 (1W4) Wuchtschaden. Wenn das Ziel eine Kreatur ist, muss es einen SG-10-Stärkerettungswurf bestehen, oder es wird umgestoßen.
:
### Aktionen
***Rammbock.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 3 (1W4+1) Wuchtschaden.
:
}}


{{monster,frame
## Zombie
*Mittelgroßer Untoter, neutral böse*
___
**Rüstungsklasse** :: 8
**Trefferpunkte**  :: 22 (3W8+9)
**Speed**          :: 6 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|13 (+1)|6  (-2)|16 (+3)|3  (-4)|6  (-2)|5  (-3)|
___
**Rettungswürfe**           :: Wei +0
**Schadensimmunitäten**     :: Gift
**Zustandsimmunitäten**     :: Vergiftet
**Sinne**                   :: Dunkelsicht 18 m, Passive Wahrnehmung 8
**Sprachen**                :: Versteht alle zu Lebzeiten bekannten Sprachen, aber kann nicht sprechen
**Herausforderung**         :: 1/4 (50 EP)
___
***Untote Ausdauer.*** Wenn die Trefferpunkte des Zombies durch Schaden auf 0 sinken, muss der Zombie einen Konstitutionsrettungswurf mit SG 5 + erlittenem Schaden ausführen, sofern der Schaden nicht gleißend ist oder von einem kritischen Treffer stammt. Bei einem Erfolg behält der Zombie 1 Trefferpunkt.
:
### Aktionen
***Hieb.*** *Nahkampfwaffenangriff*: +3 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 4 (1W6+1) Wuchtschaden.
:
}}


{{pageNumber,auto}}
{{footnote SRD 5.1 **de**}}
\page
{{monster,frame
## Zugpferd
*Großes Tier, gesinnungslos*
___
**Rüstungsklasse** :: 10
**Trefferpunkte**  :: 19 (3W10+3)
**Speed**          :: 12 m
___
|  STR  |  GES  |  KON  |  INT  |  WEI  |  CHA  |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|18 (+4)|10 (+0)|12 (+1)|2  (-4)|11 (+0)|7  (-2)|
___
**Sinne**                   :: Passive Wahrnehmung 10
**Sprachen**                :: -
**Herausforderung**         :: 1/4 (50 EP)
___
### Aktionen
***Hufe.*** *Nahkampfwaffenangriff*: +6 auf Treffer, Reichweite 1,5 m, ein Ziel. *Treffer*: 9 (2W4+4) Wuchtschaden.
:
}}


<div class='wide' style='position:absolute;bottom:40px;width:100%;left:0'><center>

**Monsterkompendium 5e**<br>
Inhalte lizensiert unter [CC-BY-4.0](https://openrpg.de/license/cc/by/4.0/de/)<br>
2023-15-09 [SRD 5.1 **de**](https://codeberg.org/nesges/SRD-5.1-DE) / [D3](https://dnddeutsch.de) / [OpenRPG.de](https://openrpg.de)
</center></div>

{{pageNumber,auto}}
